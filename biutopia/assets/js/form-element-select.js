let choices = document.querySelectorAll(".choices")
let initChoice
for (let i = 0; i < choices.length; i++) {
  if (choices[i].classList.contains("multiple-remove")) {
    initChoice = new Choices(choices[i], {
      delimiter: ",",
      editItems: true,
      maxItemCount: -1,
      removeItemButton: true,
      fuseOptions: {
        threshold: 0.1,
        distance: 1000
      }
    })
  } else {
      initChoice = new Choices(choices[i], {
          shouldSort: false,
          fuseOptions: {
            threshold: 0.1,
            distance: 1000
          }
      });
  }
}

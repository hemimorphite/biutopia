﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace biutopia
{
    public partial class PurchaseOrderForm : System.Web.UI.Page
    {
        protected static string action = "create";

        protected static Dictionary<string, string> store = new Dictionary<string, string>()
        {
            { "storeId", DBNull.Value.ToString() },
            { "storeName", DBNull.Value.ToString() },
            { "storeBuildingName", DBNull.Value.ToString() },
            { "storeStreetName", DBNull.Value.ToString() },
            { "storeNeighbourhood", DBNull.Value.ToString() },
            { "storeSubdistrict", DBNull.Value.ToString() },
            { "storeDistrict", DBNull.Value.ToString() },
            { "storeRuralDistrict", DBNull.Value.ToString() },
            { "storeProvince", DBNull.Value.ToString() },
            { "storeZipcode", DBNull.Value.ToString() },
            { "storeContactName", DBNull.Value.ToString() },
            { "storeContactPhone", DBNull.Value.ToString() },
            { "storeContactEmail", DBNull.Value.ToString() }
        };

        protected static Dictionary<string, string> supplier = new Dictionary<string, string>()
        {
            { "supplierId", DBNull.Value.ToString() },
            { "supplierName", DBNull.Value.ToString() },
            { "supplierBuildingName", DBNull.Value.ToString() },
            { "supplierStreetName", DBNull.Value.ToString() },
            { "supplierNeighbourhood", DBNull.Value.ToString() },
            { "supplierSubdistrict", DBNull.Value.ToString() },
            { "supplierDistrict", DBNull.Value.ToString() },
            { "supplierRuralDistrict", DBNull.Value.ToString() },
            { "supplierProvince", DBNull.Value.ToString() },
            { "supplierZipcode", DBNull.Value.ToString() },
            { "supplierContactName", DBNull.Value.ToString() },
            { "supplierContactPhone", DBNull.Value.ToString() },
            { "supplierContactEmail", DBNull.Value.ToString() }
        };

        protected static Dictionary<string, string> purchase = new Dictionary<string, string>()
        {
            //{ "purchaseOrderId", DBNull.Value.ToString() },
            { "purchaseNo", DBNull.Value.ToString() },
            { "purchaseDate", DBNull.Value.ToString() },
            //{ "deliveryNo", DBNull.Value.ToString() },
            //{ "deliveryDate", DBNull.Value.ToString() },
            { "totalQty", DBNull.Value.ToString() },
            { "totalPrice", DBNull.Value.ToString() },
            { "approvalDate", DBNull.Value.ToString() },
            { "approvalBy", DBNull.Value.ToString() },
            { "requestedDate", DBNull.Value.ToString() },
            { "requestedBy", DBNull.Value.ToString() },
            { "purchaseStatus", "pending" },
            { "purchaseNotes", DBNull.Value.ToString() }
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["username"]) || string.IsNullOrEmpty((string)Session["role_id"]))
            {
                Session.Clear();
                Response.Redirect("Login.aspx");
            }

            SupplierNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierBuildingNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierStreetNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierNeighbourhoodCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierSubdistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierRuralDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierProvinceCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierZipcodeCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierContactNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierContactPhoneCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierContactEmailCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreBuildingNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreStreetNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreNeighbourhoodCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreSubdistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreRuralDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreProvinceCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreZipcodeCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreContactNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreContactPhoneCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreContactEmailCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            PurchaseNoCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            PurchaseDateCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            //DeliveryNoCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            RequestedByCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            RequestedDateCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            ApprovalByCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            ApprovalDateCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            TotalQtyCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            TotalPriceCheckBox.InputAttributes.Add("class", "form-check-input mt-0");

            //foreach (ListItem item in PurchaseStatusChoice.Items)
            //{
            //    item.InputAttributes.Add("class", "form-check-input");
            //}
            List<string> pagesList = new List<string>();
            Session["stores"] = new int[0];
            Session["suppliers"] = new int[0];
            Session["auto"] = new string[0];
            Session["add"] = new string[0];
            Session["edit"] = new string[0];
            Session["read"] = new string[0];
            Session["delete"] = false;
            Session["upload"] = false;
            Session["print"] = false;
            Session["pages"] = new string[0];

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT permissions FROM roles WHERE role_id = @roleId";
                    cmd.Parameters.AddWithValue("@roleId", Session["role_id"]);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("permissions")))
                                {
                                    Newtonsoft.Json.Linq.JObject pagepermissions = Newtonsoft.Json.Linq.JObject.Parse(reader.GetString(reader.GetOrdinal("permissions")).Trim());
                                    Newtonsoft.Json.Linq.JObject accessRights = Newtonsoft.Json.Linq.JObject.Parse(Session["access_rights"].ToString());
                                    Newtonsoft.Json.Linq.JArray jstores = (Newtonsoft.Json.Linq.JArray)accessRights["stores"];

                                    int[] stores = new int[jstores.Count];

                                    int a = 0;
                                    foreach (int store in jstores)
                                    {
                                        stores[a++] = store;
                                    }

                                    Session["stores"] = stores;

                                    Newtonsoft.Json.Linq.JArray jsuppliers = (Newtonsoft.Json.Linq.JArray)accessRights["suppliers"];

                                    int[] suppliers = new int[jsuppliers.Count];

                                    int b = 0;
                                    foreach (int supplier in jsuppliers)
                                    {
                                        suppliers[b++] = supplier;
                                    }

                                    Session["suppliers"] = suppliers;

                                    Newtonsoft.Json.Linq.JArray pages = (Newtonsoft.Json.Linq.JArray)pagepermissions["pages"];

                                    for (int i = 0; i < pages.Count; i++)
                                    {
                                        Newtonsoft.Json.Linq.JObject page = (Newtonsoft.Json.Linq.JObject)pages[i];
                                        Newtonsoft.Json.Linq.JArray permissions = (Newtonsoft.Json.Linq.JArray)page["permissions"];

                                        if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                        {
                                            pagesList.Add((string)page["name"]);
                                        }

                                        if ((string)page["name"] == "purchaseOrder")
                                        {
                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[0])["auto"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[0])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["auto"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[1])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["read"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[2])["add"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[2])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["add"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[3])["edit"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[3])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["edit"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[4])["delete"])
                                            {
                                                Session["delete"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[5])["upload"])
                                            {
                                                Session["upload"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[6])["print"])
                                            {
                                                Session["print"] = true;
                                            }
                                        }
                                    }

                                    Session["pages"] = pagesList.ToArray();
                                }
                            }
                        }
                    }
                    conn.Close();

                    if (((string[])Session["read"]).Length == 0)
                    {
                        Response.Redirect("Forbidden.aspx");
                    }
                }
            }

            if (!IsPostBack)
            {
                action = "create";

                System.Data.DataTable items = new System.Data.DataTable("items");
                System.Data.DataTable inserts = new System.Data.DataTable("inserts");
                System.Data.DataTable updates = new System.Data.DataTable("updates");
                System.Data.DataTable deletes = new System.Data.DataTable("deletes");

                if (((string[])Session["read"]).Contains("item"))
                {
                    items.Columns.Add("no");
                    //items.Columns.Add("purchase_order_id");
                    items.Columns.Add("item_detail_id");
                    items.Columns.Add("item_barcode");
                    items.Columns.Add("supplier_product_code");
                    items.Columns.Add("item_name");
                    items.Columns.Add("short_description");
                    items.Columns.Add("color");
                    items.Columns.Add("size");
                    items.Columns.Add("variants");
                    items.Columns.Add("brand_name");
                    items.Columns.Add("item_price");
                    items.Columns.Add("item_qty");
                }
                ViewState["items"] = items;

                if (((string[])Session["add"]).Contains("item"))
                {
                    //inserts.Columns.Add("purchase_order_id");
                    inserts.Columns.Add("item_detail_id");
                    inserts.Columns.Add("item_json");
                    inserts.Columns.Add("item_qty");
                    inserts.Columns.Add("item_price");
                    //inserts.Columns.Add("created_date");
                    //inserts.Columns.Add("created_by");
                    //inserts.Columns.Add("modified_date");
                    //inserts.Columns.Add("modified_by");
                }
                ViewState["inserts"] = inserts;

                if (((string[])Session["edit"]).Contains("item"))
                {
                    updates.Columns.Add("purchase_order_item_id");
                    updates.Columns.Add("purchase_order_id");
                    updates.Columns.Add("item_detail_id");
                    updates.Columns.Add("item_json");
                    updates.Columns.Add("item_qty");
                    updates.Columns.Add("item_price");
                    //updates.Columns.Add("modified_date");
                    //updates.Columns.Add("modified_by");
                }
                ViewState["updates"] = updates;

                if ((bool)Session["delete"])
                {
                    deletes.Columns.Add("purchase_order_item_id");
                    deletes.Columns.Add("purchase_order_id");
                    deletes.Columns.Add("item_detail_id");
                    //deletes.Columns.Add("modified_date");
                    //deletes.Columns.Add("modified_by");
                }
                ViewState["deletes"] = deletes;

                Uri url = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);

                if (HttpUtility.ParseQueryString(url.Query).Get("purchaseId") != null &&
                    HttpUtility.ParseQueryString(url.Query).Get("purchaseUuid") != null &&
                    Session["storevalue"] != null && Session["suppliervalue"] != null &&
                    Session["origin"] != null && Session["redirect"] != null)
                {
                    int purchaseId;
                    bool redirect;
                    
                    if (int.TryParse(HttpUtility.ParseQueryString(url.Query).Get("purchaseId"), out purchaseId) &&
                        !String.IsNullOrEmpty(HttpUtility.ParseQueryString(url.Query).Get("purchaseUuid")) &&
                        bool.TryParse(Session["redirect"].ToString(), out redirect))
                    {
                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                        SqlCommand cmd = conn.CreateCommand();

                        cmd.CommandText = @"SELECT COUNT(*) FROM purchase_orders 
                        WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid 
                        AND isdeleted = 0;";

                        cmd.Parameters.AddWithValue("@purchaseId", int.Parse(HttpUtility.ParseQueryString(url.Query).Get("purchaseId")));
                        cmd.Parameters.AddWithValue("@purchaseUuid", HttpUtility.ParseQueryString(url.Query).Get("purchaseUuid").ToString().ToUpper());
                        cmd.Connection = conn;

                        conn.Open();
                        int count = 0;

                        object result = cmd.ExecuteScalar();

                        if (result != null)
                        {
                            count = int.Parse(result.ToString());
                        }
                        conn.Close();
                        
                        if (count > 0 && redirect)
                        {
                            action = "update";
                            ViewState["id"] = HttpUtility.ParseQueryString(url.Query).Get("purchaseId");
                            ViewState["uuid"] = HttpUtility.ParseQueryString(url.Query).Get("purchaseUuid").ToUpper();
                        }
                    }
                }
                
                if (action == "update")
                {
                    if (!isEditableOrDeletable() && Session["role_name"].ToString() != "superuser")
                    {
                        AlertLabel.Text = "Dokumen pembelian barang tidak bisa diubah/dihapus";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }

                    if (IsLocked())
                    {
                        AlertLabel.Text = "Dokumen pembelian barang terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }

                    LoadForm();
                    FillAutoSupplierInput();
                    FillAutoStoreInput();
                    FillAutoPurchaseInput();
                    FillPurchaseInput();
                    BindItemGridview();
                }

                if (action == "create")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                    SqlCommand cmd = conn.CreateCommand();

                    string commandText = "";

                    if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                    {
                        commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers";
                    }
                    else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                    {
                        int[] suppliers = (int[])Session["suppliers"];

                        commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers 
                        WHERE supplier_id IN ({0})";

                        string inClause = string.Join(",", suppliers);
                        commandText = string.Format(commandText, inClause);
                    }

                    if (!String.IsNullOrEmpty(commandText))
                    {
                        cmd.CommandText = commandText;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        SupplierChoice.DataSource = ds;
                        SupplierChoice.DataTextField = "supplier_name";
                        SupplierChoice.DataValueField = "supplier_id";
                        SupplierChoice.DataBind();

                        ds.Dispose();

                        LoadSupplier();
                        
                    }

                    if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                    {
                        commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores";
                    }
                    else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                    {
                        int[] stores = (int[])Session["stores"];
                        commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores 
                        WHERE store_id IN ({0})";

                        string inClause = string.Join(",", stores);

                        commandText = string.Format(commandText, inClause);
                    }

                    if (!String.IsNullOrEmpty(commandText))
                    {
                        cmd = conn.CreateCommand();
                        cmd.CommandText = commandText;

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        StoreChoice.DataSource = ds;
                        StoreChoice.DataTextField = "store_name";
                        StoreChoice.DataValueField = "store_id";
                        StoreChoice.DataBind();

                        ds.Dispose();

                        LoadStore();
                        
                    }

                    FillAutoSupplierInput();
                    FillAutoStoreInput();
                    FillAutoPurchaseInput();
                }

                ItemGridView.DataSource = items;
                ItemGridView.DataBind();

                Session["redirect"] = null;
            }
            else
            {
                if (ViewState["items"] == null || ViewState["items"].Equals("-1") ||
                    ViewState["inserts"] == null || ViewState["inserts"].Equals("-1") ||
                    ViewState["updates"] == null || ViewState["updates"].Equals("-1") ||
                    ViewState["deletes"] == null || ViewState["deletes"].Equals("-1"))
                {
                    Response.Redirect("Forbidden.aspx");
                }

                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
                System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
                System.Data.DataTable deletes = (System.Data.DataTable)ViewState["deletes"];

                if (items.Columns.Count <= 0 || inserts.Columns.Count <= 0 || updates.Columns.Count <= 0 || deletes.Columns.Count <= 0)
                {
                    Response.Redirect("Forbidden.aspx");
                }

                FillAutoSupplierInput();
                FillAutoStoreInput();
                FillAutoPurchaseInput();

                ItemGridView.DataSource = items;
                ItemGridView.DataBind();

                AlertLabel.Text = "";
                AlertLabel.CssClass = "";
                AlertModalLabel.Text = "";
                AlertModalLabel.CssClass = "";
               
            }
            
        }

        protected void LoadStore()
        {
            int number;

            if (int.TryParse(StoreChoice.SelectedValue, out number))
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                cmd.CommandText = @"SELECT store_id
                                    ,store_name
                                    ,store_building_name
                                    ,store_street_name
                                    ,store_neighbourhood
                                    ,store_subdistrict
                                    ,store_district
                                    ,store_rural_district
                                    ,store_province
                                    ,store_zipcode
                                    FROM stores
                                    WHERE store_id = @storeId";

                cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                cmd.Connection = conn;

                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {

                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("store_id")))
                        {
                            store["storeId"] = reader["store_id"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_name")))
                        {
                            store["storeName"] = reader["store_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_building_name")))
                        {
                            store["storeBuildingName"] = reader["store_building_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_street_name")))
                        {
                            store["storeStreetName"] = reader["store_street_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_neighbourhood")))
                        {
                            store["storeNeighbourhood"] = reader["store_neighbourhood"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_subdistrict")))
                        {
                            store["storeSubdistrict"] = reader["store_subdistrict"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_district")))
                        {
                            store["storeDistrict"] = reader["store_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_rural_district")))
                        {
                            store["storeRuralDistrict"] = reader["store_rural_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_province")))
                        {
                            store["storeProvince"] = reader["store_province"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_zipcode")))
                        {
                            store["storeZipcode"] = reader["store_zipcode"].ToString();

                        }
                    }
                }
                conn.Close();
            }
        }

        protected void LoadSupplier()
        {
            int number;

            if (int.TryParse(SupplierChoice.SelectedValue, out number))
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                cmd.CommandText = @"SELECT supplier_id
                                    ,supplier_name
                                    ,supplier_building_name
                                    ,supplier_street_name
                                    ,supplier_neighbourhood
                                    ,supplier_subdistrict
                                    ,supplier_district
                                    ,supplier_rural_district
                                    ,supplier_province
                                    ,supplier_zipcode
                                    FROM suppliers
                                    WHERE supplier_id = @supplierId";

                cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                cmd.Connection = conn;

                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_id")))
                        {
                            supplier["supplierId"] = reader["supplier_id"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_name")))
                        {
                            supplier["supplierName"] = reader["supplier_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_building_name")))
                        {
                            supplier["supplierBuildingName"] = reader["supplier_building_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_street_name")))
                        {
                            supplier["supplierStreetName"] = reader["supplier_street_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_neighbourhood")))
                        {
                            supplier["supplierNeighbourhood"] = reader["supplier_neighbourhood"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_subdistrict")))
                        {
                            supplier["supplierSubdistrict"] = reader["supplier_subdistrict"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_district")))
                        {
                            supplier["supplierDistrict"] = reader["supplier_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_rural_district")))
                        {
                            supplier["supplierRuralDistrict"] = reader["supplier_rural_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_province")))
                        {
                            supplier["supplierProvince"] = reader["supplier_province"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_zipcode")))
                        {
                            supplier["supplierZipcode"] = reader["supplier_zipcode"].ToString();
                        }

                    }
                }
                conn.Close();
            }
        }

        protected void FillAutoStoreInput()
        {
            StoreNameCheckBoxCheckedChanged();
            StoreBuildingNameCheckBoxCheckedChanged();
            StoreStreetNameCheckBoxCheckedChanged();
            StoreNeighbourhoodCheckBoxCheckedChanged();
            StoreSubdistrictCheckBoxCheckedChanged();
            StoreDistrictCheckBoxCheckedChanged();
            StoreRuralDistrictCheckBoxCheckedChanged();
            StoreProvinceCheckBoxCheckedChanged();
            StoreZipcodeCheckBoxCheckedChanged();
            StoreContactNameCheckBoxCheckedChanged();
            StoreContactPhoneCheckBoxCheckedChanged();
            StoreContactEmailCheckBoxCheckedChanged();
        }

        protected void FillAutoSupplierInput()
        {
            SupplierNameCheckBoxCheckedChanged();
            SupplierBuildingNameCheckBoxCheckedChanged();
            SupplierStreetNameCheckBoxCheckedChanged();
            SupplierNeighbourhoodCheckBoxCheckedChanged();
            SupplierSubdistrictCheckBoxCheckedChanged();
            SupplierDistrictCheckBoxCheckedChanged();
            SupplierRuralDistrictCheckBoxCheckedChanged();
            SupplierProvinceCheckBoxCheckedChanged();
            SupplierZipcodeCheckBoxCheckedChanged();
            SupplierContactNameCheckBoxCheckedChanged();
            SupplierContactPhoneCheckBoxCheckedChanged();
            SupplierContactEmailCheckBoxCheckedChanged();
        }

        protected void FillPurchaseInput()
        {
            PurchaseStatusChoice.SelectedValue = purchase["purchaseStatus"];
            PurchaseNotesInput.Text = purchase["purchaseNotes"];
        }

        protected void FillAutoPurchaseInput()
        {
            PurchaseNoCheckBoxCheckedChanged();
            PurchaseDateCheckBoxCheckedChanged();
            TotalQtyCheckBoxCheckedChanged();
            TotalPriceCheckBoxCheckedChanged();
            ApprovalByCheckBoxCheckedChanged();
            ApprovalDateCheckBoxCheckedChanged();
            RequestedByCheckBoxCheckedChanged();
            RequestedDateCheckBoxCheckedChanged();
        }

        protected void Clear()
        {
            PurchaseNotesInput.Text = "";
            PurchaseStatusChoice.SelectedValue = "pending";
        }

        protected bool isEditableOrDeletable()
        {
            if(action == "update")
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                cmd.CommandText = @"SELECT COUNT(*) FROM goods_receipts INNER JOIN purchase_orders 
                ON goods_receipts.purchase_order_id = purchase_orders.purchase_order_id     
                WHERE purchase_orders.purchase_order_id = @purchaseId AND purchase_orders.uuid = @purchaseUuid 
                AND purchase_orders.isdeleted = 0 AND goods_receipts.isdeleted = 0;";

                cmd.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                cmd.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                cmd.Connection = conn;

                conn.Open();
                object result = cmd.ExecuteScalar();
                conn.Close();

                int count = 0;

                if (result != null)
                {
                    count = int.Parse(result.ToString());
                }

                if (count > 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        protected bool IsLocked()
        {
            if (action == "update")
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                cmd.CommandText = @"SELECT COUNT(*) FROM purchase_orders 
                WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid 
                AND isdeleted = 0 AND locked = 1;";

                cmd.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                cmd.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                cmd.Connection = conn;

                conn.Open();
                object result = cmd.ExecuteScalar();
                conn.Close();

                int count = 0;

                if (result != null)
                {
                    count = int.Parse(result.ToString());
                }

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        protected void LoadItem()
        {
            int number;

            if (!int.TryParse(SupplierChoice.SelectedValue, out number))
            {
                AlertModalLabel.Text = "Vendor tidak ditemukan";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }

            if (!int.TryParse(StoreChoice.SelectedValue, out number))
            {
                AlertModalLabel.Text = "Toko Ritel tidak ditemukan";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }

            if (int.TryParse(SupplierChoice.SelectedValue, out number) && int.TryParse(StoreChoice.SelectedValue, out number))
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = @"SELECT item_detail_id, item_barcode FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                        AND isdeleted = 0 AND isactive = 1;";
                cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);

                ItemBarcodeChoice.DataSource = ds;
                ItemBarcodeChoice.DataTextField = "item_barcode";
                ItemBarcodeChoice.DataValueField = "item_detail_id";
                ItemBarcodeChoice.DataBind();
                ItemBarcodeChoice.Items.Insert(0, new ListItem("Pilih Barcode Barang...", ""));

                ds.Dispose();

                cmd = conn.CreateCommand();
                cmd.CommandText = @"SELECT item_detail_id, item_name FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                        AND isdeleted = 0 AND isactive = 1;";
                cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));

                da = new SqlDataAdapter(cmd);
                ds = new DataSet();
                da.Fill(ds);

                ItemNameChoice.DataSource = ds;
                ItemNameChoice.DataTextField = "item_name";
                ItemNameChoice.DataValueField = "item_detail_id";
                ItemNameChoice.DataBind();
                ItemNameChoice.Items.Insert(0, new ListItem("Pilih Nama Barang...", ""));

                ds.Dispose();
            }
        }
        
        protected void LoadForm()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);

            List<string> errors = new List<string>();

            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"SELECT supplier_id
                                    ,supplier_name
                                    ,supplier_building_name
                                    ,supplier_street_name
                                    ,supplier_neighbourhood
                                    ,supplier_subdistrict
                                    ,supplier_district
                                    ,supplier_rural_district
                                    ,supplier_province
                                    ,supplier_zipcode
                                    ,supplier_contact_name
                                    ,supplier_contact_phone
                                    ,supplier_contact_email
                                    ,store_id
                                    ,store_name
                                    ,store_building_name
                                    ,store_street_name
                                    ,store_neighbourhood
                                    ,store_subdistrict
                                    ,store_district
                                    ,store_rural_district
                                    ,store_province
                                    ,store_zipcode
                                    ,store_contact_name
                                    ,store_contact_phone
                                    ,store_contact_email
                                    ,purchase_no
                                    ,purchase_date
                                    ,total_qty
                                    ,total_price
                                    ,approval_date
                                    ,approval_by
                                    ,requested_date
                                    ,requested_by
                                    ,purchase_status
                                    ,purchase_notes
                                    FROM purchase_orders
                                    WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid AND isdeleted = 0";

            cmd.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
            cmd.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader.IsDBNull(reader.GetOrdinal("supplier_id")))
                    {
                        supplier["supplierId"] = DBNull.Value.ToString();
                        errors.Add("Vendor tidak ditemukan");
                    }
                    else
                    {
                        supplier["supplierId"] = reader["supplier_id"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_name")))
                    {
                        supplier["supplierName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierName"] = reader["supplier_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_building_name")))
                    {
                        supplier["supplierBuildingName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierBuildingName"] = reader["supplier_building_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_street_name")))
                    {
                        supplier["supplierStreetName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierStreetName"] = reader["supplier_street_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_neighbourhood")))
                    {
                        supplier["supplierNeighbourhood"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierNeighbourhood"] = reader["supplier_neighbourhood"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_subdistrict")))
                    {
                        supplier["supplierSubdistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierSubdistrict"] = reader["supplier_subdistrict"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_district")))
                    {
                        supplier["supplierDistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierDistrict"] = reader["supplier_district"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_rural_district")))
                    {
                        supplier["supplierRuralDistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierRuralDistrict"] = reader["supplier_rural_district"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_province")))
                    {
                        supplier["supplierProvince"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierProvince"] = reader["supplier_province"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_zipcode")))
                    {
                        supplier["supplierZipcode"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierZipcode"] = reader["supplier_zipcode"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_contact_name")))
                    {
                        supplier["supplierContactName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierContactName"] = reader["supplier_contact_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_contact_phone")))
                    {
                        supplier["supplierContactPhone"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierContactPhone"] = reader["supplier_contact_phone"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_contact_email")))
                    {
                        supplier["supplierContactEmail"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierContactEmail"] = reader["supplier_contact_email"].ToString();
                    }


                    if (reader.IsDBNull(reader.GetOrdinal("store_id")))
                    {
                        store["storeId"] = DBNull.Value.ToString();
                        errors.Add("Toko Ritel tidak ditemukan");
                    }
                    else
                    {
                        store["storeId"] = reader["store_id"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_name")))
                    {
                        store["storeName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeName"] = reader["store_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_building_name")))
                    {
                        store["storeBuildingName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeBuildingName"] = reader["store_building_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_street_name")))
                    {
                        store["storeStreetName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeStreetName"] = reader["store_street_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_neighbourhood")))
                    {
                        store["storeNeighbourhood"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeNeighbourhood"] = reader["store_neighbourhood"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_subdistrict")))
                    {
                        store["storeSubdistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeSubdistrict"] = reader["store_subdistrict"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_district")))
                    {
                        store["storeDistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeDistrict"] = reader["store_district"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_rural_district")))
                    {
                        store["storeRuralDistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeRuralDistrict"] = reader["store_rural_district"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_province")))
                    {
                        store["storeProvince"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeProvince"] = reader["store_province"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_zipcode")))
                    {
                        store["storeZipcode"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeZipcode"] = reader["store_zipcode"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_contact_name")))
                    {
                        store["storeContactName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeContactName"] = reader["store_contact_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_contact_phone")))
                    {
                        store["storeContactPhone"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeContactPhone"] = reader["store_contact_phone"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_contact_email")))
                    {
                        store["storeContactEmail"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeContactEmail"] = reader["store_contact_email"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("purchase_no")))
                    {
                        purchase["purchaseNo"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        purchase["purchaseNo"] = reader["purchase_no"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("purchase_date")))
                    {
                        purchase["purchaseDate"] = "";
                    }
                    else
                    {
                        purchase["purchaseDate"] = Convert.ToDateTime(reader["purchase_date"]).ToString("yyyy-MM-dd");
                    }

                    //if (reader.IsDBNull(reader.GetOrdinal("delivery_no")))
                    //{
                    //    purchase["deliveryNo"] = DBNull.Value.ToString();
                    //}
                    //else
                    //{
                    //    purchase["deliveryNo"] = reader["delivery_no"].ToString();
                    //}

                    //if (reader.IsDBNull(reader.GetOrdinal("delivery_date")))
                    //{
                    //    purchase["deliveryDate"] = DBNull.Value.ToString();
                    //}
                    //else
                    //{
                    //    purchase["deliveryDate"] = Convert.ToDateTime(reader["delivery_date"]).ToString("yyyy-MM-dd");
                    //}

                    if (reader.IsDBNull(reader.GetOrdinal("total_qty")))
                    {
                        purchase["totalQty"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        purchase["totalQty"] = reader["total_qty"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("total_price")))
                    {
                        purchase["totalPrice"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        purchase["totalPrice"] = reader["total_price"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("approval_by")))
                    {
                        purchase["approvalBy"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        purchase["approvalBy"] = reader["approval_by"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("approval_date")))
                    {
                        purchase["approvalDate"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        purchase["approvalDate"] = Convert.ToDateTime(reader["approval_date"]).ToString("yyyy-MM-dd");
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("requested_by")))
                    {
                        purchase["requestedBy"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        purchase["requestedBy"] = reader["requested_by"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("requested_date")))
                    {
                        purchase["requestedDate"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        purchase["requestedDate"] = Convert.ToDateTime(reader["requested_date"]).ToString("yyyy-MM-dd");
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("purchase_status")))
                    {
                        purchase["purchaseStatus"] = "pending";
                    }
                    else
                    {
                        purchase["purchaseStatus"] = reader["purchase_status"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("purchase_notes")))
                    {
                        purchase["purchaseNotes"] = "";
                    }
                    else
                    {
                        purchase["purchaseNotes"] = reader["purchase_notes"].ToString();
                    }
                }
            }
            conn.Close();

            cmd.CommandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers 
                        WHERE supplier_id = @supplierId";
            cmd.Parameters.AddWithValue("@supplierId", int.Parse(supplier["supplierId"]));

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            SupplierChoice.DataSource = ds;
            SupplierChoice.DataTextField = "supplier_name";
            SupplierChoice.DataValueField = "supplier_id";
            SupplierChoice.DataBind();
            ds.Clear();

            cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores 
                        WHERE store_id = @storeId";
            cmd.Parameters.AddWithValue("@storeId", int.Parse(store["storeId"]));

            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds);

            StoreChoice.DataSource = ds;
            StoreChoice.DataTextField = "store_name";
            StoreChoice.DataValueField = "store_id";
            StoreChoice.DataBind();
            ds.Clear();

            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
            items.Rows.Clear();
            updates.Rows.Clear();
            ViewState["items"] = items;
            ViewState["updates"] = updates;

            cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT ROW_NUMBER() OVER(ORDER BY purchase_order_items.purchase_order_item_id) [no],
            purchase_order_items.purchase_order_item_id, purchase_order_items.purchase_order_id, purchase_order_items.item_detail_id, 
            purchase_order_items.item_price, purchase_order_items.item_qty, item_details.store_id, item_details.supplier_id, item_details.item_id,
            item_details.item_barcode, item_details.item_code, item_details.item_category_code, item_details.item_name,
            item_details.foreign_name, item_details.short_description, item_details.supplier_product_code,
            dbo.JsonValue(item_details.variants, 'color') [color], dbo.JsonValue(item_details.variants, 'size') [size],
            item_details.variants, item_details.selling_price, item_details.purchase_margin,
            item_details.effective_date, item_details.brand_name, item_details.uom, item_details.supplier_product_code
            FROM purchase_order_items 
            INNER JOIN purchase_orders ON purchase_orders.purchase_order_id = purchase_order_items.purchase_order_id
            INNER JOIN item_details ON item_details.item_detail_id = purchase_order_items.item_detail_id
            WHERE purchase_orders.purchase_order_id = @purchaseId AND purchase_orders.uuid = @purchaseUuid
            AND purchase_orders.store_id = @storeId AND purchase_orders.supplier_id = @supplierId AND purchase_orders.isdeleted = 0
            AND purchase_order_items.isdeleted = 0";
            cmd.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
            cmd.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
            cmd.Parameters.AddWithValue("@storeId", int.Parse(store["storeId"].ToString()));
            cmd.Parameters.AddWithValue("@supplierId", int.Parse(supplier["supplierId"].ToString()));

            conn.Open();
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    DataRow item = items.NewRow();
                    DataRow update = updates.NewRow();

                    if (!reader.IsDBNull(reader.GetOrdinal("no")))
                    {
                        item["no"] = int.Parse(reader["no"].ToString());
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("purchase_order_item_id")))
                    {
                        update["purchase_order_item_id"] = int.Parse(reader["purchase_order_item_id"].ToString());
                    }
                    else
                    {
                        errors.Add("Terjadi kesalahan no identitas pemesanan pada barang no " + reader["no"].ToString());
                        update["purchase_order_item_id"] = 0;
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("purchase_order_id")))
                    {
                        //item["purchase_order_id"] = int.Parse(reader["purchase_order_id"].ToString());
                        update["purchase_order_id"] = int.Parse(reader["purchase_order_id"].ToString());
                    }
                    else
                    {
                        errors.Add("Terjadi kesalahan no identitas pemesanan pada barang no " + reader["no"].ToString());
                        //item["purchase_order_id"] = 0;
                        update["purchase_order_id"] = 0;
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("item_detail_id")))
                    {
                        item["item_detail_id"] = int.Parse(reader["item_detail_id"].ToString());
                        update["item_detail_id"] = int.Parse(reader["item_detail_id"].ToString());
                    }
                    else
                    {
                        errors.Add("Terjadi kesalahan no identitas barang pada barang no " + reader["no"].ToString());
                        item["item_detail_id"] = 0;
                        update["item_detail_id"] = 0;
                    }

                    Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();

                    if (!reader.IsDBNull(reader.GetOrdinal("store_id")))
                    {
                        json["storeId"] = int.Parse(reader["store_id"].ToString());
                    }
                    else
                    {
                        json["storeId"] = 0;
                        errors.Add("Terjadi kesalahan toko pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_id")))
                    {
                        json["supplierId"] = int.Parse(reader["supplier_id"].ToString());
                    }
                    else
                    {
                        json["supplierId"] = 0;
                        errors.Add("Terjadi kesalahan pemasok pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_id")))
                    {
                        json["itemId"] = int.Parse(reader["item_id"].ToString());
                    }
                    else
                    {
                        json["itemId"] = 0;
                        errors.Add("Terjadi kesalahan no identitas pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_barcode")))
                    {
                        json["itemBarcode"] = reader["item_barcode"].ToString();
                        item["item_barcode"] = reader["item_barcode"].ToString();
                    }
                    else
                    {
                        json["itemBarcode"] = null;
                        item["item_barcode"] = "";
                        errors.Add("Terjadi kesalahan barcode pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_code")))
                    {
                        json["itemCode"] = reader["item_code"].ToString();
                    }
                    else
                    {
                        json["itemCode"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_category_code")))
                    {
                        json["itemCategoryCode"] = reader["item_category_code"].ToString();
                    }
                    else
                    {
                        json["itemCategoryCode"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_name")))
                    {
                        json["itemName"] = reader["item_name"].ToString();
                        item["item_name"] = reader["item_name"].ToString();
                    }
                    else
                    {
                        json["itemName"] = null;
                        item["item_name"] = "";
                        errors.Add("Terjadi kesalahan nama pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("foreign_name")))
                    {
                        json["foreignName"] = reader["foreign_name"].ToString();
                    }
                    else
                    {
                        json["foreignName"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("short_description")))
                    {
                        json["shortDescription"] = reader["short_description"].ToString();
                        item["short_description"] = reader["short_description"].ToString();
                    }
                    else
                    {
                        json["shortDescription"] = null;
                        item["short_description"] = "";
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("variants")))
                    {
                        json["variants"] = reader["variants"].ToString();
                    }
                    else
                    {
                        json["variants"] = "{}";
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("selling_price")))
                    {
                        json["sellingPrice"] = double.Parse(reader["selling_price"].ToString());
                    }
                    else
                    {
                        json["sellingPrice"] = 0;
                        errors.Add("Terjadi kesalahan harga pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("purchase_margin")))
                    {
                        json["purchaseMargin"] = double.Parse(reader["purchase_margin"].ToString());
                    }
                    else
                    {
                        json["purchaseMargin"] = 0;
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("effective_date")))
                    {
                        json["effectiveDate"] = Convert.ToDateTime(reader["effective_date"]).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        json["effectiveDate"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("brand_name")))
                    {
                        json["brandName"] = reader["brand_name"].ToString();
                        item["brand_name"] = reader["brand_name"].ToString();
                    }
                    else
                    {
                        json["brandName"] = null;
                        item["brand_name"] = "";
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("uom")))
                    {
                        json["unitOfMeasure"] = reader["uom"].ToString();
                    }
                    else
                    {
                        json["unitOfMeasure"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_product_code")))
                    {
                        json["supplierProductCode"] = reader["supplier_product_code"].ToString();
                        item["supplier_product_code"] = reader["supplier_product_code"].ToString();
                    }
                    else
                    {
                        json["supplierProductCode"] = null;
                        item["supplier_product_code"] = "";
                    }

                    update["item_json"] = json.ToString();

                    if (!reader.IsDBNull(reader.GetOrdinal("color")) && !reader.IsDBNull(reader.GetOrdinal("size")))
                    {
                        item["variants"] = String.Concat("Warna", " ", reader["color"].ToString(), ",", " ", "Ukuran", " ", reader["size"].ToString());
                        item["color"] = reader["color"].ToString();
                        item["size"] = reader["size"].ToString();
                    }
                    else if (!reader.IsDBNull(reader.GetOrdinal("color")))
                    {
                        item["variants"] = String.Concat("Warna", " ", reader["color"].ToString());
                        item["color"] = reader["color"].ToString();
                    }
                    else if (!reader.IsDBNull(reader.GetOrdinal("size")))
                    {
                        item["variants"] = String.Concat("Ukuran", " ", reader["size"].ToString());
                        item["size"] = reader["size"].ToString();
                    }
                    else
                    {
                        item["variants"] = "";
                        item["color"] = "";
                        item["size"] = "";
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("item_qty")))
                    {
                        item["item_qty"] = int.Parse(reader["item_qty"].ToString()).ToString("###,###,###,###,##0");
                        update["item_qty"] = int.Parse(reader["item_qty"].ToString());
                    }
                    else
                    {
                        item["item_qty"] = 0;
                        update["item_qty"] = 0;
                        errors.Add("Terjadi kesalahan jumlah pada barang no " + reader["no"].ToString());
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("item_price")))
                    {
                        item["item_price"] = double.Parse(reader["item_price"].ToString()).ToString("###,###,###,###,##0.00");
                        update["item_price"] = double.Parse(reader["item_price"].ToString());
                    }
                    else
                    {
                        item["item_price"] = 0;
                        update["item_price"] = 0;
                        errors.Add("Terjadi kesalahan harga pada barang no " + reader["no"].ToString());
                    }
                    items.Rows.Add(item);
                    updates.Rows.Add(update);
                }
            }
            conn.Close();

            ViewState["items"] = items;

            if (errors.Count <= 0)
            {
                ViewState["updates"] = updates;
            }
            else
            {
                AlertLabel.Text = String.Join("<br>", errors.ToArray());
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void BindItemGridview()
        {
            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
            
            ItemGridView.DataSource = items;
            ItemGridView.DataBind();
        }

        protected void StoreChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(action == "create")
            {
                //LoadSupplier();
                LoadStore();
                //FillAutoSupplierInput();
                FillAutoStoreInput();
                //FillAutoPurchaseInput();
            }
        }
        protected void SupplierChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (action == "create")
            {
                LoadSupplier();
                //LoadStore();
                FillAutoSupplierInput();
                //FillAutoStoreInput();
                //FillAutoPurchaseInput();
            }
        }

        protected void ItemBarcodeChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
            {
                AlertModalLabel.Text = "Toko Ritel tidak boleh kosong";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
            {
                AlertModalLabel.Text = "Vendor tidak boleh kosong";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if ((((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue)) || ((int[])Session["stores"]).Contains(-1)) &&
                (((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue)) || ((int[])Session["suppliers"]).Contains(-1)) &&
                !String.IsNullOrEmpty(StoreChoice.SelectedValue) && !String.IsNullOrEmpty(SupplierChoice.SelectedValue) &&
                ((string[])Session["read"]).Contains("item"))
            {
                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                    
                foreach (DataRow row in items.Rows)
                {
                    if (ItemBarcodeChoice.SelectedValue == row["item_detail_id"].ToString())
                    {
                        AlertModalLabel.Text = "Barang yang sama sudah pernah ditambahkan";
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        break;
                    }
                }

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd =conn.CreateCommand ();
                //cmd.CommandText = @"SELECT item_name [name], short_description [description], JSON_VALUE(variants, '$.color') [color], 
                //    JSON_VALUE(variants, '$.size') [size], selling_price [price], brand_name [brand] FROM items WHERE 
                //    store_id = @storeId AND supplier_id = @supplierId AND item_id = @itemId;";
                cmd.CommandText = @"SELECT item_name, short_description, 
                    dbo.JsonValue(variants, 'color')[color], dbo.JsonValue(variants, 'size')[size],
                    FORMAT(selling_price, '###,###,###,###,##0.00') [selling_price], brand_name, supplier_product_code FROM item_details WHERE 
                    store_id = @storeId AND supplier_id = @supplierId AND item_detail_id = @itemId AND isdeleted = 0 AND isactive = 1;";
                
                cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice .SelectedValue ));
                cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice .SelectedValue ));
                cmd.Parameters.AddWithValue("@itemId", int.Parse(ItemBarcodeChoice.SelectedValue));
                
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    ItemNameChoice.SelectedValue = ItemBarcodeChoice.SelectedValue;
                    while (reader.Read())
                    {
                        if (reader.IsDBNull(reader.GetOrdinal("short_description")))
                        {
                            DescriptionInput.Text = "";
                        }
                        else
                        {
                            DescriptionInput.Text = reader["short_description"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("color")))
                        {
                            ColorInput.Text = "";
                        }
                        else
                        {
                            ColorInput.Text = reader["color"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("size")))
                        {
                            SizeInput.Text = "";
                        }
                        else
                        {
                            SizeInput.Text = reader["size"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("brand_name")))
                        {
                            BrandInput.Text = "";
                        }
                        else
                        {
                            BrandInput.Text = reader["brand_name"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("selling_price")))
                        {
                            PriceInput.Text = "";
                        }
                        else
                        {
                            PriceInput.Text = reader["selling_price"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("supplier_product_code")))
                        {
                            SupplierProductCodeInput.Text = "";
                        }
                        else
                        {
                            SupplierProductCodeInput.Text = reader["supplier_product_code"].ToString();
                        }
                    }
                }
                else
                {
                    AlertModalLabel.Text = "Barcode Barang tidak ditemukan" + ItemBarcodeChoice.SelectedValue;
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                conn.Close();
                
            }
            else
            {
                AlertModalLabel.Text = "Akses ditolak";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
        }
        
        protected void ItemNameChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
            {
                AlertModalLabel.Text = "Toko Ritel tidak boleh kosong";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
            {
                AlertModalLabel.Text = "Vendor tidak boleh kosong";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if ((((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue)) || ((int[])Session["stores"]).Contains(-1)) &&
                (((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue)) || ((int[])Session["suppliers"]).Contains(-1)) &&
                !String.IsNullOrEmpty(StoreChoice.SelectedValue) && !String.IsNullOrEmpty(SupplierChoice.SelectedValue) &&
                ((string[])Session["read"]).Contains("item"))
            {

                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                foreach (DataRow row in items.Rows)
                {
                    if (ItemBarcodeChoice.SelectedValue == row["item_detail_id"].ToString())
                    {
                        AlertModalLabel.Text = "Barang yang sama sudah pernah ditambahkan";
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        break;
                    }
                }

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand ();
                //cmd.CommandText = @"SELECT item_name [name], short_description [description], JSON_VALUE(variants, '$.color') [color], 
                //    JSON_VALUE(variants, '$.size') [size], selling_price [price], brand_name [brand] FROM items WHERE 
                //    store_id = @storeId AND supplier_id = @supplierId AND item_id = @itemId;";
                cmd.CommandText = @"SELECT item_name, short_description, 
                    dbo.JsonValue(variants, 'color')[color], dbo.JsonValue(variants, 'size')[size],
                    FORMAT(selling_price, '###,###,###,###,##0.00') [selling_price], brand_name, supplier_product_code FROM item_details WHERE 
                    store_id = @storeId AND supplier_id = @supplierId AND item_detail_id = @itemId AND isdeleted = 0 AND isactive = 1;";

                cmd.Parameters.AddWithValue("@storeId", int.Parse(ViewState["storeId"].ToString()));
                cmd.Parameters.AddWithValue("@supplierId", int.Parse(ViewState["supplierId"].ToString()));
                cmd.Parameters.AddWithValue("@itemId", int.Parse(ItemNameChoice.SelectedValue));
                
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    ItemBarcodeChoice.SelectedValue = ItemNameChoice.SelectedValue;
                    while (reader.Read())
                    {
                        if (reader.IsDBNull(reader.GetOrdinal("short_description")))
                        {
                            DescriptionInput.Text = "";
                        }
                        else
                        {
                            DescriptionInput.Text = reader["short_description"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("color")))
                        {
                            ColorInput.Text = "";
                        }
                        else
                        {
                            ColorInput.Text = reader["color"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("size")))
                        {
                            SizeInput.Text = "";
                        }
                        else
                        {
                            SizeInput.Text = reader["size"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("brand_name")))
                        {
                            BrandInput.Text = "";
                        }
                        else
                        {
                            BrandInput.Text = reader["brand_name"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("selling_price")))
                        {
                            PriceInput.Text = "";
                        }
                        else
                        {
                            PriceInput.Text = reader["selling_price"].ToString();
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("supplier_product_code")))
                        {
                            SupplierProductCodeInput.Text = "";
                        }
                        else
                        {
                            SupplierProductCodeInput.Text = reader["supplier_product_code"].ToString();
                        }
                    }
                }
                else
                {
                    AlertModalLabel.Text = "Barcode Barang tidak ditemukan";
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                
            }
            else
            {
                AlertModalLabel.Text = "Akses ditolak";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
        }

        protected void ClearModal()
        {
            ItemBarcodeChoice.DataSource = null;
            ItemBarcodeChoice.DataBind();
            ItemNameChoice.DataSource = null;
            ItemNameChoice.DataBind();
            DescriptionInput.Text = "";
            ColorInput.Text = "";
            SizeInput.Text = "";
            BrandInput.Text = "";
            PriceInput.Text = "";
            QtyInput.Text = "";
            SupplierProductCodeInput.Text = "";

            AddButton.CommandName = "add";
            AddButton.CommandArgument = "";
        }

        protected void OpenItemModalButton_Click(object sender, EventArgs e)
        {
            ClearModal();

            if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
            {
                AlertModalLabel.Text = "Toko Ritel tidak boleh kosong";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
            {
                AlertModalLabel.Text = "Vendor tidak boleh kosong";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if ((((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue)) || ((int[])Session["stores"]).Contains(-1)) &&
                (((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue)) || ((int[])Session["suppliers"]).Contains(-1)) &&
                !String.IsNullOrEmpty(StoreChoice.SelectedValue) && !String.IsNullOrEmpty(SupplierChoice.SelectedValue) &&
                ((string[])Session["read"]).Contains("item"))
            {
                LoadItem();
            }
            else
            {
                AlertModalLabel.Text = "Akses ditolak";
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
        }

        protected void CloseButton_Click(object sender, EventArgs e)
        {
            ClearModal();
        }

        protected void RemoveButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "delete" && arg.Length > 0)
            {
                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
                System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
                System.Data.DataTable deletes = (System.Data.DataTable)ViewState["deletes"];

                for (int i = 0; i < items.Rows.Count; i++)
                {
                    if (int.Parse(items.Rows[i]["item_detail_id"].ToString()) == int.Parse(arg[0]))
                    {
                        items.Rows[i].Delete();
                        break;
                    }
                }
                items.AcceptChanges();

                int no = 1;

                for (int i = 0; i < items.Rows.Count; i++)
                {
                    items.Rows[i]["no"] = no;
                    no++;
                }

                for (int i = 0; i < inserts.Rows.Count; i++)
                {
                    if (int.Parse(inserts.Rows[i]["item_detail_id"].ToString()) == int.Parse(arg[0]))
                    {
                        inserts.Rows[i].Delete();
                        break;
                    }
                }
                inserts.AcceptChanges();

                for (int i = 0; i < updates.Rows.Count; i++)
                {
                    if (int.Parse(updates.Rows[i]["item_detail_id"].ToString()) == int.Parse(arg[0]))
                    {
                        DataRow row = deletes.NewRow();
                        row["purchase_order_item_id"] = int.Parse(updates.Rows[i]["purchase_order_item_id"].ToString());
                        row["purchase_order_id"] = int.Parse(updates.Rows[i]["purchase_order_id"].ToString());
                        row["item_detail_id"] = int.Parse(updates.Rows[i]["item_detail_id"].ToString());
                        deletes.Rows.Add(row);
                        updates.Rows[i].Delete();
                        break;
                    }
                }
                updates.AcceptChanges();

                ViewState["items"] = items;
                ViewState["inserts"] = inserts;
                ViewState["updates"] = updates;
                ViewState["deletes"] = deletes;

                TotalQtyCheckBoxCheckedChanged();
                TotalPriceCheckBoxCheckedChanged();

                ItemGridView.DataSource = items;
                ItemGridView.DataBind();
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void EditButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "edit" && arg.Length > 0)
            {
                AddButton.CommandName = "edit";
                AddButton.CommandArgument = e.CommandArgument.ToString();

                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                DataRow row = items.Select(String.Concat("item_detail_id=", arg[0].ToString())).FirstOrDefault();
                
                if (row != null)
                {
                    
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT item_detail_id, item_barcode FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                            AND isdeleted = 0 AND item_detail_id = @itemId AND isactive = 1;";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@itemId", int.Parse(row["item_detail_id"].ToString()));

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    ItemBarcodeChoice.DataSource = ds;
                    ItemBarcodeChoice.DataTextField = "item_barcode";
                    ItemBarcodeChoice.DataValueField = "item_detail_id";
                    ItemBarcodeChoice.DataBind();

                    ds.Dispose();

                    cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT item_detail_id, item_name FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                            AND isdeleted = 0  AND item_detail_id = @itemId AND isactive = 1;";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@itemId", int.Parse(row["item_detail_id"].ToString()));

                    da = new SqlDataAdapter(cmd);
                    ds = new DataSet();
                    da.Fill(ds);

                    ItemNameChoice.DataSource = ds;
                    ItemNameChoice.DataTextField = "item_name";
                    ItemNameChoice.DataValueField = "item_detail_id";
                    ItemNameChoice.DataBind();

                    ds.Dispose();

                    DescriptionInput.Text = row["short_description"].ToString();
                    ColorInput.Text = row["color"].ToString();
                    SizeInput.Text = row["size"].ToString();
                    BrandInput.Text = row["brand_name"].ToString();
                    PriceInput.Text = row["item_price"].ToString();
                    QtyInput.Text = row["item_qty"].ToString();
                    SupplierProductCodeInput.Text = row["supplier_product_code"].ToString();
                    
                }
                else
                {
                    AlertModalLabel.Text = "Barang tidak ditemukan";
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void AddButton_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "add")
            {
                if (((string[])Session["add"]).Contains("item"))
                {
                    List<string> errors = new List<string>();

                    int qty = 0;
                    int.TryParse(QtyInput.Text?.Trim(), out qty);

                    if (qty <= 0)
                    {
                        errors.Add("Jumlah barang tidak valid");
                    }

                    if (String.IsNullOrEmpty(ItemBarcodeChoice.SelectedValue))
                    {
                        errors.Add("Barcode barang tidak ditemukan");
                    }

                    if (errors.Count <= 0)
                    {
                        System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                        System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                        DataRow[] exists = items.Select(String.Concat("item_detail_id =", " ", ItemBarcodeChoice.SelectedValue));

                        if (exists.Length > 0)
                        {
                            AlertModalLabel.Text = "Barang yang sama sudah pernah ditambahkan";
                            AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                        else
                        {
                            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                            SqlCommand cmd = conn.CreateCommand();
                            cmd.CommandText = @"SELECT item_detail_id, store_id, supplier_id, item_id, item_barcode, 
                            item_code, item_category_code, item_name, foreign_name, short_description, 
                            dbo.JsonValue(variants, 'color')[color], dbo.JsonValue(variants, 'size')[size], variants, 
                            selling_price, purchase_margin, effective_date, brand_name, uom, supplier_product_code FROM item_details WHERE 
                            store_id = @storeId AND supplier_id = @supplierId AND item_detail_id = @itemId AND isdeleted = 0 AND isactive = 1;";

                            cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                            cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                            cmd.Parameters.AddWithValue("@itemId", int.Parse(ItemBarcodeChoice.SelectedValue));

                            conn.Open();
                            SqlDataReader reader = cmd.ExecuteReader();

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    DataRow item = items.NewRow();
                                    DataRow insert = inserts.NewRow();

                                    Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();

                                    item["no"] = items.Rows.Count + 1;

                                    /*if (reader.IsDBNull(reader.GetOrdinal("purchase_order_id")))
                                    {
                                        item["purchase_order_id"] = 0;
                                        insert["purchase_order_id"] = 0;
                                        errors.Add("Terjadi kesalahan pesanan pembelian tidak ditemukan");
                                    }
                                    else
                                    {
                                        item["purchase_order_id"] = int.Parse(ViewState["id"].ToString());
                                        insert["purchase_order_id"] = int.Parse(ViewState["id"].ToString());
                                    }*/


                                    if (reader.IsDBNull(reader.GetOrdinal("item_detail_id")))
                                    {
                                        item["item_detail_id"] = 0;
                                        insert["item_detail_id"] = 0;
                                        errors.Add("Terjadi kesalahan barang tidak ditemukan");
                                    }
                                    else
                                    {
                                        item["item_detail_id"] = int.Parse(reader["item_detail_id"].ToString());
                                        insert["item_detail_id"] = int.Parse(reader["item_detail_id"].ToString());
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("store_id")))
                                    {
                                        //item["store_id"] = 0;
                                        json["storeId"] = null;
                                        errors.Add("Terjadi kesalahan toko tidak ditemukan");
                                    }
                                    else
                                    {
                                        //item["store_id"] = int.Parse(reader["store_id"].ToString());
                                        json["storeId"] = int.Parse(reader["store_id"].ToString());
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("supplier_id")))
                                    {
                                        //item["supplier_id"] = 0;
                                        json["supplierId"] = null;
                                        errors.Add("Terjadi kesalahan pemasok tidak ditemukan");
                                    }
                                    else
                                    {
                                        //item["supplier_id"] = int.Parse(reader["supplier_id"].ToString());
                                        json["supplierId"] = int.Parse(reader["supplier_id"].ToString());
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("item_id")))
                                    {
                                        json["itemId"] = null;
                                        errors.Add("Terjadi kesalahan barang tidak ditemukan");
                                    }
                                    else
                                    {
                                        json["itemId"] = int.Parse(reader["item_id"].ToString());
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("item_barcode")))
                                    {
                                        item["item_barcode"] = "";
                                        json["itemBarcode"] = null;
                                        errors.Add("Terjadi kesalahan barcode barang tidak ditemukan");
                                    }
                                    else
                                    {
                                        item["item_barcode"] = reader["item_barcode"].ToString();
                                        json["itemBarcode"] = reader["item_barcode"].ToString();
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("item_code")))
                                    {
                                        json["itemCode"] = null;
                                    }
                                    else
                                    {
                                        json["itemCode"] = reader["item_code"].ToString();
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("item_category_code")))
                                    {
                                        json["itemCategoryCode"] = null;
                                    }
                                    else
                                    {
                                        json["itemCategoryCode"] = reader["item_category_code"].ToString();
                                    }


                                    if (reader.IsDBNull(reader.GetOrdinal("item_name")))
                                    {
                                        json["itemName"] = null;
                                        item["item_name"] = "";
                                        errors.Add("Terjadi kesalahan nama barang tidak ditemukan");
                                    }
                                    else
                                    {
                                        json["itemName"] = reader["item_name"].ToString();
                                        item["item_name"] = reader["item_name"].ToString();
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("foreign_name")))
                                    {
                                        json["foreignName"] = null;
                                    }
                                    else
                                    {
                                        json["foreignName"] = reader["foreign_name"].ToString();
                                    }


                                    if (reader.IsDBNull(reader.GetOrdinal("short_description")))
                                    {
                                        json["shortDescription"] = null;
                                        item["short_description"] = "";
                                    }
                                    else
                                    {
                                        json["shortDescription"] = reader["short_description"].ToString();
                                        item["short_description"] = reader["short_description"].ToString();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("color")) && !reader.IsDBNull(reader.GetOrdinal("size")))
                                    {
                                        item["variants"] = String.Concat("Warna", " ", reader["color"].ToString(), ",", " ", "Ukuran", " ", reader["size"].ToString());
                                        item["color"] = reader["color"].ToString();
                                        item["size"] = reader["size"].ToString();
                                    }
                                    else if (!reader.IsDBNull(reader.GetOrdinal("color")))
                                    {
                                        item["variants"] = String.Concat("Warna", " ", reader["color"].ToString());
                                        item["color"] = reader["color"].ToString();
                                        item["size"] = "";
                                    }
                                    else if (!reader.IsDBNull(reader.GetOrdinal("size")))
                                    {
                                        item["variants"] = String.Concat("Ukuran", " ", reader["size"].ToString());
                                        item["size"] = reader["size"].ToString();
                                        item["color"] = "";
                                    }
                                    else
                                    {
                                        item["variants"] = "";
                                        item["size"] = "";
                                        item["color"] = "";
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("variants")))
                                    {
                                        json["variants"] = "{}";
                                    }
                                    else
                                    {
                                        json["variants"] = reader["variants"].ToString();
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("selling_price")))
                                    {
                                        json["sellingPrice"] = 0;
                                        item["item_price"] = 0;
                                        insert["item_price"] = 0;
                                        errors.Add("Terjadi kesalahan harga barang tidak ditemukan");
                                    }
                                    else
                                    {
                                        json["sellingPrice"] = double.Parse(reader["selling_price"].ToString());
                                        item["item_price"] = double.Parse(reader["selling_price"].ToString()).ToString("###,###,###,###,##0.00");
                                        insert["item_price"] = double.Parse(reader["selling_price"].ToString());
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("purchase_margin")))
                                    {
                                        json["purchaseMargin"] = 0;
                                    }
                                    else
                                    {
                                        json["purchaseMargin"] = double.Parse(reader["purchase_margin"].ToString());
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("effective_date")))
                                    {
                                        json["effectiveDate"] = null;
                                    }
                                    else
                                    {
                                        json["effectiveDate"] = reader["effective_date"].ToString();
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("brand_name")))
                                    {
                                        json["brandName"] = null;
                                        item["brand_name"] = "";
                                    }
                                    else
                                    {
                                        json["brandName"] = reader["brand_name"].ToString();
                                        item["brand_name"] = reader["brand_name"].ToString();
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("uom")))
                                    {
                                        json["unitOfMeasure"] = null;
                                    }
                                    else
                                    {
                                        json["unitOfMeasure"] = reader["uom"].ToString();
                                    }

                                    if (reader.IsDBNull(reader.GetOrdinal("supplier_product_code")))
                                    {
                                        json["supplierProductCode"] = null;
                                        item["supplier_product_code"] = "";
                                        //insert["supplier_product_code"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        json["supplierProductCode"] = reader["supplier_product_code"].ToString();
                                        item["supplier_product_code"] = reader["supplier_product_code"].ToString();
                                        //insert["supplier_product_code"] = reader["supplier_product_code"].ToString();
                                    }

                                    if (String.IsNullOrEmpty(QtyInput.Text.Trim()))
                                    {
                                        item["item_qty"] = 0;
                                        insert["item_qty"] = 0;
                                        errors.Add("Terjadi kesalahan jumlah barang tidak ditemukan");
                                    }
                                    else
                                    {
                                        item["item_qty"] = int.Parse(QtyInput.Text.Trim()).ToString("###,###,###,###,##0");
                                        insert["item_qty"] = int.Parse(QtyInput.Text.Trim());
                                    }

                                    insert["item_json"] = json.ToString();
                                    //insert["created_date"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    //insert["created_by"] = Session["username"].ToString();
                                    //insert["modified_date"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                    //insert["modified_by"] = Session["username"].ToString();

                                    items.Rows.Add(item);

                                    if (errors.Count <= 0)
                                    {
                                        inserts.Rows.Add(insert);
                                    }
                                }
                            }
                            else
                            {
                                errors.Add("Barcode barang tidak ditemukan");
                            }
                            conn.Close();

                            if (errors.Count <= 0)
                            {
                                ViewState["items"] = items;
                                ViewState["inserts"] = inserts;
                                TotalQtyCheckBoxCheckedChanged();
                                TotalPriceCheckBoxCheckedChanged();
                                ItemGridView.DataSource = items;
                                ItemGridView.DataBind();
                                ClearModal();
                                LoadItem();
                                AlertModalLabel.Text = "Barang berhasil ditambahkan";
                                AlertModalLabel.CssClass = "alert alert-light-primary color-primary d-block";
                            }
                            else
                            {
                                AlertModalLabel.Text = String.Join("<br>", errors.ToArray());
                                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                        }
                    }
                    else
                    {
                        AlertModalLabel.Text = String.Join("<br>", errors.ToArray());
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertModalLabel.Text = "Akses ditolak";
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
            }
            else if (e.CommandName == "edit")
            {
                if (((string[])Session["edit"]).Contains("item"))
                {
                    List<string> errors = new List<string>();

                    int qty = 0;
                    int.TryParse(QtyInput.Text?.Trim(), out qty);

                    if (qty <= 0)
                    {
                        errors.Add("Jumlah barang tidak valid");
                    }

                    if (String.IsNullOrEmpty(ItemBarcodeChoice.SelectedValue))
                    {
                        errors.Add("Barcode barang tidak ditemukan");
                    }

                    if (errors.Count <= 0)
                    {
                        System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                        System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
                        System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                        foreach (DataRow row in items.Rows)
                        {
                            if (int.Parse(ItemBarcodeChoice.SelectedValue) == int.Parse(row["item_detail_id"].ToString()))
                            {
                                row["item_qty"] = qty.ToString("###,###,###,###,##0");
                                break;
                            }
                        }

                        items.AcceptChanges();

                        foreach (DataRow row in inserts.Rows)
                        {
                            if (int.Parse(ItemBarcodeChoice.SelectedValue) == int.Parse(row["item_detail_id"].ToString()))
                            {
                                row["item_qty"] = qty;
                                break;
                            }
                        }

                        inserts.AcceptChanges();

                        foreach (DataRow row in updates.Rows)
                        {
                            if (int.Parse(ItemBarcodeChoice.SelectedValue) == int.Parse(row["item_detail_id"].ToString()))
                            {
                                row["item_qty"] = qty;
                                break;
                            }
                        }

                        updates.AcceptChanges();

                        ViewState["items"] = items;
                        ViewState["inserts"] = inserts;
                        ViewState["updates"] = updates;
                        TotalQtyCheckBoxCheckedChanged();
                        TotalPriceCheckBoxCheckedChanged();
                        ItemGridView.DataSource = items;
                        ItemGridView.DataBind();
                        ClearModal();
                    }
                    else
                    {
                        AlertModalLabel.Text = String.Join("<br>", errors.ToArray());
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
                    }
                }
                else
                {
                    AlertModalLabel.Text = "Akses ditolak";
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
            }
        }

        protected void NoButton_Click(object sender, EventArgs e)
        {
            
        }

        protected void YesButton_Click(object sender, CommandEventArgs e)
        {
            if (action == "create")
            {
                Session["redirect"] = true;
                Response.Redirect("PurchaseOrder.aspx");
            }

            if (action == "update")
            {
                if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))) &&
                    (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))) &&
                    !String.IsNullOrEmpty(StoreChoice.SelectedValue) && !String.IsNullOrEmpty(SupplierChoice.SelectedValue) &&
                    (bool)Session["delete"] == true)
                {
                    if (!IsLocked())
                    {
                        if (isEditableOrDeletable() || Session["role_name"].ToString() == "superuser")
                        {
                            try
                            {
                                string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                                SqlConnection conn = new SqlConnection(constr);
                                SqlTransaction transaction;

                                conn.Open();
                                transaction = conn.BeginTransaction();

                                SqlCommand command = null;

                                if (Session["role_name"].ToString() == "superuser")
                                {
                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                    FROM goods_receipt_items A 
                                    INNER JOIN goods_receipts B ON A.goods_receipt_id = B.goods_receipt_id AND B.isdeleted = 0 
                                    INNER JOIN purchase_orders C ON C.purchase_order_id = B.purchase_order_id AND C.isdeleted = 0
                                    WHERE A.isdeleted = 0 AND C.store_id = @storeId AND C.supplier_id = @supplierId AND 
                                    C.uuid = @purchaseUuid AND C.purchase_order_id = @purchaseId";
                                    command.Transaction = transaction;
                                    command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                                    command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                    command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());

                                    //conn.Open();
                                    command.ExecuteNonQuery();
                                    //conn.Close();

                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                    FROM goods_receipts A 
                                    INNER JOIN purchase_orders B ON A.purchase_order_id = B.purchase_order_id AND B.isdeleted = 0
                                    WHERE A.isdeleted = 0 AND B.store_id = @storeId AND B.supplier_id = @supplierId AND 
                                    B.uuid = @purchaseUuid AND B.purchase_order_id = @purchaseId";
                                    command.Transaction = transaction;
                                    command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                                    command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                    command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());

                                    //conn.Open();
                                    command.ExecuteNonQuery();
                                    //conn.Close();
                                }

                                command = conn.CreateCommand();
                                command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                FROM purchase_order_items A 
                                INNER JOIN purchase_orders B ON A.purchase_order_id = B.purchase_order_id AND B.isdeleted = 0
                                WHERE B.store_id = @storeId AND B.supplier_id = @supplierId AND 
                                B.uuid = @purchaseUuid AND B.purchase_order_id = @purchaseId AND A.isdeleted = 0";
                                command.Transaction = transaction;
                                command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                                command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                                command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                //conn.Open();
                                command.ExecuteNonQuery();
                                //conn.Close();

                                command = conn.CreateCommand();
                                command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                FROM purchase_orders A WHERE A.store_id = @storeId AND A.supplier_id = @supplierId AND 
                                A.uuid = @purchaseUuid AND A.purchase_order_id = @purchaseId AND A.isdeleted = 0";
                                command.Transaction = transaction;
                                command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                                command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                                command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                command.Parameters.AddWithValue("@user", Session["username"].ToString());

                                //conn.Open();
                                command.ExecuteNonQuery();
                                //conn.Close();

                                transaction.Commit();
                                conn.Close();

                                Session["redirect"] = true;
                                Response.Redirect("PurchaseOrder.aspx");
                            }
                            catch (Exception error)
                            {
                                AlertLabel.Text = error.Message.ToString();
                                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                        }
                        else
                        {
                            AlertLabel.Text = "Dokumen pembelian tidak bisa dihapus/diubah";
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen pembelian terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Permintaan akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }  
        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
            {
                AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
            {
                AlertLabel.Text = "Vendor tidak boleh kosong";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if ((((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue)) || ((int[])Session["stores"]).Contains(-1)) &&
                (((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue)) || ((int[])Session["suppliers"]).Contains(-1)) &&
                !String.IsNullOrEmpty(StoreChoice.SelectedValue) && !String.IsNullOrEmpty(SupplierChoice.SelectedValue) &&
                (bool)Session["delete"] == true)
            {
                if (!IsLocked())
                {
                    if (isEditableOrDeletable() || Session["role_name"].ToString() == "superuser")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openConfirmModal();", true);
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen pembelian tidak bisa dihapus/diubah";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Dokumen pembelian terkunci";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Akses ditolak";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            if(action == "create")
            {
                List<string> errors = new List<string>();

                if (((string[])Session["add"]).Contains("purchaseNo"))
                {
                    if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()) && !PurchaseNoCheckBox.Checked)
                    {
                        errors.Add("No pesanan tidak boleh kosong");
                    }

                }

                //if (((string[])Session["add"]).Contains("deliveryNo"))
                //{
                //    if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()) && !DeliveryNoCheckBox.Checked)
                //    {
                //        errors.Add("No pengiriman tidak boleh kosong");
                //    }
                //}
                
                //if (((string[])Session["add"]).Contains("deliveryDate"))
                //{
                //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
                //    {
                //        errors.Add("Tanggal pengiriman tidak boleh kosong");
                //    }   
                //}
                

                if (((string[])Session["add"]).Contains("totalQty"))
                {
                    if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()) && !TotalQtyCheckBox.Checked)
                    {
                        errors.Add("Total kuantitas barang tidak boleh kosong");
                    }
                }
                
                if (((string[])Session["add"]).Contains("totalPrice"))
                {
                    if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()) && !TotalPriceCheckBox.Checked)
                    {
                        errors.Add("Total harga barang tidak boleh kosong");
                    }
                }
                
                if (((string[])Session["add"]).Contains("approvalBy"))
                {
                    if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                            && String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()) && !ApprovalByCheckBox.Checked)
                    {
                        errors.Add("Ditolak/Disetujui oleh tidak boleh kosong");
                    }
                }
                
                if (((string[])Session["add"]).Contains("approvalDate"))
                {
                    if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                            && String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()) && !ApprovalDateCheckBox.Checked)
                    {
                        errors.Add("Tanggal ditolak/disetujui oleh tidak boleh kosong");
                    }   
                }
                
                if (((string[])Session["add"]).Contains("requestedBy"))
                {
                    if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()) && !RequestedByCheckBox.Checked)
                    {
                        errors.Add("Dipesan oleh tidak boleh kosong");
                    }
                }
                
                if (((string[])Session["add"]).Contains("requestedDate"))
                {
                    if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()) && !RequestedDateCheckBox.Checked)
                    {
                        errors.Add("Tanggal dipesan oleh tidak boleh kosong");
                    }
                }
                
                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
                
                if (inserts.Rows.Count <= 0)
                {
                    errors.Add("Barang yang akan dibeli tidak boleh kosong");
                }

                if(errors.Count <= 0)
                {
                    int totalQty = 0;
                    double totalPrice = 0;

                    foreach (DataRow row in inserts.Rows)
                    {
                        totalQty += int.Parse(row["item_qty"].ToString());
                        totalPrice += double.Parse(row["item_price"].ToString()) * int.Parse(row["item_qty"].ToString());
                    }

                    string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

                    SqlConnection conn = new SqlConnection(constr);
                    SqlTransaction transaction;
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    try
                    {
                        string popattern = "PO" + DateTime.Now.ToString("yyyyMMdd");
                        string purchaseNo = popattern + "1".ToString().PadLeft(3, '0');

                        SqlCommand command = conn.CreateCommand();
                        command.CommandText = @"SELECT TOP 1 CAST(RIGHT([purchase_no], 3) AS INT) FROM purchase_orders WHERE 
                                    purchase_no LIKE @purchaseNo ORDER BY purchase_no DESC";
                        command.Parameters.AddWithValue("@purchaseNo", string.Format("{0}%", popattern));
                        command.Transaction = transaction;
                        int number = 1;

                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            number = int.Parse(result.ToString()) + 1;
                        }

                        purchaseNo = popattern + number.ToString().PadLeft(3, '0');

                        string fields = "";
                        string values = "";
                        bool withComma = false;

                        if (((string[])Session["add"]).Contains("supplier") ||
                            ((string[])Session["auto"]).Contains("supplier"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_id");
                            values = String.Concat(values, " ", "@supplierId");
                        }

                        if (((string[])Session["add"]).Contains("supplierName") ||
                            ((string[])Session["auto"]).Contains("supplierName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_name");
                            values = String.Concat(values, " ", "@supplierName");
                        }


                        if (((string[])Session["add"]).Contains("supplierBuildingName") ||
                            ((string[])Session["auto"]).Contains("supplierBuildingName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_building_name");
                            values = String.Concat(values, " ", "@supplierBuildingName");
                        }

                        if (((string[])Session["add"]).Contains("supplierStreetName") ||
                            ((string[])Session["auto"]).Contains("supplierStreetName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_street_name");
                            values = String.Concat(values, " ", "@supplierStreetName");
                        }

                        if (((string[])Session["add"]).Contains("supplierNeighbourhood") ||
                            ((string[])Session["auto"]).Contains("supplierNeighbourhood"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_neighbourhood");
                            values = String.Concat(values, " ", "@supplierNeighbourhood");

                        }

                        if (((string[])Session["add"]).Contains("supplierSubdistrict") ||
                            ((string[])Session["auto"]).Contains("supplierSubdistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_subdistrict");
                            values = String.Concat(values, " ", "@supplierSubdistrict");
                        }

                        if (((string[])Session["add"]).Contains("supplierDistrict") ||
                            ((string[])Session["auto"]).Contains("supplierDistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_district");
                            values = String.Concat(values, " ", "@supplierDistrict");

                        }

                        if (((string[])Session["add"]).Contains("supplierRuralDistrict") ||
                            ((string[])Session["auto"]).Contains("supplierRuralDistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_rural_district");
                            values = String.Concat(values, " ", "@supplierRuralDistrict");
                        }

                        if (((string[])Session["add"]).Contains("supplierProvince") ||
                            ((string[])Session["auto"]).Contains("supplierProvince"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_province");
                            values = String.Concat(values, " ", "@supplierProvince");
                        }

                        if (((string[])Session["add"]).Contains("supplierZipcode") ||
                            ((string[])Session["auto"]).Contains("supplierZipcode"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_zipcode");
                            values = String.Concat(values, " ", "@supplierZipcode");
                        }

                        if (((string[])Session["add"]).Contains("supplierContactName") ||
                            ((string[])Session["auto"]).Contains("supplierContactName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_contact_name");
                            values = String.Concat(values, " ", "@supplierContactName");
                        }

                        if (((string[])Session["add"]).Contains("supplierContactPhone") ||
                            ((string[])Session["auto"]).Contains("supplierContactPhone"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_contact_phone");
                            values = String.Concat(values, " ", "@supplierContactPhone");
                        }

                        if (((string[])Session["add"]).Contains("supplierContactEmail") ||
                            ((string[])Session["auto"]).Contains("supplierContactEmail"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_contact_email");
                            values = String.Concat(values, " ", "@supplierContactEmail");
                        }

                        if (((string[])Session["add"]).Contains("store") ||
                            ((string[])Session["auto"]).Contains("store"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_id");
                            values = String.Concat(values, " ", "@storeId");
                        }

                        if (((string[])Session["add"]).Contains("storeName") ||
                            ((string[])Session["auto"]).Contains("storeName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_name");
                            values = String.Concat(values, " ", "@storeName");
                        }

                        if (((string[])Session["add"]).Contains("storeBuildingName") ||
                            ((string[])Session["auto"]).Contains("storeBuildingName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_building_name");
                            values = String.Concat(values, " ", "@storeBuildingName");
                        }

                        if (((string[])Session["add"]).Contains("storeStreetName") ||
                            ((string[])Session["auto"]).Contains("storeStreetName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_street_name");
                            values = String.Concat(values, " ", "@storeStreetName");
                        }

                        if (((string[])Session["add"]).Contains("storeNeighbourhood") ||
                            ((string[])Session["auto"]).Contains("storeNeighbourhood"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_neighbourhood");
                            values = String.Concat(values, " ", "@storeNeighbourhood");
                        }

                        if (((string[])Session["add"]).Contains("storeSubdistrict") ||
                            ((string[])Session["auto"]).Contains("storeSubdistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_subdistrict");
                            values = String.Concat(values, " ", "@storeSubdistrict");
                        }

                        if (((string[])Session["add"]).Contains("storeDistrict") ||
                            ((string[])Session["auto"]).Contains("storeDistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_district");
                            values = String.Concat(values, " ", "@storeDistrict");
                        }

                        if (((string[])Session["add"]).Contains("storeRuralDistrict") ||
                            ((string[])Session["auto"]).Contains("storeRuralDistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_rural_district");
                            values = String.Concat(values, " ", "@storeRuralDistrict");
                        }

                        if (((string[])Session["add"]).Contains("storeProvince") ||
                            ((string[])Session["auto"]).Contains("storeProvince"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_province");
                            values = String.Concat(values, " ", "@storeProvince");
                        }

                        if (((string[])Session["add"]).Contains("storeZipcode") ||
                            ((string[])Session["auto"]).Contains("storeZipcode"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_zipcode");
                            values = String.Concat(values, " ", "@storeZipcode");
                        }

                        if (((string[])Session["add"]).Contains("storeContactName") ||
                            ((string[])Session["auto"]).Contains("storeContactName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_contact_name");
                            values = String.Concat(values, " ", "@storeContactName");
                        }

                        if (((string[])Session["add"]).Contains("storeContactPhone") ||
                            ((string[])Session["auto"]).Contains("storeContactPhone"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_contact_phone");
                            values = String.Concat(values, " ", "@storeContactPhone");
                        }

                        if (((string[])Session["add"]).Contains("storeContactEmail") ||
                            ((string[])Session["auto"]).Contains("storeContactEmail"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_contact_email");
                            values = String.Concat(values, " ", "@storeContactEmail");
                        }

                        if (((string[])Session["add"]).Contains("purchaseNo") ||
                            ((string[])Session["auto"]).Contains("purchaseNo"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "purchase_no");
                            values = String.Concat(values, " ", "@purchaseNo");
                        }

                        if (((string[])Session["add"]).Contains("purchaseDate") ||
                            ((string[])Session["auto"]).Contains("purchaseDate"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "purchase_date");
                            values = String.Concat(values, " ", "@purchaseDate");
                        }

                        //if (((string[])Session["add"]).Contains("deliveryNo") ||
                        //    ((string[])Session["auto"]).Contains("deliveryNo"))
                        //{
                        //    if (withComma)
                        //    {
                        //        fields = String.Concat(fields, ",");
                        //        values = String.Concat(values, ",");
                        //    }
                        //    else
                        //    {
                        //        withComma = true;
                        //    }
                        //    fields = String.Concat(fields, " ", "delivery_no");
                        //    values = String.Concat(values, " ", "@deliveryNo");
                        //}

                        //if (((string[])Session["add"]).Contains("deliveryDate"))
                        //{
                        //    if (withComma)
                        //    {
                        //        fields = String.Concat(fields, ",");
                        //        values = String.Concat(values, ",");
                        //    }
                        //    else
                        //    {
                        //        withComma = true;
                        //    }
                        //    fields = String.Concat(fields, " ", "delivery_date");
                        //    values = String.Concat(values, " ", "@deliveryDate");
                        //}

                        if (((string[])Session["add"]).Contains("totalQty") ||
                            ((string[])Session["auto"]).Contains("totalQty"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "total_qty");
                            values = String.Concat(values, " ", "@totalQty");
                        }

                        if (((string[])Session["add"]).Contains("totalPrice") ||
                            ((string[])Session["auto"]).Contains("totalPrice"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "total_price");
                            values = String.Concat(values, " ", "@totalPrice");
                        }

                        if (((string[])Session["add"]).Contains("approvalBy") ||
                            ((string[])Session["auto"]).Contains("approvalBy"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "approval_by");
                            values = String.Concat(values, " ", "@approvalBy");
                        }

                        if (((string[])Session["add"]).Contains("approvalDate") ||
                            ((string[])Session["auto"]).Contains("approvalDate"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "approval_date");
                            values = String.Concat(values, " ", "@approvalDate");
                        }

                        if (((string[])Session["add"]).Contains("requestedBy") ||
                            ((string[])Session["auto"]).Contains("requestedBy"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "requested_by");
                            values = String.Concat(values, " ", "@requestedBy");
                        }

                        if (((string[])Session["add"]).Contains("requestedDate") ||
                            ((string[])Session["auto"]).Contains("requestedDate"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "requested_date");
                            values = String.Concat(values, " ", "@requestedDate");
                        }

                        if (((string[])Session["add"]).Contains("purchaseStatus") ||
                            ((string[])Session["auto"]).Contains("purchaseStatus"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "purchase_status");
                            values = String.Concat(values, " ", "@purchaseStatus");
                        }

                        if (((string[])Session["add"]).Contains("purchaseNotes"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "purchase_notes");
                            values = String.Concat(values, " ", "@purchaseNotes");
                        }

                        string commandText = @"INSERT INTO purchase_orders ({0}, created_date, created_by, modified_date, modified_by)
                        VALUES ({1}, getdate(), @user, getdate(), @user);SELECT SCOPE_IDENTITY();";

                        commandText = String.Format(commandText, fields, values);

                        command = conn.CreateCommand();
                        command.CommandText = commandText;
                        command.Parameters.AddWithValue("@user", Session["username"]);
                        //command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                        //command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                        //command.Parameters.AddWithValue("@user", Session["username"]);

                        if (((string[])Session["add"]).Contains("supplier"))
                        {
                            if(string.IsNullOrEmpty(SupplierChoice.SelectedValue))
                            {
                                command.Parameters.AddWithValue("@supplierId", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                            }
                            
                        }
                        else if (((string[])Session["auto"]).Contains("supplier"))
                        {
                            if(String.IsNullOrEmpty(supplier["supplierId"]))
                            {
                                command.Parameters.AddWithValue("@supplierId", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierId", int.Parse(supplier["supplierId"]));
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierName"))
                        {
                            if (SupplierNameCheckBox.Checked)
                            {
                                if(String.IsNullOrEmpty(supplier["supplierName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierName", SupplierNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierName"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierName"]))
                            {
                                command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierBuildingName"))
                        {
                            if (SupplierBuildingNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", SupplierBuildingNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierBuildingName"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                            {
                                command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierStreetName"))
                        {
                            if (SupplierStreetNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                                }
                                
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", SupplierStreetNameInput.Text?.Trim());
                                }
                                
                            }

                        }
                        else if (((string[])Session["auto"]).Contains("supplierStreetName"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                            {
                                command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                            }
                            
                        }

                        if (((string[])Session["add"]).Contains("supplierNeighbourhood"))
                        {
                            if (SupplierNeighbourhoodCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                                }
                                
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", SupplierNeighbourhoodInput.Text?.Trim());
                                }
                            }

                        }
                        else if (((string[])Session["auto"]).Contains("supplierNeighbourhood"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                            {
                                command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierSubdistrict"))
                        {
                            if (SupplierSubdistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", SupplierSubdistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierSubdistrict"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierDistrict"))
                        {
                            if (SupplierDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", SupplierDistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierDistrict"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierRuralDistrict"))
                        {
                            if (SupplierRuralDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", SupplierRuralDistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierRuralDistrict"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                            {
                                command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierProvince"))
                        {
                            if (SupplierProvinceCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", SupplierProvinceInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierProvince"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                            {
                                command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierZipcode"))
                        {
                            if (SupplierZipcodeCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", SupplierZipcodeInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierZipcode"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                            {
                                command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierContactName"))
                        {
                            if (SupplierContactNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", SupplierContactNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierContactName"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                            {
                                command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierContactPhone"))
                        {
                            if (SupplierContactPhoneCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", SupplierContactPhoneInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierContactPhone"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                            {
                                command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierContactEmail"))
                        {
                            if (SupplierContactEmailCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", SupplierContactEmailInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierContactEmail"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                            {
                                command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("store"))
                        {
                            if (string.IsNullOrEmpty(StoreChoice.SelectedValue))
                            {
                                command.Parameters.AddWithValue("@storeId", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                            }

                        }
                        else if (((string[])Session["auto"]).Contains("store"))
                        {
                            if (String.IsNullOrEmpty(store["storeId"]))
                            {
                                command.Parameters.AddWithValue("@storeId", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeId", int.Parse(store["storeId"]));
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeName"))
                        {
                            if (StoreNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeName"]))
                                {
                                    command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeName", store["storeName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeName", StoreNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeName"))
                        {
                            if (String.IsNullOrEmpty(store["storeName"]))
                            {
                                command.Parameters.AddWithValue("@storeName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeName", store["storeName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeBuildingName"))
                        {
                            if (StoreBuildingNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeBuildingName"]))
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", StoreBuildingNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeBuildingName"))
                        {
                            if (String.IsNullOrEmpty(store["storeBuildingName"]))
                            {
                                command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeStreetName"))
                        {
                            if (StoreStreetNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeStreetName"]))
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", StoreStreetNameInput.Text?.Trim());
                                }

                            }

                        }
                        else if (((string[])Session["auto"]).Contains("storeStreetName"))
                        {
                            if (String.IsNullOrEmpty(store["storeStreetName"]))
                            {
                                command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                            }

                        }

                        if (((string[])Session["add"]).Contains("storeNeighbourhood"))
                        {
                            if (StoreNeighbourhoodCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", StoreNeighbourhoodInput.Text?.Trim());
                                }
                            }

                        }
                        else if (((string[])Session["auto"]).Contains("storeNeighbourhood"))
                        {
                            if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                            {
                                command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeSubdistrict"))
                        {
                            if (StoreSubdistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", StoreSubdistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeSubdistrict"))
                        {
                            if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeDistrict"))
                        {
                            if (StoreDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", StoreDistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeDistrict"))
                        {
                            if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeRuralDistrict"))
                        {
                            if (StoreRuralDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", StoreRuralDistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeRuralDistrict"))
                        {
                            if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeProvince"))
                        {
                            if (StoreProvinceCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeProvince"]))
                                {
                                    command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeProvince", StoreProvinceInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeProvince"))
                        {
                            if (String.IsNullOrEmpty(store["storeProvince"]))
                            {
                                command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeZipcode"))
                        {
                            if (StoreZipcodeCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeZipcode"]))
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", StoreZipcodeInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeZipcode"))
                        {
                            if (String.IsNullOrEmpty(store["storeZipcode"]))
                            {
                                command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeContactName"))
                        {
                            if (StoreContactNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeContactName"]))
                                {
                                    command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactName", StoreContactNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeContactName"))
                        {
                            if (String.IsNullOrEmpty(store["storeContactName"]))
                            {
                                command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeContactPhone"))
                        {
                            if (StoreContactPhoneCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeContactPhone"]))
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", StoreContactPhoneInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeContactPhone"))
                        {
                            if (String.IsNullOrEmpty(store["storeContactPhone"]))
                            {
                                command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeContactEmail"))
                        {
                            if (StoreContactEmailCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeContactEmail"]))
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", StoreContactEmailInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeContactEmail"))
                        {
                            if (String.IsNullOrEmpty(store["storeContactEmail"]))
                            {
                                command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("purchaseNo"))
                        {
                            if (PurchaseNoCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                            }
                            else
                            {
                                if(String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@purchaseNo", PurchaseNoInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("purchaseNo"))
                        {
                            command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                        }

                        if (((string[])Session["add"]).Contains("purchaseDate"))
                        {
                            if (ApprovalDateCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(PurchaseDateInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@purchaseDate", PurchaseDateInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("purchaseDate"))
                        {
                            command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        }

                        //if (((string[])Session["add"]).Contains("deliveryNo"))
                        //{
                        //    if (DeliveryNoCheckBox.Checked)
                        //    {
                        //        command.Parameters.AddWithValue("@deliveryNo", deliveryNo);
                        //    }
                        //    else
                        //    {
                        //        if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()))
                        //        {
                        //            command.Parameters.AddWithValue("@deliveryNo", DBNull.Value);
                        //        }
                        //        else
                        //        {
                        //            command.Parameters.AddWithValue("@deliveryNo", DeliveryNoInput.Text?.Trim());
                        //        }
                        //    }
                        //}
                        //else if (((string[])Session["auto"]).Contains("deliveryNo"))
                        //{
                        //    command.Parameters.AddWithValue("@deliveryNo", deliveryNo);
                        //}

                        //if (((string[])Session["add"]).Contains("deliveryDate"))
                        //{
                        //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
                        //    {
                        //        command.Parameters.AddWithValue("@deliveryDate", DBNull.Value);
                        //    }
                        //    else
                        //    {
                        //        command.Parameters.AddWithValue("@deliveryDate", DeliveryDateInput.Text?.Trim());
                        //    }
                        //}

                        if (((string[])Session["add"]).Contains("totalQty"))
                        {
                            if (TotalQtyCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@totalQty", totalQty);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@totalQty", totalQty);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@totalQty", TotalQtyInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("totalQty"))
                        {
                            command.Parameters.AddWithValue("@totalQty", totalQty);
                        }

                        if (((string[])Session["add"]).Contains("totalPrice"))
                        {
                            if (TotalPriceCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@totalPrice", totalPrice);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@totalPrice", totalPrice);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@totalPrice", TotalPriceInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("totalPrice"))
                        {
                            command.Parameters.AddWithValue("@totalPrice", totalPrice);
                        }

                        if (((string[])Session["add"]).Contains("approvalBy"))
                        {
                            if (ApprovalDateCheckBox.Checked)
                            {
                                if (PurchaseStatusChoice.SelectedValue == "pending")
                                {
                                    command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                                }
                            }
                            else
                            {
                                if (PurchaseStatusChoice.SelectedValue == "pending")
                                {
                                    command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                                }
                                else
                                {
                                    if(String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@approvalBy", ApprovalByInput.Text?.Trim());
                                    }
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("approvalBy"))
                        {
                            if (PurchaseStatusChoice.SelectedValue == "pending")
                            {
                                command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("approvalDate"))
                        {
                            if (ApprovalDateCheckBox.Checked)
                            {
                                if (PurchaseStatusChoice.SelectedValue == "pending")
                                {
                                    command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                }
                            }
                            else
                            {
                                if (PurchaseStatusChoice.SelectedValue == "pending")
                                {
                                    command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                                }
                                else
                                {
                                    if(String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@approvalDate", ApprovalDateInput.Text?.Trim());
                                    }   
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("approvalDate"))
                        {
                            if (PurchaseStatusChoice.SelectedValue == "pending")
                            {
                                command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            }
                        }

                        if (((string[])Session["add"]).Contains("requestedBy"))
                        {
                            if (RequestedByCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@requestedBy", Session["username"]);
                            }
                            else
                            {
                                if(String.IsNullOrEmpty(RequestedByInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@requestedBy", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@requestedBy", RequestedByInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("requestedBy"))
                        {
                            command.Parameters.AddWithValue("@requestedBy", Session["username"]);
                        }

                        if (((string[])Session["add"]).Contains("requestedDate"))
                        {
                            if (RequestedDateCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@requestedDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@requestedDate", RequestedDateInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("requestedDate"))
                        {
                            command.Parameters.AddWithValue("@requestedDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        }

                        if (((string[])Session["add"]).Contains("purchaseStatus"))
                        {
                            command.Parameters.AddWithValue("@purchaseStatus", PurchaseStatusChoice.SelectedValue?.Trim());
                        }
                        else if (((string[])Session["auto"]).Contains("purchaseStatus"))
                        {
                            command.Parameters.AddWithValue("@purchaseStatus", "pending");
                        }

                        if (((string[])Session["add"]).Contains("purchaseNotes"))
                        {
                            if (String.IsNullOrEmpty(PurchaseNotesInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@purchaseNotes", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@purchaseNotes", PurchaseNotesInput.Text?.Trim());
                            }
                        }

                        command.Transaction = transaction;
                        result = command.ExecuteScalar();
                        int purchaseId = 0;
                        if (result != null)
                        {
                            purchaseId = int.Parse(result.ToString());
                        }
                        
                        if (inserts.Rows.Count > 0)
                        {
                            command = conn.CreateCommand();
                            command.CommandText = @"CREATE TABLE #temp_insert (item_detail_id INT, item_json VARCHAR(MAX), item_qty INT, 
                                item_price DECIMAL(13,2));";
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();

                            SqlBulkCopy bulkinsert = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                            bulkinsert.DestinationTableName = "#temp_insert";

                            bulkinsert.ColumnMappings.Add("item_detail_id", "item_detail_id");
                            bulkinsert.ColumnMappings.Add("item_json", "item_json");
                            bulkinsert.ColumnMappings.Add("item_qty", "item_qty");
                            bulkinsert.ColumnMappings.Add("item_price", "item_price");
                            bulkinsert.WriteToServer(inserts);

                            command = conn.CreateCommand();
                            command.CommandText = @"INSERT INTO purchase_order_items (purchase_order_id, item_detail_id, 
                                item_json, item_qty, item_price, created_date, created_by, modified_date, modified_by)
                                SELECT @purchaseId, temp.item_detail_id, temp.item_json, temp.item_qty, 
                                temp.item_price, getdate(), @user, getdate(), @user 
                                FROM #temp_insert temp;
                                DROP TABLE #temp_insert;";
                            command.Parameters.AddWithValue("@purchaseId", purchaseId);
                            command.Parameters.AddWithValue("@user", Session["username"].ToString());
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();
                        
                        
                        inserts.Rows.Clear();
                        ViewState["inserts"] = inserts;
                        System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                        items.Rows.Clear();
                        ViewState["items"] = items;
                        ItemGridView.DataSource = items;
                        ItemGridView.DataBind();
                        FillAutoStoreInput();
                        FillAutoSupplierInput();
                        FillAutoPurchaseInput();
                        Clear();

                        AlertLabel.Text = "Data berhasil disimpan";
                        AlertLabel.CssClass = "alert alert-light-primary color-primary d-block";
                    }
                    catch (Exception error)
                    {
                        transaction.Rollback();

                        AlertLabel.Text = error.Message.ToString();
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                    conn.Close();
                }
                else
                {
                    AlertLabel.Text = String.Join("<br>", errors.ToArray());
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }

            if (action == "update")
            {
                if (!IsLocked())
                {
                    if (isEditableOrDeletable() || Session["role_name"].ToString() == "superuser")
                    {
                        List<string> errors = new List<string>();

                        if (((string[])Session["edit"]).Contains("purchaseNo"))
                        {
                            if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()) && !PurchaseNoCheckBox.Checked)
                            {
                                errors.Add("No pesanan tidak boleh kosong");
                            }

                        }

                        //if (((string[])Session["edit"]).Contains("deliveryNo"))
                        //{
                        //    if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()) && !DeliveryNoCheckBox.Checked)
                        //    {
                        //        errors.Add("No pengiriman tidak boleh kosong");
                        //    }
                        //}

                        //if (((string[])Session["edit"]).Contains("deliveryDate"))
                        //{
                        //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
                        //    {
                        //        errors.Add("Tanggal pengiriman tidak boleh kosong");
                        //    }
                        //}

                        if (((string[])Session["edit"]).Contains("totalQty"))
                        {
                            if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()) && !TotalQtyCheckBox.Checked)
                            {
                                errors.Add("Total kuantitas barang tidak boleh kosong");
                            }
                        }

                        if (((string[])Session["edit"]).Contains("totalPrice"))
                        {
                            if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()) && !TotalPriceCheckBox.Checked)
                            {
                                errors.Add("Total harga barang tidak boleh kosong");
                            }

                        }

                        if (((string[])Session["edit"]).Contains("approvalBy"))
                        {
                            if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                                    && String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()) && !ApprovalByCheckBox.Checked)
                            {
                                errors.Add("Ditolak/Disetujui oleh tidak boleh kosong");
                            }
                        }


                        if (((string[])Session["edit"]).Contains("approvalDate"))
                        {
                            if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                                    && String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()) && !ApprovalDateCheckBox.Checked)
                            {
                                errors.Add("Tanggal ditolak/disetujui oleh tidak boleh kosong");
                            }
                        }


                        if (((string[])Session["edit"]).Contains("requestedBy"))
                        {
                            if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()) && !RequestedByCheckBox.Checked)
                            {
                                errors.Add("Dipesan oleh tidak boleh kosong");
                            }
                        }

                        if (((string[])Session["edit"]).Contains("requestedDate"))
                        {
                            if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()) && !RequestedDateCheckBox.Checked)
                            {
                                errors.Add("Tanggal dipesan oleh tidak boleh kosong");
                            }
                        }

                        System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
                        System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
                        System.Data.DataTable deletes = (System.Data.DataTable)ViewState["deletes"];
                        System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                        if (inserts.Rows.Count <= 0 && updates.Rows.Count <= 0)
                        {
                            errors.Add("Barang yang akan dibeli tidak boleh kosong");
                        }

                        if (errors.Count <= 0)
                        {
                            int totalQty = 0;
                            double totalPrice = 0;

                            foreach (DataRow row in inserts.Rows)
                            {
                                totalQty += int.Parse(row["item_qty"].ToString());
                                totalPrice += double.Parse(row["item_price"].ToString()) * int.Parse(row["item_qty"].ToString());
                            }

                            foreach (DataRow row in updates.Rows)
                            {
                                totalQty += int.Parse(row["item_qty"].ToString());
                                totalPrice += double.Parse(row["item_price"].ToString()) * int.Parse(row["item_qty"].ToString());
                            }

                            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

                            SqlConnection conn = new SqlConnection(constr);
                            SqlTransaction transaction;
                            conn.Open();
                            transaction = conn.BeginTransaction();

                            try
                            {
                                string popattern = "PO" + DateTime.Now.ToString("yyyyMMdd");
                                string purchaseNo = popattern + "1".ToString().PadLeft(3, '0');

                                SqlCommand command = conn.CreateCommand();
                                command.CommandText = @"SELECT TOP 1 CAST(RIGHT([purchase_no], 3) AS INT) FROM purchase_orders WHERE 
                                    purchase_no LIKE @purchaseNo ORDER BY purchase_no DESC";
                                command.Parameters.AddWithValue("@purchaseNo", string.Format("{0}%", popattern));
                                command.Transaction = transaction;
                                int number = 1;

                                object result = command.ExecuteScalar();
                                if (result != null)
                                {
                                    number = int.Parse(result.ToString()) + 1;
                                }

                                purchaseNo = popattern + number.ToString().PadLeft(3, '0');


                                string sql = "";
                                bool withComma = false;

                                if (((string[])Session["edit"]).Contains("supplierName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_name = @supplierName");
                                }


                                if (((string[])Session["edit"]).Contains("supplierBuildingName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_building_name = @supplierBuildingName");
                                }

                                if (((string[])Session["edit"]).Contains("supplierStreetName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_street_name = @supplierStreetName");
                                }

                                if (((string[])Session["edit"]).Contains("supplierNeighbourhood"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_neighbourhood = @supplierNeighbourhood");

                                }

                                if (((string[])Session["edit"]).Contains("supplierSubdistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_subdistrict = @supplierSubdistrict");

                                }

                                if (((string[])Session["edit"]).Contains("supplierDistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_district = @supplierDistrict");

                                }

                                if (((string[])Session["edit"]).Contains("supplierRuralDistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_rural_district = @supplierRuralDistrict");

                                }

                                if (((string[])Session["edit"]).Contains("supplierProvince"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_province = @supplierProvince");

                                }

                                if (((string[])Session["edit"]).Contains("supplierZipcode"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_zipcode = @supplierZipcode");
                                }

                                if (((string[])Session["edit"]).Contains("supplierContactName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_contact_name = @supplierContactName");
                                }

                                if (((string[])Session["edit"]).Contains("supplierContactPhone"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_contact_phone = @supplierContactPhone");

                                }

                                if (((string[])Session["edit"]).Contains("supplierContactEmail"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_contact_email = @supplierContactEmail");

                                }

                                if (((string[])Session["edit"]).Contains("storeName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_name = @storeName");

                                }

                                if (((string[])Session["edit"]).Contains("storeBuildingName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_building_name = @storeBuildingName");

                                }

                                if (((string[])Session["edit"]).Contains("storeStreetName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_street_name = @storeStreetName");

                                }

                                if (((string[])Session["edit"]).Contains("storeNeighbourhood"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_neighbourhood = @storeNeighbourhood");

                                }

                                if (((string[])Session["edit"]).Contains("storeSubdistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_subdistrict = @storeSubdistrict");

                                }

                                if (((string[])Session["edit"]).Contains("storeDistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_district = @storeDistrict");
                                }

                                if (((string[])Session["edit"]).Contains("storeRuralDistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_rural_district = @storeRuralDistrict");

                                }

                                if (((string[])Session["edit"]).Contains("storeProvince"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_province = @storeProvince");

                                }

                                if (((string[])Session["edit"]).Contains("storeZipcode"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_zipcode = @storeZipcode");

                                }

                                if (((string[])Session["edit"]).Contains("storeContactName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_contact_name = @storeContactName");

                                }

                                if (((string[])Session["edit"]).Contains("storeContactPhone"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_contact_phone = @storeContactPhone");

                                }

                                if (((string[])Session["edit"]).Contains("storeContactEmail"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_contact_email = @storeContactEmail");
                                }

                                if (((string[])Session["edit"]).Contains("purchaseNo"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "purchase_no = @purchaseNo");

                                }

                                if (((string[])Session["edit"]).Contains("purchaseDate"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "purchase_date = @purchaseDate");

                                }

                                //if (((string[])Session["edit"]).Contains("deliveryNo"))
                                //{
                                //    if (withComma)
                                //    {
                                //        sql = String.Concat(sql, ",");
                                //    }
                                //    else
                                //    {
                                //        withComma = true;
                                //    }
                                //    sql = String.Concat(sql, " ", "delivery_no = @deliveryNo");

                                //}

                                //if (((string[])Session["edit"]).Contains("deliveryDate"))
                                //{
                                //    if (withComma)
                                //    {
                                //        sql = String.Concat(sql, ",");
                                //    }
                                //    else
                                //    {
                                //        withComma = true;
                                //    }
                                //    sql = String.Concat(sql, " ", "delivery_date = @deliveryDate");

                                //}

                                if (((string[])Session["edit"]).Contains("totalQty"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "total_qty = @totalQty");

                                }

                                if (((string[])Session["edit"]).Contains("totalPrice"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "total_price = @totalPrice");

                                }

                                if (((string[])Session["edit"]).Contains("approvalBy"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "approval_by = @approvalBy");
                                }

                                if (((string[])Session["edit"]).Contains("approvalDate"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "approval_date = @approvalDate");
                                }

                                if (((string[])Session["edit"]).Contains("requestedBy"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "requested_by = @requestedBy");
                                }

                                if (((string[])Session["edit"]).Contains("requestedDate"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "requested_date = @requestedDate");
                                }

                                if (((string[])Session["edit"]).Contains("purchaseStatus"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "purchase_status = @purchaseStatus");
                                }

                                if (((string[])Session["edit"]).Contains("purchaseNotes"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "purchase_notes = @purchaseNotes");
                                }

                                
                                string commandText = @"UPDATE purchase_orders SET {0}, modified_date = getdate(), modified_by = @user
                                WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid AND isdeleted = 0;";

                                commandText = String.Format(commandText, sql);

                                command = conn.CreateCommand();
                                command.CommandText = commandText;
                                command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                command.Parameters.AddWithValue("@user", Session["username"]);

                                if (((string[])Session["edit"]).Contains("supplierName"))
                                {
                                    if (SupplierNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierName"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierName", SupplierNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("supplierBuildingName"))
                                {
                                    if (SupplierBuildingNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierBuildingName", SupplierBuildingNameInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierStreetName"))
                                {
                                    if (SupplierStreetNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierStreetName", SupplierStreetNameInput.Text?.Trim());
                                        }

                                    }

                                }


                                if (((string[])Session["edit"]).Contains("supplierNeighbourhood"))
                                {
                                    if (SupplierNeighbourhoodCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierNeighbourhood", SupplierNeighbourhoodInput.Text?.Trim());
                                        }
                                    }

                                }


                                if (((string[])Session["edit"]).Contains("supplierSubdistrict"))
                                {
                                    if (SupplierSubdistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierSubdistrict", SupplierSubdistrictInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierDistrict"))
                                {
                                    if (SupplierDistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierDistrict", SupplierDistrictInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierRuralDistrict"))
                                {
                                    if (SupplierRuralDistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierRuralDistrict", SupplierRuralDistrictInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierProvince"))
                                {
                                    if (SupplierProvinceCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierProvince", SupplierProvinceInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierZipcode"))
                                {
                                    if (SupplierZipcodeCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierZipcode", SupplierZipcodeInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierContactName"))
                                {
                                    if (SupplierContactNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactName", SupplierContactNameInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierContactPhone"))
                                {
                                    if (SupplierContactPhoneCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactPhone", SupplierContactPhoneInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("supplierContactEmail"))
                                {
                                    if (SupplierContactEmailCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactEmail", SupplierContactEmailInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeName"))
                                {
                                    if (StoreNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeName"]))
                                        {
                                            command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeName", store["storeName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeName", StoreNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeBuildingName"))
                                {
                                    if (StoreBuildingNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeBuildingName"]))
                                        {
                                            command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeBuildingName", StoreBuildingNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeStreetName"))
                                {
                                    if (StoreStreetNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeStreetName"]))
                                        {
                                            command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeStreetName", StoreStreetNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeNeighbourhood"))
                                {
                                    if (StoreNeighbourhoodCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                                        {
                                            command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeNeighbourhood", StoreNeighbourhoodInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeSubdistrict"))
                                {
                                    if (StoreSubdistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeSubdistrict", StoreSubdistrictInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeDistrict"))
                                {
                                    if (StoreDistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeDistrict", StoreDistrictInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeRuralDistrict"))
                                {
                                    if (StoreRuralDistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeRuralDistrict", StoreRuralDistrictInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeProvince"))
                                {
                                    if (StoreProvinceCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeProvince"]))
                                        {
                                            command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeProvince", StoreProvinceInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeZipcode"))
                                {
                                    if (StoreZipcodeCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeZipcode"]))
                                        {
                                            command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeZipcode", StoreZipcodeInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeContactName"))
                                {
                                    if (StoreContactNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeContactName"]))
                                        {
                                            command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactName", StoreContactNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeContactPhone"))
                                {
                                    if (StoreContactPhoneCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeContactPhone"]))
                                        {
                                            command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactPhone", StoreContactPhoneInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeContactEmail"))
                                {
                                    if (StoreContactEmailCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeContactEmail"]))
                                        {
                                            command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactEmail", StoreContactEmailInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("purchaseNo"))
                                {
                                    if (PurchaseNoCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(purchase["purchaseNo"]))
                                        {
                                            command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@purchaseNo", purchase["purchaseNo"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()))
                                        {
                                            if (String.IsNullOrEmpty(purchase["purchaseNo"]))
                                            {
                                                command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@purchaseNo", purchase["purchaseNo"]);
                                            }
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@purchaseNo", PurchaseNoInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("purchaseDate"))
                                {
                                    if (ApprovalDateCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(purchase["purchaseDate"]))
                                        {
                                            command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@purchaseDate", purchase["purchaseDate"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(PurchaseDateInput.Text?.Trim()))
                                        {
                                            if (String.IsNullOrEmpty(purchase["purchaseDate"]))
                                            {
                                                command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@purchaseDate", purchase["purchaseDate"]);
                                            }
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@purchaseDate", PurchaseDateInput.Text?.Trim());
                                        }
                                    }
                                }

                                //if (((string[])Session["edit"]).Contains("deliveryNo"))
                                //{
                                //    if (DeliveryNoCheckBox.Checked)
                                //    {
                                //        if (String.IsNullOrEmpty(purchase["deliveryNo"]))
                                //        {
                                //            command.Parameters.AddWithValue("@deliveryNo", DBNull.Value);
                                //        }
                                //        else
                                //        {
                                //            command.Parameters.AddWithValue("@deliveryNo", purchase["deliveryNo"]);
                                //        }
                                //    }
                                //    else
                                //    {
                                //        if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()))
                                //        {
                                //            command.Parameters.AddWithValue("@deliveryNo", DBNull.Value);
                                //        }
                                //        else
                                //        {
                                //            command.Parameters.AddWithValue("@deliveryNo", DeliveryNoInput.Text?.Trim());
                                //        }
                                //    }
                                //}

                                //if (((string[])Session["edit"]).Contains("deliveryDate"))
                                //{
                                //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
                                //    {
                                //        command.Parameters.AddWithValue("@deliveryDate", DBNull.Value);
                                //    }
                                //    else
                                //    {
                                //        command.Parameters.AddWithValue("@deliveryDate", DeliveryDateInput.Text?.Trim());
                                //    }
                                //}

                                if (((string[])Session["edit"]).Contains("totalQty"))
                                {
                                    if (TotalQtyCheckBox.Checked)
                                    {
                                        command.Parameters.AddWithValue("@totalQty", totalQty);
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@totalQty", 0);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@totalQty", TotalQtyInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("totalPrice"))
                                {
                                    if (TotalPriceCheckBox.Checked)
                                    {
                                        command.Parameters.AddWithValue("@totalPrice", totalPrice);
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@totalPrice", 0);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@totalPrice", TotalPriceInput.Text?.Trim());
                                        }
                                    }
                                }
                                

                                if (((string[])Session["edit"]).Contains("approvalBy"))
                                {
                                    if (ApprovalDateCheckBox.Checked)
                                    {
                                        if (PurchaseStatusChoice.SelectedValue == "pending" || String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                        {
                                            command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(purchase["approvalBy"]))
                                            {
                                                command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@approvalBy", purchase["approvalBy"]);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (PurchaseStatusChoice.SelectedValue == "pending" || String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                        {
                                            command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()))
                                            {
                                                command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@approvalBy", ApprovalByInput.Text?.Trim());
                                            }
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("approvalDate"))
                                {
                                    if (ApprovalDateCheckBox.Checked)
                                    {
                                        if (PurchaseStatusChoice.SelectedValue == "pending" || String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                        {
                                            command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(purchase["approvalDate"]))
                                            {
                                                command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@approvalDate", purchase["approvalDate"]);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (PurchaseStatusChoice.SelectedValue == "pending" || String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                        {
                                            command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()))
                                            {
                                                command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@approvalDate", ApprovalDateInput.Text?.Trim());
                                            }
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("requestedBy"))
                                {
                                    if (RequestedByCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(purchase["requestedBy"]))
                                        {
                                            command.Parameters.AddWithValue("@requestedBy", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@requestedBy", purchase["requestedBy"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@requestedBy", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@requestedBy", RequestedByInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("requestedDate"))
                                {
                                    if (RequestedDateCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(purchase["requestedDate"]))
                                        {
                                            command.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@requestedDate", purchase["requestedDate"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@requestedDate", RequestedDateInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("purchaseStatus"))
                                {
                                    if (String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                    {
                                        command.Parameters.AddWithValue("@purchaseStatus", "pending");
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@purchaseStatus", PurchaseStatusChoice.SelectedValue?.Trim());
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("purchaseNotes"))
                                {
                                    if (String.IsNullOrEmpty(PurchaseNotesInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@purchaseNotes", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@purchaseNotes", PurchaseNotesInput.Text?.Trim());
                                    }
                                }

                                command.Transaction = transaction;
                                command.ExecuteNonQuery();

                                if (deletes.Rows.Count > 0)
                                {
                                    command = conn.CreateCommand();
                                    command.CommandText = @"CREATE TABLE #temp_delete (purchase_order_item_id INT, purchase_order_id INT, item_detail_id INT);";
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();

                                    SqlBulkCopy bulkdelete = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                                    bulkdelete.DestinationTableName = "#temp_delete";

                                    bulkdelete.ColumnMappings.Add("purchase_order_item_id", "purchase_order_item_id");
                                    bulkdelete.ColumnMappings.Add("purchase_order_id", "purchase_order_id");
                                    bulkdelete.ColumnMappings.Add("item_detail_id", "item_detail_id");
                                    bulkdelete.WriteToServer(deletes);

                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE orig SET isdeleted = 1, modified_date = getdate(),
                                        modified_by = @user
                                        FROM purchase_order_items orig 
                                        INNER JOIN #temp_delete temp ON orig.purchase_order_item_id = temp.purchase_order_item_id AND
                                        orig.purchase_order_id = temp.purchase_order_id AND orig.item_detail_id = temp.item_detail_id;
                                        DROP TABLE #temp_delete;";
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();
                                }

                                if (updates.Rows.Count > 0)
                                {
                                    command = conn.CreateCommand();
                                    command.CommandText = @"CREATE TABLE #temp_update (purchase_order_item_id INT, purchase_order_id INT, item_detail_id INT,
                                item_json VARCHAR(MAX), item_qty INT, item_price DECIMAL(13,2));";
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();

                                    SqlBulkCopy bulkupdate = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                                    bulkupdate.DestinationTableName = "#temp_update";

                                    bulkupdate.ColumnMappings.Add("purchase_order_item_id", "purchase_order_item_id");
                                    bulkupdate.ColumnMappings.Add("purchase_order_id", "purchase_order_id");
                                    bulkupdate.ColumnMappings.Add("item_detail_id", "item_detail_id");
                                    bulkupdate.ColumnMappings.Add("item_json", "item_json");
                                    bulkupdate.ColumnMappings.Add("item_qty", "item_qty");
                                    bulkupdate.ColumnMappings.Add("item_price", "item_price");
                                    bulkupdate.WriteToServer(updates);

                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE orig SET item_json = temp.item_json, item_qty = temp.item_qty, 
                                    item_price = temp.item_price, modified_date = getdate(), modified_by = @user
                                    FROM purchase_order_items orig 
                                    INNER JOIN #temp_update temp ON orig.purchase_order_item_id = temp.purchase_order_item_id AND
                                    orig.purchase_order_id = temp.purchase_order_id AND orig.item_detail_id = temp.item_detail_id
                                    WHERE orig.isdeleted = 0;
                                    DROP TABLE #temp_update;";
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();
                                }

                                if (inserts.Rows.Count > 0)
                                {
                                    command = conn.CreateCommand();
                                    command.CommandText = @"CREATE TABLE #temp_insert (item_detail_id INT, item_json VARCHAR(MAX), item_qty INT, item_price DECIMAL(13,2));";
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();

                                    SqlBulkCopy bulkinsert = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                                    bulkinsert.DestinationTableName = "#temp_insert";

                                    bulkinsert.ColumnMappings.Add("item_detail_id", "item_detail_id");
                                    bulkinsert.ColumnMappings.Add("item_json", "item_json");
                                    bulkinsert.ColumnMappings.Add("item_qty", "item_qty");
                                    bulkinsert.ColumnMappings.Add("item_price", "item_price");
                                    bulkinsert.WriteToServer(inserts);

                                    command = conn.CreateCommand();
                                    command.CommandText = @"INSERT INTO purchase_order_items (purchase_order_id, item_detail_id, 
                                    item_json, item_qty, item_price, created_date, created_by, modified_date, modified_by)
                                    SELECT @purchaseId, temp.item_detail_id, temp.item_json, temp.item_qty, 
                                    temp.item_price, getdate(), @user, getdate(), @user 
                                    FROM #temp_insert temp;
                                    DROP TABLE #temp_insert;";
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();
                                }

                                if (Session["role_name"].ToString() == "superuser")
                                {
                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                        FROM goods_receipt_items A INNER JOIN goods_receipts B 
                                        ON A.goods_receipt_id = B.goods_receipt_id AND B.isdeleted = 0
                                        INNER JOIN purchase_orders C ON C.purchase_order_id = B.purchase_order_id AND C.isdeleted = 0
                                        WHERE A.isdeleted = 0 AND C.purchase_order_id = @purchaseId AND C.uuid = @purchaseUuid AND
                                        A.item_detail_id NOT IN (SELECT K.item_detail_id
                                        FROM goods_receipt_items K INNER JOIN goods_receipts L 
                                        ON K.goods_receipt_id = L.goods_receipt_id AND L.isdeleted = 0
                                        INNER JOIN purchase_orders M ON M.purchase_order_id = L.purchase_order_id AND M.isdeleted = 0
                                        INNER JOIN purchase_order_items N ON N.purchase_order_id = M.purchase_order_id AND N.isdeleted = 0
                                        WHERE K.isdeleted = 0 AND M.purchase_order_id = @purchaseId AND M.uuid = @purchaseUuid 
                                        AND K.item_detail_id = N.item_detail_id);";
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                    command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();

                                    command = conn.CreateCommand();
                                    command.CommandText = @"INSERT INTO goods_receipt_items (goods_receipt_id, item_detail_id, item_json,
                                        item_price, created_date, created_by, modified_date, modified_by) 
                                        SELECT C.goods_receipt_id, A.item_detail_id, A.item_json, A.item_price, getdate(), 'admin', getdate(), 'admin' 
                                        FROM purchase_order_items A INNER JOIN purchase_orders B 
                                        ON A.purchase_order_id = B.purchase_order_id AND B.isdeleted = 0
                                        INNER JOIN goods_receipts C ON C.purchase_order_id = B.purchase_order_id AND B.isdeleted = 0
                                        WHERE B.purchase_order_id = @purchaseId AND B.uuid = @purchaseUuid AND A.isdeleted = 0 AND
                                        A.item_detail_id NOT IN (SELECT K.item_detail_id
                                        FROM goods_receipt_items K INNER JOIN goods_receipts L 
                                        ON K.goods_receipt_id = L.goods_receipt_id AND L.isdeleted = 0
                                        INNER JOIN purchase_orders M ON M.purchase_order_id = L.purchase_order_id AND M.isdeleted = 0
                                        INNER JOIN purchase_order_items N ON N.purchase_order_id = M.purchase_order_id AND N.isdeleted = 0
                                        WHERE K.isdeleted = 0 AND M.purchase_order_id = @purchaseId AND M.uuid = @purchaseUuid 
                                        AND K.item_detail_id = N.item_detail_id);";
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                    command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();
                                }

                                transaction.Commit();

                                inserts.Rows.Clear();
                                ViewState["inserts"] = inserts;
                                updates.Rows.Clear();
                                ViewState["updates"] = updates;
                                deletes.Rows.Clear();
                                ViewState["deletes"] = deletes;
                                items.Rows.Clear();
                                ViewState["items"] = items;

                                LoadForm();
                                FillAutoSupplierInput();
                                FillAutoStoreInput();
                                FillAutoPurchaseInput();
                                FillPurchaseInput();
                                BindItemGridview();

                                AlertLabel.Text = "Data berhasil disimpan";
                                AlertLabel.CssClass = "alert alert-light-primary color-primary d-block";
                            }
                            catch (Exception error)
                            {
                                transaction.Rollback();

                                AlertLabel.Text = error.Message.ToString();
                                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                            conn.Close();
                        }
                        else
                        {
                            AlertLabel.Text = String.Join("<br>", errors.ToArray());
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen pembelian barang tidak bisa diubah/dihapus";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Dokumen pembelian terkunci";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }

            }
        }

        protected void LockButton_Click(object sender, EventArgs e)
        {
            if (action == "create")
            {
                List<string> errors = new List<string>();

                if (((string[])Session["add"]).Contains("purchaseNo"))
                {
                    if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()) && !PurchaseNoCheckBox.Checked)
                    {
                        errors.Add("No pesanan tidak boleh kosong");
                    }
                }

                //if (((string[])Session["add"]).Contains("deliveryNo"))
                //{
                //    if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()) && !DeliveryNoCheckBox.Checked)
                //    {
                //        errors.Add("No pengiriman tidak boleh kosong");
                //    }
                //}

                //if (((string[])Session["add"]).Contains("deliveryDate"))
                //{
                //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
                //    {
                //        errors.Add("Tanggal pengiriman tidak boleh kosong");
                //    }   
                //}


                if (((string[])Session["add"]).Contains("totalQty"))
                {
                    if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()) && !TotalQtyCheckBox.Checked)
                    {
                        errors.Add("Total kuantitas barang tidak boleh kosong");
                    }
                }

                if (((string[])Session["add"]).Contains("totalPrice"))
                {
                    if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()) && !TotalPriceCheckBox.Checked)
                    {
                        errors.Add("Total harga barang tidak boleh kosong");
                    }
                }

                if (((string[])Session["add"]).Contains("approvalBy"))
                {
                    if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                            && String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()) && !ApprovalByCheckBox.Checked)
                    {
                        errors.Add("Ditolak/Disetujui oleh tidak boleh kosong");
                    }
                }

                if (((string[])Session["add"]).Contains("approvalDate"))
                {
                    if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                            && String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()) && !ApprovalDateCheckBox.Checked)
                    {
                        errors.Add("Tanggal ditolak/disetujui oleh tidak boleh kosong");
                    }
                }

                if (((string[])Session["add"]).Contains("requestedBy"))
                {
                    if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()) && !RequestedByCheckBox.Checked)
                    {
                        errors.Add("Dipesan oleh tidak boleh kosong");
                    }
                }

                if (((string[])Session["add"]).Contains("requestedDate"))
                {
                    if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()) && !RequestedDateCheckBox.Checked)
                    {
                        errors.Add("Tanggal dipesan oleh tidak boleh kosong");
                    }
                }

                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                if (inserts.Rows.Count <= 0)
                {
                    errors.Add("Barang yang akan dibeli tidak boleh kosong");
                }

                if (errors.Count <= 0)
                {
                    int totalQty = 0;
                    double totalPrice = 0;

                    foreach (DataRow row in inserts.Rows)
                    {
                        totalQty += int.Parse(row["item_qty"].ToString());
                        totalPrice += double.Parse(row["item_price"].ToString()) * int.Parse(row["item_qty"].ToString());
                    }

                    string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

                    SqlConnection conn = new SqlConnection(constr);
                    SqlTransaction transaction;
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    try
                    {
                        string popattern = "PO" + DateTime.Now.ToString("yyyyMMdd");
                        string purchaseNo = popattern + "1".ToString().PadLeft(3, '0');

                        SqlCommand command = conn.CreateCommand();
                        command.CommandText = @"SELECT TOP 1 CAST(RIGHT([purchase_no], 3) AS INT) FROM purchase_orders WHERE 
                                    purchase_no LIKE @purchaseNo ORDER BY purchase_no DESC";
                        command.Parameters.AddWithValue("@purchaseNo", string.Format("{0}%", popattern));
                        command.Transaction = transaction;
                        int number = 1;

                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            number = int.Parse(result.ToString()) + 1;
                        }

                        purchaseNo = popattern + number.ToString().PadLeft(3, '0');

                        string fields = "";
                        string values = "";
                        bool withComma = false;

                        if (((string[])Session["add"]).Contains("supplier") ||
                            ((string[])Session["auto"]).Contains("supplier"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_id");
                            values = String.Concat(values, " ", "@supplierId");
                        }

                        if (((string[])Session["add"]).Contains("supplierName") ||
                            ((string[])Session["auto"]).Contains("supplierName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_name");
                            values = String.Concat(values, " ", "@supplierName");
                        }


                        if (((string[])Session["add"]).Contains("supplierBuildingName") ||
                            ((string[])Session["auto"]).Contains("supplierBuildingName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_building_name");
                            values = String.Concat(values, " ", "@supplierBuildingName");
                        }

                        if (((string[])Session["add"]).Contains("supplierStreetName") ||
                            ((string[])Session["auto"]).Contains("supplierStreetName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_street_name");
                            values = String.Concat(values, " ", "@supplierStreetName");
                        }

                        if (((string[])Session["add"]).Contains("supplierNeighbourhood") ||
                            ((string[])Session["auto"]).Contains("supplierNeighbourhood"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_neighbourhood");
                            values = String.Concat(values, " ", "@supplierNeighbourhood");

                        }

                        if (((string[])Session["add"]).Contains("supplierSubdistrict") ||
                            ((string[])Session["auto"]).Contains("supplierSubdistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_subdistrict");
                            values = String.Concat(values, " ", "@supplierSubdistrict");
                        }

                        if (((string[])Session["add"]).Contains("supplierDistrict") ||
                            ((string[])Session["auto"]).Contains("supplierDistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_district");
                            values = String.Concat(values, " ", "@supplierDistrict");

                        }

                        if (((string[])Session["add"]).Contains("supplierRuralDistrict") ||
                            ((string[])Session["auto"]).Contains("supplierRuralDistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_rural_district");
                            values = String.Concat(values, " ", "@supplierRuralDistrict");
                        }

                        if (((string[])Session["add"]).Contains("supplierProvince") ||
                            ((string[])Session["auto"]).Contains("supplierProvince"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_province");
                            values = String.Concat(values, " ", "@supplierProvince");
                        }

                        if (((string[])Session["add"]).Contains("supplierZipcode") ||
                            ((string[])Session["auto"]).Contains("supplierZipcode"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_zipcode");
                            values = String.Concat(values, " ", "@supplierZipcode");
                        }

                        if (((string[])Session["add"]).Contains("supplierContactName") ||
                            ((string[])Session["auto"]).Contains("supplierContactName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_contact_name");
                            values = String.Concat(values, " ", "@supplierContactName");
                        }

                        if (((string[])Session["add"]).Contains("supplierContactPhone") ||
                            ((string[])Session["auto"]).Contains("supplierContactPhone"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_contact_phone");
                            values = String.Concat(values, " ", "@supplierContactPhone");
                        }

                        if (((string[])Session["add"]).Contains("supplierContactEmail") ||
                            ((string[])Session["auto"]).Contains("supplierContactEmail"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "supplier_contact_email");
                            values = String.Concat(values, " ", "@supplierContactEmail");
                        }

                        if (((string[])Session["add"]).Contains("store") ||
                            ((string[])Session["auto"]).Contains("store"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_id");
                            values = String.Concat(values, " ", "@storeId");
                        }

                        if (((string[])Session["add"]).Contains("storeName") ||
                            ((string[])Session["auto"]).Contains("storeName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_name");
                            values = String.Concat(values, " ", "@storeName");
                        }

                        if (((string[])Session["add"]).Contains("storeBuildingName") ||
                            ((string[])Session["auto"]).Contains("storeBuildingName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_building_name");
                            values = String.Concat(values, " ", "@storeBuildingName");
                        }

                        if (((string[])Session["add"]).Contains("storeStreetName") ||
                            ((string[])Session["auto"]).Contains("storeStreetName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_street_name");
                            values = String.Concat(values, " ", "@storeStreetName");
                        }

                        if (((string[])Session["add"]).Contains("storeNeighbourhood") ||
                            ((string[])Session["auto"]).Contains("storeNeighbourhood"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_neighbourhood");
                            values = String.Concat(values, " ", "@storeNeighbourhood");
                        }

                        if (((string[])Session["add"]).Contains("storeSubdistrict") ||
                            ((string[])Session["auto"]).Contains("storeSubdistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_subdistrict");
                            values = String.Concat(values, " ", "@storeSubdistrict");
                        }

                        if (((string[])Session["add"]).Contains("storeDistrict") ||
                            ((string[])Session["auto"]).Contains("storeDistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_district");
                            values = String.Concat(values, " ", "@storeDistrict");
                        }

                        if (((string[])Session["add"]).Contains("storeRuralDistrict") ||
                            ((string[])Session["auto"]).Contains("storeRuralDistrict"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_rural_district");
                            values = String.Concat(values, " ", "@storeRuralDistrict");
                        }

                        if (((string[])Session["add"]).Contains("storeProvince") ||
                            ((string[])Session["auto"]).Contains("storeProvince"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_province");
                            values = String.Concat(values, " ", "@storeProvince");
                        }

                        if (((string[])Session["add"]).Contains("storeZipcode") ||
                            ((string[])Session["auto"]).Contains("storeZipcode"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_zipcode");
                            values = String.Concat(values, " ", "@storeZipcode");
                        }

                        if (((string[])Session["add"]).Contains("storeContactName") ||
                            ((string[])Session["auto"]).Contains("storeContactName"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_contact_name");
                            values = String.Concat(values, " ", "@storeContactName");
                        }

                        if (((string[])Session["add"]).Contains("storeContactPhone") ||
                            ((string[])Session["auto"]).Contains("storeContactPhone"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_contact_phone");
                            values = String.Concat(values, " ", "@storeContactPhone");
                        }

                        if (((string[])Session["add"]).Contains("storeContactEmail") ||
                            ((string[])Session["auto"]).Contains("storeContactEmail"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "store_contact_email");
                            values = String.Concat(values, " ", "@storeContactEmail");
                        }

                        if (((string[])Session["add"]).Contains("purchaseNo") ||
                            ((string[])Session["auto"]).Contains("purchaseNo"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "purchase_no");
                            values = String.Concat(values, " ", "@purchaseNo");
                        }

                        if (((string[])Session["add"]).Contains("purchaseDate") ||
                            ((string[])Session["auto"]).Contains("purchaseDate"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "purchase_date");
                            values = String.Concat(values, " ", "@purchaseDate");
                        }

                        //if (((string[])Session["add"]).Contains("deliveryNo") ||
                        //    ((string[])Session["auto"]).Contains("deliveryNo"))
                        //{
                        //    if (withComma)
                        //    {
                        //        fields = String.Concat(fields, ",");
                        //        values = String.Concat(values, ",");
                        //    }
                        //    else
                        //    {
                        //        withComma = true;
                        //    }
                        //    fields = String.Concat(fields, " ", "delivery_no");
                        //    values = String.Concat(values, " ", "@deliveryNo");
                        //}

                        //if (((string[])Session["add"]).Contains("deliveryDate"))
                        //{
                        //    if (withComma)
                        //    {
                        //        fields = String.Concat(fields, ",");
                        //        values = String.Concat(values, ",");
                        //    }
                        //    else
                        //    {
                        //        withComma = true;
                        //    }
                        //    fields = String.Concat(fields, " ", "delivery_date");
                        //    values = String.Concat(values, " ", "@deliveryDate");
                        //}

                        if (((string[])Session["add"]).Contains("totalQty") ||
                            ((string[])Session["auto"]).Contains("totalQty"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "total_qty");
                            values = String.Concat(values, " ", "@totalQty");
                        }

                        if (((string[])Session["add"]).Contains("totalPrice") ||
                            ((string[])Session["auto"]).Contains("totalPrice"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "total_price");
                            values = String.Concat(values, " ", "@totalPrice");
                        }

                        if (((string[])Session["add"]).Contains("approvalBy") ||
                            ((string[])Session["auto"]).Contains("approvalBy"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "approval_by");
                            values = String.Concat(values, " ", "@approvalBy");
                        }

                        if (((string[])Session["add"]).Contains("approvalDate") ||
                            ((string[])Session["auto"]).Contains("approvalDate"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "approval_date");
                            values = String.Concat(values, " ", "@approvalDate");
                        }

                        if (((string[])Session["add"]).Contains("requestedBy") ||
                            ((string[])Session["auto"]).Contains("requestedBy"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "requested_by");
                            values = String.Concat(values, " ", "@requestedBy");
                        }

                        if (((string[])Session["add"]).Contains("requestedDate") ||
                            ((string[])Session["auto"]).Contains("requestedDate"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "requested_date");
                            values = String.Concat(values, " ", "@requestedDate");
                        }

                        if (((string[])Session["add"]).Contains("purchaseStatus") ||
                            ((string[])Session["auto"]).Contains("purchaseStatus"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "purchase_status");
                            values = String.Concat(values, " ", "@purchaseStatus");
                        }

                        if (((string[])Session["add"]).Contains("purchaseNotes"))
                        {
                            if (withComma)
                            {
                                fields = String.Concat(fields, ",");
                                values = String.Concat(values, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            fields = String.Concat(fields, " ", "purchase_notes");
                            values = String.Concat(values, " ", "@purchaseNotes");
                        }
                        string commandText = @"INSERT INTO purchase_orders ({0}, created_date, created_by, modified_date, modified_by, locked)
                        VALUES ({1}, getdate(), @user, getdate(), @user, 1);SELECT SCOPE_IDENTITY();";

                        commandText = String.Format(commandText, fields, values);

                        command = conn.CreateCommand();
                        command.CommandText = commandText;
                        command.Parameters.AddWithValue("@user", Session["username"]);
                        //command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                        //command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                        //command.Parameters.AddWithValue("@user", Session["username"]);

                        if (((string[])Session["add"]).Contains("supplier"))
                        {
                            if (string.IsNullOrEmpty(SupplierChoice.SelectedValue))
                            {
                                command.Parameters.AddWithValue("@supplierId", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                            }

                        }
                        else if (((string[])Session["auto"]).Contains("supplier"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierId"]))
                            {
                                command.Parameters.AddWithValue("@supplierId", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierId", int.Parse(supplier["supplierId"]));
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierName"))
                        {
                            if (SupplierNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierName", SupplierNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierName"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierName"]))
                            {
                                command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierBuildingName"))
                        {
                            if (SupplierBuildingNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", SupplierBuildingNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierBuildingName"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                            {
                                command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierStreetName"))
                        {
                            if (SupplierStreetNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", SupplierStreetNameInput.Text?.Trim());
                                }

                            }

                        }
                        else if (((string[])Session["auto"]).Contains("supplierStreetName"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                            {
                                command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                            }

                        }

                        if (((string[])Session["add"]).Contains("supplierNeighbourhood"))
                        {
                            if (SupplierNeighbourhoodCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", SupplierNeighbourhoodInput.Text?.Trim());
                                }
                            }

                        }
                        else if (((string[])Session["auto"]).Contains("supplierNeighbourhood"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                            {
                                command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierSubdistrict"))
                        {
                            if (SupplierSubdistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", SupplierSubdistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierSubdistrict"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierDistrict"))
                        {
                            if (SupplierDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", SupplierDistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierDistrict"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierRuralDistrict"))
                        {
                            if (SupplierRuralDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", SupplierRuralDistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierRuralDistrict"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                            {
                                command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierProvince"))
                        {
                            if (SupplierProvinceCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", SupplierProvinceInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierProvince"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                            {
                                command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierZipcode"))
                        {
                            if (SupplierZipcodeCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", SupplierZipcodeInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierZipcode"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                            {
                                command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierContactName"))
                        {
                            if (SupplierContactNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", SupplierContactNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierContactName"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                            {
                                command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierContactPhone"))
                        {
                            if (SupplierContactPhoneCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", SupplierContactPhoneInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierContactPhone"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                            {
                                command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("supplierContactEmail"))
                        {
                            if (SupplierContactEmailCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", SupplierContactEmailInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("supplierContactEmail"))
                        {
                            if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                            {
                                command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("store"))
                        {
                            if (string.IsNullOrEmpty(StoreChoice.SelectedValue))
                            {
                                command.Parameters.AddWithValue("@storeId", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                            }

                        }
                        else if (((string[])Session["auto"]).Contains("store"))
                        {
                            if (String.IsNullOrEmpty(store["storeId"]))
                            {
                                command.Parameters.AddWithValue("@storeId", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeId", int.Parse(store["storeId"]));
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeName"))
                        {
                            if (StoreNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeName"]))
                                {
                                    command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeName", store["storeName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeName", StoreNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeName"))
                        {
                            if (String.IsNullOrEmpty(store["storeName"]))
                            {
                                command.Parameters.AddWithValue("@storeName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeName", store["storeName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeBuildingName"))
                        {
                            if (StoreBuildingNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeBuildingName"]))
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", StoreBuildingNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeBuildingName"))
                        {
                            if (String.IsNullOrEmpty(store["storeBuildingName"]))
                            {
                                command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeStreetName"))
                        {
                            if (StoreStreetNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeStreetName"]))
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", StoreStreetNameInput.Text?.Trim());
                                }

                            }

                        }
                        else if (((string[])Session["auto"]).Contains("storeStreetName"))
                        {
                            if (String.IsNullOrEmpty(store["storeStreetName"]))
                            {
                                command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                            }

                        }

                        if (((string[])Session["add"]).Contains("storeNeighbourhood"))
                        {
                            if (StoreNeighbourhoodCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", StoreNeighbourhoodInput.Text?.Trim());
                                }
                            }

                        }
                        else if (((string[])Session["auto"]).Contains("storeNeighbourhood"))
                        {
                            if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                            {
                                command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeSubdistrict"))
                        {
                            if (StoreSubdistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", StoreSubdistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeSubdistrict"))
                        {
                            if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeDistrict"))
                        {
                            if (StoreDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", StoreDistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeDistrict"))
                        {
                            if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeRuralDistrict"))
                        {
                            if (StoreRuralDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", StoreRuralDistrictInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeRuralDistrict"))
                        {
                            if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeProvince"))
                        {
                            if (StoreProvinceCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeProvince"]))
                                {
                                    command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeProvince", StoreProvinceInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeProvince"))
                        {
                            if (String.IsNullOrEmpty(store["storeProvince"]))
                            {
                                command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeZipcode"))
                        {
                            if (StoreZipcodeCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeZipcode"]))
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", StoreZipcodeInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeZipcode"))
                        {
                            if (String.IsNullOrEmpty(store["storeZipcode"]))
                            {
                                command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeContactName"))
                        {
                            if (StoreContactNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeContactName"]))
                                {
                                    command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactName", StoreContactNameInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeContactName"))
                        {
                            if (String.IsNullOrEmpty(store["storeContactName"]))
                            {
                                command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeContactPhone"))
                        {
                            if (StoreContactPhoneCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeContactPhone"]))
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", StoreContactPhoneInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeContactPhone"))
                        {
                            if (String.IsNullOrEmpty(store["storeContactPhone"]))
                            {
                                command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("storeContactEmail"))
                        {
                            if (StoreContactEmailCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeContactEmail"]))
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", StoreContactEmailInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("storeContactEmail"))
                        {
                            if (String.IsNullOrEmpty(store["storeContactEmail"]))
                            {
                                command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("purchaseNo"))
                        {
                            if (PurchaseNoCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@purchaseNo", PurchaseNoInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("purchaseNo"))
                        {
                            command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                        }

                        if (((string[])Session["add"]).Contains("purchaseDate"))
                        {
                            if (ApprovalDateCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(PurchaseDateInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@purchaseDate", PurchaseDateInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("purchaseDate"))
                        {
                            command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        }

                        //if (((string[])Session["add"]).Contains("deliveryNo"))
                        //{
                        //    if (DeliveryNoCheckBox.Checked)
                        //    {
                        //        command.Parameters.AddWithValue("@deliveryNo", deliveryNo);
                        //    }
                        //    else
                        //    {
                        //        if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()))
                        //        {
                        //            command.Parameters.AddWithValue("@deliveryNo", DBNull.Value);
                        //        }
                        //        else
                        //        {
                        //            command.Parameters.AddWithValue("@deliveryNo", DeliveryNoInput.Text?.Trim());
                        //        }
                        //    }
                        //}
                        //else if (((string[])Session["auto"]).Contains("deliveryNo"))
                        //{
                        //    command.Parameters.AddWithValue("@deliveryNo", deliveryNo);
                        //}

                        //if (((string[])Session["add"]).Contains("deliveryDate"))
                        //{
                        //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
                        //    {
                        //        command.Parameters.AddWithValue("@deliveryDate", DBNull.Value);
                        //    }
                        //    else
                        //    {
                        //        command.Parameters.AddWithValue("@deliveryDate", DeliveryDateInput.Text?.Trim());
                        //    }
                        //}

                        if (((string[])Session["add"]).Contains("totalQty"))
                        {
                            if (TotalQtyCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@totalQty", totalQty);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@totalQty", 0);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@totalQty", TotalQtyInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("totalQty"))
                        {
                            command.Parameters.AddWithValue("@totalQty", totalQty);
                        }

                        if (((string[])Session["add"]).Contains("totalPrice"))
                        {
                            if (TotalPriceCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@totalPrice", totalPrice);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@totalPrice", 0);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@totalPrice", TotalPriceInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("totalPrice"))
                        {
                            command.Parameters.AddWithValue("@totalPrice", totalPrice);
                        }

                        if (((string[])Session["add"]).Contains("approvalBy"))
                        {
                            if (ApprovalDateCheckBox.Checked)
                            {
                                if (PurchaseStatusChoice.SelectedValue == "pending")
                                {
                                    command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                                }
                            }
                            else
                            {
                                if (PurchaseStatusChoice.SelectedValue == "pending")
                                {
                                    command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@approvalBy", ApprovalByInput.Text?.Trim());
                                    }
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("approvalBy"))
                        {
                            if (PurchaseStatusChoice.SelectedValue == "pending")
                            {
                                command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                            }
                        }

                        if (((string[])Session["add"]).Contains("approvalDate"))
                        {
                            if (ApprovalDateCheckBox.Checked)
                            {
                                if (PurchaseStatusChoice.SelectedValue == "pending")
                                {
                                    command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                }
                            }
                            else
                            {
                                if (PurchaseStatusChoice.SelectedValue == "pending")
                                {
                                    command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@approvalDate", ApprovalDateInput.Text?.Trim());
                                    }
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("approvalDate"))
                        {
                            if (PurchaseStatusChoice.SelectedValue == "pending")
                            {
                                command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            }
                        }

                        if (((string[])Session["add"]).Contains("requestedBy"))
                        {
                            if (RequestedByCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@requestedBy", Session["username"]);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@requestedBy", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@requestedBy", RequestedByInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("requestedBy"))
                        {
                            command.Parameters.AddWithValue("@requestedBy", Session["username"]);
                        }

                        if (((string[])Session["add"]).Contains("requestedDate"))
                        {
                            if (RequestedDateCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@requestedDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@requestedDate", RequestedDateInput.Text?.Trim());
                                }
                            }
                        }
                        else if (((string[])Session["auto"]).Contains("requestedDate"))
                        {
                            command.Parameters.AddWithValue("@requestedDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        }

                        if (((string[])Session["add"]).Contains("purchaseStatus"))
                        {
                            command.Parameters.AddWithValue("@purchaseStatus", PurchaseStatusChoice.SelectedValue?.Trim());
                        }
                        else if (((string[])Session["auto"]).Contains("purchaseStatus"))
                        {
                            command.Parameters.AddWithValue("@purchaseStatus", "pending");
                        }

                        if (((string[])Session["add"]).Contains("purchaseNotes"))
                        {
                            if (String.IsNullOrEmpty(PurchaseNotesInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@purchaseNotes", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@purchaseNotes", PurchaseNotesInput.Text?.Trim());
                            }
                        }

                        command.Transaction = transaction;
                        result = command.ExecuteScalar();
                        int purchaseId = 0;
                        if (result != null)
                        {
                            purchaseId = int.Parse(result.ToString());
                        }

                        if (inserts.Rows.Count > 0)
                        {
                            command = conn.CreateCommand();
                            command.CommandText = @"CREATE TABLE #temp_insert (item_detail_id INT, item_json VARCHAR(MAX), item_qty INT, 
                                item_price DECIMAL(13,2));";
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();

                            SqlBulkCopy bulkinsert = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                            bulkinsert.DestinationTableName = "#temp_insert";

                            bulkinsert.ColumnMappings.Add("item_detail_id", "item_detail_id");
                            bulkinsert.ColumnMappings.Add("item_json", "item_json");
                            bulkinsert.ColumnMappings.Add("item_qty", "item_qty");
                            bulkinsert.ColumnMappings.Add("item_price", "item_price");
                            bulkinsert.WriteToServer(inserts);

                            command = conn.CreateCommand();
                            command.CommandText = @"INSERT INTO purchase_order_items (purchase_order_id, item_detail_id, 
                                item_json, item_qty, item_price, created_date, created_by, modified_date, modified_by)
                                SELECT @purchaseId, temp.item_detail_id, temp.item_json, temp.item_qty, 
                                temp.item_price, getdate(), @user, getdate(), @user 
                                FROM #temp_insert temp;
                                DROP TABLE #temp_insert;";
                            command.Parameters.AddWithValue("@purchaseId", purchaseId);
                            command.Parameters.AddWithValue("@user", Session["username"].ToString());
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();

                        inserts.Rows.Clear();
                        ViewState["inserts"] = inserts;
                        System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                        items.Rows.Clear();
                        ViewState["items"] = items;
                        ItemGridView.DataSource = items;
                        ItemGridView.DataBind();
                        FillAutoStoreInput();
                        FillAutoSupplierInput();
                        FillAutoPurchaseInput();

                        AlertLabel.Text = "Data berhasil disimpan";
                        AlertLabel.CssClass = "alert alert-light-primary color-primary d-block";
                    }
                    catch (Exception error)
                    {
                        transaction.Rollback();

                        AlertLabel.Text = error.Message.ToString();
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                    conn.Close();
                }
                else
                {
                    AlertLabel.Text = String.Join("<br>", errors.ToArray());
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }

            if (action == "update")
            {
                if (!IsLocked())
                {
                    if (isEditableOrDeletable() || Session["role_name"].ToString() == "superuser")
                    {
                        List<string> errors = new List<string>();

                        if (((string[])Session["edit"]).Contains("purchaseNo"))
                        {
                            if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()) && !PurchaseNoCheckBox.Checked)
                            {
                                errors.Add("No pesanan tidak boleh kosong");
                            }
                        }

                        //if (((string[])Session["edit"]).Contains("deliveryNo"))
                        //{
                        //    if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()) && !DeliveryNoCheckBox.Checked)
                        //    {
                        //        errors.Add("No pengiriman tidak boleh kosong");
                        //    }
                        //}

                        //if (((string[])Session["edit"]).Contains("deliveryDate"))
                        //{
                        //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
                        //    {
                        //        errors.Add("Tanggal pengiriman tidak boleh kosong");
                        //    }
                        //}

                        if (((string[])Session["edit"]).Contains("totalQty"))
                        {
                            if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()) && !TotalQtyCheckBox.Checked)
                            {
                                errors.Add("Total kuantitas barang tidak boleh kosong");
                            }
                        }

                        if (((string[])Session["edit"]).Contains("totalPrice"))
                        {
                            if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()) && !TotalPriceCheckBox.Checked)
                            {
                                errors.Add("Total harga barang tidak boleh kosong");
                            }

                        }

                        if (((string[])Session["edit"]).Contains("approvalBy"))
                        {
                            if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                                    && String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()) && !ApprovalByCheckBox.Checked)
                            {
                                errors.Add("Ditolak/Disetujui oleh tidak boleh kosong");
                            }
                        }


                        if (((string[])Session["edit"]).Contains("approvalDate"))
                        {
                            if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                                    && String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()) && !ApprovalDateCheckBox.Checked)
                            {
                                errors.Add("Tanggal ditolak/disetujui oleh tidak boleh kosong");
                            }
                        }


                        if (((string[])Session["edit"]).Contains("requestedBy"))
                        {
                            if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()) && !RequestedByCheckBox.Checked)
                            {
                                errors.Add("Dipesan oleh tidak boleh kosong");
                            }
                        }

                        if (((string[])Session["edit"]).Contains("requestedDate"))
                        {
                            if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()) && !RequestedDateCheckBox.Checked)
                            {
                                errors.Add("Tanggal dipesan oleh tidak boleh kosong");
                            }
                        }

                        System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
                        System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
                        System.Data.DataTable deletes = (System.Data.DataTable)ViewState["deletes"];
                        System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                        if (inserts.Rows.Count <= 0 && updates.Rows.Count <= 0)
                        {
                            errors.Add("Barang yang akan dibeli tidak boleh kosong");
                        }

                        if (errors.Count <= 0)
                        {
                            int totalQty = 0;
                            double totalPrice = 0;

                            foreach (DataRow row in inserts.Rows)
                            {
                                totalQty += int.Parse(row["item_qty"].ToString());
                                totalPrice += double.Parse(row["item_price"].ToString()) * int.Parse(row["item_qty"].ToString());
                            }

                            foreach (DataRow row in updates.Rows)
                            {
                                totalQty += int.Parse(row["item_qty"].ToString());
                                totalPrice += double.Parse(row["item_price"].ToString()) * int.Parse(row["item_qty"].ToString());
                            }

                            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

                            SqlConnection conn = new SqlConnection(constr);
                            SqlTransaction transaction;
                            conn.Open();
                            transaction = conn.BeginTransaction();

                            try
                            {
                                string popattern = "PO" + DateTime.Now.ToString("yyyyMMdd");
                                string purchaseNo = popattern + "1".ToString().PadLeft(3, '0');

                                SqlCommand command = conn.CreateCommand();
                                command.CommandText = @"SELECT TOP 1 CAST(RIGHT([purchase_no], 3) AS INT) FROM purchase_orders WHERE 
                                    purchase_no LIKE @purchaseNo ORDER BY purchase_no DESC";
                                command.Parameters.AddWithValue("@purchaseNo", string.Format("{0}%", popattern));
                                command.Transaction = transaction;
                                int number = 1;

                                object result = command.ExecuteScalar();
                                if (result != null)
                                {
                                    number = int.Parse(result.ToString()) + 1;
                                }

                                purchaseNo = popattern + number.ToString().PadLeft(3, '0');


                                string sql = "";
                                bool withComma = false;

                                if (((string[])Session["edit"]).Contains("supplierName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_name = @supplierName");
                                }


                                if (((string[])Session["edit"]).Contains("supplierBuildingName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_building_name = @supplierBuildingName");
                                }

                                if (((string[])Session["edit"]).Contains("supplierStreetName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_street_name = @supplierStreetName");
                                }

                                if (((string[])Session["edit"]).Contains("supplierNeighbourhood"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_neighbourhood = @supplierNeighbourhood");

                                }

                                if (((string[])Session["edit"]).Contains("supplierSubdistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_subdistrict = @supplierSubdistrict");

                                }

                                if (((string[])Session["edit"]).Contains("supplierDistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_district = @supplierDistrict");

                                }

                                if (((string[])Session["edit"]).Contains("supplierRuralDistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_rural_district = @supplierRuralDistrict");

                                }

                                if (((string[])Session["edit"]).Contains("supplierProvince"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_province = @supplierProvince");

                                }

                                if (((string[])Session["edit"]).Contains("supplierZipcode"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_zipcode = @supplierZipcode");
                                }

                                if (((string[])Session["edit"]).Contains("supplierContactName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_contact_name = @supplierContactName");
                                }

                                if (((string[])Session["edit"]).Contains("supplierContactPhone"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_contact_phone = @supplierContactPhone");

                                }

                                if (((string[])Session["edit"]).Contains("supplierContactEmail"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "supplier_contact_email = @supplierContactEmail");

                                }

                                if (((string[])Session["edit"]).Contains("storeName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_name = @storeName");

                                }

                                if (((string[])Session["edit"]).Contains("storeBuildingName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_building_name = @storeBuildingName");

                                }

                                if (((string[])Session["edit"]).Contains("storeStreetName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_street_name = @storeStreetName");

                                }

                                if (((string[])Session["edit"]).Contains("storeNeighbourhood"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_neighbourhood = @storeNeighbourhood");

                                }

                                if (((string[])Session["edit"]).Contains("storeSubdistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_subdistrict = @storeSubdistrict");

                                }

                                if (((string[])Session["edit"]).Contains("storeDistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_district = @storeDistrict");
                                }

                                if (((string[])Session["edit"]).Contains("storeRuralDistrict"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_rural_district = @storeRuralDistrict");

                                }

                                if (((string[])Session["edit"]).Contains("storeProvince"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_province = @storeProvince");

                                }

                                if (((string[])Session["edit"]).Contains("storeZipcode"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_zipcode = @storeZipcode");

                                }

                                if (((string[])Session["edit"]).Contains("storeContactName"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_contact_name = @storeContactName");

                                }

                                if (((string[])Session["edit"]).Contains("storeContactPhone"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_contact_phone = @storeContactPhone");

                                }

                                if (((string[])Session["edit"]).Contains("storeContactEmail"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "store_contact_email = @storeContactEmail");
                                }

                                if (((string[])Session["edit"]).Contains("purchaseNo"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "purchase_no = @purchaseNo");

                                }

                                if (((string[])Session["edit"]).Contains("purchaseDate"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "purchase_date = @purchaseDate");

                                }

                                //if (((string[])Session["edit"]).Contains("deliveryNo"))
                                //{
                                //    if (withComma)
                                //    {
                                //        sql = String.Concat(sql, ",");
                                //    }
                                //    else
                                //    {
                                //        withComma = true;
                                //    }
                                //    sql = String.Concat(sql, " ", "delivery_no = @deliveryNo");

                                //}

                                //if (((string[])Session["edit"]).Contains("deliveryDate"))
                                //{
                                //    if (withComma)
                                //    {
                                //        sql = String.Concat(sql, ",");
                                //    }
                                //    else
                                //    {
                                //        withComma = true;
                                //    }
                                //    sql = String.Concat(sql, " ", "delivery_date = @deliveryDate");

                                //}

                                if (((string[])Session["edit"]).Contains("totalQty"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "total_qty = @totalQty");

                                }

                                if (((string[])Session["edit"]).Contains("totalPrice"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "total_price = @totalPrice");

                                }

                                if (((string[])Session["edit"]).Contains("approvalBy"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "approval_by = @approvalBy");
                                }

                                if (((string[])Session["edit"]).Contains("approvalDate"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "approval_date = @approvalDate");
                                }

                                if (((string[])Session["edit"]).Contains("requestedBy"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "requested_by = @requestedBy");
                                }

                                if (((string[])Session["edit"]).Contains("requestedDate"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "requested_date = @requestedDate");
                                }

                                if (((string[])Session["edit"]).Contains("purchaseStatus"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "purchase_status = @purchaseStatus");
                                }

                                if (((string[])Session["edit"]).Contains("purchaseNotes"))
                                {
                                    if (withComma)
                                    {
                                        sql = String.Concat(sql, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    sql = String.Concat(sql, " ", "purchase_notes = @purchaseNotes");
                                }

                                string commandText = @"UPDATE purchase_orders SET {0}, modified_date = getdate(), modified_by = @user, locked = 1
                                WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid AND isdeleted = 0;";

                                commandText = String.Format(commandText, sql);

                                command = conn.CreateCommand();
                                command.CommandText = commandText;
                                command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                command.Parameters.AddWithValue("@user", Session["username"]);

                                if (((string[])Session["edit"]).Contains("supplierName"))
                                {
                                    if (SupplierNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierName"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierName", SupplierNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("supplierBuildingName"))
                                {
                                    if (SupplierBuildingNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierBuildingName", SupplierBuildingNameInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierStreetName"))
                                {
                                    if (SupplierStreetNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierStreetName", SupplierStreetNameInput.Text?.Trim());
                                        }

                                    }

                                }


                                if (((string[])Session["edit"]).Contains("supplierNeighbourhood"))
                                {
                                    if (SupplierNeighbourhoodCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierNeighbourhood", SupplierNeighbourhoodInput.Text?.Trim());
                                        }
                                    }

                                }


                                if (((string[])Session["edit"]).Contains("supplierSubdistrict"))
                                {
                                    if (SupplierSubdistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierSubdistrict", SupplierSubdistrictInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierDistrict"))
                                {
                                    if (SupplierDistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierDistrict", SupplierDistrictInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierRuralDistrict"))
                                {
                                    if (SupplierRuralDistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierRuralDistrict", SupplierRuralDistrictInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierProvince"))
                                {
                                    if (SupplierProvinceCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierProvince", SupplierProvinceInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierZipcode"))
                                {
                                    if (SupplierZipcodeCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierZipcode", SupplierZipcodeInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierContactName"))
                                {
                                    if (SupplierContactNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactName", SupplierContactNameInput.Text?.Trim());
                                        }
                                    }
                                }


                                if (((string[])Session["edit"]).Contains("supplierContactPhone"))
                                {
                                    if (SupplierContactPhoneCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactPhone", SupplierContactPhoneInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("supplierContactEmail"))
                                {
                                    if (SupplierContactEmailCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@supplierContactEmail", SupplierContactEmailInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeName"))
                                {
                                    if (StoreNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeName"]))
                                        {
                                            command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeName", store["storeName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeName", StoreNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeBuildingName"))
                                {
                                    if (StoreBuildingNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeBuildingName"]))
                                        {
                                            command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeBuildingName", StoreBuildingNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeStreetName"))
                                {
                                    if (StoreStreetNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeStreetName"]))
                                        {
                                            command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeStreetName", StoreStreetNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeNeighbourhood"))
                                {
                                    if (StoreNeighbourhoodCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                                        {
                                            command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeNeighbourhood", StoreNeighbourhoodInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeSubdistrict"))
                                {
                                    if (StoreSubdistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeSubdistrict", StoreSubdistrictInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeDistrict"))
                                {
                                    if (StoreDistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeDistrict", StoreDistrictInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeRuralDistrict"))
                                {
                                    if (StoreRuralDistrictCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                        {
                                            command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeRuralDistrict", StoreRuralDistrictInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeProvince"))
                                {
                                    if (StoreProvinceCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeProvince"]))
                                        {
                                            command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeProvince", StoreProvinceInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeZipcode"))
                                {
                                    if (StoreZipcodeCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeZipcode"]))
                                        {
                                            command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeZipcode", StoreZipcodeInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeContactName"))
                                {
                                    if (StoreContactNameCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeContactName"]))
                                        {
                                            command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactName", StoreContactNameInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeContactPhone"))
                                {
                                    if (StoreContactPhoneCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeContactPhone"]))
                                        {
                                            command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactPhone", StoreContactPhoneInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("storeContactEmail"))
                                {
                                    if (StoreContactEmailCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(store["storeContactEmail"]))
                                        {
                                            command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@storeContactEmail", StoreContactEmailInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("purchaseNo"))
                                {
                                    if (PurchaseNoCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(purchase["purchaseNo"]))
                                        {
                                            command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@purchaseNo", purchase["purchaseNo"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()))
                                        {
                                            if (String.IsNullOrEmpty(purchase["purchaseNo"]))
                                            {
                                                command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@purchaseNo", purchase["purchaseNo"]);
                                            }
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@purchaseNo", PurchaseNoInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("purchaseDate"))
                                {
                                    if (ApprovalDateCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(purchase["purchaseDate"]))
                                        {
                                            command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@purchaseDate", purchase["purchaseDate"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(PurchaseDateInput.Text?.Trim()))
                                        {
                                            if (String.IsNullOrEmpty(purchase["purchaseDate"]))
                                            {
                                                command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@purchaseDate", purchase["purchaseDate"]);
                                            }
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@purchaseDate", PurchaseDateInput.Text?.Trim());
                                        }
                                    }
                                }

                                //if (((string[])Session["edit"]).Contains("deliveryNo"))
                                //{
                                //    if (DeliveryNoCheckBox.Checked)
                                //    {
                                //        if (String.IsNullOrEmpty(purchase["deliveryNo"]))
                                //        {
                                //            command.Parameters.AddWithValue("@deliveryNo", DBNull.Value);
                                //        }
                                //        else
                                //        {
                                //            command.Parameters.AddWithValue("@deliveryNo", purchase["deliveryNo"]);
                                //        }
                                //    }
                                //    else
                                //    {
                                //        if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()))
                                //        {
                                //            command.Parameters.AddWithValue("@deliveryNo", DBNull.Value);
                                //        }
                                //        else
                                //        {
                                //            command.Parameters.AddWithValue("@deliveryNo", DeliveryNoInput.Text?.Trim());
                                //        }
                                //    }
                                //}

                                //if (((string[])Session["edit"]).Contains("deliveryDate"))
                                //{
                                //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
                                //    {
                                //        command.Parameters.AddWithValue("@deliveryDate", DBNull.Value);
                                //    }
                                //    else
                                //    {
                                //        command.Parameters.AddWithValue("@deliveryDate", DeliveryDateInput.Text?.Trim());
                                //    }
                                //}

                                if (((string[])Session["edit"]).Contains("totalQty"))
                                {
                                    if (TotalQtyCheckBox.Checked)
                                    {
                                        command.Parameters.AddWithValue("@totalQty", totalQty);
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@totalQty", 0);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@totalQty", TotalQtyInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("totalPrice"))
                                {
                                    if (TotalPriceCheckBox.Checked)
                                    {
                                        command.Parameters.AddWithValue("@totalPrice", totalPrice);
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@totalPrice", 0);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@totalPrice", TotalPriceInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("approvalBy"))
                                {
                                    if (ApprovalDateCheckBox.Checked)
                                    {
                                        if (PurchaseStatusChoice.SelectedValue == "pending" || String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                        {
                                            command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(purchase["approvalBy"]))
                                            {
                                                command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@approvalBy", purchase["approvalBy"]);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (PurchaseStatusChoice.SelectedValue == "pending" || String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                        {
                                            command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()))
                                            {
                                                command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@approvalBy", ApprovalByInput.Text?.Trim());
                                            }
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("approvalDate"))
                                {
                                    if (ApprovalDateCheckBox.Checked)
                                    {
                                        if (PurchaseStatusChoice.SelectedValue == "pending" || String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                        {
                                            command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(purchase["approvalDate"]))
                                            {
                                                command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@approvalDate", purchase["approvalDate"]);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (PurchaseStatusChoice.SelectedValue == "pending" || String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                        {
                                            command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                                        }
                                        else
                                        {
                                            if (String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()))
                                            {
                                                command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                            }
                                            else
                                            {
                                                command.Parameters.AddWithValue("@approvalDate", ApprovalDateInput.Text?.Trim());
                                            }
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("requestedBy"))
                                {
                                    if (RequestedByCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(purchase["requestedBy"]))
                                        {
                                            command.Parameters.AddWithValue("@requestedBy", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@requestedBy", purchase["requestedBy"]);
                                        }

                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@requestedBy", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@requestedBy", RequestedByInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("requestedDate"))
                                {
                                    if (RequestedDateCheckBox.Checked)
                                    {
                                        if (String.IsNullOrEmpty(purchase["requestedDate"]))
                                        {
                                            command.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@requestedDate", purchase["requestedDate"]);
                                        }
                                    }
                                    else
                                    {
                                        if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@requestedDate", RequestedDateInput.Text?.Trim());
                                        }
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("purchaseStatus"))
                                {
                                    if (String.IsNullOrEmpty(PurchaseStatusChoice.SelectedValue))
                                    {
                                        command.Parameters.AddWithValue("@purchaseStatus", "pending");
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@purchaseStatus", PurchaseStatusChoice.SelectedValue?.Trim());
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("purchaseNotes"))
                                {
                                    if (String.IsNullOrEmpty(PurchaseNotesInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@purchaseNotes", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@purchaseNotes", PurchaseNotesInput.Text?.Trim());
                                    }
                                }

                                command.Transaction = transaction;
                                command.ExecuteNonQuery();

                                if (deletes.Rows.Count > 0)
                                {
                                    command = conn.CreateCommand();
                                    command.CommandText = @"CREATE TABLE #temp_delete (purchase_order_item_id INT, purchase_order_id INT, item_detail_id INT);";
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();

                                    SqlBulkCopy bulkdelete = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                                    bulkdelete.DestinationTableName = "#temp_delete";

                                    bulkdelete.ColumnMappings.Add("purchase_order_item_id", "purchase_order_item_id");
                                    bulkdelete.ColumnMappings.Add("purchase_order_id", "purchase_order_id");
                                    bulkdelete.ColumnMappings.Add("item_detail_id", "item_detail_id");
                                    bulkdelete.WriteToServer(deletes);

                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE orig SET isdeleted = 1, modified_date = getdate(),
                                        modified_by = @user
                                        FROM purchase_order_items orig 
                                        INNER JOIN #temp_delete temp ON orig.purchase_order_item_id = temp.purchase_order_item_id AND
                                        orig.purchase_order_id = temp.purchase_order_id AND orig.item_detail_id = temp.item_detail_id;
                                        DROP TABLE #temp_delete;";
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();
                                }

                                if (updates.Rows.Count > 0)
                                {
                                    command = conn.CreateCommand();
                                    command.CommandText = @"CREATE TABLE #temp_update (purchase_order_item_id INT, purchase_order_id INT, item_detail_id INT,
                                    item_json VARCHAR(MAX), item_qty INT, item_price DECIMAL(13,2));";
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();

                                    SqlBulkCopy bulkupdate = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                                    bulkupdate.DestinationTableName = "#temp_update";

                                    bulkupdate.ColumnMappings.Add("purchase_order_item_id", "purchase_order_item_id");
                                    bulkupdate.ColumnMappings.Add("purchase_order_id", "purchase_order_id");
                                    bulkupdate.ColumnMappings.Add("item_detail_id", "item_detail_id");
                                    bulkupdate.ColumnMappings.Add("item_json", "item_json");
                                    bulkupdate.ColumnMappings.Add("item_qty", "item_qty");
                                    bulkupdate.ColumnMappings.Add("item_price", "item_price");
                                    bulkupdate.WriteToServer(updates);

                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE orig SET item_json = temp.item_json, item_qty = temp.item_qty, 
                                    item_price = temp.item_price, modified_date = getdate(), modified_by = @user
                                    FROM purchase_order_items orig 
                                    INNER JOIN #temp_update temp ON orig.purchase_order_item_id = temp.purchase_order_item_id AND
                                    orig.purchase_order_id = temp.purchase_order_id AND orig.item_detail_id = temp.item_detail_id
                                    WHERE orig.isdeleted = 0;
                                    DROP TABLE #temp_update;";
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();
                                }

                                if (inserts.Rows.Count > 0)
                                {

                                    command = conn.CreateCommand();
                                    command.CommandText = @"CREATE TABLE #temp_insert (item_detail_id INT, item_json VARCHAR(MAX), item_qty INT, item_price DECIMAL(13,2));";
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();

                                    SqlBulkCopy bulkinsert = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                                    bulkinsert.DestinationTableName = "#temp_insert";

                                    bulkinsert.ColumnMappings.Add("item_detail_id", "item_detail_id");
                                    bulkinsert.ColumnMappings.Add("item_json", "item_json");
                                    bulkinsert.ColumnMappings.Add("item_qty", "item_qty");
                                    bulkinsert.ColumnMappings.Add("item_price", "item_price");
                                    bulkinsert.WriteToServer(inserts);

                                    command = conn.CreateCommand();
                                    command.CommandText = @"INSERT INTO purchase_order_items (purchase_order_id, item_detail_id, 
                                    item_json, item_qty, item_price, created_date, created_by, modified_date, modified_by)
                                    SELECT @purchaseId, temp.item_detail_id, temp.item_json, temp.item_qty, 
                                    temp.item_price, getdate(), @user, getdate(), @user 
                                    FROM #temp_insert temp;
                                    DROP TABLE #temp_insert;";
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();
                                }

                                if (Session["role_name"].ToString() == "superuser")
                                {
                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                        FROM goods_receipt_items A INNER JOIN goods_receipts B 
                                        ON A.goods_receipt_id = B.goods_receipt_id AND B.isdeleted = 0
                                        INNER JOIN purchase_orders C ON C.purchase_order_id = B.purchase_order_id AND C.isdeleted = 0
                                        WHERE A.isdeleted = 0 AND C.purchase_order_id = @purchaseId AND C.uuid = @purchaseUuid AND
                                        A.item_detail_id NOT IN (SELECT K.item_detail_id
                                        FROM goods_receipt_items K INNER JOIN goods_receipts L 
                                        ON K.goods_receipt_id = L.goods_receipt_id AND L.isdeleted = 0
                                        INNER JOIN purchase_orders M ON M.purchase_order_id = L.purchase_order_id AND M.isdeleted = 0
                                        INNER JOIN purchase_order_items N ON N.purchase_order_id = M.purchase_order_id AND N.isdeleted = 0
                                        WHERE K.isdeleted = 0 AND M.purchase_order_id = @purchaseId AND M.uuid = @purchaseUuid 
                                        AND K.item_detail_id = N.item_detail_id);";
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                    command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();

                                    command = conn.CreateCommand();
                                    command.CommandText = @"INSERT INTO goods_receipt_items (goods_receipt_id, item_detail_id, item_json,
                                        item_price, created_date, created_by, modified_date, modified_by) 
                                        SELECT C.goods_receipt_id, A.item_detail_id, A.item_json, A.item_price, getdate(), 'admin', getdate(), 'admin' 
                                        FROM purchase_order_items A INNER JOIN purchase_orders B 
                                        ON A.purchase_order_id = B.purchase_order_id AND B.isdeleted = 0
                                        INNER JOIN goods_receipts C ON C.purchase_order_id = B.purchase_order_id AND B.isdeleted = 0
                                        WHERE B.purchase_order_id = @purchaseId AND B.uuid = @purchaseUuid AND A.isdeleted = 0 AND
                                        A.item_detail_id NOT IN (SELECT K.item_detail_id
                                        FROM goods_receipt_items K INNER JOIN goods_receipts L 
                                        ON K.goods_receipt_id = L.goods_receipt_id AND L.isdeleted = 0
                                        INNER JOIN purchase_orders M ON M.purchase_order_id = L.purchase_order_id AND M.isdeleted = 0
                                        INNER JOIN purchase_order_items N ON N.purchase_order_id = M.purchase_order_id AND N.isdeleted = 0
                                        WHERE K.isdeleted = 0 AND M.purchase_order_id = @purchaseId AND M.uuid = @purchaseUuid 
                                        AND K.item_detail_id = N.item_detail_id);";
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(ViewState["id"].ToString()));
                                    command.Parameters.AddWithValue("@purchaseUuid", ViewState["uuid"].ToString());
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();
                                }

                                transaction.Commit();

                                inserts.Rows.Clear();
                                ViewState["inserts"] = inserts;
                                updates.Rows.Clear();
                                ViewState["updates"] = updates;
                                deletes.Rows.Clear();
                                ViewState["updates"] = deletes;
                                items.Rows.Clear();
                                ViewState["items"] = items;

                                LoadForm();
                                FillAutoSupplierInput();
                                FillAutoStoreInput();
                                FillAutoPurchaseInput();
                                BindItemGridview();

                                AlertLabel.Text = "Data berhasil disimpan";
                                AlertLabel.CssClass = "alert alert-light-primary color-primary d-block";
                            }
                            catch (Exception error)
                            {
                                transaction.Rollback();

                                AlertLabel.Text = error.Message.ToString();
                                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                            conn.Close();
                        }
                        else
                        {
                            AlertLabel.Text = String.Join("<br>", errors.ToArray());
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen pembelian barang tidak bisa diubah/dihapus";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Dokumen pembelian terkunci";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }

            }
        }
        protected void BackButton_Click(object sender, EventArgs e)
        {
            Session["redirect"] = true;
            Response.Redirect("PurchaseOrder.aspx");
        }

        protected void ItemGridView_RowCancelingEdit(Object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void ItemGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void SupplierNameCheckBoxCheckedChanged()
        {
            if (SupplierNameCheckBox.Checked)
            {
                SupplierNameInput.Text = supplier["supplierName"];
                SupplierNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                {
                    SupplierNameInput.Text = supplier["supplierName"];
                }

                SupplierNameInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierNameInput.Focus();
        }

        protected void SupplierBuildingNameCheckBoxCheckedChanged()
        {
            if (SupplierBuildingNameCheckBox.Checked)
            {
                SupplierBuildingNameInput.Text = supplier["supplierBuildingName"];
                SupplierBuildingNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                {
                    SupplierBuildingNameInput.Text = supplier["supplierBuildingName"];
                }
                SupplierBuildingNameInput.Attributes.Remove("disabled");
            }
        }

        protected void SupplierBuildingNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierBuildingNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierBuildingNameInput.Focus();
        }

        protected void SupplierStreetNameCheckBoxCheckedChanged()
        {
            if (SupplierStreetNameCheckBox.Checked)
            {
                SupplierStreetNameInput.Text = supplier["supplierStreetName"];
                SupplierStreetNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                {
                    SupplierStreetNameInput.Text = supplier["supplierStreetName"];
                }
                SupplierStreetNameInput.Attributes.Remove("disabled");
            }
        }

        protected void SupplierStreetNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierStreetNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierStreetNameInput.Focus();
        }

        protected void SupplierNeighbourhoodCheckBoxCheckedChanged()
        {
            if (SupplierNeighbourhoodCheckBox.Checked)
            {
                SupplierNeighbourhoodInput.Text = supplier["supplierNeighbourhood"];
                SupplierNeighbourhoodInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                {
                    SupplierNeighbourhoodInput.Text = supplier["supplierNeighbourhood"];
                }
                SupplierNeighbourhoodInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierNeighbourhoodCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierNeighbourhoodCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierNeighbourhoodInput.Focus();
        }

        protected void SupplierSubdistrictCheckBoxCheckedChanged()
        {
            if (SupplierSubdistrictCheckBox.Checked)
            {
                SupplierSubdistrictInput.Text = supplier["supplierSubdistrict"];
                SupplierSubdistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                {
                    SupplierSubdistrictInput.Text = supplier["supplierSubdistrict"];
                }
                SupplierSubdistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierSubdistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierSubdistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierSubdistrictInput.Focus();
        }

        protected void SupplierDistrictCheckBoxCheckedChanged()
        {
            if (SupplierDistrictCheckBox.Checked)
            {
                SupplierDistrictInput.Text = supplier["supplierDistrict"];
                SupplierDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                {
                    SupplierDistrictInput.Text = supplier["supplierDistrict"];
                }
                SupplierDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierDistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierDistrictInput.Focus();
        }

        protected void SupplierRuralDistrictCheckBoxCheckedChanged()
        {
            if (SupplierRuralDistrictCheckBox.Checked)
            {
                SupplierRuralDistrictInput.Text = supplier["supplierRuralDistrict"];
                SupplierRuralDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                {
                    SupplierRuralDistrictInput.Text = supplier["supplierRuralDistrict"];
                }
                SupplierRuralDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierRuralDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierRuralDistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierRuralDistrictInput.Focus();
        }

        protected void SupplierProvinceCheckBoxCheckedChanged()
        {
            if (SupplierProvinceCheckBox.Checked)
            {
                SupplierProvinceInput.Text = supplier["supplierProvince"];
                SupplierProvinceInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                {
                    SupplierProvinceInput.Text = supplier["supplierProvince"];
                }
                SupplierProvinceInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierProvinceCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierProvinceCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierProvinceInput.Focus();
        }

        protected void SupplierZipcodeCheckBoxCheckedChanged()
        {
            if (SupplierZipcodeCheckBox.Checked)
            {
                SupplierZipcodeInput.Text = supplier["supplierZipcode"];
                SupplierZipcodeInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                {
                    SupplierZipcodeInput.Text = supplier["supplierZipcode"];
                }
                SupplierZipcodeInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierZipcodeCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierZipcodeCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierZipcodeInput.Focus();
        }

        protected void SupplierContactNameCheckBoxCheckedChanged()
        {
            if (SupplierContactNameCheckBox.Checked)
            {
                SupplierContactNameInput.Text = supplier["supplierContactName"];
                SupplierContactNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                {
                    SupplierContactNameInput.Text = supplier["supplierContactName"];
                }
                SupplierContactNameInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierContactNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierContactNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierContactNameInput.Focus();
        }

        protected void SupplierContactPhoneCheckBoxCheckedChanged()
        {
            if (SupplierContactPhoneCheckBox.Checked)
            {
                SupplierContactPhoneInput.Text = supplier["supplierContactPhone"];
                SupplierContactPhoneInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                {
                    SupplierContactPhoneInput.Text = supplier["supplierContactPhone"];
                }

                SupplierContactPhoneInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierContactPhoneCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierContactPhoneCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierContactPhoneInput.Focus();
        }

        protected void SupplierContactEmailCheckBoxCheckedChanged()
        {
            if (SupplierContactEmailCheckBox.Checked)
            {
                SupplierContactEmailInput.Text = supplier["supplierContactEmail"];
                SupplierContactEmailInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                {
                    SupplierContactEmailInput.Text = supplier["supplierContactEmail"];
                }
                SupplierContactEmailInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierContactEmailCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierContactEmailCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierContactEmailInput.Focus();
        }

        protected void StoreNameCheckBoxCheckedChanged()
        {
            if (StoreNameCheckBox.Checked)
            {
                StoreNameInput.Text = store["storeName"];
                StoreNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                {
                    StoreNameInput.Text = store["storeName"];
                }

                StoreNameInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreNameInput.Focus();
        }

        protected void StoreBuildingNameCheckBoxCheckedChanged()
        {
            if (StoreBuildingNameCheckBox.Checked)
            {
                StoreBuildingNameInput.Text = store["storeBuildingName"];
                StoreBuildingNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                {
                    StoreBuildingNameInput.Text = store["storeBuildingName"];
                }
                StoreBuildingNameInput.Attributes.Remove("disabled");
            }
        }

        protected void StoreBuildingNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreBuildingNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreBuildingNameInput.Focus();
        }

        protected void StoreStreetNameCheckBoxCheckedChanged()
        {
            if (StoreStreetNameCheckBox.Checked)
            {
                StoreStreetNameInput.Text = store["storeStreetName"];
                StoreStreetNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                {
                    StoreStreetNameInput.Text = store["storeStreetName"];
                }
                StoreStreetNameInput.Attributes.Remove("disabled");
            }
        }

        protected void StoreStreetNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreStreetNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreStreetNameInput.Focus();
        }

        protected void StoreNeighbourhoodCheckBoxCheckedChanged()
        {
            if (StoreNeighbourhoodCheckBox.Checked)
            {
                StoreNeighbourhoodInput.Text = store["storeNeighbourhood"];
                StoreNeighbourhoodInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                {
                    StoreNeighbourhoodInput.Text = store["storeNeighbourhood"];
                }
                StoreNeighbourhoodInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreNeighbourhoodCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreNeighbourhoodCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreNeighbourhoodInput.Focus();
        }

        protected void StoreSubdistrictCheckBoxCheckedChanged()
        {
            if (StoreSubdistrictCheckBox.Checked)
            {
                StoreSubdistrictInput.Text = store["storeSubdistrict"];
                StoreSubdistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                {
                    StoreSubdistrictInput.Text = store["storeSubdistrict"];
                }
                StoreSubdistrictInput.Attributes.Remove("disabled");
            }
        }

        protected void StoreSubdistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreSubdistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreSubdistrictInput.Focus();
        }

        protected void StoreDistrictCheckBoxCheckedChanged()
        {
            if (StoreDistrictCheckBox.Checked)
            {
                StoreDistrictInput.Text = store["storeDistrict"];
                StoreDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                {
                    StoreDistrictInput.Text = store["storeDistrict"];
                }
                StoreDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreDistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreDistrictInput.Focus();
        }

        protected void StoreRuralDistrictCheckBoxCheckedChanged()
        {
            if (StoreRuralDistrictCheckBox.Checked)
            {
                StoreRuralDistrictInput.Text = store["storeRuralDistrict"];
                StoreRuralDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                {
                    StoreRuralDistrictInput.Text = store["storeRuralDistrict"];
                }
                StoreRuralDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreRuralDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreRuralDistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreRuralDistrictInput.Focus();
        }

        protected void StoreProvinceCheckBoxCheckedChanged()
        {
            if (StoreProvinceCheckBox.Checked)
            {
                StoreProvinceInput.Text = store["storeProvince"];
                StoreProvinceInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                {
                    StoreProvinceInput.Text = store["storeProvince"];
                }
                StoreProvinceInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreProvinceCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreProvinceCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreProvinceInput.Focus();
        }

        protected void StoreZipcodeCheckBoxCheckedChanged()
        {
            if (StoreZipcodeCheckBox.Checked)
            {
                StoreZipcodeInput.Text = store["storeZipcode"];
                StoreZipcodeInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                {
                    StoreZipcodeInput.Text = store["storeZipcode"];
                }
                StoreZipcodeInput.Attributes.Remove("disabled");
            }
        }

        protected void StoreZipcodeCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreZipcodeCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreZipcodeInput.Focus();
        }

        protected void StoreContactNameCheckBoxCheckedChanged()
        {
            if (StoreContactNameCheckBox.Checked)
            {
                StoreContactNameInput.Text = store["storeContactName"];
                StoreContactNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                {
                    StoreContactNameInput.Text = store["storeContactName"];
                }
                StoreContactNameInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreContactNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreContactNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreContactNameInput.Focus();
        }

        protected void StoreContactPhoneCheckBoxCheckedChanged()
        {
            if (StoreContactPhoneCheckBox.Checked)
            {
                StoreContactPhoneInput.Text = store["storeContactPhone"];
                StoreContactPhoneInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                {
                    StoreContactPhoneInput.Text = store["storeContactPhone"];
                }

                StoreContactPhoneInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreContactPhoneCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreContactPhoneCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreContactPhoneInput.Focus();
        }

        protected void StoreContactEmailCheckBoxCheckedChanged()
        {
            if (StoreContactEmailCheckBox.Checked)
            {
                StoreContactEmailInput.Text = store["storeContactEmail"];
                StoreContactEmailInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                {
                    StoreContactEmailInput.Text = store["storeContactEmail"];
                }
                StoreContactEmailInput.Attributes.Remove("disabled");
            }
        }
        
        protected void StoreContactEmailCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreContactEmailCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreContactEmailInput.Focus();
        }

        protected void PurchaseNoCheckBoxCheckedChanged()
        {
            if(action == "create")
            {
                string popattern = "PO" + DateTime.Now.ToString("yyyyMMdd");
                string purchaseNo = popattern + "1".ToString().PadLeft(3, '0');

                string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

                SqlConnection conn = new SqlConnection(constr);
                SqlCommand command = conn.CreateCommand();
                command.CommandText = @"SELECT TOP 1 CAST(RIGHT([purchase_no], 3) AS INT) FROM purchase_orders WHERE 
                                    purchase_no LIKE @purchaseNo ORDER BY purchase_no DESC";
                command.Parameters.AddWithValue("@purchaseNo", string.Format("{0}%", popattern));

                int number = 1;

                conn.Open();
                object result = command.ExecuteScalar();
                conn.Close();
                
                if (result != null)
                {
                    number = int.Parse(result.ToString()) + 1;
                }
                purchaseNo = popattern + number.ToString().PadLeft(3, '0');

                if (PurchaseNoCheckBox.Checked)
                {
                    PurchaseNoInput.Text = purchaseNo + " " + "(nilai sementara)";
                    PurchaseNoInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()))
                    {
                        PurchaseNoInput.Text = purchaseNo;
                    }
                    PurchaseNoInput.Attributes.Remove("disabled");
                }
            }

            if(action == "update")
            {
                if (PurchaseNoCheckBox.Checked)
                {
                    PurchaseNoInput.Text = purchase["purchaseNo"];
                    PurchaseNoInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()))
                    {
                        PurchaseNoInput.Text = purchase["purchaseNo"];
                    }
                    PurchaseNoInput.Attributes.Remove("disabled");
                }
            }    
        }

        protected void PurchaseNoCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            PurchaseNoCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            PurchaseNoInput.Focus();
        }

        protected void PurchaseDateCheckBoxCheckedChanged()
        {
            if (action == "create")
            {
                if (PurchaseDateCheckBox.Checked)
                {
                    PurchaseDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    PurchaseDateInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(PurchaseDateInput.Text?.Trim()))
                    {
                        PurchaseDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    }
                    PurchaseDateInput.Attributes.Remove("disabled");
                }
            }

            if (action == "update")
            {
                if (PurchaseDateCheckBox.Checked)
                {
                    PurchaseDateInput.Text = purchase["purchaseDate"];
                    PurchaseDateInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(PurchaseDateInput.Text?.Trim()))
                    {
                        PurchaseDateInput.Text = purchase["purchaseDate"];
                    }
                    PurchaseDateInput.Attributes.Remove("disabled");
                }
            }
        }

        protected void PurchaseDateCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            PurchaseDateCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            PurchaseDateInput.Focus();
        }

        
        protected void RequestedByCheckBoxCheckedChanged()
        {
            if (action == "create")
            {
                if (RequestedByCheckBox.Checked)
                {
                    RequestedByInput.Text = Session["username"].ToString();
                    RequestedByInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()))
                    {
                        RequestedByInput.Text = Session["username"].ToString();
                    }
                    RequestedByInput.Attributes.Remove("disabled");
                }
            }

            if (action == "update")
            {
                if (RequestedByCheckBox.Checked)
                {
                    RequestedByInput.Text = purchase["requestedBy"];
                    RequestedByInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()))
                    {
                        RequestedByInput.Text = purchase["requestedBy"];
                    }
                    RequestedByInput.Attributes.Remove("disabled");
                }
            }
        }

        protected void RequestedByCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            RequestedByCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            RequestedByInput.Focus();
        }

        protected void RequestedDateCheckBoxCheckedChanged()
        {
            if (action == "create")
            {
                if (RequestedDateCheckBox.Checked)
                {
                    RequestedDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    RequestedDateInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()))
                    {
                        RequestedDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    }
                    RequestedDateInput.Attributes.Remove("disabled");
                }
            }

            if (action == "update")
            {
                if (RequestedDateCheckBox.Checked)
                {
                    RequestedDateInput.Text = purchase["requestedDate"];
                    RequestedDateInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()))
                    {
                        RequestedDateInput.Text = purchase["requestedDate"];
                    }
                    RequestedDateInput.Attributes.Remove("disabled");
                }
            }
        }

        protected void RequestedDateCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            RequestedDateCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            RequestedDateInput.Focus();
        }

        protected void ApprovalByCheckBoxCheckedChanged()
        {
            if (action == "create")
            {
                if (ApprovalByCheckBox.Checked)
                {
                    if (PurchaseStatusChoice.SelectedValue == "pending")
                    {
                        ApprovalByInput.Text = DBNull.Value.ToString();
                    }
                    else
                    {
                        ApprovalByInput.Text = Session["username"].ToString();
                    }
                    ApprovalByInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()))
                    {
                        ApprovalByInput.Text = Session["username"].ToString();
                    }
                    ApprovalByInput.Attributes.Remove("disabled");
                }
            }

            if (action == "update")
            {
                if (ApprovalByCheckBox.Checked)
                {
                    if (PurchaseStatusChoice.SelectedValue == "pending")
                    {
                        ApprovalByInput.Text = DBNull.Value.ToString();
                    }
                    else
                    {
                        ApprovalByInput.Text = purchase["approvalBy"];
                    }
                    ApprovalByInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()))
                    {
                        ApprovalByInput.Text = purchase["approvalBy"];
                    }
                    ApprovalByInput.Attributes.Remove("disabled");
                }
            }
        }
        
        protected void ApprovalByCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            ApprovalByCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            ApprovalByInput.Focus();
        }

        protected void ApprovalDateCheckBoxCheckedChanged()
        {
            if (action == "create")
            {
                if (ApprovalDateCheckBox.Checked)
                {
                    if (PurchaseStatusChoice.SelectedValue == "pending")
                    {
                        ApprovalDateInput.Text = "";
                    }
                    else
                    {
                        ApprovalDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    }
                    ApprovalDateInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()))
                    {
                        ApprovalDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                    }
                    ApprovalDateInput.Attributes.Remove("disabled");
                }
            }

            if (action == "update")
            {
                if (ApprovalDateCheckBox.Checked)
                {
                    if (PurchaseStatusChoice.SelectedValue == "pending")
                    {
                        ApprovalDateInput.Text = "";
                    }
                    else
                    {
                        ApprovalDateInput.Text = purchase["approvalDate"];
                    }
                    ApprovalDateInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()))
                    {
                        ApprovalDateInput.Text = purchase["approvalDate"];
                    }
                    ApprovalDateInput.Attributes.Remove("disabled");
                }
            }
        }

        protected void ApprovalDateCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            ApprovalDateCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            ApprovalDateInput.Focus();
        }

        protected void TotalQtyCheckBoxCheckedChanged()
        {
            if (action == "create")
            {
                int total = 0;
                int qty = 0;
                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                foreach (DataRow row in inserts.Rows)
                {
                    if(!String.IsNullOrEmpty(row["item_qty"].ToString()) && int.TryParse(row["item_qty"].ToString(), out qty))
                    {
                        total += int.Parse(row["item_qty"].ToString());
                    }
                }

                TotalQtyLabel.Text = total.ToString("###,###,###,###,##0");

                if (TotalQtyCheckBox.Checked)
                {
                    TotalQtyInput.Text = total.ToString("###,###,###,###,##0");
                    TotalQtyInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    TotalQtyInput.Attributes.Remove("disabled");
                }
            }

            if (action == "update")
            {
                int total = 0;
                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                foreach (DataRow row in inserts.Rows)
                {
                    total += int.Parse(row["item_qty"].ToString());
                }

                System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

                foreach (DataRow row in updates.Rows)
                {
                    total += int.Parse(row["item_qty"].ToString());
                }

                TotalQtyLabel.Text = total.ToString("###,###,###,###,##0");

                if (TotalQtyCheckBox.Checked)
                {
                    TotalQtyInput.Text = total.ToString("###,###,###,###,##0");
                    TotalQtyInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    TotalQtyInput.Attributes.Remove("disabled");
                }
            }
        }

        protected void TotalQtyCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            TotalQtyCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            TotalQtyInput.Focus();
        }

        protected void PurchaseStatusChoiceSelectedIndexChanged()
        {
            if (action == "create")
            {
                if (PurchaseStatusChoice.SelectedValue == "pending")
                {
                    ApprovalDateInput.Text = "";
                    ApprovalByInput.Text = "";
                }
                else
                {
                    if (ApprovalByCheckBox.Checked)
                    {
                        ApprovalByInput.Text = purchase["approvalBy"];
                    }
                    if (ApprovalDateCheckBox.Checked)
                    {
                        ApprovalDateInput.Text = purchase["approvalDate"]; ;
                    }
                }
            }

            if (action == "update")
            {
                if (PurchaseStatusChoice.SelectedValue == "pending")
                {
                    ApprovalDateInput.Text = "";
                    ApprovalByInput.Text = "";
                }
                else
                {
                    if (ApprovalByCheckBox.Checked)
                    {
                        ApprovalByInput.Text = purchase["approvalBy"];
                    }
                    if (ApprovalDateCheckBox.Checked)
                    {
                        ApprovalDateInput.Text = purchase["approvalDate"];
                    }
                }
            }
        }

        protected void PurchaseStatusChoice_SelectedIndexChanged(Object sender, EventArgs args)
        {
            PurchaseStatusChoiceSelectedIndexChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            ApprovalDateInput.Focus();
        }

        protected void TotalPriceCheckBoxCheckedChanged()
        {
            if (action == "create")
            {
                double total = Math.Round(0.00, 2);
                int qty = 0;
                double price = 0;
                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                foreach (DataRow row in inserts.Rows)
                {
                    if (!String.IsNullOrEmpty(row["item_qty"].ToString()) && int.TryParse(row["item_qty"].ToString(), out qty) &&
                        !String.IsNullOrEmpty(row["item_price"].ToString()) && double.TryParse(row["item_price"].ToString(), out price))
                    {
                        total += int.Parse(row["item_qty"].ToString()) * double.Parse(row["item_price"].ToString());
                    }
                }

                total = Math.Round(total, 2);

                TotalPriceLabel.Text = total.ToString("###,###,###,###,##0.00");

                if (TotalPriceCheckBox.Checked)
                {
                    TotalPriceInput.Text = total.ToString("###,###,###,###,##0.00");
                    TotalPriceInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                    {
                        TotalPriceInput.Text = total.ToString("###,###,###,###,##0.00");
                    }
                    TotalPriceInput.Attributes.Remove("disabled");
                }
            }

            if (action == "update")
            {
                double total = Math.Round(0.00, 2);
                System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

                foreach (DataRow row in updates.Rows)
                {
                    total += int.Parse(row["item_qty"].ToString()) * double.Parse(row["item_price"].ToString());
                }

                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                foreach (DataRow row in inserts.Rows)
                {
                    total += int.Parse(row["item_qty"].ToString()) * double.Parse(row["item_price"].ToString());
                }

                total = Math.Round(total, 2);

                TotalPriceLabel.Text = total.ToString("###,###,###,###,##0.00");

                if (TotalPriceCheckBox.Checked)
                {
                    TotalPriceInput.Text = total.ToString("###,###,###,###,##0.00");
                    TotalPriceInput.Attributes.Add("disabled", "disabled");
                }
                else
                {
                    if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                    {
                        TotalPriceInput.Text = total.ToString("###,###,###,###,##0.00");
                    }
                    TotalPriceInput.Attributes.Remove("disabled");
                }
            }
        }

        protected void TotalPriceCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            TotalPriceCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            TotalPriceInput.Focus();
        }
    }
}
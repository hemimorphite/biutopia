﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace biutopia
{
    public partial class CreateGoodsReceipt : System.Web.UI.Page
    {
        protected static string action = "create";
        
        protected static Dictionary<string, string> store = new Dictionary<string, string>()
        {
            { "storeId", DBNull.Value.ToString() },
            { "storeName", DBNull.Value.ToString() },
            { "storeBuildingName", DBNull.Value.ToString() },
            { "storeStreetName", DBNull.Value.ToString() },
            { "storeNeighbourhood", DBNull.Value.ToString() },
            { "storeSubdistrict", DBNull.Value.ToString() },
            { "storeDistrict", DBNull.Value.ToString() },
            { "storeRuralDistrict", DBNull.Value.ToString() },
            { "storeProvince", DBNull.Value.ToString() },
            { "storeZipcode", DBNull.Value.ToString() },
            { "storeContactName", DBNull.Value.ToString() },
            { "storeContactPhone", DBNull.Value.ToString() },
            { "storeContactEmail", DBNull.Value.ToString() }
        };

        protected static Dictionary<string, string> supplier = new Dictionary<string, string>()
        {
            { "supplierId", DBNull.Value.ToString() },
            { "supplierName", DBNull.Value.ToString() },
            { "supplierBuildingName", DBNull.Value.ToString() },
            { "supplierStreetName", DBNull.Value.ToString() },
            { "supplierNeighbourhood", DBNull.Value.ToString() },
            { "supplierSubdistrict", DBNull.Value.ToString() },
            { "supplierDistrict", DBNull.Value.ToString() },
            { "supplierRuralDistrict", DBNull.Value.ToString() },
            { "supplierProvince", DBNull.Value.ToString() },
            { "supplierZipcode", DBNull.Value.ToString() },
            { "supplierContactName", DBNull.Value.ToString() },
            { "supplierContactPhone", DBNull.Value.ToString() },
            { "supplierContactEmail", DBNull.Value.ToString() }
        };

        protected static Dictionary<string, string> receipt = new Dictionary<string, string>()
        {
            { "goodsReceiptId", DBNull.Value.ToString() },
            { "purchaseOrderId", DBNull.Value.ToString() },
            { "goodsReceiptNo", DBNull.Value.ToString() },
            { "invoiceNo", DBNull.Value.ToString() },
            { "deliveryDate", DBNull.Value.ToString() },
            { "totalQty", DBNull.Value.ToString() },
            { "totalPrice", DBNull.Value.ToString() },
            { "receivedDate", DBNull.Value.ToString() },
            { "receivedBy", DBNull.Value.ToString() },
            { "driver", DBNull.Value.ToString() },
            { "goodsReceiptNotes", DBNull.Value.ToString() },
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["username"]) || string.IsNullOrEmpty((string)Session["role_id"]))
            {
                Session.Clear();
                Response.Redirect("Login.aspx");
            }

            SupplierNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierBuildingNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierStreetNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierNeighbourhoodCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierSubdistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierRuralDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierProvinceCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierZipcodeCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierContactNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierContactPhoneCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierContactEmailCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreBuildingNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreStreetNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreNeighbourhoodCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreSubdistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreRuralDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreProvinceCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreZipcodeCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreContactNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreContactPhoneCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreContactEmailCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            GoodsReceiptNoCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            TotalQtyCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            TotalPriceCheckBox.InputAttributes.Add("class", "form-check-input mt-0");

            List<string> pagesList = new List<string>();
            Session["stores"] = new int[0];
            Session["suppliers"] = new int[0];
            Session["auto"] = new string[0];
            Session["add"] = new string[0];
            Session["edit"] = new string[0];
            Session["read"] = new string[0];
            Session["delete"] = false;
            Session["upload"] = false;
            Session["print"] = false;
            Session["pages"] = new string[0];

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT permissions FROM roles WHERE role_id = @roleId";
                    cmd.Parameters.AddWithValue("@roleId", Session["role_id"]);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("permissions")))
                                {
                                    Newtonsoft.Json.Linq.JObject pagepermissions = Newtonsoft.Json.Linq.JObject.Parse(reader.GetString(reader.GetOrdinal("permissions")).Trim());
                                    Newtonsoft.Json.Linq.JObject accessRights = Newtonsoft.Json.Linq.JObject.Parse(Session["access_rights"].ToString());
                                    Newtonsoft.Json.Linq.JArray jstores = (Newtonsoft.Json.Linq.JArray)accessRights["stores"];

                                    int[] stores = new int[jstores.Count];

                                    int a = 0;
                                    foreach (int store in jstores)
                                    {
                                        stores[a++] = store;
                                    }

                                    Session["stores"] = stores;

                                    Newtonsoft.Json.Linq.JArray jsuppliers = (Newtonsoft.Json.Linq.JArray)accessRights["suppliers"];

                                    int[] suppliers = new int[jsuppliers.Count];

                                    int b = 0;
                                    foreach (int supplier in jsuppliers)
                                    {
                                        suppliers[b++] = supplier;
                                    }

                                    Session["suppliers"] = suppliers;

                                    Newtonsoft.Json.Linq.JArray pages = (Newtonsoft.Json.Linq.JArray)pagepermissions["pages"];

                                    for (int i = 0; i < pages.Count; i++)
                                    {
                                        Newtonsoft.Json.Linq.JObject page = (Newtonsoft.Json.Linq.JObject)pages[i];
                                        Newtonsoft.Json.Linq.JArray permissions = (Newtonsoft.Json.Linq.JArray)page["permissions"];

                                        if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                        {
                                            pagesList.Add((string)page["name"]);
                                        }

                                        if ((string)page["name"] == "goodsReceipt")
                                        {
                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[0])["auto"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[0])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["auto"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[1])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["read"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[2])["add"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[2])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["add"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[3])["edit"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[3])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["edit"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[4])["delete"])
                                            {
                                                Session["delete"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[5])["upload"])
                                            {
                                                Session["upload"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[6])["print"])
                                            {
                                                Session["print"] = true;
                                            }
                                        }
                                    }

                                    Session["pages"] = pagesList.ToArray();
                                }
                            }
                        }
                    }
                    conn.Close();

                    if (((string[])Session["edit"]).Length == 0)
                    {
                        Response.Redirect("Forbidden.aspx");
                    }
                }
            }

            if (!IsPostBack)
            {
                action = "create";

                System.Data.DataTable items = new System.Data.DataTable("items");
                System.Data.DataTable updates = new System.Data.DataTable("updates");

                if (((string[])Session["read"]).Contains("item"))
                {
                    items.Columns.Add("no");
                    //items.Columns.Add("purchase_order_id");
                    items.Columns.Add("item_detail_id");
                    items.Columns.Add("item_barcode");
                    items.Columns.Add("supplier_product_code");
                    items.Columns.Add("item_name");
                    items.Columns.Add("short_description");
                    items.Columns.Add("color");
                    items.Columns.Add("size");
                    items.Columns.Add("variants");
                    items.Columns.Add("brand_name");
                    items.Columns.Add("item_price");
                    items.Columns.Add("item_qty");
                    items.Columns.Add("item_qty_constraint");
                }
                ViewState["items"] = items;

                if (((string[])Session["edit"]).Contains("item"))
                {
                    updates.Columns.Add("goods_receipt_item_id");
                    updates.Columns.Add("goods_receipt_id");
                    updates.Columns.Add("item_detail_id");
                    updates.Columns.Add("item_json");
                    updates.Columns.Add("item_qty");
                    updates.Columns.Add("item_price");
                    //updates.Columns.Add("modified_date");
                    //updates.Columns.Add("modified_by");
                }
                ViewState["updates"] = updates;

                Uri url = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                
                if (HttpUtility.ParseQueryString(url.Query).Get("receiptId") != null &&
                    HttpUtility.ParseQueryString(url.Query).Get("receiptUuid") != null &&
                    HttpUtility.ParseQueryString(url.Query).Get("purchaseId") != null &&
                    Session["storevalue"] != null && Session["suppliervalue"] != null &&
                    Session["origin"] != null && Session["redirect"] != null)
                {
                    int receiptId;
                    int purchaseId;
                    bool redirect;
                   
                    if (int.TryParse(HttpUtility.ParseQueryString(url.Query).Get("receiptId"), out receiptId) &&
                        int.TryParse(HttpUtility.ParseQueryString(url.Query).Get("purchaseId"), out purchaseId) &&
                        !String.IsNullOrEmpty(HttpUtility.ParseQueryString(url.Query).Get("receiptUuid")) &&
                        bool.TryParse(Session["redirect"].ToString(), out redirect))
                    {
                        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                        SqlCommand cmd = conn.CreateCommand();

                        cmd.CommandText = @"SELECT COUNT(*) FROM goods_receipts WHERE goods_receipt_id = @receiptId AND uuid = @receiptUuid 
                            AND isdeleted = 0;";

                        cmd.Parameters.AddWithValue("@receiptId", int.Parse(HttpUtility.ParseQueryString(url.Query).Get("receiptId")));
                        cmd.Parameters.AddWithValue("@receiptUuid", HttpUtility.ParseQueryString(url.Query).Get("receiptUuid").ToString().ToUpper());
                        cmd.Connection = conn;

                        conn.Open();
                        int count = 0;

                        object result = cmd.ExecuteScalar();

                        if (result != null)
                        {
                            count = int.Parse(result.ToString());
                        }
                        conn.Close();

                        if (count > 0 && redirect)
                        {
                            action = "update";
                            ViewState["id"] = HttpUtility.ParseQueryString(url.Query).Get("receiptId");
                            ViewState["purchaseid"] = HttpUtility.ParseQueryString(url.Query).Get("purchaseId");
                            ViewState["uuid"] = HttpUtility.ParseQueryString(url.Query).Get("receiptUuid").ToUpper();
                        }
                    }
                }
                
                if (action == "update")
                {
                    
                    LoadForm();
                    FillAutoSupplierInput();
                    FillAutoStoreInput();
                    FillAutoReceiptInput();
                    FillReceiptInput();
                    BindItemGridview();
                }
                
                if (action == "create")
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                    SqlCommand cmd = conn.CreateCommand();

                    string commandText = "";

                    if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                    {
                        commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers";
                    }
                    else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                    {
                        int[] suppliers = (int[])Session["suppliers"];

                        commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers 
                        WHERE supplier_id IN ({0})";

                        string inClause = string.Join(",", suppliers);
                        commandText = string.Format(commandText, inClause);
                    }

                    if (!String.IsNullOrEmpty(commandText))
                    {
                        cmd.CommandText = commandText;
                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        SupplierChoice.DataSource = ds;
                        SupplierChoice.DataTextField = "supplier_name";
                        SupplierChoice.DataValueField = "supplier_id";
                        SupplierChoice.DataBind();

                        ds.Dispose();

                        LoadSupplier();
                    }

                    if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                    {
                        commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores";
                    }
                    else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                    {
                        int[] stores = (int[])Session["stores"];
                        commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores 
                        WHERE store_id IN ({0})";

                        string inClause = string.Join(",", stores);

                        commandText = string.Format(commandText, inClause);
                    }

                    if (!String.IsNullOrEmpty(commandText))
                    {
                        cmd = conn.CreateCommand();
                        cmd.CommandText = commandText;

                        SqlDataAdapter da = new SqlDataAdapter(cmd);
                        DataSet ds = new DataSet();
                        da.Fill(ds);

                        StoreChoice.DataSource = ds;
                        StoreChoice.DataTextField = "store_name";
                        StoreChoice.DataValueField = "store_id";
                        StoreChoice.DataBind();

                        ds.Dispose();

                        LoadStore();

                    }

                    FillAutoSupplierInput();
                    FillAutoStoreInput();
                    FillAutoReceiptInput();
                }

                Session["redirect"] = null;
            }
            else
            {
                if (ViewState["items"] == null || ViewState["items"].Equals("-1") ||
                    ViewState["updates"] == null || ViewState["updates"].Equals("-1"))
                {
                    Response.Redirect("Forbidden.aspx");
                }

                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

                if (items.Columns.Count <= 0 || updates.Columns.Count <= 0)
                {
                    //AlertLabel.Text = items.Columns.Count.ToString() + " " + updates.Columns.Count.ToString();
                    Response.Redirect("Forbidden.aspx");
                }

                FillAutoSupplierInput();
                FillAutoStoreInput();
                FillAutoReceiptInput();
                

                ItemGridView.DataSource = items;
                ItemGridView.DataBind();

                AlertLabel.Text = "";
                AlertLabel.CssClass = "";
                AlertModalLabel.Text = "";
                AlertModalLabel.CssClass = "";

                if (GetPostBackControlName() == "SearchInput")
                {
                    SearchInput_TextChanged(null, EventArgs.Empty);
                }
                
            }
        }

        protected string GetPostBackControlName()
        {
            Control control = null;

            string ctrlname = Page.Request.Params["__EVENTTARGET"];

            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);

                if (control != null)
                {
                    return control.ID;
                }
                return "";
            }
            else
            {
                return "";
            }
        }

        protected void LoadStore()
        {
            int number;

            if (int.TryParse(StoreChoice.SelectedValue, out number))
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                cmd.CommandText = @"SELECT store_id
                                    ,store_name
                                    ,store_building_name
                                    ,store_street_name
                                    ,store_neighbourhood
                                    ,store_subdistrict
                                    ,store_district
                                    ,store_rural_district
                                    ,store_province
                                    ,store_zipcode
                                    FROM stores
                                    WHERE store_id = @storeId";

                cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                cmd.Connection = conn;

                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("store_id")))
                        {
                            store["storeId"] = reader["store_id"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_name")))
                        {
                            store["storeName"] = reader["store_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_building_name")))
                        {
                            store["storeBuildingName"] = reader["store_building_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_street_name")))
                        {
                            store["storeStreetName"] = reader["store_street_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_neighbourhood")))
                        {
                            store["storeNeighbourhood"] = reader["store_neighbourhood"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_subdistrict")))
                        {
                            store["storeSubdistrict"] = reader["store_subdistrict"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_district")))
                        {
                            store["storeDistrict"] = reader["store_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_rural_district")))
                        {
                            store["storeRuralDistrict"] = reader["store_rural_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_province")))
                        {
                            store["storeProvince"] = reader["store_province"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_zipcode")))
                        {
                            store["storeZipcode"] = reader["store_zipcode"].ToString();
                        }
                    }
                }
                conn.Close();
            }
        }

        protected void LoadSupplier()
        {
            int number;

            if (int.TryParse(SupplierChoice.SelectedValue, out number))
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                cmd.CommandText = @"SELECT supplier_id
                                    ,supplier_name
                                    ,supplier_building_name
                                    ,supplier_street_name
                                    ,supplier_neighbourhood
                                    ,supplier_subdistrict
                                    ,supplier_district
                                    ,supplier_rural_district
                                    ,supplier_province
                                    ,supplier_zipcode
                                    FROM suppliers
                                    WHERE supplier_id = @supplierId";

                cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                cmd.Connection = conn;

                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_id")))
                        {
                            supplier["supplierId"] = reader["supplier_id"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_name")))
                        {
                            supplier["supplierName"] = reader["supplier_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_building_name")))
                        {
                            supplier["supplierBuildingName"] = reader["supplier_building_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_street_name")))
                        {
                            supplier["supplierStreetName"] = reader["supplier_street_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_neighbourhood")))
                        {
                            supplier["supplierNeighbourhood"] = reader["supplier_neighbourhood"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_subdistrict")))
                        {
                            supplier["supplierSubdistrict"] = reader["supplier_subdistrict"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_district")))
                        {
                            supplier["supplierDistrict"] = reader["supplier_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_rural_district")))
                        {
                            supplier["supplierRuralDistrict"] = reader["supplier_rural_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_province")))
                        {
                            supplier["supplierProvince"] = reader["supplier_province"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_zipcode")))
                        {
                            supplier["supplierZipcode"] = reader["supplier_zipcode"].ToString();
                        }

                    }
                }
                conn.Close();
            }
        }

        protected void FillAutoStoreInput()
        {
            StoreNameCheckBoxCheckedChanged();
            StoreBuildingNameCheckBoxCheckedChanged();
            StoreStreetNameCheckBoxCheckedChanged();
            StoreNeighbourhoodCheckBoxCheckedChanged();
            StoreSubdistrictCheckBoxCheckedChanged();
            StoreDistrictCheckBoxCheckedChanged();
            StoreRuralDistrictCheckBoxCheckedChanged();
            StoreProvinceCheckBoxCheckedChanged();
            StoreZipcodeCheckBoxCheckedChanged();
            StoreContactNameCheckBoxCheckedChanged();
            StoreContactPhoneCheckBoxCheckedChanged();
            StoreContactEmailCheckBoxCheckedChanged();
        }

        protected void FillAutoSupplierInput()
        {
            SupplierNameCheckBoxCheckedChanged();
            SupplierBuildingNameCheckBoxCheckedChanged();
            SupplierStreetNameCheckBoxCheckedChanged();
            SupplierNeighbourhoodCheckBoxCheckedChanged();
            SupplierSubdistrictCheckBoxCheckedChanged();
            SupplierDistrictCheckBoxCheckedChanged();
            SupplierRuralDistrictCheckBoxCheckedChanged();
            SupplierProvinceCheckBoxCheckedChanged();
            SupplierZipcodeCheckBoxCheckedChanged();
            SupplierContactNameCheckBoxCheckedChanged();
            SupplierContactPhoneCheckBoxCheckedChanged();
            SupplierContactEmailCheckBoxCheckedChanged();
        }

        protected void FillAutoReceiptInput()
        {
            GoodsReceiptNoCheckBoxCheckedChanged();
            TotalQtyCheckBoxCheckedChanged();
            TotalPriceCheckBoxCheckedChanged();
        }

        protected void FillReceiptInput()
        {
            InvoiceNoInput.Text = receipt["invoiceNo"];
            DriverInput.Text = receipt["driver"];
            ReceivedByInput.Text = receipt["receivedBy"];
            ReceivedDateInput.Text = receipt["receivedDate"];
            GoodsReceiptNotesInput.Text = receipt["goodsReceiptNotes"];
        }

        protected void LoadForm()
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);

            List<string> errors = new List<string>();

            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"SELECT supplier_id
                                    ,supplier_name
                                    ,supplier_building_name
                                    ,supplier_street_name
                                    ,supplier_neighbourhood
                                    ,supplier_subdistrict
                                    ,supplier_district
                                    ,supplier_rural_district
                                    ,supplier_province
                                    ,supplier_zipcode
                                    ,supplier_contact_name
                                    ,supplier_contact_phone
                                    ,supplier_contact_email
                                    ,store_id
                                    ,store_name
                                    ,store_building_name
                                    ,store_street_name
                                    ,store_neighbourhood
                                    ,store_subdistrict
                                    ,store_district
                                    ,store_rural_district
                                    ,store_province
                                    ,store_zipcode
                                    ,store_contact_name
                                    ,store_contact_phone
                                    ,store_contact_email
                                    ,purchase_order_id
                                    ,goods_receipt_no
                                    ,invoice_no
                                    ,driver
                                    ,received_by
                                    ,received_date
                                    ,total_qty
                                    ,total_price
                                    ,goods_receipt_notes
                                    FROM goods_receipts
                                    WHERE goods_receipt_id = @receiptId AND uuid = @receiptUuid AND isdeleted = 0";

            cmd.Parameters.AddWithValue("@receiptId", int.Parse(ViewState["id"].ToString()));
            cmd.Parameters.AddWithValue("@receiptUuid", ViewState["uuid"].ToString());

            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    if (reader.IsDBNull(reader.GetOrdinal("supplier_id")))
                    {
                        supplier["supplierId"] = DBNull.Value.ToString();
                        errors.Add("Vendor tidak ditemukan");
                    }
                    else
                    {
                        supplier["supplierId"] = reader["supplier_id"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_name")))
                    {
                        supplier["supplierName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierName"] = reader["supplier_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_building_name")))
                    {
                        supplier["supplierBuildingName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierBuildingName"] = reader["supplier_building_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_street_name")))
                    {
                        supplier["supplierStreetName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierStreetName"] = reader["supplier_street_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_neighbourhood")))
                    {
                        supplier["supplierNeighbourhood"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierNeighbourhood"] = reader["supplier_neighbourhood"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_subdistrict")))
                    {
                        supplier["supplierSubdistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierSubdistrict"] = reader["supplier_subdistrict"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_district")))
                    {
                        supplier["supplierDistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierDistrict"] = reader["supplier_district"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_rural_district")))
                    {
                        supplier["supplierRuralDistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierRuralDistrict"] = reader["supplier_rural_district"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_province")))
                    {
                        supplier["supplierProvince"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierProvince"] = reader["supplier_province"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_zipcode")))
                    {
                        supplier["supplierZipcode"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierZipcode"] = reader["supplier_zipcode"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_contact_name")))
                    {
                        supplier["supplierContactName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierContactName"] = reader["supplier_contact_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_contact_phone")))
                    {
                        supplier["supplierContactPhone"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierContactPhone"] = reader["supplier_contact_phone"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("supplier_contact_email")))
                    {
                        supplier["supplierContactEmail"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        supplier["supplierContactEmail"] = reader["supplier_contact_email"].ToString();
                    }


                    if (reader.IsDBNull(reader.GetOrdinal("store_id")))
                    {
                        store["storeId"] = DBNull.Value.ToString();
                        errors.Add("Toko Ritel tidak ditemukan");
                    }
                    else
                    {
                        store["storeId"] = reader["store_id"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_name")))
                    {
                        store["storeName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeName"] = reader["store_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_building_name")))
                    {
                        store["storeBuildingName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeBuildingName"] = reader["store_building_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_street_name")))
                    {
                        store["storeStreetName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeStreetName"] = reader["store_street_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_neighbourhood")))
                    {
                        store["storeNeighbourhood"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeNeighbourhood"] = reader["store_neighbourhood"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_subdistrict")))
                    {
                        store["storeSubdistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeSubdistrict"] = reader["store_subdistrict"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_district")))
                    {
                        store["storeDistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeDistrict"] = reader["store_district"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_rural_district")))
                    {
                        store["storeRuralDistrict"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeRuralDistrict"] = reader["store_rural_district"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_province")))
                    {
                        store["storeProvince"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeProvince"] = reader["store_province"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_zipcode")))
                    {
                        store["storeZipcode"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeZipcode"] = reader["store_zipcode"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_contact_name")))
                    {
                        store["storeContactName"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeContactName"] = reader["store_contact_name"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_contact_phone")))
                    {
                        store["storeContactPhone"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeContactPhone"] = reader["store_contact_phone"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("store_contact_email")))
                    {
                        store["storeContactEmail"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        store["storeContactEmail"] = reader["store_contact_email"].ToString();
                    }


                    if (reader.IsDBNull(reader.GetOrdinal("purchase_order_id")))
                    {
                        receipt["purchaseOrderId"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        receipt["purchaseOrderId"] = reader["purchase_order_id"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("goods_receipt_no")))
                    {
                        receipt["goodsReceiptNo"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        receipt["goodsReceiptNo"] = reader["goods_receipt_no"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("invoice_no")))
                    {
                        receipt["invoiceNo"] = "";
                    }
                    else
                    {
                        receipt["invoiceNo"] = reader["invoice_no"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("driver")))
                    {
                        receipt["driver"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        receipt["driver"] = reader["driver"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("received_by")))
                    {
                        receipt["receivedBy"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        receipt["receivedBy"] = reader["received_by"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("received_date")))
                    {
                        receipt["receivedDate"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        receipt["receivedDate"] = Convert.ToDateTime(reader["received_date"]).ToString("yyyy-MM-dd");
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("total_qty")))
                    {
                        receipt["totalQty"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        receipt["totalQty"] = reader["total_qty"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("total_price")))
                    {
                        receipt["totalPrice"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        receipt["totalPrice"] = reader["total_price"].ToString();
                    }

                    if (reader.IsDBNull(reader.GetOrdinal("goods_receipt_notes")))
                    {
                        receipt["goodsReceiptNotes"] = DBNull.Value.ToString();
                    }
                    else
                    {
                        receipt["goodsReceiptNotes"] = reader["goods_receipt_notes"].ToString();
                    }
                }
            }
            conn.Close();

            cmd = conn.CreateCommand();

            cmd.CommandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers 
                        WHERE supplier_id = @supplierId";
            cmd.Parameters.AddWithValue("@supplierId", int.Parse(supplier["supplierId"].ToString()));

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            SupplierChoice.DataSource = ds;
            SupplierChoice.DataTextField = "supplier_name";
            SupplierChoice.DataValueField = "supplier_id";
            SupplierChoice.DataBind();
            ds.Clear();

            cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores 
                        WHERE store_id = @storeId";
            cmd.Parameters.AddWithValue("@storeId", int.Parse(store["storeId"].ToString()));

            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds);

            StoreChoice.DataSource = ds;
            StoreChoice.DataTextField = "store_name";
            StoreChoice.DataValueField = "store_id";
            StoreChoice.DataBind();
            ds.Clear();

            cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT purchase_order_id, delivery_no FROM purchase_orders 
                        WHERE purchase_order_id = @purchaseOrderId";
            cmd.Parameters.AddWithValue("@purchaseOrderId", int.Parse(ViewState["purchaseid"].ToString()));

            da = new SqlDataAdapter(cmd);
            ds = new DataSet();
            da.Fill(ds);

            PurchaseOrderChoice.DataSource = ds;
            PurchaseOrderChoice.DataTextField = "delivery_no";
            PurchaseOrderChoice.DataValueField = "purchase_order_id";
            PurchaseOrderChoice.DataBind();
            ds.Clear();

            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
            items.Rows.Clear();
            updates.Rows.Clear();
            ViewState["items"] = items;
            ViewState["updates"] = updates;

            cmd = conn.CreateCommand();
            cmd.CommandText = @"SELECT ROW_NUMBER() OVER(ORDER BY goods_receipt_items.goods_receipt_item_id) [no],
            goods_receipt_items.goods_receipt_item_id, goods_receipt_items.goods_receipt_id, goods_receipt_items.item_detail_id, 
            FORMAT(goods_receipt_items.item_price, '###,###,###,###,##0.00') [item_price], 
            FORMAT(goods_receipt_items.item_qty, '###,###,###,###,##0') [item_qty], 
            FORMAT(goods_receipt_items.item_qty_constraint, '###,###,###,###,##0') [item_qty_constraint],
            item_details.store_id, item_details.supplier_id, item_details.item_id,
            item_details.item_barcode, item_details.item_code, item_details.item_category_code, item_details.item_name,
            item_details.foreign_name, item_details.short_description, item_details.supplier_product_code,
            dbo.JsonValue(item_details.variants, 'color') [color], dbo.JsonValue(item_details.variants, 'size') [size],
            item_details.variants, item_details.selling_price, item_details.purchase_margin,
            item_details.effective_date, item_details.brand_name, item_details.uom, item_details.supplier_product_code
            FROM goods_receipt_items 
            INNER JOIN goods_receipts ON goods_receipts.goods_receipt_id = goods_receipt_items.goods_receipt_id
            INNER JOIN item_details ON item_details.item_detail_id = goods_receipt_items.item_detail_id
            WHERE goods_receipts.goods_receipt_id = @receiptId AND goods_receipts.uuid = @receiptUuid
            AND goods_receipts.store_id = @storeId AND goods_receipts.supplier_id = @supplierId AND goods_receipts.isdeleted = 0
            AND goods_receipt_items.isdeleted = 0";
            cmd.Parameters.AddWithValue("@receiptId", int.Parse(ViewState["id"].ToString()));
            cmd.Parameters.AddWithValue("@receiptUuid", ViewState["uuid"].ToString());
            cmd.Parameters.AddWithValue("@storeId", int.Parse(store["storeId"].ToString()));
            cmd.Parameters.AddWithValue("@supplierId", int.Parse(supplier["supplierId"].ToString()));

            conn.Open();
            reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    DataRow item = items.NewRow();
                    DataRow update = updates.NewRow();

                    if (!reader.IsDBNull(reader.GetOrdinal("no")))
                    {
                        item["no"] = int.Parse(reader["no"].ToString());
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("goods_receipt_item_id")))
                    {
                        update["goods_receipt_item_id"] = int.Parse(reader["goods_receipt_item_id"].ToString());
                    }
                    else
                    {
                        errors.Add("Terjadi kesalahan no identitas penerimaan pada barang no " + reader["no"].ToString());
                        update["goods_receipt_item_id"] = 0;
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("goods_receipt_id")))
                    {
                        //item["purchase_order_id"] = int.Parse(reader["purchase_order_id"].ToString());
                        update["goods_receipt_id"] = int.Parse(reader["goods_receipt_id"].ToString());
                    }
                    else
                    {
                        errors.Add("Terjadi kesalahan no identitas penerimaan pada barang no " + reader["no"].ToString());
                        //item["purchase_order_id"] = 0;
                        update["goods_receipt_id"] = 0;
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("item_detail_id")))
                    {
                        item["item_detail_id"] = int.Parse(reader["item_detail_id"].ToString());
                        update["item_detail_id"] = int.Parse(reader["item_detail_id"].ToString());
                    }
                    else
                    {
                        errors.Add("Terjadi kesalahan no identitas barang pada barang no " + reader["no"].ToString());
                        item["item_detail_id"] = 0;
                        update["item_detail_id"] = 0;
                    }

                    Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();

                    if (!reader.IsDBNull(reader.GetOrdinal("store_id")))
                    {
                        json["storeId"] = int.Parse(reader["store_id"].ToString());
                    }
                    else
                    {
                        json["storeId"] = 0;
                        errors.Add("Terjadi kesalahan toko pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_id")))
                    {
                        json["supplierId"] = int.Parse(reader["supplier_id"].ToString());
                    }
                    else
                    {
                        json["supplierId"] = 0;
                        errors.Add("Terjadi kesalahan pemasok pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_id")))
                    {
                        json["itemId"] = int.Parse(reader["item_id"].ToString());
                    }
                    else
                    {
                        json["itemId"] = 0;
                        errors.Add("Terjadi kesalahan no identitas pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_barcode")))
                    {
                        json["itemBarcode"] = reader["item_barcode"].ToString();
                        item["item_barcode"] = reader["item_barcode"].ToString();
                    }
                    else
                    {
                        json["itemBarcode"] = null;
                        item["item_barcode"] = "";
                        errors.Add("Terjadi kesalahan barcode pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_code")))
                    {
                        json["itemCode"] = reader["item_code"].ToString();
                    }
                    else
                    {
                        json["itemCode"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_category_code")))
                    {
                        json["itemCategoryCode"] = reader["item_category_code"].ToString();
                    }
                    else
                    {
                        json["itemCategoryCode"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("item_name")))
                    {
                        json["itemName"] = reader["item_name"].ToString();
                        item["item_name"] = reader["item_name"].ToString();
                    }
                    else
                    {
                        json["itemName"] = null;
                        item["item_name"] = "";
                        errors.Add("Terjadi kesalahan nama pada barang no " + reader["no"].ToString());
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("foreign_name")))
                    {
                        json["foreignName"] = reader["foreign_name"].ToString();
                    }
                    else
                    {
                        json["foreignName"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("short_description")))
                    {
                        json["shortDescription"] = reader["short_description"].ToString();
                        item["short_description"] = reader["short_description"].ToString();
                    }
                    else
                    {
                        json["shortDescription"] = null;
                        item["short_description"] = "";
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("variants")))
                    {
                        json["variants"] = reader["variants"].ToString();
                    }
                    else
                    {
                        json["variants"] = "{}";
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("selling_price")))
                    {
                        json["sellingPrice"] = double.Parse(reader["selling_price"].ToString());
                    }
                    else
                    {
                        json["sellingPrice"] = 0;
                        errors.Add("Terjadi kesalahan harga pada barang no " + reader["no"].ToString());
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("purchase_margin")))
                    {
                        json["purchaseMargin"] = double.Parse(reader["purchase_margin"].ToString());
                    }
                    else
                    {
                        json["purchaseMargin"] = 0;
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("effective_date")))
                    {
                        json["effectiveDate"] = Convert.ToDateTime(reader["effective_date"]).ToString("yyyy-MM-dd");
                    }
                    else
                    {
                        json["effectiveDate"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("brand_name")))
                    {
                        json["brandName"] = reader["brand_name"].ToString();
                        item["brand_name"] = reader["brand_name"].ToString();
                    }
                    else
                    {
                        json["brandName"] = null;
                        item["brand_name"] = "";
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("uom")))
                    {
                        json["unitOfMeasure"] = reader["uom"].ToString();
                    }
                    else
                    {
                        json["unitOfMeasure"] = null;
                    }
                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_product_code")))
                    {
                        json["supplierProductCode"] = reader["supplier_product_code"].ToString();
                        item["supplier_product_code"] = reader["supplier_product_code"].ToString();
                    }
                    else
                    {
                        json["supplierProductCode"] = null;
                        item["supplier_product_code"] = "";
                    }

                    update["item_json"] = json.ToString();

                    if (!reader.IsDBNull(reader.GetOrdinal("color")) && !reader.IsDBNull(reader.GetOrdinal("size")))
                    {
                        item["variants"] = String.Concat("Warna", " ", reader["color"].ToString(), ",", " ", "Ukuran", " ", reader["size"].ToString());
                        item["color"] = reader["color"].ToString();
                        item["size"] = reader["size"].ToString();
                    }
                    else if (!reader.IsDBNull(reader.GetOrdinal("color")))
                    {
                        item["variants"] = String.Concat("Warna", " ", reader["color"].ToString());
                        item["color"] = reader["color"].ToString();
                    }
                    else if (!reader.IsDBNull(reader.GetOrdinal("size")))
                    {
                        item["variants"] = String.Concat("Ukuran", " ", reader["size"].ToString());
                        item["size"] = reader["size"].ToString();
                    }
                    else
                    {
                        item["variants"] = "";
                        item["color"] = "";
                        item["size"] = "";
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("item_qty")))
                    {
                        item["item_qty"] = reader["item_qty"].ToString();
                        update["item_qty"] = int.Parse(reader["item_qty"].ToString());
                    }
                    else
                    {
                        item["item_qty"] = DBNull.Value;
                        update["item_qty"] = DBNull.Value;
                        //errors.Add("Terjadi kesalahan jumlah pada barang no " + reader["no"].ToString());
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("item_qty_constraint")))
                    {
                        item["item_qty_constraint"] = reader["item_qty_constraint"].ToString();
                        //update["item_qty"] = int.Parse(reader["item_qty"].ToString());
                    }
                    else
                    {
                        item["item_qty_constraint"] = DBNull.Value;
                        //update["item_qty"] = DBNull.Value;
                        errors.Add("Terjadi kesalahan jumlah batas pada barang no " + reader["no"].ToString());
                    }

                    if (!reader.IsDBNull(reader.GetOrdinal("item_price")))
                    {
                        item["item_price"] = reader["item_price"].ToString();
                        update["item_price"] = double.Parse(reader["item_price"].ToString());
                    }
                    else
                    {
                        item["item_price"] = 0;
                        update["item_price"] = 0;
                        errors.Add("Terjadi kesalahan harga pada barang no " + reader["no"].ToString());
                    }
                    items.Rows.Add(item);
                    updates.Rows.Add(update);
                }
            }
            conn.Close();

            ViewState["items"] = items;

            if (errors.Count <= 0)
            {
                //AlertLabel.Text = updates.Rows.Count.ToString();
                ViewState["updates"] = updates;
            }
            else
            {
                AlertLabel.Text = String.Join("<br>", errors.ToArray());
                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void BindItemGridview()
        {
            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

            ItemGridView.DataSource = items;
            ItemGridView.DataBind();
        }

        protected void PurchaseOrderChoice_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void StoreChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (action == "create")
            {
                //LoadSupplier();
                LoadStore();
                //FillAutoSupplierInput();
                FillAutoStoreInput();
                //FillPurchaseInput();

            }
        }

        protected void SupplierChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (action == "create")
            {
                LoadSupplier();
                //LoadStore();
                FillAutoSupplierInput();
                //FillAutoStoreInput();
                //FillPurchaseInput();
            }
        }

        protected void ItemBarcodeChoice_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ItemNameChoice_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ClearModal()
        {
            ItemBarcodeChoice.DataSource = null;
            ItemBarcodeChoice.DataBind();
            ItemNameChoice.DataSource = null;
            ItemNameChoice.DataBind();
            DescriptionInput.Text = "";
            ColorInput.Text = "";
            SizeInput.Text = "";
            BrandInput.Text = "";
            PriceInput.Text = "";
            QtyInput.Text = "";
            SupplierProductCodeInput.Text = "";

            AddButton.CommandName = "add";
            AddButton.CommandArgument = "";
        }

        protected void CloseButton_Click(object sender, EventArgs e)
        {
            ClearModal();
        }

        protected void AddButton_Command(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                if (((string[])Session["edit"]).Contains("item"))
                {
                    List<string> errors = new List<string>();

                    int qty = 0;
                    int.TryParse(QtyInput.Text?.Trim(), out qty);

                    if (qty >= 0)
                    {
                        System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                        System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

                        foreach (DataRow row in items.Rows)
                        {
                            if (int.Parse(ItemBarcodeChoice.SelectedValue) == int.Parse(row["item_detail_id"].ToString()))
                            {
                                if (qty > int.Parse(row["item_qty_constraint"].ToString()))
                                {
                                    errors.Add("Jumlah barang melebihi batas yaitu " + row["item_qty_constraint"].ToString());
                                }
                                else
                                {
                                    row["item_qty"] = qty.ToString("###,###,###,##0");
                                    break;
                                }
                            }
                        }

                        if (errors.Count <= 0)
                        {
                            items.AcceptChanges();

                            foreach (DataRow row in updates.Rows)
                            {
                                if (int.Parse(ItemBarcodeChoice.SelectedValue) == int.Parse(row["item_detail_id"].ToString()))
                                {
                                    row["item_qty"] = qty;
                                    break;
                                }
                            }

                            updates.AcceptChanges();

                            ViewState["items"] = items;
                            ViewState["updates"] = updates;
                            TotalQtyCheckBoxCheckedChanged();
                            TotalPriceCheckBoxCheckedChanged();
                            ItemGridView.DataSource = items;
                            ItemGridView.DataBind();
                            ClearModal();
                        }
                        else
                        {
                            AlertModalLabel.Text = String.Join("<br>", errors.ToArray());
                            AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
                        }
                    }
                    else
                    {
                        AlertModalLabel.Text = "Jumlah barang tidak valid";
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
                    }
                }
                else
                {
                    AlertModalLabel.Text = "Akses ditolak";
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
            }

        }

        protected void EditButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "edit" && arg.Length > 0)
            {   
                AddButton.CommandName = "edit";
                AddButton.CommandArgument = e.CommandArgument.ToString();

                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                DataRow row = items.Select(String.Concat("item_detail_id=", arg[0].ToString())).FirstOrDefault();

                if (row != null)
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT item_detail_id, item_barcode FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                            AND isdeleted = 0 AND item_detail_id = @itemId;";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@itemId", int.Parse(row["item_detail_id"].ToString()));

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    ItemBarcodeChoice.DataSource = ds;
                    ItemBarcodeChoice.DataTextField = "item_barcode";
                    ItemBarcodeChoice.DataValueField = "item_detail_id";
                    ItemBarcodeChoice.DataBind();

                    ds.Dispose();

                    cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT item_detail_id, item_name FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                            AND isdeleted = 0  AND item_detail_id = @itemId;";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@itemId", int.Parse(row["item_detail_id"].ToString()));

                    da = new SqlDataAdapter(cmd);
                    ds = new DataSet();
                    da.Fill(ds);

                    ItemNameChoice.DataSource = ds;
                    ItemNameChoice.DataTextField = "item_name";
                    ItemNameChoice.DataValueField = "item_detail_id";
                    ItemNameChoice.DataBind();

                    ds.Dispose();

                    DescriptionInput.Text = row["short_description"].ToString();
                    ColorInput.Text = row["color"].ToString();
                    SizeInput.Text = row["size"].ToString();
                    BrandInput.Text = row["brand_name"].ToString();
                    PriceInput.Text = row["item_price"].ToString();
                    QtyInput.Text = row["item_qty"].ToString();
                    SupplierProductCodeInput.Text = row["supplier_product_code"].ToString();
                }
                else
                {
                    AlertModalLabel.Text = "Barang tidak ditemukan";
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void BackButton_Click(object sender, EventArgs e)
        {
            if(Session["origin"].ToString() == "DeliveryOrder")
            {
                Session["redirect"] = true;
                Response.Redirect("DeliveryOrder.aspx");
            }

            if (Session["origin"].ToString() == "GoodsReceipt")
            {
                Session["redirect"] = true;
                Response.Redirect("GoodsReceipt.aspx");
            }
        }

        protected bool IsLocked()
        {
            if (action == "update")
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                cmd.CommandText = @"SELECT COUNT(*) FROM goods_receipts 
                WHERE goods_receipt_id = @receiptId AND uuid = @receiptUuid 
                AND isdeleted = 0 AND locked = 1;";

                cmd.Parameters.AddWithValue("@receiptId", int.Parse(ViewState["id"].ToString()));
                cmd.Parameters.AddWithValue("@receiptUuid", ViewState["uuid"].ToString());
                cmd.Connection = conn;

                conn.Open();
                object result = cmd.ExecuteScalar();
                conn.Close();

                int count = 0;

                if (result != null)
                {
                    count = int.Parse(result.ToString());
                }

                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        protected void SearchButton_Command(object sender, CommandEventArgs e)
        {
            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

            if (!String.IsNullOrEmpty(SearchInput.Text?.Trim()))
            {
                items.CaseSensitive = true;
                DataRow row = items.Select(String.Format("item_barcode = '{0}' OR supplier_product_code = '{0}'", SearchInput.Text?.Trim())).FirstOrDefault();

                if (row != null)
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT item_detail_id, item_barcode FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                            AND isdeleted = 0 AND item_detail_id = @itemId;";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@itemId", int.Parse(row["item_detail_id"].ToString()));

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    ItemBarcodeChoice.DataSource = ds;
                    ItemBarcodeChoice.DataTextField = "item_barcode";
                    ItemBarcodeChoice.DataValueField = "item_detail_id";
                    ItemBarcodeChoice.DataBind();

                    ds.Dispose();

                    cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT item_detail_id, item_name FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                            AND isdeleted = 0  AND item_detail_id = @itemId;";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@itemId", int.Parse(row["item_detail_id"].ToString()));

                    da = new SqlDataAdapter(cmd);
                    ds = new DataSet();
                    da.Fill(ds);

                    ItemNameChoice.DataSource = ds;
                    ItemNameChoice.DataTextField = "item_name";
                    ItemNameChoice.DataValueField = "item_detail_id";
                    ItemNameChoice.DataBind();

                    ds.Dispose();

                    DescriptionInput.Text = row["short_description"].ToString();
                    ColorInput.Text = row["color"].ToString();
                    SizeInput.Text = row["size"].ToString();
                    BrandInput.Text = row["brand_name"].ToString();
                    PriceInput.Text = row["item_price"].ToString();
                    QtyInput.Text = row["item_qty"].ToString();
                    SupplierProductCodeInput.Text = row["supplier_product_code"].ToString();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
                }
                else
                {
                    items.CaseSensitive = false;
                    DataRow[] rows = items.Select(String.Format("item_barcode LIKE '%{0}%' OR supplier_product_code LIKE '%{0}%'", SearchInput.Text?.Trim()));

                    System.Data.DataTable temps = new System.Data.DataTable("temps");

                    temps.Columns.Add("no");
                    temps.Columns.Add("item_detail_id");
                    temps.Columns.Add("item_barcode");
                    temps.Columns.Add("supplier_product_code");
                    temps.Columns.Add("item_name");
                    temps.Columns.Add("short_description");
                    temps.Columns.Add("color");
                    temps.Columns.Add("size");
                    temps.Columns.Add("variants");
                    temps.Columns.Add("brand_name");
                    temps.Columns.Add("item_price");
                    temps.Columns.Add("item_qty");
                    temps.Columns.Add("item_qty_constraint");

                    foreach (DataRow rw in rows)
                    {
                        temps.Rows.Add(rw.ItemArray);
                    }

                    ItemGridView.DataSource = temps;
                    ItemGridView.DataBind();
                }
            }
            else
            {
                items.CaseSensitive = true;
                ItemGridView.DataSource = items;
                ItemGridView.DataBind();
            }
        }

        protected void SearchInput_TextChanged(object sender, EventArgs e)
        {    
            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

            if (!String.IsNullOrEmpty(SearchInput.Text?.Trim()))
            {
                
                items.CaseSensitive = true;
                DataRow row = items.Select(String.Format("item_barcode = '{0}' OR supplier_product_code = '{0}'", SearchInput.Text?.Trim())).FirstOrDefault();

                if (row != null)
                {
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT item_detail_id, item_barcode FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                            AND isdeleted = 0 AND item_detail_id = @itemId;";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@itemId", int.Parse(row["item_detail_id"].ToString()));

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    ItemBarcodeChoice.DataSource = ds;
                    ItemBarcodeChoice.DataTextField = "item_barcode";
                    ItemBarcodeChoice.DataValueField = "item_detail_id";
                    ItemBarcodeChoice.DataBind();

                    ds.Dispose();

                    cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT item_detail_id, item_name FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId
                            AND isdeleted = 0  AND item_detail_id = @itemId;";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                    cmd.Parameters.AddWithValue("@itemId", int.Parse(row["item_detail_id"].ToString()));

                    da = new SqlDataAdapter(cmd);
                    ds = new DataSet();
                    da.Fill(ds);

                    ItemNameChoice.DataSource = ds;
                    ItemNameChoice.DataTextField = "item_name";
                    ItemNameChoice.DataValueField = "item_detail_id";
                    ItemNameChoice.DataBind();

                    ds.Dispose();

                    DescriptionInput.Text = row["short_description"].ToString();
                    ColorInput.Text = row["color"].ToString();
                    SizeInput.Text = row["size"].ToString();
                    BrandInput.Text = row["brand_name"].ToString();
                    PriceInput.Text = row["item_price"].ToString();
                    QtyInput.Text = row["item_qty"].ToString();
                    SupplierProductCodeInput.Text = row["supplier_product_code"].ToString();

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
                }
                else
                {
                    items.CaseSensitive = false;
                    DataRow[] rows = items.Select(String.Format("item_barcode LIKE '%{0}%' OR supplier_product_code LIKE '%{0}%'", SearchInput.Text?.Trim()));

                    System.Data.DataTable temps = new System.Data.DataTable("temps");

                    temps.Columns.Add("no");
                    temps.Columns.Add("item_detail_id");
                    temps.Columns.Add("item_barcode");
                    temps.Columns.Add("supplier_product_code");
                    temps.Columns.Add("item_name");
                    temps.Columns.Add("short_description");
                    temps.Columns.Add("color");
                    temps.Columns.Add("size");
                    temps.Columns.Add("variants");
                    temps.Columns.Add("brand_name");
                    temps.Columns.Add("item_price");
                    temps.Columns.Add("item_qty");
                    temps.Columns.Add("item_qty_constraint");

                    foreach (DataRow rw in rows)
                    {
                        temps.Rows.Add(rw.ItemArray );
                    }

                    ItemGridView.DataSource = temps;
                    ItemGridView.DataBind();
                }
            }
            else
            {
                items.CaseSensitive = true;
                ItemGridView.DataSource = items;
                ItemGridView.DataBind();
            }
        }

        protected void NoButton_Click(object sender, EventArgs e)
        {

        }

        protected void YesButton_Click(object sender, CommandEventArgs e)
        {
            if (action == "create")
            {
                Session["redirect"] = true;
                if (Session["origin"].ToString() == "DeliveryOrder")
                {
                    Response.Redirect("DeliveryOrder.aspx");
                }

                if (Session["origin"].ToString() == "GoodsReceipt")
                {
                    Response.Redirect("GoodsReceipt.aspx");
                }
            }

            if (action == "update")
            {
                if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))) &&
                    (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))) &&
                    (bool)Session["delete"] == true)
                {
                    if (!IsLocked())
                    {
                        try
                        {
                            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                            SqlConnection conn = new SqlConnection(constr);
                            SqlTransaction transaction;

                            conn.Open();
                            transaction = conn.BeginTransaction();

                            SqlCommand command = conn.CreateCommand();
                            command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                FROM goods_receipts A 
                                INNER JOIN goods_receipt_items B ON B.goods_receipt_id = A.goods_receipt_id AND B.isdeleted = 0
                                WHERE A.store_id = @storeId AND A.supplier_id = @supplierId AND 
                                A.uuid = @receiptUuid AND A.goods_receipt_id = @receiptId AND A.isdeleted = 0";
                            command.Parameters.AddWithValue("@user", Session["username"]);
                            command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                            command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                            command.Parameters.AddWithValue("@receiptId", int.Parse(ViewState["id"].ToString()));
                            command.Parameters.AddWithValue("@receiptUuid", ViewState["uuid"].ToString());
                            command.Transaction = transaction;

                            //conn.Open();
                            command.ExecuteNonQuery();
                            //conn.Close();

                            command = conn.CreateCommand();
                            command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                FROM goods_receipts A WHERE A.store_id = @storeId AND A.supplier_id = @supplierId AND 
                                A.uuid = @receiptUuid AND A.goods_receipt_id = @receiptId AND isdeleted = 0";
                            command.Parameters.AddWithValue("@user", Session["username"]);
                            command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                            command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                            command.Parameters.AddWithValue("@receiptId", int.Parse(ViewState["id"].ToString()));
                            command.Parameters.AddWithValue("@receiptUuid", ViewState["uuid"].ToString());
                            command.Transaction = transaction;

                            //conn.Open();
                            command.ExecuteNonQuery();
                            //conn.Close();

                            transaction.Commit();
                            conn.Close();

                            Session["redirect"] = true;
                            if (Session["origin"].ToString() == "DeliveryOrder")
                            {
                                Response.Redirect("DeliveryOrder.aspx");
                            }

                            if (Session["origin"].ToString() == "GoodsReceipt")
                            {
                                Response.Redirect("GoodsReceipt.aspx");
                            }

                        }
                        catch (Exception error)
                        {
                            AlertLabel.Text = error.Message.ToString();
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen penerimaan terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }

        }

        protected void DeleteButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
            {
                AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
            {
                AlertLabel.Text = "Vendor tidak boleh kosong";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if ((((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue)) || ((int[])Session["stores"]).Contains(-1)) &&
                (((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue)) || ((int[])Session["suppliers"]).Contains(-1)) &&
                (bool)Session["delete"] == true)
            {
                if (!IsLocked())
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openConfirmModal();", true);
                }
                else
                {
                    AlertLabel.Text = "Dokumen penerimaan terkunci";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                
            }
            else
            {
                AlertLabel.Text = "Akses ditolak";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }

        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            if (action == "update")
            {
                List<string> errors = new List<string>();

                if (((string[])Session["edit"]).Contains("totalQty"))
                {
                    if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()) && !TotalQtyCheckBox.Checked)
                    {
                        errors.Add("Total kuantitas barang tidak boleh kosong");
                    }
                }

                if (((string[])Session["edit"]).Contains("totalPrice"))
                {
                    if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()) && !TotalPriceCheckBox.Checked)
                    {
                        errors.Add("Total harga barang tidak boleh kosong");
                    }
                }

                System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                if (updates.Rows.Count <= 0)
                {
                    errors.Add("Barang yang akan dibeli tidak boleh kosong");
                }

                if (errors.Count <= 0)
                {
                    int totalQty = 0;
                    double totalPrice = 0;

                    foreach (DataRow row in updates.Rows)
                    {
                        if(!String.IsNullOrEmpty(row["item_qty"].ToString()) && !String.IsNullOrEmpty(row["item_price"].ToString()))
                        {
                            totalQty += int.Parse(row["item_qty"].ToString());
                            totalPrice += double.Parse(row["item_price"].ToString()) * int.Parse(row["item_qty"].ToString());
                        }
                    }

                    string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

                    SqlConnection conn = new SqlConnection(constr);

                    SqlTransaction transaction;
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    try
                    {
                        string grpattern = "GR" + DateTime.Now.ToString("yyyyMMdd");
                        string receiptNo = grpattern + "1".ToString().PadLeft(3, '0');

                        SqlCommand command = conn.CreateCommand();
                        command.CommandText = @"SELECT TOP 1 CAST(RIGHT([goods_receipt_no], 3) AS INT) FROM goods_receipts WHERE 
                                    goods_receipt_no LIKE @goodsReceiptNo ORDER BY goods_receipt_no DESC";
                        command.Parameters.AddWithValue("@goodsReceiptNo", string.Format("{0}%", grpattern));
                        command.Transaction = transaction;

                        int number = 1;

                        object result = command.ExecuteScalar();
                        if (result != null)
                        {
                            number = int.Parse(result.ToString()) + 1;
                        }

                        receiptNo = grpattern + number.ToString().PadLeft(3, '0');


                        string sql = "";
                        bool withComma = false;

                        if (((string[])Session["edit"]).Contains("supplierName"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_name = @supplierName");
                        }

                        if (((string[])Session["edit"]).Contains("supplierBuildingName"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_building_name = @supplierBuildingName");
                        }

                        if (((string[])Session["edit"]).Contains("supplierStreetName"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_street_name = @supplierStreetName");
                        }

                        if (((string[])Session["edit"]).Contains("supplierNeighbourhood"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_neighbourhood = @supplierNeighbourhood");

                        }

                        if (((string[])Session["edit"]).Contains("supplierSubdistrict"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_subdistrict = @supplierSubdistrict");

                        }

                        if (((string[])Session["edit"]).Contains("supplierDistrict"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_district = @supplierDistrict");

                        }

                        if (((string[])Session["edit"]).Contains("supplierRuralDistrict"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_rural_district = @supplierRuralDistrict");

                        }

                        if (((string[])Session["edit"]).Contains("supplierProvince"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_province = @supplierProvince");

                        }

                        if (((string[])Session["edit"]).Contains("supplierZipcode"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_zipcode = @supplierZipcode");
                        }

                        if (((string[])Session["edit"]).Contains("supplierContactName"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_contact_name = @supplierContactName");
                        }

                        if (((string[])Session["edit"]).Contains("supplierContactPhone"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_contact_phone = @supplierContactPhone");

                        }

                        if (((string[])Session["edit"]).Contains("supplierContactEmail"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "supplier_contact_email = @supplierContactEmail");

                        }

                        if (((string[])Session["edit"]).Contains("storeName"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_name = @storeName");

                        }

                        if (((string[])Session["edit"]).Contains("storeBuildingName"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_building_name = @storeBuildingName");

                        }

                        if (((string[])Session["edit"]).Contains("storeStreetName"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_street_name = @storeStreetName");

                        }

                        if (((string[])Session["edit"]).Contains("storeNeighbourhood"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_neighbourhood = @storeNeighbourhood");

                        }

                        if (((string[])Session["edit"]).Contains("storeSubdistrict"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_subdistrict = @storeSubdistrict");

                        }

                        if (((string[])Session["edit"]).Contains("storeDistrict"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_district = @storeDistrict");
                        }

                        if (((string[])Session["edit"]).Contains("storeRuralDistrict"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_rural_district = @storeRuralDistrict");

                        }

                        if (((string[])Session["edit"]).Contains("storeProvince"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_province = @storeProvince");

                        }

                        if (((string[])Session["edit"]).Contains("storeZipcode"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_zipcode = @storeZipcode");

                        }

                        if (((string[])Session["edit"]).Contains("storeContactName"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_contact_name = @storeContactName");

                        }

                        if (((string[])Session["edit"]).Contains("storeContactPhone"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_contact_phone = @storeContactPhone");

                        }

                        if (((string[])Session["edit"]).Contains("storeContactEmail"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "store_contact_email = @storeContactEmail");
                        }

                        if (((string[])Session["edit"]).Contains("goodsReceiptNo"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "goods_receipt_no = @goodsReceiptNo");

                        }

                        if (((string[])Session["edit"]).Contains("invoiceNo"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "invoice_no = @invoiceNo");

                        }

                        if (((string[])Session["edit"]).Contains("driver"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "driver = @driver");

                        }

                        if (((string[])Session["edit"]).Contains("receivedBy"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "received_by = @receivedBy");

                        }

                        if (((string[])Session["edit"]).Contains("receivedDate"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "received_date = @receivedDate");

                        }

                        if (((string[])Session["edit"]).Contains("totalQty"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "total_qty = @totalQty");

                        }

                        if (((string[])Session["edit"]).Contains("totalPrice"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "total_price = @totalPrice");

                        }

                        if (((string[])Session["edit"]).Contains("goodsReceiptNotes"))
                        {
                            if (withComma)
                            {
                                sql = String.Concat(sql, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            sql = String.Concat(sql, " ", "goods_receipt_notes = @goodsReceiptNotes");

                        }

                        string commandText = @"UPDATE goods_receipts SET {0}, modified_date = getdate(), modified_by = @user
                        WHERE goods_receipt_id = @receiptId AND uuid = @receiptUuid AND isdeleted = 0;";

                        commandText = String.Format(commandText, sql);

                        command = conn.CreateCommand();
                        command.CommandText = commandText;
                        command.Parameters.AddWithValue("@receiptId", int.Parse(ViewState["id"].ToString()));
                        command.Parameters.AddWithValue("@receiptUuid", ViewState["uuid"].ToString());
                        command.Parameters.AddWithValue("@user", Session["username"]);

                        if (((string[])Session["edit"]).Contains("supplierName"))
                        {
                            if (SupplierNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierName", SupplierNameInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("supplierBuildingName"))
                        {
                            if (SupplierBuildingNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierBuildingName", SupplierBuildingNameInput.Text?.Trim());
                                }
                            }
                        }


                        if (((string[])Session["edit"]).Contains("supplierStreetName"))
                        {
                            if (SupplierStreetNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierStreetName", SupplierStreetNameInput.Text?.Trim());
                                }

                            }

                        }


                        if (((string[])Session["edit"]).Contains("supplierNeighbourhood"))
                        {
                            if (SupplierNeighbourhoodCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierNeighbourhood", SupplierNeighbourhoodInput.Text?.Trim());
                                }
                            }

                        }


                        if (((string[])Session["edit"]).Contains("supplierSubdistrict"))
                        {
                            if (SupplierSubdistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierSubdistrict", SupplierSubdistrictInput.Text?.Trim());
                                }
                            }
                        }


                        if (((string[])Session["edit"]).Contains("supplierDistrict"))
                        {
                            if (SupplierDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierDistrict", SupplierDistrictInput.Text?.Trim());
                                }
                            }
                        }


                        if (((string[])Session["edit"]).Contains("supplierRuralDistrict"))
                        {
                            if (SupplierRuralDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierRuralDistrict", SupplierRuralDistrictInput.Text?.Trim());
                                }
                            }
                        }


                        if (((string[])Session["edit"]).Contains("supplierProvince"))
                        {
                            if (SupplierProvinceCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierProvince", SupplierProvinceInput.Text?.Trim());
                                }
                            }
                        }


                        if (((string[])Session["edit"]).Contains("supplierZipcode"))
                        {
                            if (SupplierZipcodeCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierZipcode", SupplierZipcodeInput.Text?.Trim());
                                }
                            }
                        }


                        if (((string[])Session["edit"]).Contains("supplierContactName"))
                        {
                            if (SupplierContactNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactName", SupplierContactNameInput.Text?.Trim());
                                }
                            }
                        }


                        if (((string[])Session["edit"]).Contains("supplierContactPhone"))
                        {
                            if (SupplierContactPhoneCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactPhone", SupplierContactPhoneInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("supplierContactEmail"))
                        {
                            if (SupplierContactEmailCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@supplierContactEmail", SupplierContactEmailInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeName"))
                        {
                            if (StoreNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeName"]))
                                {
                                    command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeName", store["storeName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeName", StoreNameInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeBuildingName"))
                        {
                            if (StoreBuildingNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeBuildingName"]))
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeBuildingName", StoreBuildingNameInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeStreetName"))
                        {
                            if (StoreStreetNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeStreetName"]))
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeStreetName", StoreStreetNameInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeNeighbourhood"))
                        {
                            if (StoreNeighbourhoodCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                                }

                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeNeighbourhood", StoreNeighbourhoodInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeSubdistrict"))
                        {
                            if (StoreSubdistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeSubdistrict", StoreSubdistrictInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeDistrict"))
                        {
                            if (StoreDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeDistrict", StoreDistrictInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeRuralDistrict"))
                        {
                            if (StoreRuralDistrictCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeRuralDistrict", StoreRuralDistrictInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeProvince"))
                        {
                            if (StoreProvinceCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeProvince"]))
                                {
                                    command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeProvince", StoreProvinceInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeZipcode"))
                        {
                            if (StoreZipcodeCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeZipcode"]))
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeZipcode", StoreZipcodeInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeContactName"))
                        {
                            if (StoreContactNameCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeContactName"]))
                                {
                                    command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactName", StoreContactNameInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeContactPhone"))
                        {
                            if (StoreContactPhoneCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeContactPhone"]))
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactPhone", StoreContactPhoneInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("storeContactEmail"))
                        {
                            if (StoreContactEmailCheckBox.Checked)
                            {
                                if (String.IsNullOrEmpty(store["storeContactEmail"]))
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                                }
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@storeContactEmail", StoreContactEmailInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("goodsReceiptNo"))
                        {
                            if(String.IsNullOrEmpty(receipt["goodsReceiptNo"]))
                            {
                                command.Parameters.AddWithValue("@goodsReceiptNo", receiptNo);
                            } 
                            else
                            {
                                command.Parameters.AddWithValue("@goodsReceiptNo", receipt["goodsReceiptNo"]);
                            }
                        }

                        if (((string[])Session["edit"]).Contains("invoiceNo"))
                        {
                            if (String.IsNullOrEmpty(InvoiceNoInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@invoiceNo", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@invoiceNo", InvoiceNoInput.Text?.Trim());
                            }
                            
                        }

                        if (((string[])Session["edit"]).Contains("driver"))
                        {
                            if (String.IsNullOrEmpty(DriverInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@driver", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@driver", DriverInput.Text?.Trim());
                            }
                            
                        }

                        if (((string[])Session["edit"]).Contains("receivedBy"))
                        {
                            if (String.IsNullOrEmpty(ReceivedByInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@receivedBy", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@receivedBy", ReceivedByInput.Text?.Trim());
                            }
                        }

                        if (((string[])Session["edit"]).Contains("receivedDate"))
                        {
                            if (String.IsNullOrEmpty(ReceivedDateInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@receivedDate", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@receivedDate", ReceivedDateInput.Text?.Trim());
                            }
                        }

                        if (((string[])Session["edit"]).Contains("totalQty"))
                        {
                            if (TotalQtyCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@totalQty", totalQty);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@totalQty", 0);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@totalQty", TotalQtyInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("totalPrice"))
                        {
                            if (TotalPriceCheckBox.Checked)
                            {
                                command.Parameters.AddWithValue("@totalPrice", totalPrice);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@totalPrice", 0);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@totalPrice", TotalPriceInput.Text?.Trim());
                                }
                            }
                        }

                        if (((string[])Session["edit"]).Contains("goodsReceiptNotes"))
                        {
                            if (String.IsNullOrEmpty(GoodsReceiptNotesInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@goodsReceiptNotes", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@goodsReceiptNotes", GoodsReceiptNotesInput.Text?.Trim());
                            }
                        }

                        command.Transaction = transaction;
                        command.ExecuteNonQuery();

                        if (updates.Rows.Count > 0)
                        {
                            command = conn.CreateCommand();
                            command.CommandText = @"CREATE TABLE #temp_update (goods_receipt_item_id INT, goods_receipt_id INT, item_detail_id INT,
                            item_json VARCHAR(MAX), item_qty INT, item_price DECIMAL(13,2));";
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();

                            SqlBulkCopy bulkupdate = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                            bulkupdate.DestinationTableName = "#temp_update";

                            bulkupdate.ColumnMappings.Add("goods_receipt_item_id", "goods_receipt_item_id");
                            bulkupdate.ColumnMappings.Add("goods_receipt_id", "goods_receipt_id");
                            bulkupdate.ColumnMappings.Add("item_detail_id", "item_detail_id");
                            bulkupdate.ColumnMappings.Add("item_json", "item_json");
                            bulkupdate.ColumnMappings.Add("item_qty", "item_qty");
                            bulkupdate.ColumnMappings.Add("item_price", "item_price");
                            bulkupdate.WriteToServer(updates);

                            command = conn.CreateCommand();
                            command.CommandText = @"UPDATE orig SET item_json = temp.item_json, item_qty = temp.item_qty, 
                            item_price = temp.item_price, modified_date = getdate(), modified_by = @user
                            FROM goods_receipt_items orig 
                            INNER JOIN #temp_update temp ON orig.goods_receipt_item_id = temp.goods_receipt_item_id AND
                            orig.goods_receipt_id = temp.goods_receipt_id AND orig.item_detail_id = temp.item_detail_id
                            WHERE orig.isdeleted = 0;
                            DROP TABLE #temp_update;";
                            command.Parameters.AddWithValue("@user", Session["username"].ToString());
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();

                        updates.Rows.Clear();
                        ViewState["updates"] = updates;
                        items.Rows.Clear();
                        ViewState["items"] = items;

                        LoadForm();
                        FillAutoSupplierInput();
                        FillAutoStoreInput();
                        FillAutoReceiptInput();
                        BindItemGridview();

                        AlertLabel.Text = "Data berhasil disimpan";
                        AlertLabel.CssClass = "alert alert-light-primary color-primary d-block";
                    }
                    catch (Exception error)
                    {
                        transaction.Rollback();

                        AlertLabel.Text = error.Message.ToString();
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                    conn.Close();
                }
                else
                {
                    AlertLabel.Text = String.Join("<br>", errors.ToArray());
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
        }

        protected void LockButton_Click(object sender, EventArgs e)
        {
            if (action == "update")
            {
                if (!IsLocked())
                {
                    List<string> errors = new List<string>();

                    if (((string[])Session["edit"]).Contains("totalQty"))
                    {
                        if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()) && !TotalQtyCheckBox.Checked)
                        {
                            errors.Add("Total kuantitas barang tidak boleh kosong");
                        }
                    }

                    if (((string[])Session["edit"]).Contains("totalPrice"))
                    {
                        if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()) && !TotalPriceCheckBox.Checked)
                        {
                            errors.Add("Total harga barang tidak boleh kosong");
                        }
                    }

                    System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
                    System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                    if (updates.Rows.Count <= 0)
                    {
                        errors.Add("Barang yang akan dibeli tidak boleh kosong");
                    }

                    if (errors.Count <= 0)
                    {
                        int totalQty = 0;
                        double totalPrice = 0;

                        foreach (DataRow row in updates.Rows)
                        {
                            if (!String.IsNullOrEmpty(row["item_qty"].ToString()) && !String.IsNullOrEmpty(row["item_price"].ToString()))
                            {
                                totalQty += int.Parse(row["item_qty"].ToString());
                                totalPrice += double.Parse(row["item_price"].ToString()) * int.Parse(row["item_qty"].ToString());
                            }
                        }

                        string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

                        SqlConnection conn = new SqlConnection(constr);

                        SqlTransaction transaction;
                        conn.Open();
                        transaction = conn.BeginTransaction();

                        try
                        {
                            string grpattern = "GR" + DateTime.Now.ToString("yyyyMMdd");
                            string receiptNo = grpattern + "1".ToString().PadLeft(3, '0');

                            SqlCommand command = conn.CreateCommand();
                            command.CommandText = @"SELECT TOP 1 CAST(RIGHT([goods_receipt_no], 3) AS INT) FROM goods_receipts WHERE 
                                    goods_receipt_no LIKE @goodsReceiptNo ORDER BY goods_receipt_no DESC";
                            command.Parameters.AddWithValue("@goodsReceiptNo", string.Format("{0}%", grpattern));
                            command.Transaction = transaction;

                            int number = 1;

                            object result = command.ExecuteScalar();
                            if (result != null)
                            {
                                number = int.Parse(result.ToString()) + 1;
                            }

                            receiptNo = grpattern + number.ToString().PadLeft(3, '0');


                            string sql = "";
                            bool withComma = false;

                            if (((string[])Session["edit"]).Contains("supplierName"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_name = @supplierName");
                            }

                            if (((string[])Session["edit"]).Contains("supplierBuildingName"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_building_name = @supplierBuildingName");
                            }

                            if (((string[])Session["edit"]).Contains("supplierStreetName"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_street_name = @supplierStreetName");
                            }

                            if (((string[])Session["edit"]).Contains("supplierNeighbourhood"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_neighbourhood = @supplierNeighbourhood");

                            }

                            if (((string[])Session["edit"]).Contains("supplierSubdistrict"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_subdistrict = @supplierSubdistrict");

                            }

                            if (((string[])Session["edit"]).Contains("supplierDistrict"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_district = @supplierDistrict");

                            }

                            if (((string[])Session["edit"]).Contains("supplierRuralDistrict"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_rural_district = @supplierRuralDistrict");

                            }

                            if (((string[])Session["edit"]).Contains("supplierProvince"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_province = @supplierProvince");

                            }

                            if (((string[])Session["edit"]).Contains("supplierZipcode"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_zipcode = @supplierZipcode");
                            }

                            if (((string[])Session["edit"]).Contains("supplierContactName"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_contact_name = @supplierContactName");
                            }

                            if (((string[])Session["edit"]).Contains("supplierContactPhone"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_contact_phone = @supplierContactPhone");

                            }

                            if (((string[])Session["edit"]).Contains("supplierContactEmail"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "supplier_contact_email = @supplierContactEmail");

                            }

                            if (((string[])Session["edit"]).Contains("storeName"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_name = @storeName");

                            }

                            if (((string[])Session["edit"]).Contains("storeBuildingName"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_building_name = @storeBuildingName");

                            }

                            if (((string[])Session["edit"]).Contains("storeStreetName"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_street_name = @storeStreetName");

                            }

                            if (((string[])Session["edit"]).Contains("storeNeighbourhood"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_neighbourhood = @storeNeighbourhood");

                            }

                            if (((string[])Session["edit"]).Contains("storeSubdistrict"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_subdistrict = @storeSubdistrict");

                            }

                            if (((string[])Session["edit"]).Contains("storeDistrict"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_district = @storeDistrict");
                            }

                            if (((string[])Session["edit"]).Contains("storeRuralDistrict"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_rural_district = @storeRuralDistrict");

                            }

                            if (((string[])Session["edit"]).Contains("storeProvince"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_province = @storeProvince");

                            }

                            if (((string[])Session["edit"]).Contains("storeZipcode"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_zipcode = @storeZipcode");

                            }

                            if (((string[])Session["edit"]).Contains("storeContactName"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_contact_name = @storeContactName");

                            }

                            if (((string[])Session["edit"]).Contains("storeContactPhone"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_contact_phone = @storeContactPhone");

                            }

                            if (((string[])Session["edit"]).Contains("storeContactEmail"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "store_contact_email = @storeContactEmail");
                            }

                            if (((string[])Session["edit"]).Contains("goodsReceiptNo"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "goods_receipt_no = @goodsReceiptNo");

                            }

                            if (((string[])Session["edit"]).Contains("invoiceNo"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "invoice_no = @invoiceNo");

                            }

                            if (((string[])Session["edit"]).Contains("driver"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "driver = @driver");

                            }

                            if (((string[])Session["edit"]).Contains("receivedBy"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "received_by = @receivedBy");

                            }

                            if (((string[])Session["edit"]).Contains("receivedDate"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "received_date = @receivedDate");

                            }

                            if (((string[])Session["edit"]).Contains("totalQty"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "total_qty = @totalQty");

                            }

                            if (((string[])Session["edit"]).Contains("totalPrice"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "total_price = @totalPrice");

                            }

                            if (((string[])Session["edit"]).Contains("goodsReceiptNotes"))
                            {
                                if (withComma)
                                {
                                    sql = String.Concat(sql, ",");
                                }
                                else
                                {
                                    withComma = true;
                                }
                                sql = String.Concat(sql, " ", "goods_receipt_notes = @goodsReceiptNotes");

                            }

                            string commandText = @"UPDATE goods_receipts SET {0}, modified_date = getdate(), modified_by = @user, locked = 1
                            WHERE goods_receipt_id = @receiptId AND uuid = @receiptUuid AND isdeleted = 0;";

                            commandText = String.Format(commandText, sql);

                            command = conn.CreateCommand();
                            command.CommandText = commandText;
                            command.Parameters.AddWithValue("@receiptId", int.Parse(ViewState["id"].ToString()));
                            command.Parameters.AddWithValue("@receiptUuid", ViewState["uuid"].ToString());
                            command.Parameters.AddWithValue("@user", Session["username"]);

                            if (((string[])Session["edit"]).Contains("supplierName"))
                            {
                                if (SupplierNameCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierName"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierName", SupplierNameInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("supplierBuildingName"))
                            {
                                if (SupplierBuildingNameCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierBuildingName", SupplierBuildingNameInput.Text?.Trim());
                                    }
                                }
                            }


                            if (((string[])Session["edit"]).Contains("supplierStreetName"))
                            {
                                if (SupplierStreetNameCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                                    }

                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierStreetName", SupplierStreetNameInput.Text?.Trim());
                                    }

                                }

                            }


                            if (((string[])Session["edit"]).Contains("supplierNeighbourhood"))
                            {
                                if (SupplierNeighbourhoodCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                                    }

                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierNeighbourhood", SupplierNeighbourhoodInput.Text?.Trim());
                                    }
                                }

                            }


                            if (((string[])Session["edit"]).Contains("supplierSubdistrict"))
                            {
                                if (SupplierSubdistrictCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierSubdistrict", SupplierSubdistrictInput.Text?.Trim());
                                    }
                                }
                            }


                            if (((string[])Session["edit"]).Contains("supplierDistrict"))
                            {
                                if (SupplierDistrictCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierDistrict", SupplierDistrictInput.Text?.Trim());
                                    }
                                }
                            }


                            if (((string[])Session["edit"]).Contains("supplierRuralDistrict"))
                            {
                                if (SupplierRuralDistrictCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierRuralDistrict", SupplierRuralDistrictInput.Text?.Trim());
                                    }
                                }
                            }


                            if (((string[])Session["edit"]).Contains("supplierProvince"))
                            {
                                if (SupplierProvinceCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierProvince", SupplierProvinceInput.Text?.Trim());
                                    }
                                }
                            }


                            if (((string[])Session["edit"]).Contains("supplierZipcode"))
                            {
                                if (SupplierZipcodeCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierZipcode", SupplierZipcodeInput.Text?.Trim());
                                    }
                                }
                            }


                            if (((string[])Session["edit"]).Contains("supplierContactName"))
                            {
                                if (SupplierContactNameCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierContactName", SupplierContactNameInput.Text?.Trim());
                                    }
                                }
                            }


                            if (((string[])Session["edit"]).Contains("supplierContactPhone"))
                            {
                                if (SupplierContactPhoneCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierContactPhone", SupplierContactPhoneInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("supplierContactEmail"))
                            {
                                if (SupplierContactEmailCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                                    {
                                        command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierContactEmail", SupplierContactEmailInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeName"))
                            {
                                if (StoreNameCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeName"]))
                                    {
                                        command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeName", store["storeName"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeName", StoreNameInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeBuildingName"))
                            {
                                if (StoreBuildingNameCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeBuildingName"]))
                                    {
                                        command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeBuildingName", StoreBuildingNameInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeStreetName"))
                            {
                                if (StoreStreetNameCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeStreetName"]))
                                    {
                                        command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                                    }

                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeStreetName", StoreStreetNameInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeNeighbourhood"))
                            {
                                if (StoreNeighbourhoodCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                                    {
                                        command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                                    }

                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeNeighbourhood", StoreNeighbourhoodInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeSubdistrict"))
                            {
                                if (StoreSubdistrictCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                    {
                                        command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeSubdistrict", StoreSubdistrictInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeDistrict"))
                            {
                                if (StoreDistrictCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                    {
                                        command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeDistrict", StoreDistrictInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeRuralDistrict"))
                            {
                                if (StoreRuralDistrictCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                                    {
                                        command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeRuralDistrict", StoreRuralDistrictInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeProvince"))
                            {
                                if (StoreProvinceCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeProvince"]))
                                    {
                                        command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeProvince", StoreProvinceInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeZipcode"))
                            {
                                if (StoreZipcodeCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeZipcode"]))
                                    {
                                        command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeZipcode", StoreZipcodeInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeContactName"))
                            {
                                if (StoreContactNameCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeContactName"]))
                                    {
                                        command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeContactName", StoreContactNameInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeContactPhone"))
                            {
                                if (StoreContactPhoneCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeContactPhone"]))
                                    {
                                        command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeContactPhone", StoreContactPhoneInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("storeContactEmail"))
                            {
                                if (StoreContactEmailCheckBox.Checked)
                                {
                                    if (String.IsNullOrEmpty(store["storeContactEmail"]))
                                    {
                                        command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                                    }
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@storeContactEmail", StoreContactEmailInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("goodsReceiptNo"))
                            {
                                if (String.IsNullOrEmpty(receipt["goodsReceiptNo"]))
                                {
                                    command.Parameters.AddWithValue("@goodsReceiptNo", receiptNo);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@goodsReceiptNo", receipt["goodsReceiptNo"]);
                                }
                            }

                            if (((string[])Session["edit"]).Contains("invoiceNo"))
                            {
                                if (String.IsNullOrEmpty(InvoiceNoInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@invoiceNo", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@invoiceNo", InvoiceNoInput.Text?.Trim());
                                }

                            }

                            if (((string[])Session["edit"]).Contains("driver"))
                            {
                                if (String.IsNullOrEmpty(DriverInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@driver", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@driver", DriverInput.Text?.Trim());
                                }

                            }

                            if (((string[])Session["edit"]).Contains("receivedBy"))
                            {
                                if (String.IsNullOrEmpty(ReceivedByInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@receivedBy", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@receivedBy", ReceivedByInput.Text?.Trim());
                                }
                            }

                            if (((string[])Session["edit"]).Contains("receivedDate"))
                            {
                                if (String.IsNullOrEmpty(ReceivedDateInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@receivedDate", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@receivedDate", ReceivedDateInput.Text?.Trim());
                                }
                            }

                            if (((string[])Session["edit"]).Contains("totalQty"))
                            {
                                if (TotalQtyCheckBox.Checked)
                                {
                                    command.Parameters.AddWithValue("@totalQty", totalQty);
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@totalQty", 0);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@totalQty", TotalQtyInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("totalPrice"))
                            {
                                if (TotalPriceCheckBox.Checked)
                                {
                                    command.Parameters.AddWithValue("@totalPrice", totalPrice);
                                }
                                else
                                {
                                    if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@totalPrice", 0);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@totalPrice", TotalPriceInput.Text?.Trim());
                                    }
                                }
                            }

                            if (((string[])Session["edit"]).Contains("goodsReceiptNotes"))
                            {
                                if (String.IsNullOrEmpty(GoodsReceiptNotesInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@goodsReceiptNotes", DBNull.Value);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@goodsReceiptNotes", GoodsReceiptNotesInput.Text?.Trim());
                                }
                            }

                            command.Transaction = transaction;
                            command.ExecuteNonQuery();

                            if (updates.Rows.Count > 0)
                            {
                                command = conn.CreateCommand();
                                command.CommandText = @"CREATE TABLE #temp_update (goods_receipt_item_id INT, goods_receipt_id INT, item_detail_id INT,
                                item_json VARCHAR(MAX), item_qty INT, item_price DECIMAL(13,2));";
                                command.Transaction = transaction;
                                command.ExecuteNonQuery();

                                SqlBulkCopy bulkupdate = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                                bulkupdate.DestinationTableName = "#temp_update";

                                bulkupdate.ColumnMappings.Add("goods_receipt_item_id", "goods_receipt_item_id");
                                bulkupdate.ColumnMappings.Add("goods_receipt_id", "goods_receipt_id");
                                bulkupdate.ColumnMappings.Add("item_detail_id", "item_detail_id");
                                bulkupdate.ColumnMappings.Add("item_json", "item_json");
                                bulkupdate.ColumnMappings.Add("item_qty", "item_qty");
                                bulkupdate.ColumnMappings.Add("item_price", "item_price");
                                bulkupdate.WriteToServer(updates);

                                command = conn.CreateCommand();
                                command.CommandText = @"UPDATE orig SET item_json = temp.item_json, item_qty = temp.item_qty, 
                                item_price = temp.item_price, modified_date = getdate(), modified_by = @user
                                FROM goods_receipt_items orig 
                                INNER JOIN #temp_update temp ON orig.goods_receipt_item_id = temp.goods_receipt_item_id AND
                                orig.goods_receipt_id = temp.goods_receipt_id AND orig.item_detail_id = temp.item_detail_id
                                WHERE orig.isdeleted = 0;
                                DROP TABLE #temp_update;";
                                command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                command.Transaction = transaction;
                                command.ExecuteNonQuery();
                            }

                            transaction.Commit();

                            updates.Rows.Clear();
                            ViewState["updates"] = updates;
                            items.Rows.Clear();
                            ViewState["items"] = items;

                            LoadForm();
                            FillAutoSupplierInput();
                            FillAutoStoreInput();
                            FillAutoReceiptInput();
                            BindItemGridview();

                            AlertLabel.Text = "Data berhasil disimpan";
                            AlertLabel.CssClass = "alert alert-light-primary color-primary d-block";
                        }
                        catch (Exception error)
                        {
                            transaction.Rollback();

                            AlertLabel.Text = error.Message.ToString();
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                        conn.Close();
                    }
                    else
                    {
                        AlertLabel.Text = String.Join("<br>", errors.ToArray());
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Dokumen penerimaan terkunci";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                
            }
        }

        protected void ItemGridView_RowCancelingEdit(Object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void ItemGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }
        
        protected void SupplierNameCheckBoxCheckedChanged()
        {
            if (SupplierNameCheckBox.Checked)
            {
                SupplierNameInput.Text = supplier["supplierName"];
                SupplierNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                {
                    SupplierNameInput.Text = supplier["supplierName"];
                }

                SupplierNameInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierNameInput.Focus();
        }

        protected void SupplierBuildingNameCheckBoxCheckedChanged()
        {
            if (SupplierBuildingNameCheckBox.Checked)
            {
                SupplierBuildingNameInput.Text = supplier["supplierBuildingName"];
                SupplierBuildingNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                {
                    SupplierBuildingNameInput.Text = supplier["supplierBuildingName"];
                }
                SupplierBuildingNameInput.Attributes.Remove("disabled");
            }
        }

        protected void SupplierBuildingNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierBuildingNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierBuildingNameInput.Focus();
        }

        protected void SupplierStreetNameCheckBoxCheckedChanged()
        {
            if (SupplierStreetNameCheckBox.Checked)
            {
                SupplierStreetNameInput.Text = supplier["supplierStreetName"];
                SupplierStreetNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                {
                    SupplierStreetNameInput.Text = supplier["supplierStreetName"];
                }
                SupplierStreetNameInput.Attributes.Remove("disabled");
            }
        }

        protected void SupplierStreetNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierStreetNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierStreetNameInput.Focus();
        }

        protected void SupplierNeighbourhoodCheckBoxCheckedChanged()
        {
            if (SupplierNeighbourhoodCheckBox.Checked)
            {
                SupplierNeighbourhoodInput.Text = supplier["supplierNeighbourhood"];
                SupplierNeighbourhoodInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                {
                    SupplierNeighbourhoodInput.Text = supplier["supplierNeighbourhood"];
                }
                SupplierNeighbourhoodInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierNeighbourhoodCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierNeighbourhoodCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierNeighbourhoodInput.Focus();
        }

        protected void SupplierSubdistrictCheckBoxCheckedChanged()
        {
            if (SupplierSubdistrictCheckBox.Checked)
            {
                SupplierSubdistrictInput.Text = supplier["supplierSubdistrict"];
                SupplierSubdistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                {
                    SupplierSubdistrictInput.Text = supplier["supplierSubdistrict"];
                }
                SupplierSubdistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierSubdistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierSubdistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierSubdistrictInput.Focus();
        }

        protected void SupplierDistrictCheckBoxCheckedChanged()
        {
            if (SupplierDistrictCheckBox.Checked)
            {
                SupplierDistrictInput.Text = supplier["supplierDistrict"];
                SupplierDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                {
                    SupplierDistrictInput.Text = supplier["supplierDistrict"];
                }
                SupplierDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierDistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierDistrictInput.Focus();
        }

        protected void SupplierRuralDistrictCheckBoxCheckedChanged()
        {
            if (SupplierRuralDistrictCheckBox.Checked)
            {
                SupplierRuralDistrictInput.Text = supplier["supplierRuralDistrict"];
                SupplierRuralDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                {
                    SupplierRuralDistrictInput.Text = supplier["supplierRuralDistrict"];
                }
                SupplierRuralDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierRuralDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierRuralDistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierRuralDistrictInput.Focus();
        }

        protected void SupplierProvinceCheckBoxCheckedChanged()
        {
            if (SupplierProvinceCheckBox.Checked)
            {
                SupplierProvinceInput.Text = supplier["supplierProvince"];
                SupplierProvinceInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                {
                    SupplierProvinceInput.Text = supplier["supplierProvince"];
                }
                SupplierProvinceInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierProvinceCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierProvinceCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierProvinceInput.Focus();
        }

        protected void SupplierZipcodeCheckBoxCheckedChanged()
        {
            if (SupplierZipcodeCheckBox.Checked)
            {
                SupplierZipcodeInput.Text = supplier["supplierZipcode"];
                SupplierZipcodeInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                {
                    SupplierZipcodeInput.Text = supplier["supplierZipcode"];
                }
                SupplierZipcodeInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierZipcodeCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierZipcodeCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierZipcodeInput.Focus();
        }

        protected void SupplierContactNameCheckBoxCheckedChanged()
        {
            if (SupplierContactNameCheckBox.Checked)
            {
                SupplierContactNameInput.Text = supplier["supplierContactName"];
                SupplierContactNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                {
                    SupplierContactNameInput.Text = supplier["supplierContactName"];
                }
                SupplierContactNameInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierContactNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierContactNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierContactNameInput.Focus();
        }

        protected void SupplierContactPhoneCheckBoxCheckedChanged()
        {
            if (SupplierContactPhoneCheckBox.Checked)
            {
                SupplierContactPhoneInput.Text = supplier["supplierContactPhone"];
                SupplierContactPhoneInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                {
                    SupplierContactPhoneInput.Text = supplier["supplierContactPhone"];
                }

                SupplierContactPhoneInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierContactPhoneCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierContactPhoneCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierContactPhoneInput.Focus();
        }

        protected void SupplierContactEmailCheckBoxCheckedChanged()
        {
            if (SupplierContactEmailCheckBox.Checked)
            {
                SupplierContactEmailInput.Text = supplier["supplierContactEmail"];
                SupplierContactEmailInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                {
                    SupplierContactEmailInput.Text = supplier["supplierContactEmail"];
                }
                SupplierContactEmailInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierContactEmailCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierContactEmailCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            SupplierContactEmailInput.Focus();
        }

        protected void StoreNameCheckBoxCheckedChanged()
        {
            if (StoreNameCheckBox.Checked)
            {
                StoreNameInput.Text = store["storeName"];
                StoreNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                {
                    StoreNameInput.Text = store["storeName"];
                }

                StoreNameInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreNameInput.Focus();
        }

        protected void StoreBuildingNameCheckBoxCheckedChanged()
        {
            if (StoreBuildingNameCheckBox.Checked)
            {
                StoreBuildingNameInput.Text = store["storeBuildingName"];
                StoreBuildingNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                {
                    StoreBuildingNameInput.Text = store["storeBuildingName"];
                }
                StoreBuildingNameInput.Attributes.Remove("disabled");
            }
        }

        protected void StoreBuildingNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreBuildingNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreBuildingNameInput.Focus();
        }

        protected void StoreStreetNameCheckBoxCheckedChanged()
        {
            if (StoreStreetNameCheckBox.Checked)
            {
                StoreStreetNameInput.Text = store["storeStreetName"];
                StoreStreetNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                {
                    StoreStreetNameInput.Text = store["storeStreetName"];
                }
                StoreStreetNameInput.Attributes.Remove("disabled");
            }
        }

        protected void StoreStreetNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreStreetNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreStreetNameInput.Focus();
        }

        protected void StoreNeighbourhoodCheckBoxCheckedChanged()
        {
            if (StoreNeighbourhoodCheckBox.Checked)
            {
                StoreNeighbourhoodInput.Text = store["storeNeighbourhood"];
                StoreNeighbourhoodInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                {
                    StoreNeighbourhoodInput.Text = store["storeNeighbourhood"];
                }
                StoreNeighbourhoodInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreNeighbourhoodCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreNeighbourhoodCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreNeighbourhoodInput.Focus();
        }

        protected void StoreSubdistrictCheckBoxCheckedChanged()
        {
            if (StoreSubdistrictCheckBox.Checked)
            {
                StoreSubdistrictInput.Text = store["storeSubdistrict"];
                StoreSubdistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                {
                    StoreSubdistrictInput.Text = store["storeSubdistrict"];
                }
                StoreSubdistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreSubdistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreSubdistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreSubdistrictInput.Focus();
        }

        protected void StoreDistrictCheckBoxCheckedChanged()
        {
            if (StoreDistrictCheckBox.Checked)
            {
                StoreDistrictInput.Text = store["storeDistrict"];
                StoreDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                {
                    StoreDistrictInput.Text = store["storeDistrict"];
                }
                StoreDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreDistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreDistrictInput.Focus();
        }

        protected void StoreRuralDistrictCheckBoxCheckedChanged()
        {
            if (StoreRuralDistrictCheckBox.Checked)
            {
                StoreRuralDistrictInput.Text = store["storeRuralDistrict"];
                StoreRuralDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                {
                    StoreRuralDistrictInput.Text = store["storeRuralDistrict"];
                }
                StoreRuralDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreRuralDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreRuralDistrictCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreRuralDistrictInput.Focus();
        }

        protected void StoreProvinceCheckBoxCheckedChanged()
        {
            if (StoreProvinceCheckBox.Checked)
            {
                StoreProvinceInput.Text = store["storeProvince"];
                StoreProvinceInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                {
                    StoreProvinceInput.Text = store["storeProvince"];
                }
                StoreProvinceInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreProvinceCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreProvinceCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreProvinceInput.Focus();
        }

        protected void StoreZipcodeCheckBoxCheckedChanged()
        {
            if (StoreZipcodeCheckBox.Checked)
            {
                StoreZipcodeInput.Text = store["storeZipcode"];
                StoreZipcodeInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                {
                    StoreZipcodeInput.Text = store["storeZipcode"];
                }
                StoreZipcodeInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreZipcodeCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreZipcodeCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreZipcodeInput.Focus();
        }

        protected void StoreContactNameCheckBoxCheckedChanged()
        {
            if (StoreContactNameCheckBox.Checked)
            {
                StoreContactNameInput.Text = store["storeContactName"];
                StoreContactNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                {
                    StoreContactNameInput.Text = store["storeContactName"];
                }
                StoreContactNameInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreContactNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreContactNameCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreContactNameInput.Focus();
        }

        protected void StoreContactPhoneCheckBoxCheckedChanged()
        {
            if (StoreContactPhoneCheckBox.Checked)
            {
                StoreContactPhoneInput.Text = store["storeContactPhone"];
                StoreContactPhoneInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                {
                    StoreContactPhoneInput.Text = store["storeContactPhone"];
                }

                StoreContactPhoneInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreContactPhoneCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreContactPhoneCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreContactPhoneInput.Focus();
        }

        protected void StoreContactEmailCheckBoxCheckedChanged()
        {
            if (StoreContactEmailCheckBox.Checked)
            {
                StoreContactEmailInput.Text = store["storeContactEmail"];
                StoreContactEmailInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                {
                    StoreContactEmailInput.Text = store["storeContactEmail"];
                }
                StoreContactEmailInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreContactEmailCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreContactEmailCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            StoreContactEmailInput.Focus();
        }

        protected void GoodsReceiptNoCheckBoxCheckedChanged()
        {
            if (GoodsReceiptNoCheckBox.Checked)
            {
                GoodsReceiptNoInput.Text = receipt["goodsReceiptNo"];
                GoodsReceiptNoInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(GoodsReceiptNoInput.Text?.Trim()))
                {
                    GoodsReceiptNoInput.Text = receipt["goodsReceiptNo"];
                }
                GoodsReceiptNoInput.Attributes.Remove("disabled");
            }
        }

        protected void GoodsReceiptNoCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            TotalQtyCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            GoodsReceiptNoInput.Focus();
        }

        protected void TotalQtyCheckBoxCheckedChanged()
        {
            int total = 0;
            int qty = 0;
            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

            foreach (DataRow row in updates.Rows)
            {
                if (!String.IsNullOrEmpty(row["item_qty"].ToString()) && int.TryParse(row["item_qty"].ToString(), out qty))
                {
                    total += int.Parse(row["item_qty"].ToString());
                }
            }
            
            TotalQtyLabel.Text = total.ToString("###,###,###,###,##0");

            if (TotalQtyCheckBox.Checked)
            {
                TotalQtyInput.Text = total.ToString("###,###,###,###,##0");
                
                TotalQtyInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()))
                {
                    TotalQtyInput.Text = total.ToString("###,###,###,###,##0");
                }
                TotalQtyInput.Attributes.Remove("disabled");
            }
        }

        protected void TotalQtyCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            TotalQtyCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            TotalQtyInput.Focus();
        }

        protected void TotalPriceCheckBoxCheckedChanged()
        {
            double total = Math.Round(0.00, 2);
            int qty = 0;
            double price = 0;
            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

            foreach (DataRow row in updates.Rows)
            {
                if (!String.IsNullOrEmpty(row["item_price"].ToString()) && !String.IsNullOrEmpty(row["item_qty"].ToString()) &&
                    int.TryParse(row["item_qty"].ToString(), out qty) && double.TryParse(row["item_price"].ToString(), out price))
                {
                    total += int.Parse(row["item_qty"].ToString()) * double.Parse(row["item_price"].ToString());
                }    
            }

            total = Math.Round(total, 2);
            
            TotalPriceLabel.Text = total.ToString("###,###,###,###,##0.00");

            if (TotalPriceCheckBox.Checked)
            {
                TotalPriceInput.Text = total.ToString("###,###,###,###,##0.00");
                
                TotalPriceInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                {
                    TotalPriceInput.Text = total.ToString("###,###,###,###,##0.00");
                }
                TotalPriceInput.Attributes.Remove("disabled");
            }
        }

        protected void TotalPriceCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            TotalPriceCheckBoxCheckedChanged();

            //ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            TotalPriceInput.Focus();
        }
    }
}
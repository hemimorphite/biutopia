﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" MaintainScrollPositionOnPostBack="true"  AutoEventWireup="true" CodeBehind="GoodsReceipt.aspx.cs" Inherits="biutopia.GoodsReceipt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main-content">
        <div class="page-heading">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 order-md-1 order-last">
                        <h3>Penerimaan Barang</h3>
                        <p class="text-subtitle text-muted"></p>
                    </div>
                </div>
            </div>
            <section class="section">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <% if (((string[])Session["read"]).Length > 0)
                                { %>
                            <li class="nav-item" role="presentation">
                                <asp:LinkButton ID="TabularTabButton" runat="server" OnClick="TabularTabButton_Click">
                                    <span class="nav-link active" id="tabularTab" data-bs-toggle="tab" role="tab"
                                    aria-controls="tabular" aria-selected="true">Penerimaan Barang</span>
                                </asp:LinkButton>
                                
                            </li>
                            <% } %>
                            <% if ((bool)Session["upload"] == true)
                                { %>
                            <li class="nav-item" role="presentation">
                                <asp:LinkButton ID="UploadTabButton" runat="server" OnClick="UploadTabButton_Click">
                                    <span class="nav-link" id="uploadTab" data-bs-toggle="tab" role="tab"
                                    aria-controls="upload" aria-selected="false">Unggah</span>
                                </asp:LinkButton>
                            </li>
                            <% } %>
                        </ul>
                        <div class="tab-content mt-4" id="myTabContent">
                            <div class="tab-pane fade show active" id="tabular" role="tabpanel" aria-labelledby="tabularTab">
                                <div class="row">
                                    <div class="col-12">
                                        <asp:Label ID="AlertLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreChoice">Toko Ritel</label>
                                            <asp:DropDownList ID="StoreChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="StoreChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierChoice">Pemasok</label>
                                            <asp:DropDownList ID="SupplierChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="SupplierChoice_SelectedIndexChanged" AutoPostBack="true">

                                            </asp:DropDownList>
                                        </div>
                                    </div> 
                                    <div class="col-12 mb-5"></div>
                                    <div class="col-12 col-md-6">
                                        
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <asp:Panel ID="SearchPanel" runat="server" CssClass="input-group mb-3" DefaultButton="SearchButton">
                                            <asp:DropDownList ID="FilterChoice" OnSelectedIndexChanged="FilterChoice_SelectedIndexChanged" style="min-width:150px;width:150px;text-overflow: ellipsis;color: #607080;padding: 0.375rem 0.75rem;border: 1px solid #dce7f1;background-color: #fff;" AutoPostBack="True" runat="server">
                                                
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="OperatorChoice" OnSelectedIndexChanged="OperatorChoice_SelectedIndexChanged" style="min-width:50px;width:50px;color: #607080;padding: 0.37rem 0.4rem;border: 1px solid #dce7f1;background-color: #fff;" AutoPostBack="True" runat="server">
                                                <asp:ListItem Selected="True" Value="61">&#61;</asp:ListItem>
                                                <asp:ListItem Value="8596">&#8596;</asp:ListItem>
                                                <asp:ListItem Value="62">&gt;</asp:ListItem>
                                                <asp:ListItem Value="60">&lt;</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="ValueOneInput" CssClass="form-control" runat="server" placeholder="" onfocus="this.select();"></asp:TextBox>
                                            <asp:TextBox ID="ValueTwoInput" CssClass="form-control" runat="server" placeholder="" onfocus="this.select();"></asp:TextBox>
                                            <asp:LinkButton ID="SearchButton" CssClass="btn btn-primary align-middle" runat="server" OnCommand="SearchButton_Command">
                                                <i class="bi bi-search"></i>
                                            </asp:LinkButton>
                                        </asp:Panel>
                                    </div> 
                                    <div class="col-12">
                                        <div class="overflow-auto mb-4">
                                            <% if (((string[])Session["read"]).Length > 0)
                                                { %>
                                            <asp:GridView ID="GoodsReceiptGridView" CssClass="table table-striped" runat="server" ShowHeaderWhenEmpty="True" Autogeneratecolumns="false" AllowSorting="True" OnSorting="GoodsReceiptGridView_Sorting" AllowPaging="True" PageSize="5" OnPageIndexChanging="GoodsReceiptGridView_PageIndexChanging" OnRowDeleting="GoodsReceiptGridView_RowDeleting" OnRowCancelingEdit="GoodsReceiptGridView_RowCancelingEdit" OnRowEditing="GoodsReceiptGridView_RowEditing" OnRowDataBound="GoodsReceiptGridView_RowDataBound">
                                                <%--<Columns>
                                                    <asp:BoundField HeaderText="No" DataField="no" SortExpression="no" ReadOnly="true" ItemStyle-CssClass="text-nowrap"/>
                                                    <asp:BoundField HeaderText="No Penerimaan" DataField="goods_receipt_no" SortExpression="goods_receipt_no" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="No Pengiriman" DataField="delivery_no" SortExpression="delivery_no" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="No Pemesanan" DataField="purchase_no" SortExpression="purchase_no" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Toko Ritel" DataField="store_name" SortExpression="store_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Pemasok" DataField="supplier_name" SortExpression="supplier_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Diterima Oleh" DataField="received_by" SortExpression="received_by" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Tanggal Penerimaan" DataField="received_date" SortExpression="received_date" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Supir" DataField="driver" SortExpression="driver" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="No Surat Jalan" DataField="invoice_no" SortExpression="invoice_no" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Jumlah Barang" DataField="total_qty" SortExpression="total_qty" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Total Harga" DataField="total_price" SortExpression="total_price" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    
                                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="text-nowrap">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="ShowButton" CssClass="btn btn-primary" runat="server" CommandName="show" CommandArgument='<%# Eval("goods_receipt_id") + ";" + Eval("goods_receipt_uuid")  %>' OnCommand="ShowButton_Command">Lihat</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Colum--%>
                                                <PagerTemplate>
                                                    <div class="pagination justify-content-center">
                                                        <asp:Panel ID="FirstPanel" CssClass="page-item me-2" runat="server">
                                                            <asp:LinkButton ID="FirstButton" CssClass="page-link" runat="server" OnClick="FirstButton_Click">First</asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="PreviousPanel" CssClass="page-item me-2" runat="server">
                                                            <asp:LinkButton ID="PreviousButton" CssClass="page-link" runat="server" OnClick="PreviousButton_Click">Previous</asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="PagePanel" CssClass="page-item me-2" runat="server">
                                                            <asp:DropDownList ID="PageDropDownList" class="page-link" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PageDropDownList_SelectedIndexChanged">
                                                        
                                                            </asp:DropDownList>
                                                        </asp:Panel>
                                                        <asp:Panel ID="NextPanel" CssClass="page-item me-2" runat="server">
                                                            <asp:LinkButton ID="NextButton" CssClass="page-link" runat="server" OnClick="NextButton_Click">Next</asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="LastPanel" CssClass="page-item" runat="server">
                                                            <asp:LinkButton ID="LastButton" CssClass="page-link" runat="server" OnClick="LastButton_Click">Last</asp:LinkButton>
                                                        </asp:Panel>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" />
                                            </asp:GridView>
                                            <% } %>
                                        </div>
                                        <asp:Panel ID="LoadingPanel" CssClass="d-none mb-2" runat="server">
                                            <div class="spinner-grow spinner-grow-sm text-primary" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-secondary" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-success" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-danger" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-warning" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-info" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-light" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-dark" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <span>Loading</span>
                                        </asp:Panel>

                                        <asp:Button ID="DownloadAllButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Semua" CommandName="downloadall" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:Button ID="DownloadSelectedSupplierButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Berdasarkan Vendor" CommandName="downloadselectedsupplier" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:Button ID="DownloadSelectedStoreButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Berdasarkan Toserba" CommandName="downloadselectedstore" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:Button ID="DownloadSelectedStoreAndSupplierButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Berdasarkan Toserba dan Vendor" CommandName="downloadselectedstoreandsupplier" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:HiddenField ID="FilenameInput" runat="server" />
                                        <div class="mb-5"></div>
                                    </div> 
                                </div>
                            </div>
                            <% if ((bool)Session["upload"] == true)
                                { %>
                            <div class="tab-pane fade" id="upload" role="tabpanel" aria-labelledby="uploadTab">
                                
                            </div>
                            <% } %>
                        </div>
                    </div>
                </div>
                <% if ((bool)Session["delete"] == true)
                    { %>
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="confirmModalTitle">Konfirmasi</h5>
                                <button type="button" data-bs-dismiss="modal" class="btn btn-link" aria-label="Close">
                                    <i class="bi bi-x-lg"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Apakah ingin menghapus dokumen penerimaan no <asp:Label ID="GoodsReceiptNoModalLabel" runat="server" Text=""></asp:Label> ?</p>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="NoButton" CssClass="btn btn-secondary" runat="server" Text="Tidak" OnClick="NoButton_Click" />
                                                    
                                <asp:Button ID="YesButton" CssClass="btn btn-primary ms-1" runat="server" Text="Ya" CommandName="delete" OnCommand ="YesButton_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
            </section>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <script type="text/javascript">
        function selectUploadTab() {
            var tab = new bootstrap.Tab(document.querySelector('#uploadTab'));
            tab.show();
        }
        function selectTabularTab() {
            var tab = new bootstrap.Tab(document.querySelector('#tabularTab'));
            tab.show();
        }
        function openConfirmModal() {
            var modal = new bootstrap.Modal(document.getElementById('confirmModal'));
            modal.show();
        }

        function download() {
            var input = document.getElementById("<%= FilenameInput.ClientID %>");
            var link = document.createElement("a");
            link.download = input.value;
            link.href = window.location.protocol + '//' + window.location.host + "/uploads/" + input.value;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            delete link;
        }


        var DownloadAllButton = document.getElementById("<%= DownloadAllButton.ClientID %>");
        var DownloadSelectedSupplierButton = document.getElementById("<%= DownloadSelectedSupplierButton.ClientID %>");
        var DownloadSelectedStoreButton = document.getElementById("<%= DownloadSelectedStoreButton.ClientID %>");
        var DownloadSelectedStoreAndSupplierButton = document.getElementById("<%= DownloadSelectedStoreAndSupplierButton.ClientID %>");
        var LoadingPanel = document.getElementById("<%= LoadingPanel.ClientID %>");

        DownloadAllButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
        DownloadSelectedSupplierButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
        DownloadSelectedStoreButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
        DownloadSelectedStoreAndSupplierButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
    </script>
</asp:Content>
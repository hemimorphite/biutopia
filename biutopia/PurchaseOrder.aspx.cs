﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;
using System.Collections;
using System.Data.OleDb;

namespace biutopia
{
    public partial class PurchaseOrder : System.Web.UI.Page
    {
        protected static string menu = "tabular";

        protected static Dictionary<string, string> store = new Dictionary<string, string>()
        {
            { "storeId", DBNull.Value.ToString() },
            { "storeName", DBNull.Value.ToString() },
            { "storeBuildingName", DBNull.Value.ToString() },
            { "storeStreetName", DBNull.Value.ToString() },
            { "storeNeighbourhood", DBNull.Value.ToString() },
            { "storeSubdistrict", DBNull.Value.ToString() },
            { "storeDistrict", DBNull.Value.ToString() },
            { "storeRuralDistrict", DBNull.Value.ToString() },
            { "storeProvince", DBNull.Value.ToString() },
            { "storeZipcode", DBNull.Value.ToString() },
            { "storeContactName", DBNull.Value.ToString() },
            { "storeContactPhone", DBNull.Value.ToString() },
            { "storeContactEmail", DBNull.Value.ToString() }
        };

        protected static Dictionary<string, string> supplier = new Dictionary<string, string>()
        {
            { "supplierId", DBNull.Value.ToString() },
            { "supplierName", DBNull.Value.ToString() },
            { "supplierBuildingName", DBNull.Value.ToString() },
            { "supplierStreetName", DBNull.Value.ToString() },
            { "supplierNeighbourhood", DBNull.Value.ToString() },
            { "supplierSubdistrict", DBNull.Value.ToString() },
            { "supplierDistrict", DBNull.Value.ToString() },
            { "supplierRuralDistrict", DBNull.Value.ToString() },
            { "supplierProvince", DBNull.Value.ToString() },
            { "supplierZipcode", DBNull.Value.ToString() },
            { "supplierContactName", DBNull.Value.ToString() },
            { "supplierContactPhone", DBNull.Value.ToString() },
            { "supplierContactEmail", DBNull.Value.ToString() }
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["username"]) || string.IsNullOrEmpty((string)Session["role_id"]))
            {
                Session.Clear();
                Response.Redirect("Login.aspx");
            }

            SupplierNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierBuildingNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierStreetNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierNeighbourhoodCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierSubdistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierRuralDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierProvinceCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierZipcodeCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierContactNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierContactPhoneCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            SupplierContactEmailCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreBuildingNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreStreetNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreNeighbourhoodCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreSubdistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreRuralDistrictCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreProvinceCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreZipcodeCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreContactNameCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreContactPhoneCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            StoreContactEmailCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            PurchaseNoCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            PurchaseDateCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            //DeliveryNoCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            RequestedByCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            RequestedDateCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            ApprovalByCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            ApprovalDateCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            TotalQtyCheckBox.InputAttributes.Add("class", "form-check-input mt-0");
            TotalPriceCheckBox.InputAttributes.Add("class", "form-check-input mt-0");

            List<string> pagesList = new List<string>();
            Session["stores"] = new int[0];
            Session["suppliers"] = new int[0];
            Session["auto"] = new string[0];
            Session["add"] = new string[0];
            Session["edit"] = new string[0];
            Session["read"] = new string[0];
            Session["delete"] = false;
            Session["upload"] = false;
            Session["print"] = false;
            Session["pages"] = new string[0];

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT permissions FROM roles WHERE role_id = @roleId";
                    cmd.Parameters.AddWithValue("@roleId", Session["role_id"]);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("permissions")))
                                {
                                    Newtonsoft.Json.Linq.JObject pagepermissions = Newtonsoft.Json.Linq.JObject.Parse(reader.GetString(reader.GetOrdinal("permissions")).Trim());
                                    Newtonsoft.Json.Linq.JObject accessRights = Newtonsoft.Json.Linq.JObject.Parse(Session["access_rights"].ToString());
                                    Newtonsoft.Json.Linq.JArray jstores = (Newtonsoft.Json.Linq.JArray)accessRights["stores"];

                                    int[] stores = new int[jstores.Count];

                                    int a = 0;
                                    foreach (int store in jstores)
                                    {
                                        stores[a++] = store;
                                    }

                                    Session["stores"] = stores;

                                    Newtonsoft.Json.Linq.JArray jsuppliers = (Newtonsoft.Json.Linq.JArray)accessRights["suppliers"];

                                    int[] suppliers = new int[jsuppliers.Count];

                                    int b = 0;
                                    foreach (int supplier in jsuppliers)
                                    {
                                        suppliers[b++] = supplier;
                                    }

                                    Session["suppliers"] = suppliers;

                                    Newtonsoft.Json.Linq.JArray pages = (Newtonsoft.Json.Linq.JArray)pagepermissions["pages"];

                                    for (int i = 0; i < pages.Count; i++)
                                    {
                                        Newtonsoft.Json.Linq.JObject page = (Newtonsoft.Json.Linq.JObject)pages[i];
                                        Newtonsoft.Json.Linq.JArray permissions = (Newtonsoft.Json.Linq.JArray)page["permissions"];

                                        if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                        {
                                            pagesList.Add((string)page["name"]);
                                        }

                                        if ((string)page["name"] == "purchaseOrder")
                                        {
                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[0])["auto"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[0])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["auto"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[1])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["read"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[2])["add"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[2])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["add"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[3])["edit"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[3])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["edit"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[4])["delete"])
                                            {
                                                Session["delete"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[5])["upload"])
                                            {
                                                Session["upload"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[6])["print"])
                                            {
                                                Session["print"] = true;
                                            }
                                        }
                                    }

                                    Session["pages"] = pagesList.ToArray();
                                }
                            }
                        }
                    }
                    conn.Close();

                    if (((string[])Session["read"]).Length == 0)
                    {
                        Response.Redirect("Forbidden.aspx");
                    }
                }
            }

            if (!IsPostBack)
            {
                menu = "tabular";

                SaveUploadButton.Visible = false;
                CancelUploadButton.Visible = false;
                InfoPanel.Visible = false;
                ValueTwoInput.Visible = false;

                System.Data.DataTable purchases = new System.Data.DataTable("purchases");

                purchases.Columns.Add("no", typeof(Int32));
                purchases.Columns.Add("purchase_order_id", typeof(Int32));
                purchases.Columns.Add("uuid", typeof(String));

                if (((string[])Session["read"]).Contains("supplier"))
                {
                    purchases.Columns.Add("supplier_name", typeof(String));
                }

                if (((string[])Session["read"]).Contains("store"))
                {
                    purchases.Columns.Add("store_name", typeof(String));
                }

                if (((string[])Session["read"]).Contains("purchaseNo"))
                {
                    purchases.Columns.Add("purchase_no", typeof(String));
                }

                if (((string[])Session["read"]).Contains("purchaseDate"))
                {
                    purchases.Columns.Add("purchase_date", typeof(DateTime));
                }

                if (((string[])Session["read"]).Contains("deliveryOrder"))
                {
                    purchases.Columns.Add("delivery_no", typeof(String));
                }

                //if (((string[])Session["read"]).Contains("deliveryDate"))
                //{
                //    purchases.Columns.Add("delivery_date", typeof(DateTime));
                //}

                if (((string[])Session["read"]).Contains("requestedBy"))
                {
                    purchases.Columns.Add("requested_by", typeof(String));
                }

                if (((string[])Session["read"]).Contains("requestedDate"))
                {
                    purchases.Columns.Add("requested_date", typeof(DateTime));
                }

                if (((string[])Session["read"]).Contains("totalQty"))
                {
                    purchases.Columns.Add("total_qty", typeof(int));
                }

                if (((string[])Session["read"]).Contains("totalPrice"))
                {
                    purchases.Columns.Add("total_price", typeof(double));
                }

                if (((string[])Session["read"]).Contains("approvalBy"))
                {
                    purchases.Columns.Add("approval_by", typeof(String));
                }

                if (((string[])Session["read"]).Contains("approvalDate"))
                {
                    purchases.Columns.Add("approval_date", typeof(DateTime));
                }

                if (((string[])Session["read"]).Contains("purchaseStatus"))
                {
                    purchases.Columns.Add("purchase_status", typeof(String));
                }

                purchases.Columns.Add("locked", typeof(Int32));
                purchases.Columns.Add("goods_receipt_no", typeof(String));

                ViewState["purchases"] = purchases;
                
                System.Data.DataTable items = new System.Data.DataTable("items");

                if (((string[])Session["read"]).Contains("item"))
                {
                    items.Columns.Add("no", typeof(Int32));
                    items.Columns.Add("item_detail_id", typeof(Int32));
                    items.Columns.Add("item_barcode", typeof(String));
                    items.Columns.Add("item_name", typeof(String));
                    items.Columns.Add("short_description", typeof(String));
                    items.Columns.Add("color", typeof(String));
                    items.Columns.Add("size", typeof(String));
                    items.Columns.Add("variants", typeof(String));
                    items.Columns.Add("brand_name", typeof(String));
                    items.Columns.Add("supplier_product_code", typeof(String));
                    items.Columns.Add("item_qty", typeof(Int32));
                    items.Columns.Add("item_price", typeof(Double));
                }

                ViewState["items"] = items;
                //inserts.Columns.Add("created_date");
                //inserts.Columns.Add("created_by");
                //inserts.Columns.Add("modified_date");
                //inserts.Columns.Add("modified_by");

                System.Data.DataTable inserts = new System.Data.DataTable("inserts");

                if (((string[])Session["add"]).Contains("item"))
                {
                    inserts.Columns.Add("item_detail_id", typeof(Int32));
                    inserts.Columns.Add("item_json", typeof(String));
                    inserts.Columns.Add("item_qty", typeof(Int32));
                    inserts.Columns.Add("item_price", typeof(Double));
                }
                ViewState["inserts"] = inserts;

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                string commandText = "";

                if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                {
                    commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers";
                } else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                {
                    int[] suppliers = (int[])Session["suppliers"];

                    commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers 
                        WHERE supplier_id IN ({0})";

                    string inClause = string.Join(",", suppliers);
                    commandText = string.Format(commandText, inClause);
                }

                if(!String.IsNullOrEmpty(commandText))
                {
                    cmd.CommandText = commandText;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    SupplierChoice.DataSource = ds;
                    SupplierChoice.DataTextField = "supplier_name";
                    SupplierChoice.DataValueField = "supplier_id";
                    SupplierChoice.DataBind();
                    SupplierChoice.Items.Insert(0, new ListItem("Pilih Vendor...", ""));

                    SupplierUploadChoice.DataSource = ds;
                    SupplierUploadChoice.DataTextField = "supplier_name";
                    SupplierUploadChoice.DataValueField = "supplier_id";
                    SupplierUploadChoice.DataBind();

                    ds.Dispose();

                    LoadSupplier();    
                }
                
                if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                {
                    commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores";
                }
                else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                {
                    int[] stores = (int[])Session["stores"];
                    commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores 
                        WHERE store_id IN ({0})";

                    string inClause = string.Join(",", stores);

                    commandText = string.Format(commandText, inClause);
                }

                if(!String.IsNullOrEmpty(commandText))
                {
                    cmd = conn.CreateCommand();
                    cmd.CommandText = commandText;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    StoreChoice.DataSource = ds;
                    StoreChoice.DataTextField = "store_name";
                    StoreChoice.DataValueField = "store_id";
                    StoreChoice.DataBind();
                    StoreChoice.Items.Insert(0, new ListItem("Pilih Toko Ritel...", ""));

                    StoreUploadChoice.DataSource = ds;
                    StoreUploadChoice.DataTextField = "store_name";
                    StoreUploadChoice.DataValueField = "store_id";
                    StoreUploadChoice.DataBind();

                    ds.Dispose();

                    LoadStore();    
                }

                if (Session["redirect"] != null && (bool)Session["redirect"] == true)
                {
                    if (Session["origin"] != null && Session["origin"].ToString() == "PurchaseOrder")
                    {
                        StoreChoice.SelectedValue = Session["storevalue"].ToString();
                        SupplierChoice.SelectedValue = Session["suppliervalue"].ToString();
                        ViewState["page"] = Session["pagevalue"].ToString();
                        ViewState["sort"] = Session["sortorder"].ToString();
                        ViewState["column"] = Session["columnname"].ToString();
                        ViewState["search"] = Session["searchquery"].ToString();
                        Session["redirect"] = null;
                        Session["origin"] = null;
                        Session["pagevalue"] = null;
                        Session["sortorder"] = null;
                        Session["columnname"] = null;
                        Session["searchquery"] = null;
                    }
                    else
                    {
                        ViewState["page"] = 0;
                        ViewState["sort"] = "ASC";
                        ViewState["column"] = "no";
                        ViewState["search"] = "";
                        Session["redirect"] = null;
                        Session["origin"] = null;
                        Session["pagevalue"] = null;
                        Session["sortorder"] = null;
                        Session["columnname"] = null;
                        Session["searchquery"] = null;
                    }
                }
                else
                {
                    ViewState["page"] = 0;
                    ViewState["sort"] = "ASC";
                    ViewState["column"] = "no";
                    ViewState["search"] = "";
                    Session["redirect"] = null;
                    Session["origin"] = null;
                    Session["pagevalue"] = null;
                    Session["sortorder"] = null;
                    Session["columnname"] = null;
                    Session["searchquery"] = null;
                }

                CreatePurchaseOrderGridView();
                BindPurchaseOrderGridView();
                CreateFilterChoice();

                FillStoreInput();
                FillSupplierInput();
                FillPurchaseInput();
            }
            else
            {
                if (ViewState["items"] == null || ViewState["items"].Equals("-1") ||
                    ViewState["inserts"] == null || ViewState["inserts"].Equals("-1") ||
                    ViewState["purchases"] == null || ViewState["purchases"].Equals("-1"))
                {   
                    Response.Redirect("Forbidden.aspx");
                }

                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
                System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];

                if (items.Columns.Count <= 0 || inserts.Columns.Count <= 0 || purchases.Columns.Count <= 0)
                {    
                    Response.Redirect("Forbidden.aspx");
                }
                
                if (GetPostBackControlName() != "PageDropDownList")
                {
                    PurchaseOrderGridView.DataSource = purchases;
                    PurchaseOrderGridView.DataBind();
                }
                    
                FillStoreInput();
                FillSupplierInput();
                FillPurchaseInput();

                AlertUploadLabel.Text = "";
                AlertUploadLabel.CssClass = "";
                AlertModalLabel.Text = "";
                AlertModalLabel.CssClass = "";
                AlertLabel.Text = "";
                AlertLabel.CssClass = "";
            }
        }

        protected string GetPostBackControlName()
        {
            Control control = null;

            string ctrlname = Page.Request.Params["__EVENTTARGET"];

            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);

                if (control != null)
                {
                    return control.ID;
                }
                return "";
            }
            else
            {
                return "";
            }
        }

        protected void LoadStore()
        {
            int number;

            if (int.TryParse(StoreUploadChoice.SelectedValue, out number))
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                cmd.CommandText = @"SELECT store_id
                                    ,store_name
                                    ,store_building_name
                                    ,store_street_name
                                    ,store_neighbourhood
                                    ,store_subdistrict
                                    ,store_district
                                    ,store_rural_district
                                    ,store_province
                                    ,store_zipcode
                                    FROM stores
                                    WHERE store_id = @storeId";

                cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreUploadChoice.SelectedValue));
                cmd.Connection = conn;

                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("store_id")))
                        {
                            store["storeId"] = reader["store_id"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_name")))
                        {
                            store["storeName"] = reader["store_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_building_name")))
                        {
                            store["storeBuildingName"] = reader["store_building_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_street_name")))
                        {
                            store["storeStreetName"] = reader["store_street_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_neighbourhood")))
                        {
                            store["storeNeighbourhood"] = reader["store_neighbourhood"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_subdistrict")))
                        {
                            store["storeSubdistrict"] = reader["store_subdistrict"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_district")))
                        {
                            store["storeDistrict"] = reader["store_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_rural_district")))
                        {
                            store["storeRuralDistrict"] = reader["store_rural_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_province")))
                        {
                            store["storeProvince"] = reader["store_province"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("store_zipcode")))
                        {
                            store["storeZipcode"] = reader["store_zipcode"].ToString();
                            
                        }
                    }
                }
                conn.Close();
            }
        }

        protected void LoadSupplier()
        {
            int number;

            if (int.TryParse(SupplierUploadChoice.SelectedValue, out number))
            {
                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                cmd.CommandText = @"SELECT supplier_id
                                    ,supplier_name
                                    ,supplier_building_name
                                    ,supplier_street_name
                                    ,supplier_neighbourhood
                                    ,supplier_subdistrict
                                    ,supplier_district
                                    ,supplier_rural_district
                                    ,supplier_province
                                    ,supplier_zipcode
                                    FROM suppliers
                                    WHERE supplier_id = @supplierId";

                cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierUploadChoice.SelectedValue));
                cmd.Connection = conn;

                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_id")))
                        {
                            supplier["supplierId"] = reader["supplier_id"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_name")))
                        {
                            supplier["supplierName"] = reader["supplier_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_building_name")))
                        {
                            supplier["supplierBuildingName"] = reader["supplier_building_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_street_name")))
                        {
                            supplier["supplierStreetName"] = reader["supplier_street_name"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_neighbourhood")))
                        {
                            supplier["supplierNeighbourhood"] = reader["supplier_neighbourhood"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_subdistrict")))
                        {
                            supplier["supplierSubdistrict"] = reader["supplier_subdistrict"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_district")))
                        {
                            supplier["supplierDistrict"] = reader["supplier_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_rural_district")))
                        {
                            supplier["supplierRuralDistrict"] = reader["supplier_rural_district"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_province")))
                        {
                            supplier["supplierProvince"] = reader["supplier_province"].ToString();
                        }

                        if (!reader.IsDBNull(reader.GetOrdinal("supplier_zipcode")))
                        {
                            supplier["supplierZipcode"] = reader["supplier_zipcode"].ToString();
                        }

                    }
                }
                conn.Close();
            }
        }

        protected void FillStoreInput()
        {
            StoreNameCheckBoxCheckedChanged();
            StoreBuildingNameCheckBoxCheckedChanged();
            StoreStreetNameCheckBoxCheckedChanged();
            StoreNeighbourhoodCheckBoxCheckedChanged();
            StoreSubdistrictCheckBoxCheckedChanged();
            StoreDistrictCheckBoxCheckedChanged();
            StoreRuralDistrictCheckBoxCheckedChanged();
            StoreProvinceCheckBoxCheckedChanged();
            StoreZipcodeCheckBoxCheckedChanged();
            StoreContactNameCheckBoxCheckedChanged();
            StoreContactPhoneCheckBoxCheckedChanged();
            StoreContactEmailCheckBoxCheckedChanged();
        }

        protected void FillSupplierInput()
        {
            SupplierNameCheckBoxCheckedChanged();
            SupplierBuildingNameCheckBoxCheckedChanged();
            SupplierStreetNameCheckBoxCheckedChanged();
            SupplierNeighbourhoodCheckBoxCheckedChanged();
            SupplierSubdistrictCheckBoxCheckedChanged();
            SupplierDistrictCheckBoxCheckedChanged();
            SupplierRuralDistrictCheckBoxCheckedChanged();
            SupplierProvinceCheckBoxCheckedChanged();
            SupplierZipcodeCheckBoxCheckedChanged();
            SupplierContactNameCheckBoxCheckedChanged();
            SupplierContactPhoneCheckBoxCheckedChanged();
            SupplierContactEmailCheckBoxCheckedChanged();
        }

        protected void FillPurchaseInput()
        {
            PurchaseNoCheckBoxCheckedChanged();
            PurchaseDateCheckBoxCheckedChanged();
            //DeliveryNoCheckBoxCheckedChanged();
            TotalQtyCheckBoxCheckedChanged();
            TotalPriceCheckBoxCheckedChanged();
            ApprovalByCheckBoxCheckedChanged();
            ApprovalDateCheckBoxCheckedChanged();
            RequestedByCheckBoxCheckedChanged();
            RequestedDateCheckBoxCheckedChanged();
        }

        protected void CreateFilterChoice()
        {
            Dictionary<string, string> columns = new Dictionary<string, string>();
            columns.Add("no", "No");

            if (((string[])Session["read"]).Contains("store"))
            {
                columns.Add("storeName", "Toko Ritel");
            }

            if (((string[])Session["read"]).Contains("supplier"))
            {
                columns.Add("supplierName", "Vendor");
            }

            //if (((string[])Session["read"]).Contains("deliveryNo"))
            //{
            //    columns.Add("deliveryNo", "No Pengiriman");
            //}

            //if (((string[])Session["read"]).Contains("deliveryDate"))
            //{
            //    columns.Add("deliveryDate", "Tanggal Pengiriman");
            //}

            if (((string[])Session["read"]).Contains("purchaseNo"))
            {
                columns.Add("purchaseNo", "No Pemesanan");
            }

            if (((string[])Session["read"]).Contains("purchaseDate"))
            {
                columns.Add("purchaseDate", "Tanggal Pemesanan");
            }

            if (((string[])Session["read"]).Contains("requestedBy"))
            {
                columns.Add("requestedBy", "Dipesan Oleh");
            }

            if (((string[])Session["read"]).Contains("requestedDate"))
            {
                columns.Add("requestedDate", "Tanggal Pemesanan");
            }

            if (((string[])Session["read"]).Contains("totalPrice"))
            {
                columns.Add("totalPrice", "Total Harga");
            }

            if (((string[])Session["read"]).Contains("totalQty"))
            {
                columns.Add("totalQty", "Jumlah Barang");
            }

            if (((string[])Session["read"]).Contains("approvalDate"))
            {
                columns.Add("approvalDate", "Tanggal Disetujui/Ditolak");
            }

            if (((string[])Session["read"]).Contains("approvalBy"))
            {
                columns.Add("approvalBy", "Disetujui/Ditolak");
            }

            if (((string[])Session["read"]).Contains("purchaseStatus"))
            {
                columns.Add("purchaseStatus", "Status");
            }

            FilterChoice.DataSource = columns;
            FilterChoice.DataValueField = "Key";
            FilterChoice.DataTextField = "Value";
            FilterChoice.DataBind();
            FilterChoice.SelectedValue = "no";
        }

        protected void FilterChoice_SelectedIndexChanged(Object sender, EventArgs args)
        {
            if (FilterChoice.SelectedValue == "purchaseDate" || 
                FilterChoice.SelectedValue == "requestedDate" || FilterChoice.SelectedValue == "approvalDate")
            {
                ValueOneInput.CssClass = "form-control flatpickr-no-config";
                ValueTwoInput.CssClass = "form-control flatpickr-no-config";
            }
            else
            {
                ValueOneInput.CssClass = "form-control";
                ValueTwoInput.CssClass = "form-control";
            }
        }

        protected void OperatorChoice_SelectedIndexChanged(Object sender, EventArgs args)
        {
            if ("8596" == OperatorChoice.SelectedValue)
            {
                ValueTwoInput.Visible = true;
            }
            else
            {
                ValueTwoInput.Visible = false;
            }
        }

        protected void Clear()
        {
            PurchaseStatusChoice.SelectedValue = "pending";
            PurchaseNotesInput.Text = "";
        }

        protected void CreatePurchaseOrderGridView()
        {
            PurchaseOrderGridView.Columns.Clear();

            System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];

            foreach (DataColumn col in purchases.Columns)
            {
                BoundField boundfield = new BoundField();
                boundfield.DataField = col.ColumnName;
                boundfield.SortExpression = col.ColumnName;
                boundfield.ReadOnly = true;

                if (col.ColumnName == "no")
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                //if (col.ColumnName == "delivery_no" && ((string[])Session["read"]).Contains("deliveryNo"))
                //{
                //    boundfield.ItemStyle.CssClass = "text-nowrap";
                //    boundfield.HeaderText = "No Pengiriman";
                //    PurchaseOrderGridView.Columns.Add(boundfield);
                //}

                //if (col.ColumnName == "delivery_date" && ((string[])Session["read"]).Contains("deliveryDate"))
                //{
                //    boundfield.ItemStyle.CssClass = "text-nowrap";
                //    boundfield.HeaderText = "Tanggal Pengiriman";
                //    boundfield.DataFormatString = "{0:dd MMM yyyy}";
                //    PurchaseOrderGridView.Columns.Add(boundfield);
                //}

                if (col.ColumnName == "purchase_no" && ((string[])Session["read"]).Contains("purchaseNo"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No Pemesanan";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "purchase_date" && ((string[])Session["read"]).Contains("purchaseDate"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Tanggal Pembayaran";
                    boundfield.DataFormatString = "{0:dd MMM yyyy}";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "requested_by" && ((string[])Session["read"]).Contains("requestedBy"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Dipesan Oleh";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "requested_date" && ((string[])Session["read"]).Contains("requestedDate"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Tanggal Pemesanan";
                    boundfield.DataFormatString = "{0:dd MMM yyyy}";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "store_name" && ((string[])Session["read"]).Contains("store"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Toko Ritel";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "supplier_name" && ((string[])Session["read"]).Contains("supplier"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Pemasok";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "total_price" && ((string[])Session["read"]).Contains("totalPrice"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Total Harga";
                    boundfield.DataFormatString = "{0:###,###,###,###,##0.00}";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "total_qty" && ((string[])Session["read"]).Contains("totalQty"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Jumlah Barang";
                    boundfield.DataFormatString = "{0:###,###,###,###,##0}";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "approval_date" && ((string[])Session["read"]).Contains("approvalDate"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Tanggal Disetujui/Ditolak";
                    boundfield.DataFormatString = "{0:dd MMM yyyy}";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "approval_by" && ((string[])Session["read"]).Contains("approvalBy"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Disetujui/Ditolak Oleh";
                    PurchaseOrderGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "purchase_status" && ((string[])Session["read"]).Contains("purchaseStatus"))
                {
                    TemplateField tempfield = new TemplateField();
                    tempfield.ItemStyle.CssClass = "text-nowrap";
                    tempfield.HeaderText = "Status";
                    tempfield.SortExpression = col.ColumnName;
                    PurchaseOrderGridView.Columns.Add(tempfield);
                }
            }

            TemplateField templatefield = new TemplateField();
            templatefield.ItemStyle.CssClass = "text-nowrap";
            templatefield.HeaderText = "";
            PurchaseOrderGridView.Columns.Add(templatefield);
        }

        protected void PurchaseOrderGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;

                LinkButton linkButton = new LinkButton();

                if ((row["purchase_status"].ToString() == "pending" || row["purchase_status"].ToString() == "cancelled")
                    && String.IsNullOrEmpty(row["goods_receipt_no"].ToString()) && String.IsNullOrEmpty(row["delivery_no"].ToString()) 
                    && ((string[])Session["auto"]).Contains("approvalBy") && ((string[])Session["auto"]).Contains("approvalDate") &&
                    ((string[])Session["auto"]).Contains("purchaseStatus"))
                {
                    linkButton = new LinkButton();
                    linkButton.Text = "Setuju";
                    linkButton.CssClass = "btn btn-primary align-middle ms-2";
                    linkButton.CommandName = "approve";
                    linkButton.CommandArgument = row["purchase_order_id"].ToString() + ";" + row["uuid"].ToString()
                        + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                    linkButton.Command += new CommandEventHandler(ApproveButton_Command);
                    e.Row.Cells[PurchaseOrderGridView.Columns.Count - 1].Controls.Add(linkButton);
                }

                if ((row["purchase_status"].ToString() == "pending" || row["purchase_status"].ToString() == "approved")
                    && String.IsNullOrEmpty(row["goods_receipt_no"].ToString()) && String.IsNullOrEmpty(row["delivery_no"].ToString())
                    && ((string[])Session["auto"]).Contains("approvalBy") && ((string[])Session["auto"]).Contains("approvalDate") &&
                    ((string[])Session["auto"]).Contains("purchaseStatus"))
                {
                    linkButton = new LinkButton();
                    linkButton.Text = "Batal";
                    linkButton.CssClass = "btn btn-danger align-middle ms-2";
                    linkButton.CommandName = "cancel";
                    linkButton.CommandArgument = row["purchase_order_id"].ToString() + ";" + row["uuid"].ToString()
                        + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                    linkButton.Command += new CommandEventHandler(CancelButton_Command);
                    e.Row.Cells[PurchaseOrderGridView.Columns.Count - 1].Controls.Add(linkButton);
                }

                if (row["purchase_status"].ToString() == "approved" && !String.IsNullOrEmpty(row["purchase_no"].ToString())
                    && ((string[])Session["read"]).Length > 0)
                {
                    linkButton = new LinkButton();
                    linkButton.Text = "Lihat";
                    linkButton.CssClass = "btn btn-primary align-middle ms-2";
                    linkButton.CommandName = "show";
                    linkButton.CommandArgument = row["purchase_order_id"].ToString() + ";" + row["uuid"].ToString()
                        + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                    linkButton.Command += new CommandEventHandler(ShowButton_Command);
                    e.Row.Cells[PurchaseOrderGridView.Columns.Count - 1].Controls.Add(linkButton);
                }

                if (row["purchase_status"].ToString() == "approved" && String.IsNullOrEmpty(row["delivery_no"].ToString())
                    && String.IsNullOrEmpty(row["goods_receipt_no"].ToString()) && ((string[])Session["add"]).Contains("deliveryOrder")) 
                {
                    linkButton = new LinkButton();
                    linkButton.Text = "Buat Dokumen Pengiriman";
                    linkButton.CssClass = "btn btn-primary align-middle ms-2";
                    linkButton.CommandName = "create";
                    linkButton.CommandArgument = row["purchase_order_id"].ToString() + ";" + row["uuid"].ToString()
                        + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                    linkButton.Command += new CommandEventHandler(CreateButton_Command);
                    e.Row.Cells[PurchaseOrderGridView.Columns.Count - 1].Controls.Add(linkButton);
                }

                if(!String.IsNullOrEmpty(row["locked"].ToString()) && row["locked"].ToString() == "0") {
                    if (((string[])Session["edit"]).Length > 0 && row["purchase_status"].ToString() != "approved")
                    {
                        linkButton = new LinkButton();
                        linkButton.Text = "<i class='bi bi-pencil-fill'></i>";
                        linkButton.CssClass = "btn btn-primary align-middle ms-2";
                        linkButton.CommandName = "edit";
                        linkButton.CommandArgument = row["purchase_order_id"].ToString() + ";" + row["uuid"].ToString()
                            + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                        linkButton.Command += new CommandEventHandler(EditButton_Command);
                        e.Row.Cells[PurchaseOrderGridView.Columns.Count - 1].Controls.Add(linkButton);
                    }

                    if (((bool)Session["delete"]) && row["purchase_status"].ToString() != "approved")
                    {
                        linkButton = new LinkButton();
                        linkButton.Text = "<i class='bi bi-x-lg'></i>";
                        linkButton.CssClass = "btn btn-danger align-middle ms-2";
                        linkButton.CommandName = "delete";
                        linkButton.CommandArgument = row["purchase_order_id"].ToString() + ";" + row["uuid"].ToString() + ";" + row["purchase_no"].ToString()
                            + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                        linkButton.Command += new CommandEventHandler(DeleteButton_Command);
                        e.Row.Cells[PurchaseOrderGridView.Columns.Count - 1].Controls.Add(linkButton);
                    }

                    if (Session["role_name"].ToString() == "superuser")
                    {
                        linkButton = new LinkButton();
                        linkButton.Text = "<i class='bi bi-unlock-fill'></i>";
                        linkButton.CssClass = "btn btn-primary align-middle ms-2";
                        linkButton.CommandName = "lock";
                        linkButton.CommandArgument = row["purchase_order_id"].ToString() + ";" + row["uuid"].ToString()
                            + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                        linkButton.Command += new CommandEventHandler(LockButton_Command);
                        e.Row.Cells[PurchaseOrderGridView.Columns.Count - 1].Controls.Add(linkButton);
                    }
                    
                }
                else
                {
                    if (Session["role_name"].ToString() == "superuser")
                    {
                        linkButton = new LinkButton();
                        linkButton.Text = "<i class='bi bi-lock-fill'></i>";
                        linkButton.CssClass = "btn btn-danger align-middle ms-2";
                        linkButton.CommandName = "lock";
                        linkButton.CommandArgument = row["purchase_order_id"].ToString() + ";" + row["uuid"].ToString()
                            + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                        linkButton.Command += new CommandEventHandler(LockButton_Command);
                        e.Row.Cells[PurchaseOrderGridView.Columns.Count - 1].Controls.Add(linkButton);
                    }
                    
                }
                

                Label label = new Label();
                label.Text = row["purchase_status"].ToString();
                if (row["purchase_status"].ToString() == "pending")
                {
                    label.CssClass = "badge rounded-pill bg-warning";
                }
                else if (row["purchase_status"].ToString() == "approved")
                {
                    label.CssClass = "badge rounded-pill bg-primary";
                }
                else if (row["purchase_status"].ToString() == "cancelled")
                {
                    label.CssClass = "badge rounded-pill bg-danger";
                }
                e.Row.Cells[PurchaseOrderGridView.Columns.Count - 2].Controls.Add(label);
            }

            if (e.Row.RowType == DataControlRowType.Pager)
            {
                DropDownList PageDropDownList = (DropDownList)e.Row.FindControl("PageDropDownList");
                System.Web.UI.WebControls.LinkButton FirstButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("Firstbutton");
                System.Web.UI.WebControls.LinkButton PreviousButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("PreviousButton");
                System.Web.UI.WebControls.LinkButton NextButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("NextButton");
                System.Web.UI.WebControls.LinkButton LastButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("LastButton");
                PageDropDownList.Items.Clear();

                int page = 0;

                for (int i = 0; i < PurchaseOrderGridView.PageCount; i++)
                {
                    page = i + 1;
                    PageDropDownList.Items.Add(page.ToString());
                }
                page = int.Parse(ViewState["page"].ToString()) + 1;
                if (page == 1)
                {
                    FirstButton.CssClass += " " + "disabled";
                    PreviousButton.CssClass += " " + "disabled";
                    NextButton.CssClass = "page-link";
                    LastButton.CssClass = "page-link";
                }
                else if (page == PurchaseOrderGridView.PageCount)
                {
                    FirstButton.CssClass = "page-link";
                    PreviousButton.CssClass = "page-link";
                    NextButton.CssClass += " " + "disabled";
                    LastButton.CssClass += " " + "disabled";
                }
                else
                {
                    FirstButton.CssClass = "page-link";
                    PreviousButton.CssClass = "page-link";
                    NextButton.CssClass = "page-link";
                    LastButton.CssClass = "page-link";
                }
                PageDropDownList.SelectedValue = page.ToString();
            }
        }

        protected void PageDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int page = int.Parse(((DropDownList)sender).SelectedValue);

            if (page <= 0)
            {
                ViewState["page"] = 0;
                PurchaseOrderGridView.PageIndex = 0;
            }
            else
            {
                ViewState["page"] = page - 1;
                PurchaseOrderGridView.PageIndex = page - 1;
            }

            System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];
            //items.DefaultView.Sort = ViewState["column"].ToString() + " " + ViewState["sort"].ToString();
            //ViewState["items"] = items;
            purchases.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            PurchaseOrderGridView.DataSource = purchases;
            PurchaseOrderGridView.DataBind();
        }

        protected void FirstButton_Click(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            PurchaseOrderGridView.PageIndex = 0;

            System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];
            purchases.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            PurchaseOrderGridView.DataSource = purchases;
            PurchaseOrderGridView.DataBind();
        }

        protected void LastButton_Click(object sender, EventArgs e)
        {
            ViewState["page"] = PurchaseOrderGridView.PageCount - 1;
            PurchaseOrderGridView.PageIndex = PurchaseOrderGridView.PageCount - 1;

            System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];
            purchases.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            PurchaseOrderGridView.DataSource = purchases;
            PurchaseOrderGridView.DataBind();
        }

        protected void NextButton_Click(object sender, EventArgs e)
        {
            int page = int.Parse(ViewState["page"].ToString());
            page++;

            if (page > PurchaseOrderGridView.PageCount - 1)
            {
                page = PurchaseOrderGridView.PageCount - 1;
            }
            ViewState["page"] = page;
            PurchaseOrderGridView.PageIndex = page;
            //AlertLabel.Text = page.ToString();
            System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];
            purchases.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            PurchaseOrderGridView.DataSource = purchases;
            PurchaseOrderGridView.DataBind();
        }

        protected void PreviousButton_Click(object sender, EventArgs e)
        {
            int page = int.Parse(ViewState["page"].ToString());
            page--;

            if (page < 0)
            {
                page = 0;
            }
            ViewState["page"] = page;
            PurchaseOrderGridView.PageIndex = page;

            System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];
            purchases.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            PurchaseOrderGridView.DataSource = purchases;
            PurchaseOrderGridView.DataBind();
        }

        protected void SearchInput_TextChanged(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["column"] = "no";
            ViewState["sort"] = "ASC";

            if (OperatorChoice.SelectedValue == "61")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindPurchaseOrderGridView();
            }
            else if (OperatorChoice.SelectedValue == "60")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindPurchaseOrderGridView();
            }
            else if (OperatorChoice.SelectedValue == "62")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindPurchaseOrderGridView();
            }
            else if (OperatorChoice.SelectedValue == "8596")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()) || String.IsNullOrEmpty(ValueTwoInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim() + ";" + ValueTwoInput.Text?.Trim();
                }
                BindPurchaseOrderGridView();
            }
            else
            {
                ViewState["search"] = "";
                BindPurchaseOrderGridView();
            }
        }

        protected void SearchButton_Command(object sender, CommandEventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["column"] = "no";
            ViewState["sort"] = "ASC";

            if (OperatorChoice.SelectedValue == "61")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindPurchaseOrderGridView();
            }
            else if (OperatorChoice.SelectedValue == "60")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindPurchaseOrderGridView();
            }
            else if (OperatorChoice.SelectedValue == "62")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindPurchaseOrderGridView();
            }
            else if (OperatorChoice.SelectedValue == "8596")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()) || String.IsNullOrEmpty(ValueTwoInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim() + ";" + ValueTwoInput.Text?.Trim();
                }
                BindPurchaseOrderGridView();
            }
            else
            {
                ViewState["search"] = "";
                BindPurchaseOrderGridView();
            }
        }

        protected void BindPurchaseOrderGridView()
        {
            string[] search = new string[0];
            string column = "";
            string symbol = "";

            if (!String.IsNullOrEmpty(ViewState["search"].ToString()))
            {
                search = ViewState["search"].ToString().Split(';');
            }

            if (search.Length > 0)
            {
                column = search[0];
                FilterChoice.SelectedValue = search[0];
                if (FilterChoice.SelectedValue == "purchaseDate" || FilterChoice.SelectedValue == "deliveryDate" ||
                FilterChoice.SelectedValue == "requestedDate" || FilterChoice.SelectedValue == "approvalDate")
                {
                    ValueOneInput.CssClass = "form-control flatpickr-no-config";
                    ValueTwoInput.CssClass = "form-control flatpickr-no-config";
                }
                else
                {
                    ValueOneInput.CssClass = "form-control";
                    ValueTwoInput.CssClass = "form-control";
                }
            }

            if (search.Length > 1)
            {
                symbol = search[1];
                OperatorChoice.SelectedValue = symbol;
            }

            if (search.Length > 2)
            {
                ValueOneInput.Text = search[2];
            }

            if (search.Length > 3)
            {
                ValueTwoInput.Text = search[3];
                ValueTwoInput.Visible = true;
            }

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

            SqlConnection conn = new SqlConnection(constr);
            SqlCommand cmd = conn.CreateCommand();

            string select = "";
            string where = "";
            bool withComma = false;
            bool withAnd = false;

            if (((string[])Session["read"]).Contains("store"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }

                select = string.Concat(select, " ", "stores.store_name");
            }

            if (((string[])Session["read"]).Contains("supplier"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }

                select = string.Concat(select, " ", "suppliers.supplier_name");
            }

            //if (((string[])Session["read"]).Contains("deliveryNo"))
            //{
            //    if (withComma)
            //    {
            //        select = string.Concat(select, ",");
            //    }
            //    else
            //    {
            //        withComma = true;
            //    }
            //    select = string.Concat(select, " ", "purchase_orders.delivery_no");

            //}

            //if (((string[])Session["read"]).Contains("deliveryDate"))
            //{
            //    if (withComma)
            //    {
            //        select = string.Concat(select, ",");
            //    }
            //    else
            //    {
            //        withComma = true;
            //    }
            //    select = string.Concat(select, " ", "purchase_orders.delivery_date");
            //}

            if (((string[])Session["read"]).Contains("purchaseNo"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.purchase_no");
            }

            if (((string[])Session["read"]).Contains("purchaseDate"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.purchase_date");
            }

            if (((string[])Session["read"]).Contains("requestedBy"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.requested_by");
            }

            if (((string[])Session["read"]).Contains("requestedDate"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.requested_date");
            }

            if (((string[])Session["read"]).Contains("totalPrice"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.total_price");
            }

            if (((string[])Session["read"]).Contains("totalQty"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.total_qty");
            }

            if (((string[])Session["read"]).Contains("approvalDate"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.approval_date");
            }

            if (((string[])Session["read"]).Contains("approvalBy"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.approval_by");
            }

            if (((string[])Session["read"]).Contains("purchaseStatus"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.purchase_status");
            }

            //AlertLabel.Text += ViewState["search"].ToString();
            //ViewState["search"] = "";

            if (!String.IsNullOrEmpty(ViewState["search"].ToString()))
            {
                if (column == "no")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "no LIKE @no");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "no < @no");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "no > @no");
                    }
                    else if (symbol == "8596")
                    {

                        where = string.Concat(where, " ", "no BETWEEN @no AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("store") && column == "storeName")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "store_name LIKE @storeName");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "store_name < @storeName");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "store_name > @storeName");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "store_name BETWEEN @storeName AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("supplier") && column == "supplierName")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "supplier_name LIKE @supplierName");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "supplier_name < @supplierName");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "supplier_name > @supplierName");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "supplier_name BETWEEN @supplierName AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("purchaseNo") && column == "purchaseNo")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "purchase_no LIKE @purchaseNo");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "purchase_no < @purchaseNo");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "purchase_no > @purchaseNo");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "purchase_no BETWEEN @purchaseNo AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("purchaseDate") && column == "purchaseDate")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "FORMAT(purchase_date, 'yyyy-MM-dd') = @purchaseDate");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "FORMAT(purchase_date, 'yyyy-MM-dd') < @purchaseDate");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "FORMAT(purchase_date, 'yyyy-MM-dd') > @purchaseDate");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "FORMAT(purchase_date, 'yyyy-MM-dd') BETWEEN @purchaseDate AND @value");
                    }
                }

                //if (((string[])Session["read"]).Contains("deliveryNo") && column == "deliveryNo")
                //{
                //    if (symbol == "61")
                //    {
                //        where = string.Concat(where, " ", "delivery_no LIKE @deliveryNo");
                //    }
                //    else if (symbol == "60")
                //    {
                //        where = string.Concat(where, " ", "delivery_no < @deliveryNo");
                //    }
                //    else if (symbol == "62")
                //    {
                //        where = string.Concat(where, " ", "delivery_no > @deliveryNo");
                //    }
                //    else if (symbol == "8596")
                //    {
                //        where = string.Concat(where, " ", "delivery_no BETWEEN @deliveryNo AND @value");
                //    }
                //}


                //if (((string[])Session["read"]).Contains("deliveryDate") && column == "deliveryDate")
                //{
                //    if (symbol == "61")
                //    {
                //        where = string.Concat(where, " ", "FORMAT(delivery_date, 'yyyy-MM-dd') = @deliveryDate");
                //    }
                //    else if (symbol == "60")
                //    {
                //        where = string.Concat(where, " ", "FORMAT(delivery_date, 'yyyy-MM-dd') < @deliveryDate");
                //    }
                //    else if (symbol == "62")
                //    {
                //        where = string.Concat(where, " ", "FORMAT(delivery_date, 'yyyy-MM-dd') > @deliveryDate");
                //    }
                //    else if (symbol == "8596")
                //    {
                //        where = string.Concat(where, " ", "FORMAT(delivery_date, 'yyyy-MM-dd') BETWEEN @deliveryDate AND @value");
                //    }
                //}

                if (((string[])Session["read"]).Contains("requestedBy") && column == "requestedBy")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "requested_by LIKE @requestedBy");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "requested_by < @requestedBy");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "requested_by > @requestedBy");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "requested_by BETWEEN @requestedBy AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("requestedDate") && column == "requestedDate")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "FORMAT(requested_date, 'yyyy-MM-dd') LIKE @requestedDate");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "FORMAT(requested_date, 'yyyy-MM-dd') < @requestedDate");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "FORMAT(requested_date, 'yyyy-MM-dd') > @requestedDate");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "FORMAT(requested_date, 'yyyy-MM-dd') BETWEEN @requestedDate AND @value");
                    }
                }


                if (((string[])Session["read"]).Contains("approvalBy") && column == "approvalBy")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "approval_by LIKE @approvalBy");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "approval_by < @approvalBy");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "approval_by > @approvalBy");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "approval_by BETWEEN @approvalBy AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("approvalDate") && column == "approvalDate")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "FORMAT(approval_date, 'yyyy-MM-dd') = @approvalDate");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "FORMAT(approval_date, 'yyyy-MM-dd') < @approvalDate");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "FORMAT(approval_date, 'yyyy-MM-dd') > @approvalDate");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "FORMAT(approval_date, 'yyyy-MM-dd') BETWEEN @approvalDate AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("purchaseStatus") && column == "purchaseStatus")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "purchase_status LIKE @purchaseStatus");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "purchase_status < @purchaseStatus");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "purchase_status > @purchaseStatus");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "purchase_status BETWEEN @purchaseStatus AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("totalQty") && column == "totalQty")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "total_qty = @totalQty");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "total_qty < @totalQty");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "total_qty > @totalQty");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "total_qty BETWEEN @totalQty AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("totalPrice") && column == "totalPrice")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "total_price = @totalPrice");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "total_price < @totalPrice");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "total_price > @totalPrice");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "total_price BETWEEN @totalPrice AND @value");
                    }
                }
            }
            else
            {
                if (withAnd)
                {
                    where = string.Concat(where, " ", "AND");
                }
                else
                {
                    withAnd = true;
                }
                where = string.Concat(where, " ", "1 = 1");
            }

            if(!String.IsNullOrEmpty(StoreChoice.SelectedValue))
            {
                if ((((int[])Session["stores"]).Contains(-1)) || (((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))))
                {
                    if (withAnd)
                    {
                        where = string.Concat(where, " ", "AND");
                    }
                    else
                    {
                        withAnd = true;
                    }

                    where = string.Concat(where, " ", "store_id = @storeId");
                }
            }
            
            if(!String.IsNullOrEmpty(SupplierChoice.SelectedValue))
            {
                if ((((int[])Session["suppliers"]).Contains(-1)) || (((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))))
                {
                    if (withAnd)
                    {
                        where = string.Concat(where, " ", "AND");
                    }
                    else
                    {
                        withAnd = true;
                    }

                    where = string.Concat(where, " ", "supplier_id = @supplierId");
                }
            }
            

            string commandText = @"WITH purchaseOrders AS (
                SELECT ROW_NUMBER() OVER(ORDER BY purchase_orders.purchase_order_id DESC) [no],
                stores.store_id, suppliers.supplier_id,
                purchase_orders.purchase_order_id, purchase_orders.uuid, purchase_orders.delivery_no,
                purchase_orders.locked, goods_receipts.goods_receipt_no, {0}
                FROM purchase_orders LEFT JOIN goods_receipts 
                ON goods_receipts.purchase_order_id = purchase_orders.purchase_order_id AND goods_receipts.isdeleted = 0
                INNER JOIN stores ON purchase_orders.store_id = stores.store_id
                INNER JOIN suppliers ON purchase_orders.supplier_id = suppliers.supplier_id
                WHERE purchase_orders.isdeleted = 0
                AND (purchase_orders.purchase_no IS NOT NULL OR LTRIM(RTRIM(purchase_orders.purchase_no)) != ''))
                SELECT * FROM purchaseOrders WHERE {1}";


            commandText = String.Format(commandText, select, where);
            cmd.CommandText = commandText;

            
            if(!String.IsNullOrEmpty(StoreChoice.SelectedValue))
            {
                if ((((int[])Session["stores"]).Contains(-1)) || (((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))))
                {
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                }
            }
            
            if(!String.IsNullOrEmpty(SupplierChoice.SelectedValue))
            {
                if ((((int[])Session["suppliers"]).Contains(-1)) || (((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))))
                {
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                }
            }
            
            if (!String.IsNullOrEmpty(ViewState["search"].ToString()))
            {
                if (column == "no")
                {
                    int min = 0;
                    int max = 0;

                    int.TryParse(ValueOneInput.Text?.Trim(), out min);
                    int.TryParse(ValueTwoInput.Text?.Trim(), out max);

                    if (symbol == "61")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@no", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@no", min);
                        }
                    }
                    else if (symbol == "60")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@no", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@no", min);
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@no", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@no", min);
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@no", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@no", min);
                        }

                        if (max <= 0)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", max);
                        }
                    }
                }

                if (((string[])Session["read"]).Contains("store") && column == "storeName")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@storeName", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@storeName", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@storeName", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@storeName", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }

                }

                if (((string[])Session["read"]).Contains("supplier") && column == "supplierName")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@supplierName", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@supplierName", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@supplierName", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@supplierName", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("purchaseNo") && column == "purchaseNo")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@purchaseNo", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@purchaseNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@purchaseNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@purchaseNo", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("purchaseDate") && column == "purchaseDate")
                {
                    DateTime from = DateTime.MinValue;
                    DateTime to = DateTime.MinValue;
                    DateTime.TryParse(ValueOneInput.Text?.Trim(), out from);
                    DateTime.TryParse(ValueTwoInput.Text?.Trim(), out to);

                    if (symbol == "61")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@purchaseDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@purchaseDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "60")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@purchaseDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@purchaseDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@purchaseDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@purchaseDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@purchaseDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@purchaseDate", ValueOneInput.Text?.Trim());
                        }

                        if (to == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                        }
                    }
                }

                //if (((string[])Session["read"]).Contains("deliveryNo") && column == "deliveryNo")
                //{
                //    if (symbol == "61")
                //    {
                //        cmd.Parameters.AddWithValue("@deliveryNo", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                //    }
                //    else if (symbol == "60")
                //    {
                //        cmd.Parameters.AddWithValue("@deliveryNo", ValueOneInput.Text?.Trim());
                //    }
                //    else if (symbol == "62")
                //    {
                //        cmd.Parameters.AddWithValue("@deliveryNo", ValueOneInput.Text?.Trim());
                //    }
                //    else if (symbol == "8596")
                //    {
                //        cmd.Parameters.AddWithValue("@deliveryNo", ValueOneInput.Text?.Trim());
                //        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                //    }
                //}

                //if (((string[])Session["read"]).Contains("deliveryDate") && column == "deliveryDate")
                //{
                //    DateTime from = DateTime.MinValue;
                //    DateTime to = DateTime.MinValue;
                //    DateTime.TryParse(ValueOneInput.Text?.Trim(), out from);
                //    DateTime.TryParse(ValueTwoInput.Text?.Trim(), out to);

                //    if (symbol == "61")
                //    {
                //        if (from == DateTime.MinValue)
                //        {
                //            cmd.Parameters.AddWithValue("@deliveryDate", DBNull.Value);
                //        }
                //        else
                //        {
                //            cmd.Parameters.AddWithValue("@deliveryDate", ValueOneInput.Text?.Trim());
                //        }
                //    }
                //    else if (symbol == "60")
                //    {
                //        if (from == DateTime.MinValue)
                //        {
                //            cmd.Parameters.AddWithValue("@deliveryDate", DBNull.Value);
                //        }
                //        else
                //        {
                //            cmd.Parameters.AddWithValue("@deliveryDate", ValueOneInput.Text?.Trim());
                //        }
                //    }
                //    else if (symbol == "62")
                //    {
                //        if (from == DateTime.MinValue)
                //        {
                //            cmd.Parameters.AddWithValue("@deliveryDate", DBNull.Value);
                //        }
                //        else
                //        {
                //            cmd.Parameters.AddWithValue("@deliveryDate", ValueOneInput.Text?.Trim());
                //        }
                //    }
                //    else if (symbol == "8596")
                //    {
                //        if (from == DateTime.MinValue)
                //        {
                //            cmd.Parameters.AddWithValue("@deliveryDate", DBNull.Value);
                //        }
                //        else
                //        {
                //            cmd.Parameters.AddWithValue("@deliveryDate", ValueOneInput.Text?.Trim());
                //        }

                //        if (to == DateTime.MinValue)
                //        {
                //            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                //        }
                //        else
                //        {
                //            cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                //        }
                //    }
                //}

                if (((string[])Session["read"]).Contains("requestedBy") && column == "requestedBy")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@requestedBy", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@requestedBy", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@requestedBy", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@requestedBy", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("requestedDate") && column == "requestedDate")
                {
                    DateTime from = DateTime.MinValue;
                    DateTime to = DateTime.MinValue;
                    DateTime.TryParse(ValueOneInput.Text?.Trim(), out from);
                    DateTime.TryParse(ValueTwoInput.Text?.Trim(), out to);

                    if (symbol == "61")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@requestedDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "60")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@requestedDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@requestedDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@requestedDate", ValueOneInput.Text?.Trim());
                        }

                        if (to == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                        }
                    }
                }

                if (((string[])Session["read"]).Contains("approvalBy") && column == "approvalBy")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@approvalBy", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@approvalBy", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@approvalBy", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@approvalBy", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("approvalDate") && column == "approvalDate")
                {
                    DateTime from = DateTime.MinValue;
                    DateTime to = DateTime.MinValue;
                    DateTime.TryParse(ValueOneInput.Text?.Trim(), out from);
                    DateTime.TryParse(ValueTwoInput.Text?.Trim(), out to);

                    if (symbol == "61")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@approvalDate", ValueOneInput.Text?.Trim());
                        }

                    }
                    else if (symbol == "60")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@approvalDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@approvalDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@approvalDate", ValueOneInput.Text?.Trim());
                        }

                        if (to == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                        }
                    }
                }

                if (((string[])Session["read"]).Contains("approvalStatus") && column == "approvalStatus")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@approvalStatus", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@approvalStatus", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@approvalStatus", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@approvalStatus", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("totalQty") && column == "totalQty")
                {
                    int min = 0;
                    int max = 0;

                    int.TryParse(ValueOneInput.Text?.Trim(), out min);
                    int.TryParse(ValueTwoInput.Text?.Trim(), out max);

                    if (symbol == "61")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalQty", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalQty", min);
                        }
                    }
                    else if (symbol == "60")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalQty", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalQty", min);
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalQty", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalQty", min);
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalQty", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalQty", min);
                        }

                        if (max <= 0)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", max);
                        }
                    }
                }

                if (((string[])Session["read"]).Contains("totalPrice") && column == "totalPrice")
                {
                    int min = 0;
                    int max = 0;

                    int.TryParse(ValueOneInput.Text?.Trim(), out min);
                    int.TryParse(ValueTwoInput.Text?.Trim(), out max);

                    if (symbol == "61")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", min);
                        }
                    }
                    else if (symbol == "60")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", min);
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", min);
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", min);
                        }

                        if (max <= 0)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", max);
                        }
                    }
                }
            }

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];
            purchases.Rows.Clear();
            sda.Fill(purchases);

            purchases.DefaultView.Sort = ViewState["column"].ToString() + " " + ViewState["sort"].ToString();
            if (PurchaseOrderGridView.PageCount - 1 < int.Parse(ViewState["page"].ToString()) && PurchaseOrderGridView.PageCount > 0)
            {
                ViewState["page"] = PurchaseOrderGridView.PageCount - 1;
            }
            PurchaseOrderGridView.PageIndex = int.Parse(ViewState["page"].ToString());
            PurchaseOrderGridView.DataSource = purchases;
            PurchaseOrderGridView.DataBind();
        }

        protected void DownloadButton_Command(object sender, CommandEventArgs e)
        {
            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            SqlConnection conn = new SqlConnection(constr);
            SqlCommand cmd = conn.CreateCommand();
            List<string> errors = new List<string>();

            string query = "";
            string columns = "";
            string wheresupplier = "";
            string wherestore = "";
            bool withComma = false;

            if (((string[])Session["read"]).Contains("store"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "stores.store_name");
                columns = String.Concat(columns, "[Store Name] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("supplier"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "suppliers.supplier_name");
                columns = String.Concat(columns, "[Supplier Name] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("purchaseNo"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.purchase_no");
                columns = String.Concat(columns, "[Purchase Order No] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("purchaseDate"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.purchase_date");
                columns = String.Concat(columns, "[Purchase Order Date] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("requestedBy"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.requested_by");
                columns = String.Concat(columns, "[Requested By] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("requestedDate"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.requested_date");
                columns = String.Concat(columns, "[Requested Date] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("approvalBy"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.approval_by");
                columns = String.Concat(columns, "[Approval By] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("approvalDate"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.approval_date");
                columns = String.Concat(columns, "[Approval Date] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("purchaseNotes"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.purchase_notes");
                columns = String.Concat(columns, "[Notes] LongText");
            }

            if (((string[])Session["read"]).Contains("item"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", @"dbo.JsonValue(purchase_order_items.item_json, 'itemName') [item_name], 
                dbo.JsonValue(purchase_order_items.item_json, 'brandName') [brand_name], purchase_order_items.item_qty, purchase_order_items.item_price");
                columns = String.Concat(columns, "[Item Name] NVARCHAR(255), [Brand Name] NVARCHAR(255), [Item Price] DOUBLE, [Item Qty] INTEGER");
            }

            if (((string[])Session["read"]).Contains("totalQty"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.total_qty");
                columns = String.Concat(columns, "[Total Qty] INTEGER");
            }

            if (((string[])Session["read"]).Contains("totalPrice"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.total_price");
                columns = String.Concat(columns, "[Total Price] DOUBLE");
            }

            if (e.CommandName == "downloadall")
            {
                if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = @"SELECT supplier_id FROM suppliers";
                }
                else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = String.Join(",", ((int[])Session["suppliers"]));
                }

                if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = @"SELECT store_id FROM stores";
                }
                else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = String.Join(",", ((int[])Session["stores"]));
                }
            }

            if (e.CommandName == "downloadselectedsupplier")
            {
                if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
                {
                    errors.Add("Vendor tidak boleh kosong");
                }
                else
                {
                    wheresupplier = SupplierChoice.SelectedValue;
                }


                if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = @"SELECT store_id FROM stores";
                }
                else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = String.Join(",", ((int[])Session["stores"]));
                }
            }

            if (e.CommandName == "downloadselectedstore")
            {
                if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = @"SELECT supplier_id FROM suppliers";
                }
                else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = String.Join(",", ((int[])Session["suppliers"]));
                }

                if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
                {
                    errors.Add("Toko Ritel tidak boleh kosong");
                }
                else
                {
                    wherestore = StoreChoice.SelectedValue;
                }
            }

            if (e.CommandName == "downloadselectedstoreandsupplier")
            {
                if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
                {
                    errors.Add("Toko Ritel tidak boleh kosong");
                }
                else
                {
                    wherestore = StoreChoice.SelectedValue;
                }

                if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
                {
                    errors.Add("Vendor tidak boleh kosong");
                }
                else
                {
                    wheresupplier = SupplierChoice.SelectedValue;
                }

            }

            if (errors.Count <= 0)
            {
                int purchaseId = 0;
                int oldPurchaseId = 0;
                bool newRow = false;

                List<string> rows = new List<string>();

                string commandText = @"SELECT purchase_orders.purchase_order_id, {0}
                    FROM purchase_orders INNER JOIN purchase_order_items 
                    ON purchase_orders.purchase_order_id = purchase_order_items.purchase_order_id AND purchase_order_items.isdeleted = 0
                    INNER JOIN stores ON purchase_orders.store_id = stores.store_id
                    INNER JOIN suppliers ON purchase_orders.supplier_id = suppliers.supplier_id
                    WHERE purchase_orders.store_id IN ({1}) AND purchase_orders.supplier_id IN ({2}) AND
                    purchase_orders.purchase_status = 'approved' AND purchase_orders.isdeleted = 0
                    ORDER BY purchase_orders.purchase_order_id;";
                commandText = String.Format(commandText, query, wherestore, wheresupplier);

                cmd.CommandText = commandText;
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        withComma = false;

                        string row = "";

                        if (reader.IsDBNull(reader.GetOrdinal("purchase_order_id")))
                        {
                            continue;
                        }
                        else
                        {
                            purchaseId = int.Parse(reader["purchase_order_id"].ToString());

                            if (oldPurchaseId != purchaseId)
                            {
                                newRow = true;
                                oldPurchaseId = purchaseId;
                            }
                            else
                            {
                                newRow = false;
                            }
                        }

                        if (((string[])Session["read"]).Contains("store"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("store_name")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["store_name"].ToString().Replace("'", ""), "'");
                            }
                        }

                        if (((string[])Session["read"]).Contains("supplier"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("supplier_name")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["supplier_name"].ToString().Replace("'", ""), "'");
                            }
                        }

                        if (((string[])Session["read"]).Contains("purchaseNo"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("purchase_no")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["purchase_no"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("purchaseDate"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("purchase_date")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["purchase_date"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("requestedBy"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("requested_by")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["requested_by"].ToString().Replace("'", ""), "'");
                            }
                        }

                        if (((string[])Session["read"]).Contains("requestedDate"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("requested_date")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["requested_date"].ToString().Replace("'", ""), "'");
                            }
                        }

                        if (((string[])Session["read"]).Contains("approvalBy"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("approval_by")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["approval_by"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("approvalDate"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("approval_date")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["approval_date"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("purchaseNotes"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("purchase_notes")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["purchase_notes"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("item"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("item_name")))
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["item_name"].ToString().Replace("'", ""), "'");
                            }

                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("brand_name")))
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["brand_name"].ToString().Replace("'", ""), "'");
                            }

                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("item_qty")))
                            {
                                row = String.Concat(row, " ", "NULL");
                            }
                            else
                            {
                                row = String.Concat(row, " ", reader["item_qty"].ToString());
                            }

                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("item_price")))
                            {
                                row = String.Concat(row, " ", "NULL");
                            }
                            else
                            {
                                row = String.Concat(row, " ", reader["item_price"].ToString());
                            }
                        }

                        if (((string[])Session["read"]).Contains("totalQty"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("total_qty")) || !newRow)
                            {
                                row = String.Concat(row, " ", "NULL");
                            }
                            else
                            {
                                row = String.Concat(row, " ", reader["total_qty"].ToString());
                            }
                        }

                        if (((string[])Session["read"]).Contains("totalPrice"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("total_price")) || !newRow)
                            {
                                row = String.Concat(row, " ", "NULL");
                            }
                            else
                            {
                                row = String.Concat(row, " ", reader["total_price"].ToString());
                            }
                        }

                        row = String.Format("INSERT INTO Sheet1 VALUES ({0});", row);
                        rows.Add(row);
                    }
                }

                conn.Close();

                string sourcefile = MapPath("~/uploads/blank.xlsx");
                string filename = "purchaseorders-" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".xlsx";
                string destinationfile = MapPath("~/uploads/" + filename);

                File.Copy(sourcefile, destinationfile, true);

                string extension = Path.GetExtension(destinationfile);

                string connectionstring = "";
                string mimetypes = "";

                if (extension == ".xls")
                {
                    connectionstring = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + destinationfile + "';Extended Properties=\"Excel 8.0;HDR=YES;\"";
                    mimetypes = "application/vnd.ms-excel";
                }
                else if (extension == ".xlsx")
                {
                    connectionstring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + destinationfile + "';Extended Properties=\"Excel 12.0;HDR=YES;\"";
                    mimetypes = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                using (OleDbConnection oleconn = new OleDbConnection(connectionstring))
                {
                    oleconn.Open();
                    OleDbCommand olecommand = new OleDbCommand();
                    olecommand.Connection = oleconn;
                    olecommand.CommandText = String.Format("CREATE TABLE Sheet1 ({0});", columns);
                    olecommand.ExecuteNonQuery();

                    foreach (string row in rows)
                    {
                        olecommand.CommandText = row;
                        olecommand.ExecuteNonQuery();
                    }

                    oleconn.Close();
                }

                FilenameInput.Value = filename;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "download", "download();", true);
            }
            else
            {
                AlertLabel.Text = String.Join("<br>", errors.ToArray());
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            if (((string[])Session["add"]).Length > 0)
            {
                Session["origin"] = "PurchaseOrder";
                Session["storevalue"] = StoreChoice.SelectedValue;
                Session["suppliervalue"] = SupplierChoice.SelectedValue;
                Session["pagevalue"] = PurchaseOrderGridView.PageIndex;
                Session["sortorder"] = ViewState["sort"];
                Session["columnname"] = ViewState["column"];
                Session["searchquery"] = ViewState["search"];
                Session["redirect"] = true;
                Response.Redirect("PurchaseOrderForm.aspx");
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void ApproveButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "approve" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[2].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[2].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["suppliers"]).Contains(-1)) &&
                    ((string[])Session["auto"]).Contains("approvalBy") && ((string[])Session["auto"]).Contains("approvalDate") &&
                    ((string[])Session["auto"]).Contains("purchaseStatus"))
                {
                    //if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    //{
                        if (isEditableOrDeletable(int.Parse(arg[0].ToString()), arg[1].ToString()))
                        {
                            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

                            SqlConnection connection = new SqlConnection(constr);
                            SqlCommand cmd = connection.CreateCommand();
                            cmd.CommandText = @"SELECT COUNT(*) FROM purchase_orders
                            WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid AND
                            store_id = @storeId AND supplier_id = @supplierId AND 
                            (purchase_status = 'pending' OR purchase_status = 'cancelled') AND 
                            (purchase_no IS NOT NULL OR LTRIM(RTRIM(purchase_no)) != '') AND
                            isdeleted = 0;";
                            cmd.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                            cmd.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                            cmd.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0].ToString()));
                            cmd.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());

                            int count = 0;

                            connection.Open();
                            object result = cmd.ExecuteScalar();
                            connection.Close();

                            if (result != null)
                            {
                                count = int.Parse(result.ToString());
                            }

                            if (count > 0)
                            {
                                cmd = connection.CreateCommand();
                                cmd.CommandText = @"UPDATE purchase_orders SET approval_date = getdate(), 
                                approval_by = @user, purchase_status = @purchaseStatus, modified_date = getdate(), modified_by = @user
                                WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid
                                AND store_id = @storeId AND supplier_id = @supplierId AND isdeleted = 0";
                                
                                cmd.Parameters.AddWithValue("@user", Session["username"]);
                                cmd.Parameters.AddWithValue("@purchaseStatus", "approved");
                                cmd.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0]));
                                cmd.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                                cmd.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                                cmd.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                                connection.Open();
                                cmd.ExecuteNonQuery();
                                connection.Close();

                                BindPurchaseOrderGridView();

                            }
                            else
                            {
                                AlertLabel.Text = "Dokumen pembelian tidak tersedia";
                                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                        }
                        else
                        {
                            AlertLabel.Text = "Data tidak dapat dihapus/diubah";
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    //}
                    //else
                    //{
                    //    AlertLabel.Text = "Data terkunci";
                    //    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    //}
                }
                else
                {
                    AlertLabel.Text = "Permintaan akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void UploadTabButton_Click(object sender, EventArgs e)
        {
            menu = "upload";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
        }

        protected void TabularTabButton_Click(object sender, EventArgs e)
        {
            menu = "tabular";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectTabularTab();", true);
        }

        protected void CancelButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "cancel" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[2].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[2].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["suppliers"]).Contains(-1)) &&
                    ((string[])Session["auto"]).Contains("approvalBy") && ((string[])Session["auto"]).Contains("approvalDate") &&
                    ((string[])Session["auto"]).Contains("purchaseStatus"))
                {
                    
                    //if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    //{
                        if (isEditableOrDeletable(int.Parse(arg[0].ToString()), arg[1].ToString()))
                        {
                            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                            SqlConnection conn = new SqlConnection(constr);

                            SqlCommand cmd = conn.CreateCommand();
                            cmd.CommandText = @"SELECT COUNT(*) FROM purchase_orders WHERE store_id = @storeId AND 
                            supplier_id = @supplierId AND (purchase_status = 'pending' OR purchase_status = 'approved') AND
                            (purchase_no IS NOT NULL OR LTRIM(RTRIM(purchase_no)) != '') AND
                            isdeleted = 0 AND purchase_order_id = @purchaseId AND uuid = @purchaseUuid;";
                            cmd.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                            cmd.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                            cmd.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0].ToString()));
                            cmd.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());

                            int count = 0;

                            conn.Open();
                            object result = cmd.ExecuteScalar();
                            conn.Close();

                            if (result != null)
                            {
                                count = int.Parse(result.ToString());
                            }

                            if (count > 0)
                            {
                                SqlCommand command = conn.CreateCommand();
                                command.CommandText = @"UPDATE purchase_orders SET approval_date = getdate(), 
                                approval_by = @user, purchase_status = @purchaseStatus, modified_date = getdate(), modified_by = @user
                                WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid
                                AND store_id = @storeId AND supplier_id = @supplierId AND isdeleted = 0";
                                command.Parameters.AddWithValue("@user", Session["username"]);
                                command.Parameters.AddWithValue("@purchaseStatus", "cancelled");
                                command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0]));
                                command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                                command.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                                command.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                                conn.Open();
                                command.ExecuteNonQuery();
                                conn.Close();

                                
                                BindPurchaseOrderGridView();

                            }
                            else
                            {
                                AlertLabel.Text = "Terjadi kesalahan";
                                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                        }
                        else
                        {
                            AlertLabel.Text = "Dokumen pembelian tidak bisa dibatalkan";
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    //}
                    //else
                    //{
                    //    AlertLabel.Text = "Data terkunci";
                    //    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    //}
                }
                else
                {
                    AlertLabel.Text = "Permintaan akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected bool isEditableOrDeletable(int id, string uuid)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
            SqlCommand cmd = conn.CreateCommand();

            //cmd.CommandText = @"SELECT COUNT(*) FROM goods_receipts INNER JOIN purchase_orders 
            //    ON goods_receipts.purchase_order_id = purchase_orders.purchase_order_id
            //    WHERE purchase_orders.purchase_order_id = @purchaseId AND purchase_orders.uuid = @purchaseUuid 
            //    AND purchase_orders.isdeleted = 0 AND (goods_receipts.goods_receipt_no IS NULL OR goods_receipts.goods_receipt_no = '')
            //    AND goods_receipts.isdeleted = 0;";
            cmd.CommandText = @"SELECT COUNT(*) FROM goods_receipts INNER JOIN purchase_orders 
                ON goods_receipts.purchase_order_id = purchase_orders.purchase_order_id
                WHERE purchase_orders.purchase_order_id = @purchaseId AND purchase_orders.uuid = @purchaseUuid 
                AND purchase_orders.isdeleted = 0 AND goods_receipts.isdeleted = 0;";

            cmd.Parameters.AddWithValue("@purchaseId", id);
            cmd.Parameters.AddWithValue("@purchaseUuid", uuid);
            cmd.Connection = conn;

            int count = 0;

            conn.Open();
            object result = cmd.ExecuteScalar();
            conn.Close();

            if (result != null)
            {
                count = int.Parse(result.ToString());
            }

            if (count > 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        protected bool IsLocked(int id, string uuid)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"SELECT COUNT(*) FROM purchase_orders 
                WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid 
                AND isdeleted = 0 AND locked = 1;";

            cmd.Parameters.AddWithValue("@purchaseId", id);
            cmd.Parameters.AddWithValue("@purchaseUuid", uuid);
            cmd.Connection = conn;

            int count = 0;

            conn.Open();
            object result = cmd.ExecuteScalar();
            conn.Close();

            if (result != null)
            {
                count = int.Parse(result.ToString());
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void NoButton_Click(object sender, EventArgs e)
        {
            
        }

        protected void YesButton_Click(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "delete" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[2].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(arg[3].ToString()))) &&
                    (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(arg[4].ToString()))) &&
                    (bool)Session["delete"] == true)
                {
                    if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    {
                        if (isEditableOrDeletable(int.Parse(arg[0].ToString()), arg[1].ToString()) || Session["role_name"].ToString() == "superuser")
                        {
                            try
                            {
                                string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                                SqlConnection conn = new SqlConnection(constr);
                                SqlTransaction transaction;

                                conn.Open();
                                transaction = conn.BeginTransaction();

                                SqlCommand command = new SqlCommand();

                                if (Session["role_name"].ToString() == "superuser")
                                {
                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                    FROM goods_receipt_items A 
                                    INNER JOIN goods_receipts B ON A.goods_receipt_id = B.goods_receipt_id AND B.isdeleted = 0 
                                    INNER JOIN purchase_orders C ON C.purchase_order_id = B.purchase_order_id AND C.isdeleted = 0
                                    WHERE A.isdeleted = 0 AND C.store_id = @storeId AND C.supplier_id = @supplierId AND 
                                    C.uuid = @purchaseUuid AND C.purchase_order_id = @purchaseId";
                                    command.Transaction = transaction;
                                    command.Parameters.AddWithValue("@storeId", int.Parse(arg[3].ToString()));
                                    command.Parameters.AddWithValue("@supplierId", int.Parse(arg[4].ToString()));
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0].ToString()));
                                    command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());

                                    //conn.Open();
                                    command.ExecuteNonQuery();
                                    //conn.Close();

                                    command = conn.CreateCommand();
                                    command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                    FROM goods_receipts A 
                                    INNER JOIN purchase_orders B ON A.purchase_order_id = B.purchase_order_id AND B.isdeleted = 0
                                    WHERE A.isdeleted = 0 AND B.store_id = @storeId AND B.supplier_id = @supplierId AND 
                                    B.uuid = @purchaseUuid AND B.purchase_order_id = @purchaseId";
                                    command.Transaction = transaction;
                                    command.Parameters.AddWithValue("@storeId", int.Parse(arg[3].ToString()));
                                    command.Parameters.AddWithValue("@supplierId", int.Parse(arg[4].ToString()));
                                    command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0].ToString()));
                                    command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                                    command.Parameters.AddWithValue("@user", Session["username"].ToString());

                                    //conn.Open();
                                    command.ExecuteNonQuery();
                                    //conn.Close();
                                }

                                command = conn.CreateCommand();
                                command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                FROM purchase_order_items A 
                                INNER JOIN purchase_orders B ON A.purchase_order_id = B.purchase_order_id AND B.isdeleted = 0
                                WHERE B.store_id = @storeId AND B.supplier_id = @supplierId AND 
                                B.uuid = @purchaseUuid AND B.purchase_order_id = @purchaseId AND A.isdeleted = 0";
                                command.Transaction = transaction;
                                command.Parameters.AddWithValue("@storeId", int.Parse(arg[3].ToString()));
                                command.Parameters.AddWithValue("@supplierId", int.Parse(arg[4].ToString()));
                                command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0].ToString()));
                                command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                                command.Parameters.AddWithValue("@user", Session["username"].ToString());
                                //conn.Open();
                                command.ExecuteNonQuery();
                                //conn.Close();

                                command = conn.CreateCommand();
                                command.CommandText = @"UPDATE A SET isdeleted = 1, modified_date = getdate(), modified_by = @user 
                                FROM purchase_orders A WHERE A.store_id = @storeId AND A.supplier_id = @supplierId AND 
                                A.uuid = @purchaseUuid AND A.purchase_order_id = @purchaseId AND A.isdeleted = 0";
                                command.Transaction = transaction;
                                command.Parameters.AddWithValue("@storeId", int.Parse(arg[3].ToString()));
                                command.Parameters.AddWithValue("@supplierId", int.Parse(arg[4].ToString()));
                                command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0].ToString()));
                                command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                                command.Parameters.AddWithValue("@user", Session["username"].ToString());

                                //conn.Open();
                                command.ExecuteNonQuery();
                                //conn.Close();

                                transaction.Commit();
                                conn.Close();

                                BindPurchaseOrderGridView();
                                AlertLabel.Text = "Dokumen pemesanan berhasil dihapus";
                                AlertLabel.CssClass = "alert alert-light-primary color-primary d-block";
                            }
                            catch (Exception error)
                            {
                                AlertLabel.Text = error.Message.ToString();
                                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                        }
                        else
                        {
                            AlertLabel.Text = "Dokumen pemesanan tidak bisa dihapus/diubah";
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen pemesanan terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                    
                }
                else
                {
                    AlertLabel.Text = "Akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void LockButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "lock" && arg.Length > 0)
            {
                if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                {
                    string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                    SqlConnection conn = new SqlConnection(constr);
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE purchase_orders SET locked = 1
                                    WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid
                                    AND store_id = @storeId AND supplier_id = @supplierId;";
                    command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0]));
                    command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                    command.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                    command.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();

                    BindPurchaseOrderGridView();
                }
                else
                {
                    if (Session["role_name"].ToString() == "superuser")
                    {
                        string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                        SqlConnection conn = new SqlConnection(constr);
                        SqlCommand command = conn.CreateCommand();
                        command.CommandText = @"UPDATE purchase_orders SET locked = 0
                                    WHERE purchase_order_id = @purchaseId AND uuid = @purchaseUuid
                                    AND store_id = @storeId AND supplier_id = @supplierId;";
                        command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0]));
                        command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                        command.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                        command.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                        conn.Open();
                        command.ExecuteNonQuery();
                        conn.Close();


                        BindPurchaseOrderGridView();
                    }
                    else
                    {
                        AlertLabel.Text = "Data sudah terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }

                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void DeleteButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "delete" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[4].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[4].ToString())) || ((int[])Session["suppliers"]).Contains(-1)) &&
                    (bool)Session["delete"] == true)
                {
                    if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    {
                        if (isEditableOrDeletable(int.Parse(arg[0].ToString()), arg[1].ToString()) || Session["role_name"].ToString() == "superuser")
                        {
                            YesButton.CommandName = "delete";
                            YesButton.CommandArgument = e.CommandArgument.ToString();
                            PurchaseNoModalLabel.Text = arg[2];

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openConfirmModal();", true);
                        }
                        else
                        {
                            AlertLabel.Text = "Dokumen pengiriman tidak bisa dihapus/diubah";
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen pengiriman terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Permintaan akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void PurchaseOrderGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];

            if (ViewState["column"].ToString() == e.SortExpression)
            {
                if (ViewState["sort"].ToString() == "ASC")
                {
                    ViewState["sort"] = "DESC";
                    ViewState["column"] = e.SortExpression;
                    purchases.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
                }
                else if (ViewState["sort"].ToString() == "DESC")
                {
                    ViewState["sort"] = "ASC";
                    ViewState["column"] = e.SortExpression;
                    purchases.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
                }
            }
            else
            {
                ViewState["column"] = e.SortExpression;
                ViewState["sort"] = "ASC";
                purchases.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            }

            ViewState["purchases"] = purchases;
            PurchaseOrderGridView.DataSource = purchases;
            PurchaseOrderGridView.DataBind();

        }

        protected void PurchaseOrderGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            PurchaseOrderGridView.PageIndex = e.NewPageIndex;
            ViewState["page"] = e.NewPageIndex;

            System.Data.DataTable purchases = (System.Data.DataTable)ViewState["purchases"];

            if (purchases.Rows.Count > 0)
            {
                ViewState["purchases"] = purchases;
                PurchaseOrderGridView.DataSource = purchases;
                PurchaseOrderGridView.DataBind();
            }
        }

        protected void StoreChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["sort"] = "ASC";
            ViewState["column"] = "no";
            ViewState["page"] = 0;
            ViewState["search"] = "";
            ValueOneInput.Text = "";
            ValueTwoInput.Text = "";
            BindPurchaseOrderGridView();
        }

        protected void SupplierChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["sort"] = "ASC";
            ViewState["column"] = "no";
            ViewState["page"] = 0;
            ViewState["search"] = "";
            ValueOneInput.Text = "";
            ValueTwoInput.Text = "";
            BindPurchaseOrderGridView();
        }

        protected void StoreUploadChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            UploadLabel.Visible = true;
            UploadInput.Visible = true;
            UploadButton.Visible = true;
            SaveUploadButton.Visible = false;
            CancelUploadButton.Visible = false;
            InfoPanel.Visible = false;
            ItemGridView.DataSource = null;
            ItemGridView.DataBind();
            
            LoadStore();
            FillStoreInput();
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
        }

        protected void SupplierUploadChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            UploadLabel.Visible = true;
            UploadInput.Visible = true;
            UploadButton.Visible = true;
            SaveUploadButton.Visible = false;
            CancelUploadButton.Visible = false;
            InfoPanel.Visible = false;
            ItemGridView.DataSource = null;
            ItemGridView.DataBind();

            LoadSupplier();
            FillSupplierInput();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
        }

        protected void PurchaseOrderGridView_RowCancelingEdit(Object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void PurchaseOrderGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void PurchaseOrderGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void ItemGridView_RowCancelingEdit(Object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void ItemGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void CloseDeliveryModalButton_Click(object sender, EventArgs e)
        {
            DeliveryDateInput.Text = "";
            DeliveryNotesInput.Text = "";
        }

        protected void SaveDeliveryModalButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "edit" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[2].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[2].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["suppliers"]).Contains(-1))
                    && ((string[])Session["add"]).Contains("deliveryOrder"))
                {
                    //if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    //{
                        //if (isEditableOrDeletable(int.Parse(arg[0].ToString()), arg[1].ToString()))
                        //{
                            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                            SqlConnection conn = new SqlConnection(constr);
                            SqlCommand command = conn.CreateCommand();
                            command.CommandText = @"SELECT COUNT(*) FROM purchase_orders 
                            WHERE store_id = @storeId AND supplier_id = @supplierId AND
                            isdeleted = 0 AND purchase_order_id = @purchaseId AND
                            uuid = @purchaseUuid AND (purchase_no IS NOT NULL OR LTRIM(RTRIM(purchase_no)) != '')
                            AND (delivery_no IS NULL OR LTRIM(RTRIM(delivery_no)) = '');";
                            command.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                            command.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                            command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0].ToString()));
                            command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());

                            conn.Open();
                            int count = 0;

                            object result = command.ExecuteScalar();

                            if (result != null)
                            {
                                count = int.Parse(result.ToString());
                            }
                            conn.Close();

                            if (count > 0)
                            {
                                DateTime deliveryDate;

                                if (!String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()) &&
                                    DateTime.TryParse(DeliveryDateInput.Text?.Trim(), out deliveryDate))
                                {
                                    try
                                    {
                                        string dopattern = "DO" + DateTime.Now.ToString("yyyyMMdd");
                                        string deliveryNo = dopattern + "1".ToString().PadLeft(3, '0');

                                        command = conn.CreateCommand();
                                        command.CommandText = @"SELECT TOP 1 CAST(RIGHT([delivery_no], 3) AS INT) FROM purchase_orders WHERE 
                                        delivery_no LIKE @deliveryNo ORDER BY delivery_no DESC";
                                        command.Parameters.AddWithValue("@deliveryNo", string.Format("{0}%", dopattern));

                                        int number = 1;
                                        conn.Open();
                                        object rslt = command.ExecuteScalar();
                                        conn.Close();

                                        if (rslt != null)
                                        {
                                            number = int.Parse(rslt.ToString()) + 1;
                                        }

                                        deliveryNo = dopattern + number.ToString().PadLeft(3, '0');

                                        command = conn.CreateCommand();
                                        command.CommandText = @"UPDATE purchase_orders SET delivery_no = @deliveryNo, delivery_date = @deliveryDate,
                                        delivery_notes = @deliveryNotes
                                        WHERE store_id = @storeId AND supplier_id = @supplierId AND
                                        isdeleted = 0 AND purchase_order_id = @purchaseId AND
                                        uuid = @purchaseUuid AND (delivery_no IS NULL OR delivery_no = '');";
                                        command.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                                        command.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                                        command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0].ToString()));
                                        command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                                        command.Parameters.AddWithValue("@deliveryNo", deliveryNo);
                                        command.Parameters.AddWithValue("@deliveryDate", DeliveryDateInput.Text?.Trim());
                                        if(String.IsNullOrEmpty(DeliveryNotesInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@deliveryNotes", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@deliveryNotes", DeliveryNotesInput.Text?.Trim());
                                        }

                                        conn.Open();
                                        command.ExecuteNonQuery();
                                        conn.Close();

                                        DeliveryDateInput.Text = "";
                                        DeliveryNotesInput.Text = "";

                                        
                                        BindPurchaseOrderGridView();

                                    }
                                    catch (Exception error)
                                    {
                                        AlertLabel.Text = error.Message.ToString();
                                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                                    }
                                }
                                else
                                {
                                    AlertLabel.Text = "Tanggal pengirimanan tidak boleh kosong";
                                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                                }
                            }
                            else
                            {
                                AlertLabel.Text = "Dokumen pengirimanan barang sudah ada";
                                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                        
                        //}
                        //else
                        //{
                        //    AlertLabel.Text = "Dokumen pengirimanan barang sudah ada";
                        //    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        //}
                    
                }
                else
                {
                    AlertLabel.Text = "Äkses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void CreateButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "create" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[2].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[2].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["suppliers"]).Contains(-1))
                    && ((string[])Session["add"]).Contains("deliveryOrder"))
                {
                    //if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    //{
                        if (isEditableOrDeletable(int.Parse(arg[0].ToString()), arg[1].ToString()))
                        {
                            SaveDeliveryModalButton.CommandName = "edit";
                            SaveDeliveryModalButton.CommandArgument = e.CommandArgument.ToString();
                            
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openDeliveryModal();", true);
                        }
                        else
                        {
                            AlertLabel.Text = "Dokumen pengiriman sudah tersedia";
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    //}
                    //else
                    //{
                    //    AlertLabel.Text = "Data terkunci";
                    //    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    //}            
                }
                else
                {
                    AlertLabel.Text = "Akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void EditButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "edit" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[2].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[2].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["suppliers"]).Contains(-1))
                    && ((string[])Session["edit"]).Length > 0)
                {
                    if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    {
                        if (isEditableOrDeletable(int.Parse(arg[0].ToString()), arg[1].ToString()) || Session["role_name"].ToString() == "superuser")
                        {
                            Session["origin"] = "PurchaseOrder";
                            Session["storevalue"] = StoreChoice.SelectedValue;
                            Session["suppliervalue"] = SupplierChoice.SelectedValue;
                            Session["pagevalue"] = PurchaseOrderGridView.PageIndex;
                            Session["sortorder"] = ViewState["sort"];
                            Session["columnname"] = ViewState["column"];
                            Session["searchquery"] = ViewState["search"];
                            Session["redirect"] = true;
                            Response.Redirect("PurchaseOrderForm.aspx?purchaseId=" + arg[0].ToString() + "&purchaseUuid=" + arg[1].ToString());
                        }
                        else
                        {
                            AlertLabel.Text = "Dokumen pembelian tidak bisa dihapus/diubah";
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen pembelian terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                    //Session["PageName"] = System.IO.Path.GetFileName(System.Web.HttpContext.Current.Request.Url.AbsolutePath);
                    
                }
                else
                {
                    AlertLabel.Text = "Akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void ShowButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "show" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[2].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[2].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["suppliers"]).Contains(-1))
                    && ((string[])Session["read"]).Length > 0)
                {
                    string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                    SqlConnection conn = new SqlConnection(constr);
                    SqlCommand cmd = conn.CreateCommand();

                    cmd.CommandText = @"SELECT COUNT(*) FROM purchase_orders WHERE store_id = @storeId AND supplier_id = @supplierId AND 
                    purchase_order_id = @purchaseId AND uuid = @purchaseUuid AND (purchase_no IS NOT NULL OR LTRIM(RTRIM(purchase_no)) != '')
                    AND purchase_status = 'approved'";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                    cmd.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0].ToString()));
                    cmd.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                    int count = 0;

                    conn.Open();
                    object result = cmd.ExecuteScalar();
                    conn.Close();

                    if (result != null)
                    {
                        count = int.Parse(result.ToString());
                    }

                    if (count > 0)
                    {
                        Session["origin"] = "PurchaseOrder";
                        Session["storevalue"] = StoreChoice.SelectedValue;
                        Session["suppliervalue"] = SupplierChoice.SelectedValue;
                        Session["pagevalue"] = PurchaseOrderGridView.PageIndex;
                        Session["sortorder"] = ViewState["sort"];
                        Session["columnname"] = ViewState["column"];
                        Session["searchquery"] = ViewState["search"];
                        Session["redirect"] = true;
                        Response.Redirect("PurchaseOrderDetail.aspx?purchaseId=" + arg[0].ToString() + "&purchaseUuid=" + arg[1].ToString());
                    }
                    else
                    {
                        AlertLabel.Text = "No pembelian tidak tersedia";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            //Response.Redirect("deliveryOrder.aspx");
        }

        protected void SaveUploadButton_Click(object sender, EventArgs e)
        {
            List<string> errors = new List<string>();

            if (((string[])Session["add"]).Contains("purchaseNo"))
            {
                if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()) && !PurchaseNoCheckBox.Checked)
                {
                    errors.Add("No pesanan tidak boleh kosong");
                }
            }

            //if (((string[])Session["add"]).Contains("deliveryNo"))
            //{
            //    if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()) && !DeliveryNoCheckBox.Checked)
            //    {
            //        errors.Add("No pengiriman tidak boleh kosong");
            //    }

            //}

            //if (((string[])Session["add"]).Contains("deliveryDate"))
            //{
            //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
            //    {
            //        errors.Add("Tanggal pengiriman tidak boleh kosong");
            //    }
            //}


            if (((string[])Session["add"]).Contains("totalQty"))
            {
                if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()) && !TotalQtyCheckBox.Checked)
                {
                    errors.Add("Total kuantitas barang tidak boleh kosong");
                }

            }

            if (((string[])Session["add"]).Contains("totalPrice"))
            {
                if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()) && !TotalPriceCheckBox.Checked)
                {
                    errors.Add("Total harga barang tidak boleh kosong");
                }

            }

            if (((string[])Session["add"]).Contains("approvalBy"))
            {
                if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                        && String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()) && !ApprovalByCheckBox.Checked)
                {
                    errors.Add("Ditolak/Disetujui oleh tidak boleh kosong");
                }

            }

            if (((string[])Session["add"]).Contains("approvalDate"))
            {
                if ((PurchaseStatusChoice.SelectedValue == "approved" || PurchaseStatusChoice.SelectedValue == "cancelled")
                        && String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()) && !ApprovalDateCheckBox.Checked)
                {
                    errors.Add("Tanggal ditolak/disetujui oleh tidak boleh kosong");
                }

            }

            if (((string[])Session["add"]).Contains("requestedBy"))
            {
                if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()) && !RequestedByCheckBox.Checked)
                {
                    errors.Add("Dipesan oleh tidak boleh kosong");
                }
            }

            if (((string[])Session["add"]).Contains("requestedDate"))
            {
                if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()) && !RequestedDateCheckBox.Checked)
                {
                    errors.Add("Tanggal dipesan oleh tidak boleh kosong");
                }
            }

            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

            if (inserts.Rows.Count <= 0)
            {
                errors.Add("Barang yang akan dibeli tidak boleh kosong");
            }

            if (errors.Count <= 0)
            {
                string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

                SqlConnection conn = new SqlConnection(constr);
                SqlTransaction transaction;
                conn.Open();
                transaction = conn.BeginTransaction();

                try
                {
                    string popattern = "PO" + DateTime.Now.ToString("yyyyMMdd");
                    string purchaseNo = popattern + "1".ToString().PadLeft(3, '0');

                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"SELECT TOP 1 CAST(RIGHT([purchase_no], 3) AS INT) FROM purchase_orders WHERE 
                                    purchase_no LIKE @purchaseNo ORDER BY purchase_no DESC";
                    command.Parameters.AddWithValue("@purchaseNo", string.Format("{0}%", popattern));
                    command.Transaction = transaction;
                    int number = 1;

                    object result = command.ExecuteScalar();
                    
                    if (result != null)
                    {
                        number = int.Parse(result.ToString()) + 1;
                    }

                    purchaseNo = popattern + number.ToString().PadLeft(3, '0');

                    int totalQty = 0;
                    double totalPrice = 0;

                    foreach (DataRow row in inserts.Rows)
                    {
                        totalQty += int.Parse(row["item_qty"].ToString());
                        totalPrice += double.Parse(row["item_price"].ToString()) * int.Parse(row["item_qty"].ToString());
                    }

                    string fields = "";
                    string values = "";
                    bool withComma = false;

                    if (((string[])Session["add"]).Contains("supplier") ||
                        ((string[])Session["auto"]).Contains("supplier"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_id");
                        values = String.Concat(values, " ", "@supplierId");
                    }

                    if (((string[])Session["add"]).Contains("supplierName") ||
                        ((string[])Session["auto"]).Contains("supplierName"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_name");
                        values = String.Concat(values, " ", "@supplierName");
                    }

                    if (((string[])Session["add"]).Contains("supplierBuildingName") ||
                        ((string[])Session["auto"]).Contains("supplierBuildingName"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_building_name");
                        values = String.Concat(values, " ", "@supplierBuildingName");
                    }

                    if (((string[])Session["add"]).Contains("supplierStreetName") ||
                        ((string[])Session["auto"]).Contains("supplierStreetName"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_street_name");
                        values = String.Concat(values, " ", "@supplierStreetName");
                    }

                    if (((string[])Session["add"]).Contains("supplierNeighbourhood") ||
                        ((string[])Session["auto"]).Contains("supplierNeighbourhood"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_neighbourhood");
                        values = String.Concat(values, " ", "@supplierNeighbourhood");
                    }

                    if (((string[])Session["add"]).Contains("supplierSubdistrict") ||
                        ((string[])Session["auto"]).Contains("supplierSubdistrict"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_subdistrict");
                        values = String.Concat(values, " ", "@supplierSubdistrict");
                    }

                    if (((string[])Session["add"]).Contains("supplierDistrict") ||
                        ((string[])Session["auto"]).Contains("supplierDistrict"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_district");
                        values = String.Concat(values, " ", "@supplierDistrict");
                    }

                    if (((string[])Session["add"]).Contains("supplierRuralDistrict") ||
                        ((string[])Session["auto"]).Contains("supplierRuralDistrict"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_rural_district");
                        values = String.Concat(values, " ", "@supplierRuralDistrict");
                    }

                    if (((string[])Session["add"]).Contains("supplierProvince") ||
                        ((string[])Session["auto"]).Contains("supplierProvince"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_province");
                        values = String.Concat(values, " ", "@supplierProvince");
                    }

                    if (((string[])Session["add"]).Contains("supplierZipcode") ||
                        ((string[])Session["auto"]).Contains("supplierZipcode"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_zipcode");
                        values = String.Concat(values, " ", "@supplierZipcode");
                    }

                    if (((string[])Session["add"]).Contains("supplierContactName") ||
                        ((string[])Session["auto"]).Contains("supplierContactName"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_contact_name");
                        values = String.Concat(values, " ", "@supplierContactName");
                    }

                    if (((string[])Session["add"]).Contains("supplierContactPhone") ||
                        ((string[])Session["auto"]).Contains("supplierContactPhone"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_contact_phone");
                        values = String.Concat(values, " ", "@supplierContactPhone");
                    }

                    if (((string[])Session["add"]).Contains("supplierContactEmail") ||
                        ((string[])Session["auto"]).Contains("supplierContactEmail"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "supplier_contact_email");
                        values = String.Concat(values, " ", "@supplierContactEmail");
                    }

                    if (((string[])Session["add"]).Contains("store") ||
                        ((string[])Session["auto"]).Contains("store"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_id");
                        values = String.Concat(values, " ", "@storeId");
                    }

                    if (((string[])Session["add"]).Contains("storeName") ||
                        ((string[])Session["auto"]).Contains("storeName"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_name");
                        values = String.Concat(values, " ", "@storeName");
                    }

                    if (((string[])Session["add"]).Contains("storeBuildingName") ||
                        ((string[])Session["auto"]).Contains("storeBuildingName"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_building_name");
                        values = String.Concat(values, " ", "@storeBuildingName");
                    }

                    if (((string[])Session["add"]).Contains("storeStreetName") ||
                        ((string[])Session["auto"]).Contains("storeStreetName"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_street_name");
                        values = String.Concat(values, " ", "@storeStreetName");
                    }

                    if (((string[])Session["add"]).Contains("storeNeighbourhood") ||
                        ((string[])Session["auto"]).Contains("storeNeighbourhood"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_neighbourhood");
                        values = String.Concat(values, " ", "@storeNeighbourhood");
                    }

                    if (((string[])Session["add"]).Contains("storeSubdistrict") ||
                        ((string[])Session["auto"]).Contains("storeSubdistrict"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_subdistrict");
                        values = String.Concat(values, " ", "@storeSubdistrict");
                    }

                    if (((string[])Session["add"]).Contains("storeDistrict") ||
                        ((string[])Session["auto"]).Contains("storeDistrict"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_district");
                        values = String.Concat(values, " ", "@storeDistrict");
                    }

                    if (((string[])Session["add"]).Contains("storeRuralDistrict") ||
                        ((string[])Session["auto"]).Contains("storeRuralDistrict"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_rural_district");
                        values = String.Concat(values, " ", "@storeRuralDistrict");
                    }

                    if (((string[])Session["add"]).Contains("storeProvince") ||
                        ((string[])Session["auto"]).Contains("storeProvince"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_province");
                        values = String.Concat(values, " ", "@storeProvince");
                    }

                    if (((string[])Session["add"]).Contains("storeZipcode") ||
                        ((string[])Session["auto"]).Contains("storeZipcode"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_zipcode");
                        values = String.Concat(values, " ", "@storeZipcode");
                    }

                    if (((string[])Session["add"]).Contains("storeContactName") ||
                        ((string[])Session["auto"]).Contains("storeContactName"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_contact_name");
                        values = String.Concat(values, " ", "@storeContactName");
                    }

                    if (((string[])Session["add"]).Contains("storeContactPhone") ||
                        ((string[])Session["auto"]).Contains("storeContactPhone"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_contact_phone");
                        values = String.Concat(values, " ", "@storeContactPhone");
                    }

                    if (((string[])Session["add"]).Contains("storeContactEmail") ||
                        ((string[])Session["auto"]).Contains("storeContactEmail"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "store_contact_email");
                        values = String.Concat(values, " ", "@storeContactEmail");
                    }

                    if (((string[])Session["add"]).Contains("purchaseNo") ||
                        ((string[])Session["auto"]).Contains("purchaseNo"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "purchase_no");
                        values = String.Concat(values, " ", "@purchaseNo");
                    }

                    if (((string[])Session["add"]).Contains("purchaseDate") ||
                        ((string[])Session["auto"]).Contains("purchaseDate"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "purchase_date");
                        values = String.Concat(values, " ", "@purchaseDate");
                    }

                    //if (((string[])Session["add"]).Contains("deliveryNo") ||
                    //    ((string[])Session["auto"]).Contains("deliveryNo"))
                    //{
                    //    if (withComma)
                    //    {
                    //        fields = String.Concat(fields, ",");
                    //        values = String.Concat(values, ",");
                    //    }
                    //    else
                    //    {
                    //        withComma = true;
                    //    }
                    //    fields = String.Concat(fields, " ", "delivery_no");
                    //    values = String.Concat(values, " ", "@deliveryNo");
                    //}

                    //if (((string[])Session["add"]).Contains("deliveryDate"))
                    //{
                    //    if (withComma)
                    //    {
                    //        fields = String.Concat(fields, ",");
                    //        values = String.Concat(values, ",");
                    //    }
                    //    else
                    //    {
                    //        withComma = true;
                    //    }
                    //    fields = String.Concat(fields, " ", "delivery_date");
                    //    values = String.Concat(values, " ", "@deliveryDate");
                    //}

                    if (((string[])Session["add"]).Contains("totalQty") ||
                        ((string[])Session["auto"]).Contains("totalQty"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "total_qty");
                        values = String.Concat(values, " ", "@totalQty");
                    }

                    if (((string[])Session["add"]).Contains("totalPrice") ||
                        ((string[])Session["auto"]).Contains("totalPrice"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "total_price");
                        values = String.Concat(values, " ", "@totalPrice");
                    }

                    if (((string[])Session["add"]).Contains("approvalBy") ||
                        ((string[])Session["auto"]).Contains("approvalBy"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "approval_by");
                        values = String.Concat(values, " ", "@approvalBy");
                    }

                    if (((string[])Session["add"]).Contains("approvalDate") ||
                        ((string[])Session["auto"]).Contains("approvalDate"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "approval_date");
                        values = String.Concat(values, " ", "@approvalDate");
                    }

                    if (((string[])Session["add"]).Contains("requestedBy") ||
                        ((string[])Session["auto"]).Contains("requestedBy"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "requested_by");
                        values = String.Concat(values, " ", "@requestedBy");
                    }

                    if (((string[])Session["add"]).Contains("requestedDate") ||
                        ((string[])Session["auto"]).Contains("requestedDate"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "requested_date");
                        values = String.Concat(values, " ", "@requestedDate");
                    }

                    if (((string[])Session["add"]).Contains("purchaseStatus") ||
                        ((string[])Session["auto"]).Contains("purchaseStatus"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "purchase_status");
                        values = String.Concat(values, " ", "@purchaseStatus");
                    }

                    if (((string[])Session["add"]).Contains("purchaseNotes") ||
                        ((string[])Session["auto"]).Contains("purchaseNotes"))
                    {
                        if (withComma)
                        {
                            fields = String.Concat(fields, ",");
                            values = String.Concat(values, ",");
                        }
                        else
                        {
                            withComma = true;
                        }
                        fields = String.Concat(fields, " ", "purchase_notes");
                        values = String.Concat(values, " ", "@purchaseNotes");
                    }

                    string commandText = @"INSERT INTO purchase_orders ({0}, created_date, created_by, modified_date, modified_by)
                        VALUES ({1}, getdate(), @user, getdate(), @user);SELECT SCOPE_IDENTITY();";

                    commandText = String.Format(commandText, fields, values);

                    command = conn.CreateCommand();
                    command.CommandText = commandText;
                    command.Parameters.AddWithValue("@user", Session["username"]);

                    if (((string[])Session["add"]).Contains("supplier"))
                    {
                        if (string.IsNullOrEmpty(SupplierChoice.SelectedValue))
                        {
                            command.Parameters.AddWithValue("@supplierId", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierUploadChoice.SelectedValue));
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplier"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierId"]))
                        {
                            command.Parameters.AddWithValue("@supplierId", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierId", int.Parse(supplier["supplierId"]));
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierName"))
                    {
                        if (SupplierNameCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierName"]))
                            {
                                command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierName", SupplierNameInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierName"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierName"]))
                        {
                            command.Parameters.AddWithValue("@supplierName", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierName", supplier["supplierName"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierBuildingName"))
                    {
                        if (SupplierBuildingNameCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                            {
                                command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierBuildingName", SupplierBuildingNameInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierBuildingName"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierBuildingName"]))
                        {
                            command.Parameters.AddWithValue("@supplierBuildingName", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierBuildingName", supplier["supplierBuildingName"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierStreetName"))
                    {
                        if (SupplierStreetNameCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                            {
                                command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                            }

                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierStreetName", SupplierStreetNameInput.Text?.Trim());
                            }

                        }

                    }
                    else if (((string[])Session["auto"]).Contains("supplierStreetName"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierStreetName"]))
                        {
                            command.Parameters.AddWithValue("@supplierStreetName", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierStreetName", supplier["supplierStreetName"]);
                        }

                    }

                    if (((string[])Session["add"]).Contains("supplierNeighbourhood"))
                    {
                        if (SupplierNeighbourhoodCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                            {
                                command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                            }

                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierNeighbourhood", SupplierNeighbourhoodInput.Text?.Trim());
                            }
                        }

                    }
                    else if (((string[])Session["auto"]).Contains("supplierNeighbourhood"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierNeighbourhood"]))
                        {
                            command.Parameters.AddWithValue("@supplierNeighbourhood", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierNeighbourhood", supplier["supplierNeighbourhood"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierSubdistrict"))
                    {
                        if (SupplierSubdistrictCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierSubdistrict", SupplierSubdistrictInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierSubdistrict"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                        {
                            command.Parameters.AddWithValue("@supplierSubdistrict", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierSubdistrict", supplier["supplierSubdistrict"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierDistrict"))
                    {
                        if (SupplierDistrictCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierDistrict", SupplierDistrictInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierDistrict"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierSubdistrict"]))
                        {
                            command.Parameters.AddWithValue("@supplierDistrict", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierDistrict", supplier["supplierDistrict"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierRuralDistrict"))
                    {
                        if (SupplierRuralDistrictCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                            {
                                command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierRuralDistrict", SupplierRuralDistrictInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierRuralDistrict"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierRuralDistrict"]))
                        {
                            command.Parameters.AddWithValue("@supplierRuralDistrict", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierRuralDistrict", supplier["supplierRuralDistrict"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierProvince"))
                    {
                        if (SupplierProvinceCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                            {
                                command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierProvince", SupplierProvinceInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierProvince"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierProvince"]))
                        {
                            command.Parameters.AddWithValue("@supplierProvince", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierProvince", supplier["supplierProvince"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierZipcode"))
                    {
                        if (SupplierZipcodeCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                            {
                                command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierZipcode", SupplierZipcodeInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierZipcode"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierZipcode"]))
                        {
                            command.Parameters.AddWithValue("@supplierZipcode", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierZipcode", supplier["supplierZipcode"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierContactName"))
                    {
                        if (SupplierContactNameCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                            {
                                command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactName", SupplierContactNameInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierContactName"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierContactName"]))
                        {
                            command.Parameters.AddWithValue("@supplierContactName", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierContactName", supplier["supplierContactName"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierContactPhone"))
                    {
                        if (SupplierContactPhoneCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                            {
                                command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactPhone", SupplierContactPhoneInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierContactPhone"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierContactPhone"]))
                        {
                            command.Parameters.AddWithValue("@supplierContactPhone", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierContactPhone", supplier["supplierContactPhone"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("supplierContactEmail"))
                    {
                        if (SupplierContactEmailCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                            {
                                command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@supplierContactEmail", SupplierContactEmailInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("supplierContactEmail"))
                    {
                        if (String.IsNullOrEmpty(supplier["supplierContactEmail"]))
                        {
                            command.Parameters.AddWithValue("@supplierContactEmail", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@supplierContactEmail", supplier["supplierContactEmail"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("store"))
                    {
                        if (string.IsNullOrEmpty(StoreChoice.SelectedValue))
                        {
                            command.Parameters.AddWithValue("@storeId", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeId", int.Parse(StoreUploadChoice.SelectedValue));
                        }

                    }
                    else if (((string[])Session["auto"]).Contains("store"))
                    {
                        if (String.IsNullOrEmpty(store["storeId"]))
                        {
                            command.Parameters.AddWithValue("@storeId", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeId", int.Parse(store["storeId"]));
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeName"))
                    {
                        if (StoreNameCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeName"]))
                            {
                                command.Parameters.AddWithValue("@storeName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeName", store["storeName"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeName", StoreNameInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeName"))
                    {
                        if (String.IsNullOrEmpty(store["storeName"]))
                        {
                            command.Parameters.AddWithValue("@storeName", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeName", store["storeName"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeBuildingName"))
                    {
                        if (StoreBuildingNameCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeBuildingName"]))
                            {
                                command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeBuildingName", StoreBuildingNameInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeBuildingName"))
                    {
                        if (String.IsNullOrEmpty(store["storeBuildingName"]))
                        {
                            command.Parameters.AddWithValue("@storeBuildingName", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeBuildingName", store["storeBuildingName"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeStreetName"))
                    {
                        if (StoreStreetNameCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeStreetName"]))
                            {
                                command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                            }

                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeStreetName", StoreStreetNameInput.Text?.Trim());
                            }

                        }

                    }
                    else if (((string[])Session["auto"]).Contains("storeStreetName"))
                    {
                        if (String.IsNullOrEmpty(store["storeStreetName"]))
                        {
                            command.Parameters.AddWithValue("@storeStreetName", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeStreetName", store["storeStreetName"]);
                        }

                    }

                    if (((string[])Session["add"]).Contains("storeNeighbourhood"))
                    {
                        if (StoreNeighbourhoodCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                            {
                                command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                            }

                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeNeighbourhood", StoreNeighbourhoodInput.Text?.Trim());
                            }
                        }

                    }
                    else if (((string[])Session["auto"]).Contains("storeNeighbourhood"))
                    {
                        if (String.IsNullOrEmpty(store["storeNeighbourhood"]))
                        {
                            command.Parameters.AddWithValue("@storeNeighbourhood", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeNeighbourhood", store["storeNeighbourhood"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeSubdistrict"))
                    {
                        if (StoreSubdistrictCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeSubdistrict", StoreSubdistrictInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeSubdistrict"))
                    {
                        if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                        {
                            command.Parameters.AddWithValue("@storeSubdistrict", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeSubdistrict", store["storeSubdistrict"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeDistrict"))
                    {
                        if (StoreDistrictCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeDistrict", StoreDistrictInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeDistrict"))
                    {
                        if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                        {
                            command.Parameters.AddWithValue("@storeDistrict", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeDistrict", store["storeDistrict"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeRuralDistrict"))
                    {
                        if (StoreRuralDistrictCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                            {
                                command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeRuralDistrict", StoreRuralDistrictInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeRuralDistrict"))
                    {
                        if (String.IsNullOrEmpty(store["storeSubdistrict"]))
                        {
                            command.Parameters.AddWithValue("@storeRuralDistrict", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeRuralDistrict", store["storeRuralDistrict"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeProvince"))
                    {
                        if (StoreProvinceCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeProvince"]))
                            {
                                command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeProvince", StoreProvinceInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeProvince"))
                    {
                        if (String.IsNullOrEmpty(store["storeProvince"]))
                        {
                            command.Parameters.AddWithValue("@storeProvince", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeProvince", store["storeProvince"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeZipcode"))
                    {
                        if (StoreZipcodeCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeZipcode"]))
                            {
                                command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeZipcode", StoreZipcodeInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeZipcode"))
                    {
                        if (String.IsNullOrEmpty(store["storeZipcode"]))
                        {
                            command.Parameters.AddWithValue("@storeZipcode", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeZipcode", store["storeZipcode"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeContactName"))
                    {
                        if (StoreContactNameCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeContactName"]))
                            {
                                command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactName", StoreContactNameInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeContactName"))
                    {
                        if (String.IsNullOrEmpty(store["storeContactName"]))
                        {
                            command.Parameters.AddWithValue("@storeContactName", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeContactName", store["storeContactName"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeContactPhone"))
                    {
                        if (StoreContactPhoneCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeContactPhone"]))
                            {
                                command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactPhone", StoreContactPhoneInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeContactPhone"))
                    {
                        if (String.IsNullOrEmpty(store["storeContactPhone"]))
                        {
                            command.Parameters.AddWithValue("@storeContactPhone", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeContactPhone", store["storeContactPhone"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("storeContactEmail"))
                    {
                        if (StoreContactEmailCheckBox.Checked)
                        {
                            if (String.IsNullOrEmpty(store["storeContactEmail"]))
                            {
                                command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                            }
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@storeContactEmail", StoreContactEmailInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("storeContactEmail"))
                    {
                        if (String.IsNullOrEmpty(store["storeContactEmail"]))
                        {
                            command.Parameters.AddWithValue("@storeContactEmail", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@storeContactEmail", store["storeContactEmail"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("purchaseNo"))
                    {
                        if (PurchaseNoCheckBox.Checked)
                        {
                            command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@purchaseNo", PurchaseNoInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("purchaseNo"))
                    {
                        command.Parameters.AddWithValue("@purchaseNo", purchaseNo);
                    }

                    if (((string[])Session["add"]).Contains("purchaseDate"))
                    {
                        if (ApprovalDateCheckBox.Checked)
                        {
                            command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(PurchaseDateInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@purchaseDate", PurchaseDateInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("purchaseDate"))
                    {
                        command.Parameters.AddWithValue("@purchaseDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    }

                    //if (((string[])Session["add"]).Contains("deliveryNo"))
                    //{
                    //    if (DeliveryNoCheckBox.Checked)
                    //    {
                    //        command.Parameters.AddWithValue("@deliveryNo", deliveryNo);
                    //    }
                    //    else
                    //    {
                    //        if (String.IsNullOrEmpty(DeliveryNoInput.Text?.Trim()))
                    //        {
                    //            command.Parameters.AddWithValue("@deliveryNo", DBNull.Value);
                    //        }
                    //        else
                    //        {
                    //            command.Parameters.AddWithValue("@deliveryNo", DeliveryNoInput.Text?.Trim());
                    //        }
                    //    }
                    //}
                    //else if (((string[])Session["auto"]).Contains("deliveryNo"))
                    //{
                    //    command.Parameters.AddWithValue("@deliveryNo", deliveryNo);
                    //}

                    //if (((string[])Session["add"]).Contains("deliveryDate"))
                    //{
                    //    if (String.IsNullOrEmpty(DeliveryDateInput.Text?.Trim()))
                    //    {
                    //        command.Parameters.AddWithValue("@deliveryDate", DBNull.Value);
                    //    }
                    //    else
                    //    {
                    //        command.Parameters.AddWithValue("@deliveryDate", DeliveryDateInput.Text?.Trim());
                    //    }
                    //}

                    if (((string[])Session["add"]).Contains("totalQty"))
                    {
                        if (TotalQtyCheckBox.Checked)
                        {
                            command.Parameters.AddWithValue("@totalQty", totalQty);
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(TotalQtyInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@totalQty", totalQty);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@totalQty", TotalQtyInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("totalQty"))
                    {
                        command.Parameters.AddWithValue("@totalQty", totalQty);
                    }

                    if (((string[])Session["add"]).Contains("totalPrice"))
                    {
                        if (TotalPriceCheckBox.Checked)
                        {
                            command.Parameters.AddWithValue("@totalPrice", totalPrice);
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@totalPrice", totalPrice);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@totalPrice", TotalPriceInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("totalPrice"))
                    {
                        command.Parameters.AddWithValue("@totalPrice", totalPrice);
                    }

                    if (((string[])Session["add"]).Contains("approvalBy"))
                    {
                        if (ApprovalDateCheckBox.Checked)
                        {
                            if (PurchaseStatusChoice.SelectedValue == "pending")
                            {
                                command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                            }
                        }
                        else
                        {
                            if (PurchaseStatusChoice.SelectedValue == "pending")
                            {
                                command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@approvalBy", ApprovalByInput.Text?.Trim());
                                }
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("approvalBy"))
                    {
                        if (PurchaseStatusChoice.SelectedValue == "pending")
                        {
                            command.Parameters.AddWithValue("@approvalBy", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@approvalBy", Session["username"]);
                        }
                    }

                    if (((string[])Session["add"]).Contains("approvalDate"))
                    {
                        if (ApprovalDateCheckBox.Checked)
                        {
                            if (PurchaseStatusChoice.SelectedValue == "pending")
                            {
                                command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                            }
                        }
                        else
                        {
                            if (PurchaseStatusChoice.SelectedValue == "pending")
                            {
                                command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                            }
                            else
                            {
                                if (String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()))
                                {
                                    command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                }
                                else
                                {
                                    command.Parameters.AddWithValue("@approvalDate", ApprovalDateInput.Text?.Trim());
                                }
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("approvalDate"))
                    {
                        if (PurchaseStatusChoice.SelectedValue == "pending")
                        {
                            command.Parameters.AddWithValue("@approvalDate", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@approvalDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        }
                    }

                    if (((string[])Session["add"]).Contains("requestedBy"))
                    {
                        if (RequestedByCheckBox.Checked)
                        {
                            command.Parameters.AddWithValue("@requestedBy", Session["username"]);
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@requestedBy", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@requestedBy", RequestedByInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("requestedBy"))
                    {
                        command.Parameters.AddWithValue("@requestedBy", Session["username"]);
                    }

                    if (((string[])Session["add"]).Contains("requestedDate"))
                    {
                        if (RequestedDateCheckBox.Checked)
                        {
                            command.Parameters.AddWithValue("@requestedDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                        }
                        else
                        {
                            if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()))
                            {
                                command.Parameters.AddWithValue("@requestedDate", DBNull.Value);
                            }
                            else
                            {
                                command.Parameters.AddWithValue("@requestedDate", RequestedDateInput.Text?.Trim());
                            }
                        }
                    }
                    else if (((string[])Session["auto"]).Contains("requestedDate"))
                    {
                        command.Parameters.AddWithValue("@requestedDate", Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                    }

                    if (((string[])Session["add"]).Contains("purchaseStatus"))
                    {
                        command.Parameters.AddWithValue("@purchaseStatus", PurchaseStatusChoice.SelectedValue?.Trim());
                    }
                    else if (((string[])Session["auto"]).Contains("purchaseStatus"))
                    {
                        command.Parameters.AddWithValue("@purchaseStatus", "pending");
                    }

                    if (((string[])Session["add"]).Contains("purchaseNotes"))
                    {
                        if(String.IsNullOrEmpty(PurchaseNotesInput.Text?.Trim()))
                        {
                            command.Parameters.AddWithValue("@purchaseNotes", DBNull.Value);
                        }
                        else
                        {
                            command.Parameters.AddWithValue("@purchaseNotes", PurchaseNotesInput.Text?.Trim());
                        }   
                    }
                    
                    command.Transaction = transaction;
                    result = null;
                    result = command.ExecuteScalar();

                    int purchaseId = 0;

                    if (result != null)
                    {
                        purchaseId = int.Parse(result.ToString());
                    }
                    
                    if (inserts.Rows.Count > 0)
                    {
                        command = conn.CreateCommand();
                        command.CommandText = @"CREATE TABLE #temp_insert (item_detail_id INT, item_json VARCHAR(MAX), item_qty INT, item_price DECIMAL(13,2));";
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();

                        SqlBulkCopy bulkinsert = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                        bulkinsert.DestinationTableName = "#temp_insert";

                        bulkinsert.ColumnMappings.Add("item_detail_id", "item_detail_id");
                        bulkinsert.ColumnMappings.Add("item_json", "item_json");
                        bulkinsert.ColumnMappings.Add("item_qty", "item_qty");
                        bulkinsert.ColumnMappings.Add("item_price", "item_price");
                        bulkinsert.WriteToServer(inserts);

                        command = conn.CreateCommand();
                        command.CommandText = @"INSERT INTO purchase_order_items (purchase_order_id, item_detail_id, 
                                item_json, item_qty, item_price, created_date, created_by, modified_date, modified_by)
                                SELECT @purchaseId, temp.item_detail_id, temp.item_json, temp.item_qty, 
                                temp.item_price, getdate(), @user, getdate(), @user 
                                FROM #temp_insert temp;
                                DROP TABLE #temp_insert;";
                        command.Parameters.AddWithValue("@purchaseId", purchaseId);
                        command.Parameters.AddWithValue("@user", Session["username"].ToString());
                        command.Transaction = transaction;
                        command.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    FillStoreInput();
                    FillSupplierInput();
                    FillPurchaseInput();
                    Clear();

                    UploadLabel.Visible = true;
                    UploadInput.Visible = true;
                    UploadButton.Visible = true;
                    SaveUploadButton.Visible = false;
                    CancelUploadButton.Visible = false;
                    InfoPanel.Visible = false;
                    ItemGridView.DataSource = null;
                    ItemGridView.DataBind();
                    inserts.Rows.Clear();
                    ViewState["inserts"] = inserts;
                    System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                    items.Rows.Clear();
                    ViewState["items"] = items;
                    AlertUploadLabel.Text = "Data berhasil disimpan";
                    AlertUploadLabel.CssClass = "alert alert-light-primary color-primary d-block";
                }
                catch (Exception error)
                {
                    transaction.Rollback();

                    AlertUploadLabel.Text = error.Message.ToString();
                    AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                conn.Close();
            }
            else
            {
                AlertUploadLabel.Text = String.Join("<br>", errors.ToArray());
                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
        }

        protected void CancelUploadButton_Click(object sender, EventArgs e)
        {
            UploadLabel.Visible = true;
            UploadInput.Visible = true;
            UploadButton.Visible = true;
            SaveUploadButton.Visible = false;
            CancelUploadButton.Visible = false;
            InfoPanel.Visible = false;
            ItemGridView.DataSource = null;
            ItemGridView.DataBind();
            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
            items.Rows.Clear();
            ViewState["items"] = items;
            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
            inserts.Rows.Clear();
            ViewState["inserts"] = inserts;
        }

        protected void ExampleButton_Click(object sender, EventArgs e)
        {
            Response.ContentType = "application/vnd.ms-excel";
            Response.AppendHeader("Content-Disposition", "attachment; filename=TemplateUploadPurchaseOrderExample.xlsx");
            Response.TransmitFile(Server.MapPath("~/uploads/TemplateUploadPurchaseOrderExample.xlsx"));
            Response.End();
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(StoreUploadChoice.SelectedValue))
            {
                AlertUploadLabel.Text = "Toko Ritel tidak boleh kosong";
                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if (String.IsNullOrEmpty(SupplierUploadChoice.SelectedValue))
            {
                AlertUploadLabel.Text = "Vendor tidak boleh kosong";
                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(StoreUploadChoice.SelectedValue))) &&
                (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(SupplierUploadChoice.SelectedValue))) &&
                (bool)Session["upload"] == true)
            {
                if (UploadInput.HasFile)
                {
                    try
                    {
                        string[] store = StoreUploadChoice.SelectedItem.Text.Split('-');
                        string[] supplier = SupplierUploadChoice.SelectedItem.Text.Split('-');

                        int unixTimestamp = (int)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
                        List<string> errors = new List<string>();
                        List<string> barcodes = new List<string>();

                        bool barcodeColumnExists = false;
                        bool supplierProductCodeColumnExists = false;
                        bool qtyColumnExists = false;

                        int headerRow = -1;

                        string[] barcodeHeader = { "barcode", "plu", "barcodebarang", "barcodeitem", "itembarcode" };
                        string[] supplierProductCodeHeader = { "kodeproduksupplier", "kodeprodukpemasok", "kodeproduk", "supplierproductcode", "kodeproduk" };
                        string[] qtyHeader = { "jumlahbarang", "banyakbarang", "jumlahitem", "banyakitem", "qty", "kuantitas", "quantity", "itemqty" };

                        string extension = Path.GetExtension(UploadInput.PostedFile.FileName);

                        if (extension == ".xls" || extension == ".xlsx")
                        {
                            string excelPath = MapPath("~/uploads/delivery-order-" + store[0].Trim() + "-" + supplier[0].Trim() + "-" + unixTimestamp + extension);

                            UploadInput.SaveAs(excelPath);

                            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
                            inserts.Rows.Clear();
                            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                            items.Rows.Clear();

                            string connectionstring = "";

                            if (extension == ".xls")
                            {
                                connectionstring = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + excelPath + "';Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1;\"";
                            }
                            else if (extension == ".xlsx")
                            {
                                connectionstring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + excelPath + "';Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1;\"";
                            }

                            using (OleDbConnection oleconn = new OleDbConnection(connectionstring))
                            {
                                OleDbCommand olecommand = new OleDbCommand();
                                olecommand.Connection = oleconn;
                                oleconn.Open();

                                System.Data.DataTable workbook = oleconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                                foreach (DataRow table in workbook.Rows)
                                {
                                    int barcodeColumn = -1;
                                    int supplierProductCodeColumn = -1;
                                    int qtyColumn = -1;

                                    headerRow = -1;

                                    string sheetName = table["TABLE_NAME"]?.ToString().Replace("'", "");

                                    if (!sheetName.EndsWith("$"))
                                    {
                                        continue;
                                    }

                                    olecommand.CommandText = "SELECT * FROM [" + sheetName + "]";

                                    System.Data.DataTable worksheet = new System.Data.DataTable("worksheet");

                                    OleDbDataAdapter adapter = new OleDbDataAdapter(olecommand);
                                    adapter.Fill(worksheet);

                                    int emptyColumns = 0;
                                    int emptyRows = 0;

                                    for (int i = 0; i < worksheet.Rows.Count; i++)
                                    {
                                        if (emptyColumns > 100)
                                        {
                                            break;
                                        }

                                        emptyColumns++;

                                        for (int j = 0; j < worksheet.Columns.Count; j++)
                                        {
                                            if (String.IsNullOrEmpty(worksheet.Rows[i][j].ToString()))
                                            {
                                                if (emptyRows > 100)
                                                {
                                                    break;
                                                }
                                                emptyRows++;
                                                continue;
                                            }
                                            else
                                            {
                                                emptyRows = 0;
                                                emptyColumns = 0;
                                            }

                                            string str = worksheet.Rows[i][j].ToString();
                                            string value = string.Concat(str.Where(Char.IsLetter));


                                            if (((IList)barcodeHeader).Contains(value.ToLower()))
                                            {
                                                barcodeColumnExists = true;
                                                barcodeColumn = j;
                                            }

                                            if (((IList)supplierProductCodeHeader).Contains(value.ToLower()))
                                            {
                                                supplierProductCodeColumnExists = true;
                                                supplierProductCodeColumn = j;
                                            }

                                            if (((IList)qtyHeader).Contains(value.ToLower()))
                                            {
                                                qtyColumnExists = true;
                                                qtyColumn = j;
                                            }

                                        }

                                        if ((barcodeColumn > -1 || supplierProductCodeColumn > -1) && qtyColumn > -1)
                                        {
                                            headerRow = i;
                                            break;
                                        }

                                    }

                                    if (headerRow > -1)
                                    {
                                        for (int i = 0; i < worksheet.Rows.Count; i++)
                                        {
                                            if (emptyColumns > 100)
                                            {
                                                break;
                                            }

                                            if (i > headerRow)
                                            {
                                                string barcode = "";
                                                string productcode = "";

                                                if (barcodeColumn > -1)
                                                {
                                                    barcode = worksheet.Rows[i][barcodeColumn].ToString();

                                                    if (barcode.Length > 0 && barcode.Substring(0, 1) == "'")
                                                    {
                                                        barcode = barcode.Remove(0, 1);
                                                    }

                                                    if (String.IsNullOrEmpty(barcode))
                                                    {
                                                        emptyColumns++;
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        emptyColumns = 0;
                                                    }
                                                }
                                                if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                {
                                                    productcode = worksheet.Rows[i][supplierProductCodeColumn].ToString();

                                                    if (productcode.Length > 0 && productcode.Substring(0, 1) == "'")
                                                    {
                                                        productcode = productcode.Remove(0, 1);
                                                    }

                                                    if (String.IsNullOrEmpty(productcode))
                                                    {
                                                        emptyColumns++;
                                                        continue;
                                                    }
                                                    else
                                                    {
                                                        emptyColumns = 0;
                                                    }
                                                }

                                                if (qtyColumn > -1)
                                                {
                                                    int qty = 0;

                                                    int.TryParse(worksheet.Rows[i][qtyColumn].ToString(), out qty);

                                                    if (qty <= 0)
                                                    {
                                                        if (barcodeColumn > -1)
                                                        {
                                                            errors.Add("Barcode " + barcode + " : jumlah barang tidak valid");
                                                        }
                                                        if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                        {
                                                            errors.Add("Kode produk " + productcode + " : jumlah barang tidak valid");
                                                        }
                                                        continue;
                                                    }
                                                }

                                                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                                                SqlCommand cmd = conn.CreateCommand();
                                                cmd.CommandText = @"SELECT item_detail_id, store_id, supplier_id, item_id, item_barcode, 
                                                item_code, item_category_code, item_name, foreign_name, short_description, 
                                                dbo.JsonValue(variants, 'color')[color], dbo.JsonValue(variants, 'size')[size], variants, 
                                                selling_price, purchase_margin, effective_date, brand_name, uom, supplier_product_code 
                                                FROM item_details WHERE store_id = @storeId AND supplier_id = @supplierId AND 
                                                (item_barcode = @itemBarcode OR supplier_product_code = @supplierProductCode) AND isdeleted = 0 AND isactive = 1;";

                                                cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreUploadChoice.SelectedValue));
                                                cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierUploadChoice.SelectedValue));
                                                if (barcodeColumn > -1)
                                                {
                                                    cmd.Parameters.AddWithValue("@itemBarcode", barcode);
                                                    cmd.Parameters.AddWithValue("@supplierProductCode", DBNull.Value );
                                                }
                                                if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                {
                                                    cmd.Parameters.AddWithValue("@itemBarcode", DBNull.Value);
                                                    cmd.Parameters.AddWithValue("@supplierProductCode", productcode);
                                                }

                                                conn.Open();
                                                SqlDataReader reader = cmd.ExecuteReader();

                                                if (reader.HasRows)
                                                {
                                                    while (reader.Read())
                                                    {
                                                        DataRow item = items.NewRow();
                                                        DataRow insert = inserts.NewRow();

                                                        Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();

                                                        item["no"] = items.Rows.Count + 1;

                                                        /*if (reader.IsDBNull(reader.GetOrdinal("purchase_order_id")))
                                                        {
                                                            item["purchase_order_id"] = 0;
                                                            insert["purchase_order_id"] = 0;
                                                            errors.Add("Terjadi kesalahan pesanan pembelian tidak ditemukan");
                                                        }
                                                        else
                                                        {
                                                            item["purchase_order_id"] = int.Parse(ViewState["id"].ToString());
                                                            insert["purchase_order_id"] = int.Parse(ViewState["id"].ToString());
                                                        }*/

                                                        if (reader.IsDBNull(reader.GetOrdinal("item_detail_id")))
                                                        {
                                                            item["item_detail_id"] = "";
                                                            insert["item_detail_id"] = DBNull.Value;
                                                            if (barcodeColumn > -1)
                                                            {
                                                                errors.Add("Barcode " + barcode + " : barang tidak ditemukan");
                                                            }
                                                            if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                            {
                                                                errors.Add("Kode produk " + productcode + " : barang tidak ditemukan");
                                                            }
                                                            //errors.Add("Terjadi kesalahan barang tidak ditemukan");
                                                        }
                                                        else
                                                        {
                                                            item["item_detail_id"] = int.Parse(reader["item_detail_id"].ToString());
                                                            insert["item_detail_id"] = int.Parse(reader["item_detail_id"].ToString());
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("store_id")))
                                                        {
                                                            json["storeId"] = null;
                                                            if (barcodeColumn > -1)
                                                            {
                                                                errors.Add("Barcode " + barcode + " : toko tidak ditemukan");
                                                            }
                                                            if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                            {
                                                                errors.Add("Kode produk " + productcode + " : toko tidak ditemukan");
                                                            }
                                                            //errors.Add("Terjadi kesalahan ");
                                                        }
                                                        else
                                                        {
                                                            json["storeId"] = int.Parse(reader["store_id"].ToString());
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("supplier_id")))
                                                        {
                                                            json["supplierId"] = null;
                                                            if (barcodeColumn > -1)
                                                            {
                                                                errors.Add("Barcode " + barcode + " : pemasok tidak ditemukan");
                                                            }
                                                            if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                            {
                                                                errors.Add("Kode produk " + productcode + " : pemasok tidak ditemukan");
                                                            }
                                                            //errors.Add("Terjadi kesalahan pemasok tidak ditemukan");
                                                        }
                                                        else
                                                        {
                                                            
                                                            json["supplierId"] = int.Parse(reader["supplier_id"].ToString());
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("item_id")))
                                                        {
                                                            json["itemId"] = null;
                                                            if (barcodeColumn > -1)
                                                            {
                                                                errors.Add("Barcode " + barcode + " : barang tidak ditemukan");
                                                            }
                                                            if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                            {
                                                                errors.Add("Kode produk " + productcode + " : barang tidak ditemukan");
                                                            }
                                                            //errors.Add("Terjadi kesalahan barang tidak ditemukan");
                                                        }
                                                        else
                                                        {
                                                            json["itemId"] = int.Parse(reader["item_id"].ToString());
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("item_barcode")))
                                                        {
                                                            item["item_barcode"] = "";
                                                            json["itemBarcode"] = null;
                                                            if (barcodeColumn > -1)
                                                            {
                                                                errors.Add("Barcode " + barcode + " : barcode tidak ditemukan");
                                                            }
                                                            if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                            {
                                                                errors.Add("Kode produk " + productcode + " : barcode tidak ditemukan");
                                                            }
                                                            //errors.Add("Terjadi kesalahan barcode barang tidak ditemukan");
                                                        }
                                                        else
                                                        {
                                                            item["item_barcode"] = reader["item_barcode"].ToString();
                                                            json["itemBarcode"] = reader["item_barcode"].ToString();
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("item_code")))
                                                        {
                                                            json["itemCode"] = null;
                                                        }
                                                        else
                                                        {
                                                            json["itemCode"] = reader["item_code"].ToString();
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("item_category_code")))
                                                        {
                                                            json["itemCategoryCode"] = null;
                                                        }
                                                        else
                                                        {
                                                            json["itemCategoryCode"] = reader["item_category_code"].ToString();
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("item_name")))
                                                        {
                                                            json["itemName"] = null;
                                                            item["item_name"] = "";
                                                            if (barcodeColumn > -1)
                                                            {
                                                                errors.Add("Barcode " + barcode + " : nama barang tidak ditemukan");
                                                            }
                                                            if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                            {
                                                                errors.Add("Kode produk " + productcode + " : nama barang tidak ditemukan");
                                                            }
                                                            //errors.Add("Terjadi kesalahan nama barang tidak ditemukan");
                                                        }
                                                        else
                                                        {
                                                            json["itemName"] = reader["item_name"].ToString();
                                                            item["item_name"] = reader["item_name"].ToString();
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("foreign_name")))
                                                        {
                                                            json["foreignName"] = null;
                                                        }
                                                        else
                                                        {
                                                            json["foreignName"] = reader["foreign_name"].ToString();
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("short_description")))
                                                        {
                                                            json["shortDescription"] = null;
                                                            item["short_description"] = "";
                                                        }
                                                        else
                                                        {
                                                            json["shortDescription"] = reader["short_description"].ToString();
                                                            item["short_description"] = reader["short_description"].ToString();
                                                        }

                                                        if (!reader.IsDBNull(reader.GetOrdinal("color")) && !reader.IsDBNull(reader.GetOrdinal("size")))
                                                        {
                                                            item["variants"] = String.Concat("Warna", " ", reader["color"].ToString(), ",", " ", "Ukuran", " ", reader["size"].ToString());
                                                            item["color"] = reader["color"].ToString();
                                                            item["size"] = reader["size"].ToString();
                                                        }
                                                        else if (!reader.IsDBNull(reader.GetOrdinal("color")))
                                                        {
                                                            item["variants"] = String.Concat("Warna", " ", reader["color"].ToString());
                                                            item["color"] = reader["color"].ToString();
                                                            item["size"] = "";
                                                        }
                                                        else if (!reader.IsDBNull(reader.GetOrdinal("size")))
                                                        {
                                                            item["variants"] = String.Concat("Ukuran", " ", reader["size"].ToString());
                                                            item["size"] = reader["size"].ToString();
                                                            item["color"] = "";
                                                        }
                                                        else
                                                        {
                                                            item["variants"] = "";
                                                            item["size"] = "";
                                                            item["color"] = "";
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("variants")))
                                                        {
                                                            json["variants"] = "{}";
                                                        }
                                                        else
                                                        {
                                                            json["variants"] = reader["variants"].ToString();
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("selling_price")))
                                                        {
                                                            json["sellingPrice"] = null;
                                                            item["item_price"] = "";
                                                            insert["item_price"] = DBNull.Value;
                                                            if (barcodeColumn > -1)
                                                            {
                                                                errors.Add("Barcode " + barcode + " : harga barang tidak ditemukan");
                                                            }
                                                            if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                            {
                                                                errors.Add("Kode produk " + productcode + " : harga barang tidak ditemukan");
                                                            }
                                                            //errors.Add("Terjadi kesalahan harga barang tidak ditemukan");
                                                        }
                                                        else
                                                        {
                                                            json["sellingPrice"] = double.Parse(reader["selling_price"].ToString());
                                                            item["item_price"] = double.Parse(reader["selling_price"].ToString());
                                                            insert["item_price"] = double.Parse(reader["selling_price"].ToString());
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("purchase_margin")))
                                                        {
                                                            json["purchaseMargin"] = null;
                                                        }
                                                        else
                                                        {
                                                            json["purchaseMargin"] = double.Parse(reader["purchase_margin"].ToString());
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("effective_date")))
                                                        {
                                                            json["effectiveDate"] = null;
                                                        }
                                                        else
                                                        {
                                                            json["effectiveDate"] = reader["effective_date"].ToString();
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("brand_name")))
                                                        {
                                                            json["brandName"] = null;
                                                            item["brand_name"] = "";
                                                        }
                                                        else
                                                        {
                                                            json["brandName"] = reader["brand_name"].ToString();
                                                            item["brand_name"] = reader["brand_name"].ToString();
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("uom")))
                                                        {
                                                            json["unitOfMeasure"] = null;
                                                        }
                                                        else
                                                        {
                                                            json["unitOfMeasure"] = reader["uom"].ToString();
                                                        }

                                                        if (reader.IsDBNull(reader.GetOrdinal("supplier_product_code")))
                                                        {
                                                            json["supplierProductCode"] = null;
                                                            item["supplier_product_code"] = "";
                                                            
                                                        }
                                                        else
                                                        {
                                                            json["supplierProductCode"] = reader["supplier_product_code"].ToString();
                                                            item["supplier_product_code"] = reader["supplier_product_code"].ToString();
                                                            
                                                        }

                                                        if (String.IsNullOrEmpty(worksheet.Rows[i][qtyColumn].ToString()))
                                                        {
                                                            item["item_qty"] = "";
                                                            insert["item_qty"] = DBNull.Value;
                                                            if (barcodeColumn > -1)
                                                            {
                                                                errors.Add("Barcode " + barcode + " : jumlah barang tidak ditemukan");
                                                            }
                                                            if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                            {
                                                                errors.Add("Kode produk " + productcode + " : jumlah barang tidak ditemukan");
                                                            }
                                                            //errors.Add("Barcode " +  + " jumlah barang tidak ditemukan");
                                                        }
                                                        else
                                                        {
                                                            item["item_qty"] = int.Parse(worksheet.Rows[i][qtyColumn].ToString());
                                                            insert["item_qty"] = int.Parse(worksheet.Rows[i][qtyColumn].ToString());
                                                        }

                                                        insert["item_json"] = json.ToString();
                                                        //insert["created_date"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                                        //insert["created_by"] = Session["username"].ToString();
                                                        //insert["modified_date"] = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff");
                                                        //insert["modified_by"] = Session["username"].ToString();

                                                        items.Rows.Add(item);

                                                        if (errors.Count <= 0)
                                                        {
                                                            inserts.Rows.Add(insert);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if (barcodeColumn > -1)
                                                    {
                                                        errors.Add("Barcode " + barcode + " tidak ditemukan");
                                                    }
                                                    if (supplierProductCodeColumn > -1 && barcodeColumn == -1)
                                                    {
                                                        errors.Add("Kode produk " + productcode + " tidak ditemukan");
                                                    }
                                                }
                                                conn.Close();
                                            }
                                        }
                                    }
                                }
                            }

                            if (!barcodeColumnExists && !supplierProductCodeColumnExists)
                            {
                                errors.Add("Kolom barcode/kode produk pemasok barang tidak ditemukan");
                            }

                            if (!qtyColumnExists)
                            {
                                errors.Add("Kolom kuantitas barang tidak ditemukan");
                            }

                            if (inserts.Rows.Count == 0)
                            {
                                errors.Add("Tidak ada data untuk disimpan");
                            }

                            if (errors.Count > 0)
                            {
                                string alert = String.Join("<br>", errors.ToArray());
                                inserts.Rows.Clear();
                                AlertUploadLabel.Text = alert;
                                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                            else
                            {
                                if (inserts.Rows.Count > 0)
                                {
                                    ViewState["inserts"] = inserts;
                                    TotalQtyCheckBoxCheckedChanged();
                                    TotalPriceCheckBoxCheckedChanged();
                                }

                                ItemGridView.DataSource = items;
                                ItemGridView.DataBind();
                                
                                UploadLabel.Visible = false;
                                UploadInput.Visible = false;
                                UploadButton.Visible = false;
                                SaveUploadButton.Visible = true;
                                CancelUploadButton.Visible = true;
                                InfoPanel.Visible = true;
                                //MarginBottomButtonLabel.Visible = true;
                                //MarginBottomGVInsertLabel.Visible = true;
                                //MarginBottomGVUpdateLabel.Visible = true;

                                AlertUploadLabel.Text = "Excel berhasil di unggah";
                                AlertUploadLabel.CssClass = "alert alert-light-primary color-primary d-block";
                            }
                        }   
                    }
                    catch (Exception ex)
                    {
                        AlertUploadLabel.Text = ex.Message.ToString();
                        AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        UploadLabel.Visible = true;
                        UploadInput.Visible = true;
                        UploadButton.Visible = true;
                        SaveUploadButton.Visible = false;
                        CancelUploadButton.Visible = false;
                        InfoPanel.Visible = false;
                        ItemGridView.DataSource = null;
                        ItemGridView.DataBind();
                        
                    }
                }
                else
                {
                    AlertUploadLabel.Text = "Pilih file excel yang ingin di unggah";
                    AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    UploadLabel.Visible = true;
                    UploadInput.Visible = true;
                    UploadButton.Visible = true;
                    SaveUploadButton.Visible = false;
                    InfoPanel.Visible = false;
                    CancelUploadButton.Visible = false;
                    ItemGridView.DataSource = null;
                    ItemGridView.DataBind();
                }
            }
            else
            {
                AlertUploadLabel.Text = "Permintaan akses ditolak";
                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                UploadLabel.Visible = true;
                UploadInput.Visible = true;
                UploadButton.Visible = true;
                SaveUploadButton.Visible = false;
                CancelUploadButton.Visible = false;
                InfoPanel.Visible = false;
                ItemGridView.DataSource = null;
                ItemGridView.DataBind();
            }
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
        }

        protected void EditQtyButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "edit" && arg.Length > 0)
            {
                SaveModalButton.CommandName = e.CommandName;
                SaveModalButton.CommandArgument = e.CommandArgument.ToString();

                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                foreach (DataRow row in items.Rows)
                {
                    if (arg[0].ToString() == row["item_detail_id"].ToString())
                    {
                        ItemBarcodeInput.Text = row["item_barcode"]?.ToString();
                        ItemNameInput.Text = row["item_name"]?.ToString();
                        DescriptionInput.Text = row["short_description"]?.ToString();
                        ColorInput.Text = row["color"]?.ToString();
                        SizeInput.Text = row["size"]?.ToString();
                        BrandInput.Text = row["brand_name"]?.ToString();
                        PriceInput.Text = row["item_price"]?.ToString();
                        QtyInput.Text = row["item_qty"]?.ToString();
                        SupplierProductCodeInput.Text = row["supplier_product_code"]?.ToString();

                        break;
                    }
                }
                
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
            }
            else
            {
                AlertUploadLabel.Text = "Terjadi kesalahan server";
                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void DeleteItemButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "delete" && arg.Length > 0)
            {
                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                foreach (DataRow row in items.Rows)
                {
                    if (arg[0].ToString() == row["item_detail_id"].ToString())
                    {
                        row.Delete();

                        break;
                    }
                }
                items.AcceptChanges();

                int no = 1;

                for (int i = 0; i < items.Rows.Count; i++)
                {
                    items.Rows[i]["no"] = no;
                    no++;
                }

                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                foreach (DataRow row in inserts.Rows)
                {
                    if (arg[0].ToString() == row["item_detail_id"].ToString())
                    {
                        row.Delete();

                        break;
                    }
                }
                inserts.AcceptChanges();

                ViewState["items"] = items;

                ItemGridView.DataSource = items;
                ItemGridView.DataBind();

                TotalQtyCheckBoxCheckedChanged();
                TotalPriceCheckBoxCheckedChanged();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "selectUploadTab();", true);
            }
            else
            {
                AlertUploadLabel.Text = "Terjadi kesalahan server";
                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void CloseModalButton_Click(object sender, EventArgs e)
        {
            ItemBarcodeInput.Text = "";
            ItemNameInput.Text = "";
            DescriptionInput.Text = "";
            ColorInput.Text = "";
            SizeInput.Text = "";
            BrandInput.Text = "";
            PriceInput.Text = "";
            QtyInput.Text = "";
            SupplierProductCodeInput.Text = "";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
        }

        protected void SaveModalButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "edit" && arg.Length > 0)
            {
                if (((string[])Session["read"]).Contains("item"))
                {
                    int qty;
                    bool isNumeric = int.TryParse(QtyInput.Text.ToString(), out qty);

                    if (isNumeric && qty > 0)
                    {
                        System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

                        foreach (DataRow row in items.Rows)
                        {
                            if (arg[0].ToString() == row["item_detail_id"].ToString())
                            {
                                row["item_qty"] = qty;
                                break;
                            }
                        }
                        items.AcceptChanges();

                        System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                        foreach (DataRow row in inserts.Rows)
                        {
                            if (arg[0].ToString() == row["item_detail_id"].ToString())
                            {
                                row["item_qty"] = qty;
                                break;
                            }
                        }
                        ItemGridView.DataSource = items;
                        ItemGridView.DataBind();

                        TotalQtyCheckBoxCheckedChanged();
                        TotalPriceCheckBoxCheckedChanged();
                    }
                    else
                    {
                        AlertModalLabel.Text = "Jumlah barang tidak valid";
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openItemModal();", true);
                    }
                }
                else
                {
                    AlertUploadLabel.Text = "Akses ditolak";
                    AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
            }
            else
            {
                AlertUploadLabel.Text = "Terjadi kesalahan server";
                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }
        
        protected void SupplierNameCheckBoxCheckedChanged()
        {
            if (SupplierNameCheckBox.Checked)
            {
                SupplierNameInput.Text = supplier["supplierName"];
                SupplierNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierNameInput.Text?.Trim()))
                {
                    SupplierNameInput.Text = supplier["supplierName"];
                }

                SupplierNameInput.Attributes.Remove("disabled");
            }
        }

        protected void SupplierNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierNameCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierNameInput.Focus();
            }
        }

        protected void SupplierBuildingNameCheckBoxCheckedChanged()
        {
            if (SupplierBuildingNameCheckBox.Checked)
            {
                SupplierBuildingNameInput.Text = supplier["supplierBuildingName"];
                SupplierBuildingNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierBuildingNameInput.Text?.Trim()))
                {
                    SupplierBuildingNameInput.Text = supplier["supplierBuildingName"];
                }
                SupplierBuildingNameInput.Attributes.Remove("disabled");
            }
        }

        protected void SupplierBuildingNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierBuildingNameCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierBuildingNameInput.Focus();
            }

        }

        protected void SupplierStreetNameCheckBoxCheckedChanged()
        {
            if (SupplierStreetNameCheckBox.Checked)
            {
                SupplierStreetNameInput.Text = supplier["supplierStreetName"];
                SupplierStreetNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierStreetNameInput.Text?.Trim()))
                {
                    SupplierStreetNameInput.Text = supplier["supplierStreetName"];
                }
                SupplierStreetNameInput.Attributes.Remove("disabled");
            }
        }

        protected void SupplierStreetNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierStreetNameCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierStreetNameInput.Focus();
            }

        }

        protected void SupplierNeighbourhoodCheckBoxCheckedChanged()
        {
            if (SupplierNeighbourhoodCheckBox.Checked)
            {
                SupplierNeighbourhoodInput.Text = supplier["supplierNeighbourhood"];
                SupplierNeighbourhoodInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierNeighbourhoodInput.Text?.Trim()))
                {
                    SupplierNeighbourhoodInput.Text = supplier["supplierNeighbourhood"];
                }
                SupplierNeighbourhoodInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierNeighbourhoodCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierNeighbourhoodCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierNeighbourhoodInput.Focus();
            }

        }

        protected void SupplierSubdistrictCheckBoxCheckedChanged()
        {
            if (SupplierSubdistrictCheckBox.Checked)
            {
                SupplierSubdistrictInput.Text = supplier["supplierSubdistrict"];
                SupplierSubdistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierSubdistrictInput.Text?.Trim()))
                {
                    SupplierSubdistrictInput.Text = supplier["supplierSubdistrict"];
                }
                SupplierSubdistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierSubdistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierSubdistrictCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierSubdistrictInput.Focus();
            }
        }

        protected void SupplierDistrictCheckBoxCheckedChanged()
        {
            if (SupplierDistrictCheckBox.Checked)
            {
                SupplierDistrictInput.Text = supplier["supplierDistrict"];
                SupplierDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierDistrictInput.Text?.Trim()))
                {
                    SupplierDistrictInput.Text = supplier["supplierDistrict"];
                }
                SupplierDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierDistrictCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierDistrictInput.Focus();
            }
        }

        protected void SupplierRuralDistrictCheckBoxCheckedChanged()
        {
            if (SupplierRuralDistrictCheckBox.Checked)
            {
                SupplierRuralDistrictInput.Text = supplier["supplierRuralDistrict"];
                SupplierRuralDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierRuralDistrictInput.Text?.Trim()))
                {
                    SupplierRuralDistrictInput.Text = supplier["supplierRuralDistrict"];
                }
                SupplierRuralDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierRuralDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierRuralDistrictCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierRuralDistrictInput.Focus();
            }

        }

        protected void SupplierProvinceCheckBoxCheckedChanged()
        {
            if (SupplierProvinceCheckBox.Checked)
            {
                SupplierProvinceInput.Text = supplier["supplierProvince"];
                SupplierProvinceInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierProvinceInput.Text?.Trim()))
                {
                    SupplierProvinceInput.Text = supplier["supplierProvince"];
                }
                SupplierProvinceInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierProvinceCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierProvinceCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierProvinceInput.Focus();
            }

        }

        protected void SupplierZipcodeCheckBoxCheckedChanged()
        {
            if (SupplierZipcodeCheckBox.Checked)
            {
                SupplierZipcodeInput.Text = supplier["supplierZipcode"];
                SupplierZipcodeInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierZipcodeInput.Text?.Trim()))
                {
                    SupplierZipcodeInput.Text = supplier["supplierZipcode"];
                }
                SupplierZipcodeInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierZipcodeCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierZipcodeCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierZipcodeInput.Focus();
            }
        }

        protected void SupplierContactNameCheckBoxCheckedChanged()
        {
            if (SupplierContactNameCheckBox.Checked)
            {
                SupplierContactNameInput.Text = supplier["supplierContactName"];
                SupplierContactNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierContactNameInput.Text?.Trim()))
                {
                    SupplierContactNameInput.Text = supplier["supplierContactName"];
                }
                SupplierContactNameInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierContactNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierContactNameCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierContactNameInput.Focus();
            }

        }

        protected void SupplierContactPhoneCheckBoxCheckedChanged()
        {
            if (SupplierContactPhoneCheckBox.Checked)
            {
                SupplierContactPhoneInput.Text = supplier["supplierContactPhone"];
                SupplierContactPhoneInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierContactPhoneInput.Text?.Trim()))
                {
                    SupplierContactPhoneInput.Text = supplier["supplierContactPhone"];
                }

                SupplierContactPhoneInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierContactPhoneCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierContactPhoneCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierContactPhoneInput.Focus();
            }
        }

        protected void SupplierContactEmailCheckBoxCheckedChanged()
        {
            if (SupplierContactEmailCheckBox.Checked)
            {
                SupplierContactEmailInput.Text = supplier["supplierContactEmail"];
                SupplierContactEmailInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(SupplierContactEmailInput.Text?.Trim()))
                {
                    SupplierContactEmailInput.Text = supplier["supplierContactEmail"];
                }
                SupplierContactEmailInput.Attributes.Remove("disabled");
            }
        }
        protected void SupplierContactEmailCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            SupplierContactEmailCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                SupplierContactEmailInput.Focus();
            }
        }

        protected void StoreNameCheckBoxCheckedChanged()
        {
            if (StoreNameCheckBox.Checked)
            {
                StoreNameInput.Text = store["storeName"];
                StoreNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreNameInput.Text?.Trim()))
                {
                    StoreNameInput.Text = store["storeName"];
                }

                StoreNameInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreNameCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreNameInput.Focus();
            }
        }

        protected void StoreBuildingNameCheckBoxCheckedChanged()
        {
            if (StoreBuildingNameCheckBox.Checked)
            {
                StoreBuildingNameInput.Text = store["storeBuildingName"];
                StoreBuildingNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreBuildingNameInput.Text?.Trim()))
                {
                    StoreBuildingNameInput.Text = store["storeBuildingName"];
                }
                StoreBuildingNameInput.Attributes.Remove("disabled");
            }
        }

        protected void StoreBuildingNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreBuildingNameCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreBuildingNameInput.Focus();
            }
                
        }

        protected void StoreStreetNameCheckBoxCheckedChanged()
        {
            if (StoreStreetNameCheckBox.Checked)
            {
                StoreStreetNameInput.Text = store["storeStreetName"];
                StoreStreetNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreStreetNameInput.Text?.Trim()))
                {
                    StoreStreetNameInput.Text = store["storeStreetName"];
                }
                StoreStreetNameInput.Attributes.Remove("disabled");
            }
        }

        protected void StoreStreetNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreStreetNameCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreStreetNameInput.Focus();
            }
                
        }

        protected void StoreNeighbourhoodCheckBoxCheckedChanged()
        {
            if (StoreNeighbourhoodCheckBox.Checked)
            {
                StoreNeighbourhoodInput.Text = store["storeNeighbourhood"];
                StoreNeighbourhoodInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreNeighbourhoodInput.Text?.Trim()))
                {
                    StoreNeighbourhoodInput.Text = store["storeNeighbourhood"];
                }
                StoreNeighbourhoodInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreNeighbourhoodCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreNeighbourhoodCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreNeighbourhoodInput.Focus();
            }
            
        }

        protected void StoreSubdistrictCheckBoxCheckedChanged()
        {
            if (StoreSubdistrictCheckBox.Checked)
            {
                StoreSubdistrictInput.Text = store["storeSubdistrict"];
                StoreSubdistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreSubdistrictInput.Text?.Trim()))
                {
                    StoreSubdistrictInput.Text = store["storeSubdistrict"];
                }
                StoreSubdistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreSubdistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreSubdistrictCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreSubdistrictInput.Focus();
            }
        }

        protected void StoreDistrictCheckBoxCheckedChanged()
        {
            if (StoreDistrictCheckBox.Checked)
            {
                StoreDistrictInput.Text = store["storeDistrict"];
                StoreDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreDistrictInput.Text?.Trim()))
                {
                    StoreDistrictInput.Text = store["storeDistrict"];
                }
                StoreDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreDistrictCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreDistrictInput.Focus();
            }
        }

        protected void StoreRuralDistrictCheckBoxCheckedChanged()
        {
            if (StoreRuralDistrictCheckBox.Checked)
            {
                StoreRuralDistrictInput.Text = store["storeRuralDistrict"];
                StoreRuralDistrictInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreRuralDistrictInput.Text?.Trim()))
                {
                    StoreRuralDistrictInput.Text = store["storeRuralDistrict"];
                }
                StoreRuralDistrictInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreRuralDistrictCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreRuralDistrictCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreRuralDistrictInput.Focus();
            }
            
        }

        protected void StoreProvinceCheckBoxCheckedChanged()
        {
            if (StoreProvinceCheckBox.Checked)
            {
                StoreProvinceInput.Text = store["storeProvince"];
                StoreProvinceInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreProvinceInput.Text?.Trim()))
                {
                    StoreProvinceInput.Text = store["storeProvince"];
                }
                StoreProvinceInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreProvinceCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreProvinceCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreProvinceInput.Focus();
            }
        }

        protected void StoreZipcodeCheckBoxCheckedChanged()
        {
            if (StoreZipcodeCheckBox.Checked)
            {
                StoreZipcodeInput.Text = store["storeZipcode"];
                StoreZipcodeInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreZipcodeInput.Text?.Trim()))
                {
                    StoreZipcodeInput.Text = store["storeZipcode"];
                }
                StoreZipcodeInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreZipcodeCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreZipcodeCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreZipcodeInput.Focus();
            }
        }

        protected void StoreContactNameCheckBoxCheckedChanged()
        {
            if (StoreContactNameCheckBox.Checked)
            {
                StoreContactNameInput.Text = store["storeContactName"];
                StoreContactNameInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreContactNameInput.Text?.Trim()))
                {
                    StoreContactNameInput.Text = store["storeContactName"];
                }
                StoreContactNameInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreContactNameCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreContactNameCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreContactNameInput.Focus();
            }    
        }

        protected void StoreContactPhoneCheckBoxCheckedChanged()
        {
            if (StoreContactPhoneCheckBox.Checked)
            {
                StoreContactPhoneInput.Text = store["storeContactPhone"];
                StoreContactPhoneInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreContactPhoneInput.Text?.Trim()))
                {
                    StoreContactPhoneInput.Text = store["storeContactPhone"];
                }

                StoreContactPhoneInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreContactPhoneCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreContactPhoneCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreContactPhoneInput.Focus();
            }
        }

        protected void StoreContactEmailCheckBoxCheckedChanged()
        {
            if (StoreContactEmailCheckBox.Checked)
            {
                StoreContactEmailInput.Text = store["storeContactEmail"];
                StoreContactEmailInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(StoreContactEmailInput.Text?.Trim()))
                {
                    StoreContactEmailInput.Text = store["storeContactEmail"];
                }
                StoreContactEmailInput.Attributes.Remove("disabled");
            }
        }
        protected void StoreContactEmailCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            StoreContactEmailCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                StoreContactEmailInput.Focus();
            }
            
        }

        protected void PurchaseNoCheckBoxCheckedChanged()
        {
            string popattern = "PO" + DateTime.Now.ToString("yyyyMMdd");
            string purchaseNo = popattern + "1".ToString().PadLeft(3, '0');

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

            SqlConnection conn = new SqlConnection(constr);
            SqlCommand command = conn.CreateCommand();
            command.CommandText = @"SELECT TOP 1 CAST(RIGHT([purchase_no], 3) AS INT) FROM purchase_orders WHERE 
                                    purchase_no LIKE @purchaseNo ORDER BY purchase_no DESC";
            command.Parameters.AddWithValue("@purchaseNo", string.Format("{0}%", popattern));

            int number = 1;
            
            conn.Open();
            object result = command.ExecuteScalar();
            conn.Close();

            if (result != null)
            {
                number = int.Parse(result.ToString()) + 1;
            }
            purchaseNo = popattern + number.ToString().PadLeft(3, '0');

            if (PurchaseNoCheckBox.Checked)
            {
                PurchaseNoInput.Text = purchaseNo + " " + "(nilai sementara)";
                PurchaseNoInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(PurchaseNoInput.Text?.Trim()))
                {
                    PurchaseNoInput.Text = purchaseNo;
                }
                PurchaseNoInput.Attributes.Remove("disabled");
            }
        }

        protected void PurchaseNoCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            PurchaseNoCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                PurchaseNoInput.Focus();
            }
        }

        protected void PurchaseDateCheckBoxCheckedChanged()
        {
            if (PurchaseDateCheckBox.Checked)
            {
                PurchaseDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                PurchaseDateInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(PurchaseDateInput.Text?.Trim()))
                {
                    PurchaseDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                }
                PurchaseDateInput.Attributes.Remove("disabled");
            }
        }
        protected void PurchaseDateCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            PurchaseDateCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                PurchaseDateInput.Focus();
            }
            
        }

       
        protected void RequestedByCheckBoxCheckedChanged()
        {
            if (RequestedByCheckBox.Checked)
            {
                RequestedByInput.Text = Session["username"].ToString();
                RequestedByInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(RequestedByInput.Text?.Trim()))
                {
                    RequestedByInput.Text = Session["username"].ToString();
                }
                RequestedByInput.Attributes.Remove("disabled");
            }
        }
        
        protected void RequestedByCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            RequestedByCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                RequestedByInput.Focus();
            }
                
        }

        protected void RequestedDateCheckBoxCheckedChanged()
        {
            if (RequestedDateCheckBox.Checked)
            {
                RequestedDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                RequestedDateInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(RequestedDateInput.Text?.Trim()))
                {
                    RequestedDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                }
                RequestedDateInput.Attributes.Remove("disabled");
            }
        }

        protected void RequestedDateCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            RequestedDateCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                RequestedDateInput.Focus();
            }
        }

        protected void ApprovalByCheckBoxCheckedChanged()
        {
            if (ApprovalByCheckBox.Checked)
            {
                if (PurchaseStatusChoice.SelectedValue == "pending")
                {
                    ApprovalByInput.Text = DBNull.Value.ToString();
                }
                else
                {
                    ApprovalByInput.Text = Session["username"].ToString();
                }
                ApprovalByInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(ApprovalByInput.Text?.Trim()))
                {
                    ApprovalByInput.Text = Session["username"].ToString();
                }
                ApprovalByInput.Attributes.Remove("disabled");
            }
        }

        protected void ApprovalByCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            ApprovalByCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                ApprovalByInput.Focus();
            }
                
        }

        protected void ApprovalDateCheckBoxCheckedChanged()
        {
            if (ApprovalDateCheckBox.Checked)
            {
                if (PurchaseStatusChoice.SelectedValue == "pending")
                {
                    ApprovalDateInput.Text = "";
                }
                else
                {
                    ApprovalDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                }
                ApprovalDateInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if (String.IsNullOrEmpty(ApprovalDateInput.Text?.Trim()))
                {
                    ApprovalDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                }
                ApprovalDateInput.Attributes.Remove("disabled");
            }
        }

        protected void ApprovalDateCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            ApprovalDateCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                ApprovalDateInput.Focus();
            }
        }

        protected void TotalQtyCheckBoxCheckedChanged()
        {
            int total = 0;
            int number = 0;
            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

            foreach (DataRow row in inserts.Rows)
            {
                if(!String.IsNullOrEmpty(row["item_qty"].ToString()) && int.TryParse(row["item_qty"].ToString(), out number))
                {
                    total += int.Parse(row["item_qty"].ToString());
                }
            }

            TotalQtyLabel.Text = total.ToString("###,###,###,###,##0");

            if (TotalQtyCheckBox.Checked)
            {    
                TotalQtyInput.Text = total.ToString("###,###,###,###,##0");
                TotalQtyInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                TotalQtyInput.Attributes.Remove("disabled");
            }
        }

        protected void TotalQtyCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            TotalQtyCheckBoxCheckedChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                TotalQtyInput.Focus();
            }
        }

        protected void PurchaseStatusChoiceSelectedIndexChanged()
        {
            if (PurchaseStatusChoice.SelectedValue == "pending")
            {
                ApprovalDateInput.Text = "";
                ApprovalByInput.Text = "";
            }
            else
            {
                if (ApprovalByCheckBox.Checked)
                {
                    ApprovalByInput.Text = Session["username"].ToString();
                }
                if (ApprovalDateCheckBox.Checked)
                {
                    ApprovalDateInput.Text = Convert.ToDateTime(DateTime.Now).ToString("yyyy-MM-dd");
                }
            }
        }

        protected void PurchaseStatusChoice_SelectedIndexChanged(Object sender, EventArgs args)
        {
            PurchaseStatusChoiceSelectedIndexChanged();

            if (menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                ApprovalDateInput.Focus();
            }
        }

        protected void TotalPriceCheckBoxCheckedChanged()
        {
            double total = Math.Round(0.00, 2);
            int number = 0;
            double price = 0;
            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

            foreach (DataRow row in inserts.Rows)
            {
                if (!String.IsNullOrEmpty(row["item_qty"].ToString()) && int.TryParse(row["item_qty"].ToString(), out number) &&
                    !String.IsNullOrEmpty(row["item_price"].ToString()) && double.TryParse(row["item_price"].ToString(), out price))
                {
                    total += int.Parse(row["item_qty"].ToString()) * double.Parse(row["item_price"].ToString());
                }
            }
            total = Math.Round(total, 2);

            TotalPriceLabel.Text = total.ToString("###,###,###,###,##0.00");

            if (TotalPriceCheckBox.Checked)
            {
                TotalPriceInput.Text = total.ToString("###,###,###,###,##0.00");
                TotalPriceInput.Attributes.Add("disabled", "disabled");
            }
            else
            {
                if(String.IsNullOrEmpty(TotalPriceInput.Text?.Trim()))
                {
                    TotalPriceInput.Text = total.ToString("###,###,###,###,##0.00");
                }
                TotalPriceInput.Attributes.Remove("disabled");
            }
        }

        protected void TotalPriceCheckBox_CheckedChanged(Object sender, EventArgs args)
        {
            TotalPriceCheckBoxCheckedChanged();

            if(menu == "upload")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
                TotalPriceInput.Focus();
            }
        }
    }
}
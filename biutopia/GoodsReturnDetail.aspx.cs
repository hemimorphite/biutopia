﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;

namespace biutopia
{
    public partial class GoodsReturnDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["username"]) || string.IsNullOrEmpty((string)Session["role_id"]))
            {
                Session.Clear();
                Response.Redirect("Login.aspx");
            }

            List<string> pagesList = new List<string>();
            Session["stores"] = new int[0];
            Session["suppliers"] = new int[0];
            Session["auto"] = new string[0];
            Session["add"] = new string[0];
            Session["edit"] = new string[0];
            Session["read"] = new string[0];
            Session["delete"] = false;
            Session["upload"] = false;
            Session["print"] = false;
            Session["pages"] = new string[0];

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT permissions FROM roles WHERE role_id = @roleId";
                    cmd.Parameters.AddWithValue("@roleId", Session["role_id"]);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("permissions")))
                                {
                                    Newtonsoft.Json.Linq.JObject pagepermissions = Newtonsoft.Json.Linq.JObject.Parse(reader.GetString(reader.GetOrdinal("permissions")).Trim());
                                    Newtonsoft.Json.Linq.JObject accessRights = Newtonsoft.Json.Linq.JObject.Parse(Session["access_rights"].ToString());
                                    Newtonsoft.Json.Linq.JArray jstores = (Newtonsoft.Json.Linq.JArray)accessRights["stores"];

                                    int[] stores = new int[jstores.Count];

                                    int a = 0;
                                    foreach (int store in jstores)
                                    {
                                        stores[a++] = store;
                                    }

                                    Session["stores"] = stores;

                                    Newtonsoft.Json.Linq.JArray jsuppliers = (Newtonsoft.Json.Linq.JArray)accessRights["suppliers"];

                                    int[] suppliers = new int[jsuppliers.Count];

                                    int b = 0;
                                    foreach (int supplier in jsuppliers)
                                    {
                                        suppliers[b++] = supplier;
                                    }

                                    Session["suppliers"] = suppliers;

                                    Newtonsoft.Json.Linq.JArray pages = (Newtonsoft.Json.Linq.JArray)pagepermissions["pages"];

                                    for (int i = 0; i < pages.Count; i++)
                                    {
                                        Newtonsoft.Json.Linq.JObject page = (Newtonsoft.Json.Linq.JObject)pages[i];
                                        Newtonsoft.Json.Linq.JArray permissions = (Newtonsoft.Json.Linq.JArray)page["permissions"];

                                        if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                        {
                                            pagesList.Add((string)page["name"]);
                                        }

                                        if ((string)page["name"] == "goodsReturn")
                                        {
                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[0])["auto"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[0])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["auto"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[1])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["read"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[2])["add"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[2])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["add"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[3])["edit"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[3])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["edit"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[4])["delete"])
                                            {
                                                Session["delete"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[5])["upload"])
                                            {
                                                Session["upload"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[6])["print"])
                                            {
                                                Session["print"] = true;
                                            }
                                        }
                                    }

                                    Session["pages"] = pagesList.ToArray();
                                }
                            }
                        }
                    }
                    conn.Close();

                    if (((string[])Session["read"]).Length == 0)
                    {
                        Response.Redirect("Forbidden.aspx");
                    }
                }
            }

            if (!IsPostBack)
            {
                Uri url = new Uri(HttpContext.Current.Request.Url.AbsoluteUri);
                
                if (HttpUtility.ParseQueryString(url.Query).Get("returnId") != null &&
                    HttpUtility.ParseQueryString(url.Query).Get("returnUuid") != null &&
                    Session["storevalue"] != null && Session["suppliervalue"] != null &&
                    Session["origin"] != null)
                {
                    int number;
                    //bool boolean;

                    if (int.TryParse(HttpUtility.ParseQueryString(url.Query).Get("returnId"), out number) &&
                        Session["origin"].ToString() == "GoodsReturn" &&
                        !String.IsNullOrEmpty(HttpUtility.ParseQueryString(url.Query).Get("returnUuid").Trim()))
                    {
                        if (((string[])Session["read"]).Contains("item") && ((string[])Session["read"]).Contains("supplier") && 
                        ((string[])Session["read"]).Contains("store") && ((string[])Session["read"]).Contains("goodsReturnNo") &&
                        ((string[])Session["read"]).Contains("goodsReturnDate") && ((string[])Session["read"]).Contains("totalQty") &&
                        ((string[])Session["read"]).Contains("totalPrice"))
                        {
                            int id = int.Parse(HttpUtility.ParseQueryString(url.Query).Get("returnId"));
                            string uuid = HttpUtility.ParseQueryString(url.Query).Get("returnUuid").Trim().ToUpper();
                            
                            string totalQty = "", totalPrice = "";

                            SqlConnection conn = new SqlConnection(constr);
                            SqlCommand cmd = conn.CreateCommand();

                            cmd.CommandText = @"SELECT supplier_name,supplier_building_name,supplier_street_name,
                            supplier_neighbourhood,supplier_subdistrict,supplier_district,supplier_rural_district,
                            supplier_province,supplier_zipcode,supplier_contact_name,supplier_contact_phone,
                            supplier_contact_email,store_name,store_building_name,store_street_name,store_neighbourhood,
                            store_subdistrict,store_district,store_rural_district,store_province,
                            store_zipcode,store_contact_name,store_contact_phone,store_contact_email,goods_return_no,
                            goods_return_date,FORMAT(total_qty, '###,###,###,###,##0') [total_qty],
                            FORMAT(total_price, '###,###,###,###,##0.00') [total_price],
                            goods_return_notes FROM goods_returns
                            WHERE goods_return_id = @returnId AND uuid = @returnUuid AND isdeleted = 0;";
                            cmd.Parameters.AddWithValue("@returnId", id);
                            cmd.Parameters.AddWithValue("@returnUuid", uuid);

                            conn.Open();
                            SqlDataReader reader = cmd.ExecuteReader();

                            if (reader.HasRows)
                            {
                                while (reader.Read())
                                {
                                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_name")))
                                    {
                                        string[] suppliername = reader["supplier_name"].ToString().Split(',');
                                        if (suppliername.Length == 2)
                                        {
                                            SupplierNameLabel.Text = suppliername[1].Trim() + " " + suppliername[0].Trim();
                                        }
                                        else
                                        {
                                            SupplierNameLabel.Text = suppliername[0].Trim();
                                        }
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_building_name")))
                                    {
                                        SupplierBuildingNameLabel.Text = reader["supplier_building_name"].ToString().Trim();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_street_name")))
                                    {
                                        if (!String.IsNullOrEmpty(SupplierBuildingNameLabel.Text))
                                        {
                                            SupplierBuildingNameLabel.Text = SupplierBuildingNameLabel.Text + ", ";
                                        }
                                        SupplierStreetNameLabel.Text = reader["supplier_street_name"].ToString().Trim();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_neighbourhood")))
                                    {
                                        SupplierNeighbourhoodLabel.Text = reader["supplier_neighbourhood"].ToString().Trim();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_subdistrict")))
                                    {
                                        if (!String.IsNullOrEmpty(SupplierNeighbourhoodLabel.Text))
                                        {
                                            SupplierNeighbourhoodLabel.Text = SupplierNeighbourhoodLabel.Text + ", ";
                                        }
                                        SupplierSubdistrictLabel.Text = reader["supplier_subdistrict"].ToString().Trim();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_district")))
                                    {
                                        if (!String.IsNullOrEmpty(SupplierNeighbourhoodLabel.Text))
                                        {
                                            SupplierNeighbourhoodLabel.Text = SupplierNeighbourhoodLabel.Text + ", ";
                                        }
                                        if (!String.IsNullOrEmpty(SupplierSubdistrictLabel.Text))
                                        {
                                            SupplierSubdistrictLabel.Text = SupplierSubdistrictLabel.Text + ", ";
                                        }
                                        SupplierDistrictLabel.Text = reader["supplier_district"].ToString().Trim();
                                    }

                                    //purchase_orders.supplier_rural_district(7),
                                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_rural_district")))
                                    {
                                        SupplierRuralDistrictLabel.Text = reader["supplier_rural_district"].ToString().Trim();
                                    }

                                    //purchase_orders.supplier_province(8),
                                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_province")))
                                    {
                                        if (!String.IsNullOrEmpty(SupplierRuralDistrictLabel.Text))
                                        {
                                            SupplierRuralDistrictLabel.Text = SupplierRuralDistrictLabel.Text + ", ";
                                        }
                                        SupplierProvinceLabel.Text = reader["supplier_province"].ToString().Trim();
                                    }

                                    //purchase_orders.supplier_zipcode(9),
                                    if (!reader.IsDBNull(reader.GetOrdinal("supplier_zipcode")))
                                    {
                                        SupplierZipcodeLabel.Text = reader["supplier_zipcode"].ToString().Trim();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("store_name")))
                                    {
                                        string[] storename = reader["store_name"].ToString().Split(',');

                                        if (storename.Length == 2)
                                        {
                                            StoreNameLabel.Text = storename[1].Trim() + " " + storename[0].Trim();
                                            HeaderLabel.Text = storename[1].Trim() + " " + storename[0].Trim();
                                        }
                                        else
                                        {
                                            StoreNameLabel.Text = storename[0].Trim();
                                            HeaderLabel.Text = storename[0].Trim();
                                        }
                                    }
                                    //purchase_orders.store_building_name(14),
                                    if (!reader.IsDBNull(reader.GetOrdinal("store_building_name")))
                                    {
                                        StoreBuildingNameLabel.Text = reader["store_building_name"].ToString().Trim();
                                    }
                                    //purchase_orders.store_street_name(15),
                                    if (!reader.IsDBNull(reader.GetOrdinal("store_street_name")))
                                    {
                                        if (!String.IsNullOrEmpty(StoreBuildingNameLabel.Text))
                                        {
                                            StoreBuildingNameLabel.Text = StoreBuildingNameLabel.Text + ", ";
                                        }
                                        StoreStreetNameLabel.Text = reader["store_street_name"].ToString().Trim();
                                    }
                                    //purchase_orders.store_neighbourhood(16),
                                    if (!reader.IsDBNull(reader.GetOrdinal("store_neighbourhood")))
                                    {
                                        StoreNeighbourhoodLabel.Text = reader["store_neighbourhood"].ToString().Trim();
                                    }
                                    //purchase_orders.store_subdistrict(17),
                                    if (!reader.IsDBNull(reader.GetOrdinal("store_subdistrict")))
                                    {
                                        if (!String.IsNullOrEmpty(StoreNeighbourhoodLabel.Text))
                                        {
                                            StoreNeighbourhoodLabel.Text = StoreNeighbourhoodLabel.Text + ", ";
                                        }
                                        StoreSubdistrictLabel.Text = reader["store_subdistrict"].ToString().Trim();
                                    }
                                    
                                    if (!reader.IsDBNull(reader.GetOrdinal("store_district")))
                                    {
                                        if (!String.IsNullOrEmpty(StoreNeighbourhoodLabel.Text))
                                        {
                                            StoreNeighbourhoodLabel.Text = StoreNeighbourhoodLabel.Text + ", ";
                                        }
                                        if (!String.IsNullOrEmpty(StoreSubdistrictLabel.Text))
                                        {
                                            StoreSubdistrictLabel.Text = StoreSubdistrictLabel.Text + ", ";
                                        }
                                        StoreDistrictLabel.Text = reader["store_district"].ToString().Trim();
                                    }
                                    
                                    if (!reader.IsDBNull(reader.GetOrdinal("store_rural_district")))
                                    {
                                        StoreRuralDistrictLabel.Text = reader["store_rural_district"].ToString().Trim();
                                    }
                                    
                                    if (!reader.IsDBNull(reader.GetOrdinal("store_province")))
                                    {
                                        if (!String.IsNullOrEmpty(StoreRuralDistrictLabel.Text))
                                        {
                                            StoreRuralDistrictLabel.Text = StoreRuralDistrictLabel.Text + ", ";
                                        }
                                        StoreProvinceLabel.Text = reader["store_province"].ToString().Trim();
                                    }
                                    
                                    if (!reader.IsDBNull(reader.GetOrdinal("store_zipcode")))
                                    {
                                        StoreZipcodeLabel.Text = reader["store_zipcode"].ToString().Trim();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("goods_return_no")))
                                    {
                                        GoodsReturnNoLabel.Text = reader["goods_return_no"].ToString().Trim();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("goods_return_date")))
                                    {
                                        GoodsReturnDateLabel.Text = Convert.ToDateTime(reader["goods_return_date"]).ToString("dd MMM yyyy");
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("total_qty")))
                                    {
                                        totalQty = reader["total_qty"].ToString().Trim();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("total_price")))
                                    {
                                        totalPrice = reader["total_price"].ToString();
                                    }

                                    if (!reader.IsDBNull(reader.GetOrdinal("goods_return_notes")))
                                    {
                                        if (String.IsNullOrEmpty(reader["goods_return_notes"].ToString()?.Trim()))
                                        {
                                            GoodsReturnNotesPanel.Visible = false;
                                        }
                                        else
                                        {
                                            GoodsReturnNotesPanel.Visible = true;
                                            GoodsReturnNotesLabel.Text = reader["goods_return_notes"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        GoodsReturnNotesPanel.Visible = false;
                                    }
                                }
                            }
                            conn.Close();

                            cmd = conn.CreateCommand();
                            cmd.CommandText = @"SELECT ROW_NUMBER() OVER(ORDER BY goods_return_items.goods_return_item_id) [no],
                            item_details.item_barcode,item_details.supplier_product_code,
                            IIF(dbo.JsonValue(variants, 'color') IS NOT NULL AND dbo.JsonValue(variants, 'size') IS NOT NULL, 
                            CONCAT(item_details.item_name, ' ', dbo.JsonValue(variants, 'color'), ' ', dbo.JsonValue(variants, 'size')),
                            IIF(dbo.JsonValue(variants, 'color') IS NOT NULL, CONCAT(item_details.item_name, ' ', dbo.JsonValue(variants, 'color')),
                            IIF(dbo.JsonValue(variants, 'size') IS NOT NULL, CONCAT(item_details.item_name, ' ', dbo.JsonValue(variants, 'size')), 
                            item_details.item_name))) [details], goods_return_items.reason,
                            FORMAT(goods_return_items.item_qty, '###,###,###,###,##0') [item_qty],
                            FORMAT(goods_return_items.item_price, '###,###,###,###,##0.00') [item_price], 
                            FORMAT(goods_return_items.item_qty * goods_return_items.item_price, '###,###,###,###,##0.00') [amount]
                            FROM goods_returns INNER JOIN goods_return_items ON goods_returns.goods_return_id = goods_return_items.goods_return_id AND 
                            goods_return_items.isdeleted = 0
                            INNER JOIN item_details ON goods_return_items.item_detail_id = item_details.item_detail_id
                            WHERE goods_returns.goods_return_id = @returnId AND goods_returns.uuid = @returnUuid AND goods_returns.isdeleted = 0;";
                            cmd.Parameters.AddWithValue("@returnId", id);
                            cmd.Parameters.AddWithValue("@returnUuid", uuid);

                            SqlDataAdapter da = new SqlDataAdapter(cmd);
                            DataTable table = new DataTable();
                            da.Fill(table);
                            
                            ItemGridView.DataSource = table;
                            ItemGridView.DataBind();

                            table.Clear();

                            GridViewRow row = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Normal);
                            TableCell cell = new TableCell();
                            cell.Text = "Total";
                            cell.CssClass = "text-end";
                            cell.ColumnSpan = 5;
                            row.Controls.Add(cell);
                            cell = new TableCell();
                            cell.Text = totalQty;
                            cell.CssClass = "text-end";
                            row.Controls.Add(cell);
                            cell = new TableCell();
                            cell.Text = "";
                            row.Controls.Add(cell);
                            cell = new TableCell();
                            cell.Text = totalPrice;
                            cell.CssClass = "text-end";
                            row.Controls.Add(cell);

                            ItemGridView.Controls[0].Controls.AddAt(ItemGridView.Rows.Count + 1, row);
                        }
                        else
                        {
                            Response.Redirect("Forbidden.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("NotFound.aspx");
                    }
                }
                else
                {
                    Response.Redirect("NotFound.aspx");
                }
            }
        }

        protected void BackButton_Click(object sender, EventArgs e)
        {
            Session["redirect"] = true;
            Response.Redirect("GoodsReturn.aspx");
        }
    }
}
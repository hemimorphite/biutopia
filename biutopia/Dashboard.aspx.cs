﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;

namespace biutopia
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["username"]) || string.IsNullOrEmpty((string)Session["role_id"]))
            {
                Session.Clear();
                Response.Redirect("login.aspx");
            }

            List<string> pagesList = new List<string>();
            Session["stores"] = new int[0];
            Session["suppliers"] = new int[0];
            Session["add"] = new string[0];
            Session["edit"] = new string[0];
            Session["read"] = new string[0];
            Session["delete"] = false;
            Session["upload"] = false;
            Session["print"] = false;
            Session["pages"] = new string[0];

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT permissions FROM roles WHERE role_id = @roleId";
                    cmd.Parameters.AddWithValue("@roleId", Session["role_id"]);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("permissions")))
                                {
                                    Newtonsoft.Json.Linq.JObject pagepermissions = Newtonsoft.Json.Linq.JObject.Parse(reader.GetString(reader.GetOrdinal("permissions")).Trim());
                                    Newtonsoft.Json.Linq.JObject accessRights = Newtonsoft.Json.Linq.JObject.Parse(Session["access_rights"].ToString());
                                    Newtonsoft.Json.Linq.JArray jstores = (Newtonsoft.Json.Linq.JArray)accessRights["stores"];

                                    int[] stores = new int[jstores.Count];

                                    int a = 0;
                                    foreach (int store in jstores)
                                    {
                                        stores[a++] = store;
                                    }

                                    Session["stores"] = stores;

                                    Newtonsoft.Json.Linq.JArray jsuppliers = (Newtonsoft.Json.Linq.JArray)accessRights["suppliers"];

                                    int[] suppliers = new int[jsuppliers.Count];

                                    int b = 0;
                                    foreach (int supplier in jsuppliers)
                                    {
                                        suppliers[b++] = supplier;
                                    }

                                    Session["suppliers"] = suppliers;

                                    Newtonsoft.Json.Linq.JArray pages = (Newtonsoft.Json.Linq.JArray)pagepermissions["pages"];

                                    for (int i = 0; i < pages.Count; i++)
                                    {
                                        Newtonsoft.Json.Linq.JObject page = (Newtonsoft.Json.Linq.JObject)pages[i];
                                        Newtonsoft.Json.Linq.JArray permissions = (Newtonsoft.Json.Linq.JArray)page["permissions"];

                                        if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                        {
                                            pagesList.Add((string)page["name"]);
                                        }

                                        if ((string)page["name"] == "dashboard")
                                        {
                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[0])["auto"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[0])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["auto"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[0])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["read"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[2])["add"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[1])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["add"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[3])["edit"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[2])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["edit"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[4])["delete"])
                                            {
                                                Session["delete"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[5])["upload"])
                                            {
                                                Session["upload"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[6])["print"])
                                            {
                                                Session["print"] = true;
                                            }
                                        }
                                    }

                                    Session["pages"] = pagesList.ToArray();
                                }
                            }
                        }
                    }
                    conn.Close();

                    //if (((string[])Session["read"]).Length == 0)
                    //{
                    //    Response.Redirect("Forbidden.aspx");
                    //}
                }
            }
        }
    }
}
﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Item.aspx.cs" EnableViewState="True" Inherits="biutopia.Item" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main-content">
        <div class="page-heading">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 order-md-1 order-last">
                        <h3>Barang</h3>
                        <p class="text-subtitle text-muted"></p>
                    </div>
                </div>
            </div>
            <section class="section">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <% if (((string[])Session["read"]).Length > 0)
                                { %>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link active" id="itemTab" data-bs-toggle="tab" href="#item" role="tab"
                                    aria-controls="item" aria-selected="true">Barang</a>
                            </li>
                            <% } %>
                            <% if ((bool)Session["upload"] == true)
                                { %>
                            <li class="nav-item" role="presentation">
                                <a class="nav-link" id="uploadItemsTab" data-bs-toggle="tab" href="#uploadItems" role="tab"
                                    aria-controls="uploadItems" aria-selected="false">Unggah</a>
                            </li>
                            <% } %>
                        </ul>
                        <div class="tab-content mt-4" id="myTabContent">
                            <% if (((string[])Session["read"]).Length > 0)
                                { %>
                            <div class="tab-pane fade show active" id="item" role="tabpanel" aria-labelledby="itemTab">
                                <div class="row">
                                    <div class="col-12">
                                        <asp:Label ID="AlertLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreChoice">Toko Ritel</label>
                                            <asp:DropDownList ID="StoreChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="StoreChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierChoice">Pemasok</label>
                                            <asp:DropDownList ID="SupplierChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="SupplierChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-5"></div>
                                    <div class="col-12 col-md-6">
                                        <% if (((string[])Session["add"]).Length > 0)
                                            { %>
                                        <asp:LinkButton ID="AddButton" CssClass="btn btn-primary align-middle" runat="server" OnClick="AddButton_Click">
                                            <i class="bi bi-plus-lg"></i> Barang
                                        </asp:LinkButton>
                                        <% } %>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <asp:Panel ID="SearchPanel" runat="server" CssClass="input-group mb-3" DefaultButton="SearchButton">
                                            <asp:TextBox ID="SearchInput" CssClass="form-control" runat="server" placeholder="Cari (Tekan Enter)" OnTextChanged="SearchInput_TextChanged" AutoPostBack="true"></asp:TextBox>
                                            <asp:LinkButton ID="SearchButton" CssClass="btn btn-primary align-middle" runat="server" OnCommand="SearchButton_Command">
                                                <i class="bi bi-search"></i>
                                            </asp:LinkButton>
                                        </asp:Panel>
                                    </div> 
                                    <div class="col-12">
                                        <div class="overflow-auto mb-4">
                                            <asp:GridView ID="ItemGridView" CssClass="table table-striped table-fixed" runat="server" ShowHeaderWhenEmpty="True" Autogeneratecolumns="false" AllowSorting="True" onsorting="ItemGridView_Sorting" AllowPaging="True" PageSize="5" OnPageIndexChanging="ItemGridView_PageIndexChanging" OnRowEditing="ItemGridView_RowEditing" OnRowDeleting="ItemGridView_RowDeleting" OnRowDataBound="ItemGridView_RowDataBound" OnRowCreated="ItemGridView_RowCreated">
                                                <%--<Columns>
                                                    <asp:BoundField HeaderText="No" DataField="no" SortExpression="no" ReadOnly="true" ItemStyle-CssClass="text-nowrap"/>
                                                    <asp:BoundField HeaderText="Barcode" DataField="item_barcode" SortExpression="item_barcode" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Kode Kategori" DataField="item_category_code" SortExpression="item_category_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Kode Produk Pemasok" DataField="supplier_product_code" SortExpression="supplier_product_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Kode Barang" DataField="item_code" SortExpression="item_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Nama Barang" DataField="item_name" SortExpression="item_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Merek" DataField="brand_name" SortExpression="brand_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Deskripsi Singkat" DataField="short_description" SortExpression="short_description" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Varian" DataField="variants" SortExpression="variants" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Harga Jual" DataField="selling_price" SortExpression="selling_price" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Margin" DataField="purchase_margin" SortExpression="purchase_margin" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Tanggal Berlaku" DataField="effective_date" SortExpression="effective_date" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Satuan Ukuran" DataField="uom" SortExpression="uom" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Toko" DataField="store_name" SortExpression="store_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Pemasok" DataField="supplier_name" SortExpression="supplier_name" ReadOnly="true" />
                                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="text-nowrap">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="EditButton" CssClass="btn btn-primary align-middle" runat="server" CommandName="edit" CommandArgument='<%# Eval("item_id") + ";" + Eval("uuid")  %>' OnCommand="EditButton_Click">
                                                                <i class="bi bi-pencil-fill"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="DeleteButton" CssClass="btn btn-danger align-middle ms-2" runat="server" CommandName="delete" CommandArgument='<%# Eval("item_id") + ";" + Eval("uuid") + ";" + Eval("item_barcode") + ";" + Eval("item_name") %>' OnCommand="DeleteButton_Click">
                                                                <i class="bi bi-x-lg"></i>
                                                            </asp:LinkButton>
                                                        
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>--%>
                                                <PagerTemplate>
                                                    <div class="pagination justify-content-center">
                                                        <asp:Panel ID="FirstPanel" CssClass="page-item me-2" runat="server">
                                                            <asp:LinkButton ID="FirstButton" CssClass="page-link" runat="server" OnClick="FirstButton_Click">First</asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="PreviousPanel" CssClass="page-item me-2" runat="server">
                                                            <asp:LinkButton ID="PreviousButton" CssClass="page-link" runat="server" OnClick="PreviousButton_Click">Previous</asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="PagePanel" CssClass="page-item me-2" runat="server">
                                                            <asp:DropDownList ID="PageDropDownList" class="page-link" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PageDropDownList_SelectedIndexChanged">
                                                        
                                                            </asp:DropDownList>
                                                        </asp:Panel>
                                                        <asp:Panel ID="NextPanel" CssClass="page-item me-2" runat="server">
                                                            <asp:LinkButton ID="NextButton" CssClass="page-link" runat="server" OnClick="NextButton_Click">Next</asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="LastPanel" CssClass="page-item" runat="server">
                                                            <asp:LinkButton ID="LastButton" CssClass="page-link" runat="server" OnClick="LastButton_Click">Last</asp:LinkButton>
                                                        </asp:Panel>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" />
                                            </asp:GridView>
                                        </div>
                                        <asp:Panel ID="LoadingPanel" CssClass="d-none mb-2" runat="server">
                                            <div class="spinner-grow spinner-grow-sm text-primary" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-secondary" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-success" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-danger" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-warning" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-info" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-light" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-dark" role="status">
                                                <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <span>Loading</span>
                                        </asp:Panel>

                                        <asp:Button ID="DownloadAllButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Semua" CommandName="downloadall" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:Button ID="DownloadSelectedSupplierButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Berdasarkan Vendor" CommandName="downloadselectedsupplier" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:Button ID="DownloadSelectedStoreButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Berdasarkan Toserba" CommandName="downloadselectedstore" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:Button ID="DownloadSelectedStoreAndSupplierButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Berdasarkan Toserba dan Vendor" CommandName="downloadselectedstoreandsupplier" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:HiddenField ID="FilenameInput" runat="server" />
                                        <div class="mb-5"></div>
                                    </div>
                                
                                </div>
                            </div>
                            <% } %>
                            <% if ((bool)Session["upload"] == true)
                                { %>
                            <div class="tab-pane fade" id="uploadItems" role="tabpanel" aria-labelledby="uploadItemsTab">           
                                <div class="row">
                                    <div class="col-12">
                                        <asp:Label ID="AlertUploadLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreUploadChoice">Toko Ritel</label>
                                            <asp:DropDownList ID="StoreUploadChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="StoreUploadChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierUploadChoice">Pemasok</label>
                                            <asp:DropDownList ID="SupplierUploadChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="SupplierUploadChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </div>
                                    </div>
                                    <div class="col-12 mb-2">
                                        <div class="form-group">
                                            <asp:Label ID="UploadLabel"
                                                        Text="Berkas Excel"
                                                        AssociatedControlID="UploadInput"
                                                        runat="server">
                                            </asp:Label>
                                            <asp:FileUpload ID="UploadInput" CssClass="form-control" runat="server" />
                                            <asp:RegularExpressionValidator ID="UploadRegularExpressionValidator" runat="server"
                                            ControlToValidate="UploadInput"
                                            ErrorMessage="Ekstensi berkas tidak didukung, hanya berkas excel yang didukung"
                                            ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.xls|.xlsx|.csv)$" ForeColor="#dc3545">
                                            </asp:RegularExpressionValidator>
                                        </div>
                                    </div>
                                    <div class="btn-group">
                                        <asp:Button ID="UploadButton" CssClass="btn btn-primary mb-5" runat="server" Text="Unggah Berkas" OnClick="UploadButton_Click" />  
                                    </div>
                                    <div class="col-12">
                                        <div class="overflow-auto">
                                            <asp:GridView ID="ItemInsertGridView" CssClass="table table-striped" runat="server" ShowHeaderWhenEmpty="False" AutoGenerateColumns="false" Caption="Tabel data barang yang akan ditambahkan" CaptionAlign="Top" OnRowDataBound="ItemInsertGridView_RowDataBound">
                                                <%--<Columns>
                                                    <asp:BoundField HeaderText="No" DataField="no"/>
                                                    <asp:BoundField HeaderText="Barcode" DataField="item_barcode" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Kode Kategori" DataField="item_category_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Kode Produk Pemasok" DataField="supplier_product_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Kode Barang" DataField="item_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Nama Barang" DataField="item_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Merek" DataField="brand_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Deskripsi Singkat" DataField="short_description" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Warna" DataField="color" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Ukuran" DataField="size" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Harga Jual" DataField="selling_price" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Margin" DataField="purchase_margin" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Tanggal Berlaku" DataField="effective_date" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Satuan Ukuran" DataField="uom" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="text-nowrap">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="EditUploadButton" CssClass="btn btn-primary align-middle" runat="server" CommandName="editgvinsert" CommandArgument='<%# Eval("item_barcode")  %>' OnCommand="EditUploadButton_Click">
                                                                <i class="bi bi-pencil-fill"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="DeleteUploadButton" CssClass="btn btn-danger align-middle ms-2" runat="server" CommandName="deletegvinsert" CommandArgument='<%# Eval("item_barcode") %>' OnCommand="DeleteUploadButton_Click">
                                                                <i class="bi bi-x-lg"></i>
                                                            </asp:LinkButton>
                                                        
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>--%>
                                            </asp:GridView>
                                        </div>
                                        <asp:Label ID="MarginBottomGVInsertLabel" CssClass="d-block mb-4" runat="server" Text=""></asp:Label>
                                    </div>

                                    <div class="col-12">
                                        <div class="overflow-auto">
                                            <asp:GridView ID="ItemUpdateGridView" CssClass="table table-striped" runat="server" ShowHeaderWhenEmpty="False" AutoGenerateColumns="false" Caption="Tabel data barang  yang akan diperbaharui" CaptionAlign="Top" OnRowDataBound="ItemUpdateGridView_RowDataBound">
                                                <%--<Columns>
                                                    <asp:BoundField HeaderText="No" DataField="no"/>
                                                    <asp:BoundField HeaderText="Barcode" DataField="item_barcode" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Kode Kategori" DataField="item_category_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Kode Produk Pemasok" DataField="supplier_product_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Kode Barang" DataField="item_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Nama Barang" DataField="item_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Merek" DataField="brand_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Deskripsi Singkat" DataField="short_description" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Warna" DataField="color" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Ukuran" DataField="size" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Harga Jual" DataField="selling_price" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Margin" DataField="purchase_margin" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Tanggal Berlaku" DataField="effective_date" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Satuan Ukuran" DataField="uom" ItemStyle-CssClass="text-nowrap" ReadOnly="true" />
                                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="text-nowrap">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="EditUploadButton" CssClass="btn btn-primary align-middle" runat="server" CommandName="editgvupdate" CommandArgument='<%# Eval("item_barcode")  %>' OnCommand="EditUploadButton_Click">
                                                                <i class="bi bi-pencil-fill"></i>
                                                            </asp:LinkButton>
                                                            <asp:LinkButton ID="DeleteUploadButton" CssClass="btn btn-danger align-middle ms-2" runat="server" CommandName="deletegvupdate" CommandArgument='<%# Eval("item_barcode") + ";" + Eval("item_name") %>' OnCommand="DeleteUploadButton_Click">
                                                                <i class="bi bi-x-lg"></i>
                                                            </asp:LinkButton>
                                                        
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>--%>
                                            </asp:GridView>
                                        </div>
                                        <asp:Label ID="MarginBottomGVUpdateLabel" CssClass="d-block mb-4" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="col-12">
                                        <asp:Button ID="SaveUploadButton" CssClass="btn btn-primary me-2 px-5" runat="server" Text="Simpan" OnClick="SaveUploadButton_Click" />
                                        <asp:Button ID="CancelUploadButton" CssClass="btn btn-secondary px-5" runat="server" Text="Batal" OnClick="CancelUploadButton_Click" />
                                        <asp:Label ID="MarginBottomButtonLabel" CssClass="d-block mb-5" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                        </div>
                    </div>
                </div>
                
                <% if (((string[])Session["add"]).Length > 0)
                    { %>
                <div class="modal fade" id="itemModal" tabindex="-1" role="dialog" aria-labelledby="itemModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable modal-lg"
                        role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="itemModalTitle">Tambah Barang</h5>
                                <button type="button" class="close" data-bs-dismiss="modal"
                                    aria-label="Close">
                                    <i data-feather="x"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <asp:Label ID="AlertModalLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <% if ((((string[])Session["add"]).Contains("itemBarcode") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("itemBarcode") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="ItemBarcodeLabel" Text="Barcode Barang" AssociatedControlID="ItemBarcodeInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="ItemBarcodeInput" CssClass="form-control" runat="server" placeholder="Barcode Barang" autocomplete="off"></asp:TextBox>
                                            <asp:CompareValidator ID="ItemBarcodeCompareValidator" runat="server"
                                            ControlToValidate="ItemBarcodeInput"
                                            ErrorMessage="Hanya angka"
                                            Operator="DataTypeCheck" Type="Integer" ForeColor="#dc3545">
                                            </asp:CompareValidator>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("supplierProductCode") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("supplierProductCode") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="SupplierProductCodeLabel" Text="Kode Produk Pemasok (opsional)" AssociatedControlID="SupplierProductCodeInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="SupplierProductCodeInput" CssClass="form-control" runat="server" placeholder="Kode Produk Pemasok (opsional)" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("itemCode") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("itemCode") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="ItemCodeLabel" Text="Kode Barang (opsional)" AssociatedControlID="ItemCodeInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="ItemCodeInput" CssClass="form-control" runat="server" placeholder="Kode Barang (opsional)" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("itemCategoryCode") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("itemCategoryCode") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="ItemCategoryCodeLabel" Text="Kode Kategori (opsional)" AssociatedControlID="ItemCategoryCodeInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="ItemCategoryCodeInput" CssClass="form-control" runat="server" placeholder="Kode Kategori (opsional)" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("itemName") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("itemName") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="ItemNameLabel" Text="Nama Barang" AssociatedControlID="ItemNameInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="ItemNameInput" CssClass="form-control" runat="server" placeholder="Nama Barang" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("brandName") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("brandName") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="BrandNameLabel" Text="Merek Barang" AssociatedControlID="BrandNameInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="BrandNameInput" CssClass="form-control" runat="server" placeholder="Merek" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("foreignName") || ((string[])Session["edit"]).Contains("foreignName"))
                                        { %>
                                    <!--<div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="ForeignNameLabel" Text="Nama Asing (opsional)" AssociatedControlID="ForeignNameInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="ForeignNameInput" CssClass="form-control" runat="server" placeholder="Nama Asing (opsional)" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>-->
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("shortDescription") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("shortDescription") && action == "update"))
                                        { %>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <asp:Label ID="ShortDescriptionLabel" Text="Deskripsi Singkat" AssociatedControlID="ShortDescriptionInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="ShortDescriptionInput" CssClass="form-control" runat="server" placeholder="Deskripsi Singkat" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("variants") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("variants") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="ColorLabel" Text="Varian Warna (opsional)" AssociatedControlID="ColorInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="ColorInput" CssClass="form-control" runat="server" placeholder="Varian Warna (opsional)" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="SizeLabel" Text="Varian Ukuran (opsional)" AssociatedControlID="SizeInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="SizeInput" CssClass="form-control" runat="server" placeholder="Varian Ukuran (opsional)" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("sellingPrice") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("sellingPrice") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="PriceLabel" Text="Harga Jual" AssociatedControlID="PriceInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="PriceInput" CssClass="form-control" runat="server" placeholder="Harga Jual" autocomplete="off"></asp:TextBox>
                                            <asp:CompareValidator ID="PriceCompareValidator" runat="server"
                                            ControlToValidate="PriceInput"
                                            ErrorMessage="Hanya angka"
                                            Operator="DataTypeCheck" Type="Double" ForeColor="#dc3545">
                                            </asp:CompareValidator>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("purchaseMargin") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("purchaseMargin") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="MarginLabel" Text="Margin (opsional)" AssociatedControlID="MarginInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="MarginInput" CssClass="form-control" runat="server" placeholder="Margin (opsional)" autocomplete="off"></asp:TextBox>
                                            <asp:CompareValidator ID="MarginCompareValidator" runat="server"
                                            ControlToValidate="MarginInput"
                                            ErrorMessage="Hanya angka"
                                            Operator="DataTypeCheck" Type="Double" ForeColor="#dc3545">
                                            </asp:CompareValidator>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("effectiveDate") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("effectiveDate") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="EfectiveDateLabel" Text="Tanggal Berlaku (opsional)" AssociatedControlID="EffectiveDateInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="EffectiveDateInput" CssClass="form-control flatpickr-no-config" runat="server" placeholder="Tanggal Berlaku (opsional)" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if ((((string[])Session["add"]).Contains("unitOfMeasure") && action == "create") || 
                                            (((string[])Session["edit"]).Contains("unitOfMeasure") && action == "update"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <asp:Label ID="UnitOfMeasureLabel" Text="Satuan Unit Aset (opsional)" AssociatedControlID="UnitOfMeasureInput" runat="server"></asp:Label>
                                            <asp:TextBox ID="UnitOfMeasureInput" CssClass="form-control" runat="server" placeholder="Satuan Unit Aset (opsional)" autocomplete="off"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="CancelButton" CssClass="btn btn-secondary" runat="server" Text="Batal" OnClick="CancelButton_Click" />
                                <asp:Button ID="SaveButton" CssClass="btn btn-primary ms-1" runat="server" Text="Simpan" OnCommand="SaveButton_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
                <% if ((bool)Session["delete"] == true)
                    { %>
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="confirmModalTitle">Konfirmasi</h5>
                                <button type="button" class="close" data-bs-dismiss="modal"
                                    aria-label="Close">
                                    <i data-feather="x"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Apakah ingin menghapus barang <asp:Label ID="ItemNameModalLabel" runat="server" Text=""></asp:Label> dengan barcode <asp:Label ID="ItemBarcodeModalLabel" runat="server" Text=""></asp:Label></p>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="NoButton" CssClass="btn btn-secondary" runat="server" Text="Tidak" OnClick="NoButton_Click" />
                                                    
                                <asp:Button ID="YesButton" CssClass="btn btn-primary ms-1" runat="server" Text="Ya" CommandName="delete" OnCommand ="YesButton_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
            </section>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script type="text/javascript">
        function selectUploadItemsTab() {
            var tab = new bootstrap.Tab(document.querySelector('#uploadItemsTab'));
            tab.show();
        }
        function openItemModal() {
            var modal = new bootstrap.Modal(document.getElementById('itemModal'));
            modal.show();
        }
        function openConfirmModal() {
            var modal = new bootstrap.Modal(document.getElementById('confirmModal'));
            modal.show();
        }
    </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <script type="text/javascript">

        function download() {
            var input = document.getElementById("<%= FilenameInput.ClientID %>");
            var link = document.createElement("a");
            link.download = input.value;
            link.href = window.location.protocol + '//' + window.location.host + "/uploads/" + input.value;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            delete link;
        }


        var DownloadAllButton = document.getElementById("<%= DownloadAllButton.ClientID %>");
        var DownloadSelectedSupplierButton = document.getElementById("<%= DownloadSelectedSupplierButton.ClientID %>");
        var DownloadSelectedStoreButton = document.getElementById("<%= DownloadSelectedStoreButton.ClientID %>");
        var DownloadSelectedStoreAndSupplierButton = document.getElementById("<%= DownloadSelectedStoreAndSupplierButton.ClientID %>");
        var LoadingPanel = document.getElementById("<%= LoadingPanel.ClientID %>");

        DownloadAllButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
        DownloadSelectedSupplierButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
        DownloadSelectedStoreButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
        DownloadSelectedStoreAndSupplierButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
    </script>
</asp:Content>
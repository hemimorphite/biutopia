﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;
using Microsoft.Office.Interop.Excel;
using System.Runtime.InteropServices;
using System.Collections;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Data.OleDb;

namespace biutopia
{
    public partial class Item : System.Web.UI.Page
    {
        protected string action = "create";

        protected static readonly Dictionary<string, string> columns = new Dictionary<string, string>()
        {
            { "itemId", "item_id" },
            { "storeId", "store_id" },
            { "supplierId", "supplier_id" },
            { "itemBarcode", "item_barcode" },
            { "itemCode", "item_code" },
            { "itemCategoryCode", "item_category_code" },
            { "itemName", "item_name" },
            { "foreignName", "foreign_name" },
            { "shortDescription", "short_description" },
            { "variants", "variants" },
            { "sellingPrice", "selling_price" },
            { "purchaseMargin", "purchase_margin" },
            { "effectiveDate", "effective_date" },
            { "brandName", "brand_name" },
            //{ "createdDate", "created_date" },
            //{ "modifiedDate", "modified_date" },
            //{ "createdBy", "created_by" },
            //{ "modifiedBy", "modified_by" },
            { "unitOfMeasure", "uom" },
            { "supplierProductCode", "supplier_product_code" },
            //{ "uuid", "uuid" }
        };

        protected static readonly Dictionary<string, string> datatypes = new Dictionary<string, string>()
        {
            { "itemId", "int" },
            { "storeId", "int" },
            { "supplierId", "int" },
            { "itemBarcode", "varchar (255)" },
            { "itemCode", "varchar (255)" },
            { "itemCategoryCode", "varchar (255)" },
            { "itemName", "varchar (255)" },
            { "foreignName", "varchar (255)" },
            { "shortDescription", "varchar (255)" },
            { "variants", "varchar (max)" },
            { "sellingPrice", "decimal (18, 2)" },
            { "purchaseMargin", "decimal (18, 2)" },
            { "effectiveDate", "datetime" },
            { "brandName", "varchar (255)" },
            //{ "createdDate", "datetime" },
            //{ "modifiedDate", "datetime" },
            //{ "createdBy", "varchar (255)" },
            //{ "modifiedBy", "varchar (255)" },
            { "unitOfMeasure", "varchar (255)" },
            { "supplierProductCode", "varchar (50)" },
            //{ "uuid", "UNIQUEIDENTIFIER" }
        };

        //private string GetPostBackControlName()
        //{
        //    Control control = null;

        //    string ctrlname = Page.Request.Params["__EVENTTARGET"];

        //    if (ctrlname != null && ctrlname != String.Empty)
        //    {
        //        control = Page.FindControl(ctrlname);
        //    }
        //    // if __EVENTTARGET is null, the control is a button type and we need to
        //    // iterate over the form collection to find it
        //    else
        //    {
        //        string ctrlStr = String.Empty;

        //        Control c = null;

        //        foreach (string ctl in Page.Request.Form)
        //        {
        //            // handle ImageButton they having an additional "quasi-property" in
        //            // their Id which identifies

        //            // mouse x and y coordinates

        //            if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))

        //            {
        //                ctrlStr = ctl.Substring(0, ctl.Length - 2);

        //                c = Page.FindControl(ctrlStr);

        //            }

        //            else

        //            {
        //                c = Page.FindControl(ctl);
        //            }

        //            if (c is System.Web.UI.WebControls.Button ||

        //                c is System.Web.UI.WebControls.ImageButton)

        //            {
        //                control = c;

        //                break;
        //            }
        //        }
        //    }

        //    return control.ID;
        //}

        protected string GetPostBackControlName()
        {
            Control control = null;

            string ctrlname = Page.Request.Params["__EVENTTARGET"];

            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);

                if (control != null)
                {
                    return control.ID;
                }
                return "";
            }
            else
            {
                return "";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["username"]) || string.IsNullOrEmpty((string)Session["role_id"]))
            {
                Session.Clear();
                Response.Redirect("login.aspx");
            }

            List<string> pagesList = new List<string>();
            Session["stores"] = new int[0];
            Session["suppliers"] = new int[0];
            Session["add"] = new string[0];
            Session["edit"] = new string[0];
            Session["read"] = new string[0];
            Session["delete"] = false;
            Session["upload"] = false;
            Session["print"] = false;
            Session["pages"] = new string[0];

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT permissions FROM roles WHERE role_id = @roleId";
                    cmd.Parameters.AddWithValue("@roleId", Session["role_id"]);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("permissions")))
                                {
                                    Newtonsoft.Json.Linq.JObject pagepermissions = Newtonsoft.Json.Linq.JObject.Parse(reader.GetString(reader.GetOrdinal("permissions")).Trim());
                                    Newtonsoft.Json.Linq.JObject accessRights = Newtonsoft.Json.Linq.JObject.Parse(Session["access_rights"].ToString());
                                    Newtonsoft.Json.Linq.JArray jstores = (Newtonsoft.Json.Linq.JArray)accessRights["stores"];

                                    int[] stores = new int[jstores.Count];

                                    int a = 0;
                                    foreach (int store in jstores)
                                    {
                                        stores[a++] = store;
                                    }

                                    Session["stores"] = stores;

                                    Newtonsoft.Json.Linq.JArray jsuppliers = (Newtonsoft.Json.Linq.JArray)accessRights["suppliers"];

                                    int[] suppliers = new int[jsuppliers.Count];

                                    int b = 0;
                                    foreach (int supplier in jsuppliers)
                                    {
                                        suppliers[b++] = supplier;
                                    }

                                    Session["suppliers"] = suppliers;

                                    Newtonsoft.Json.Linq.JArray pages = (Newtonsoft.Json.Linq.JArray)pagepermissions["pages"];

                                    for (int i = 0; i < pages.Count; i++)
                                    {
                                        Newtonsoft.Json.Linq.JObject page = (Newtonsoft.Json.Linq.JObject)pages[i];
                                        Newtonsoft.Json.Linq.JArray permissions = (Newtonsoft.Json.Linq.JArray)page["permissions"];

                                        if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                        {
                                            pagesList.Add((string)page["name"]);
                                        }

                                        if ((string)page["name"] == "item")
                                        {
                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[0])["auto"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[0])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["auto"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[1])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["read"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[2])["add"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[2])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["add"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[3])["edit"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[3])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["edit"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[4])["delete"])
                                            {
                                                Session["delete"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[5])["upload"])
                                            {
                                                Session["upload"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[6])["print"])
                                            {
                                                Session["print"] = true;
                                            }
                                        }
                                    }

                                    Session["pages"] = pagesList.ToArray();
                                }
                            }
                        }
                    }

                    conn.Close();

                    if (((string[])Session["read"]).Length == 0)
                    {
                        Response.Redirect("Forbidden.aspx");
                    }
                }
            }

            if (!this.IsPostBack)
            {
                action = "create";

                SaveUploadButton.Visible = false;
                CancelUploadButton.Visible = false;
                MarginBottomButtonLabel.Visible = false;
                MarginBottomGVInsertLabel.Visible = false;
                MarginBottomGVUpdateLabel.Visible = false;

                System.Data.DataTable items = new System.Data.DataTable("items");
                items.Columns.Add("no", typeof(Int32));
                items.Columns.Add("item_id");
                items.Columns.Add("uuid");

                if (((string[])Session["read"]).Contains("store"))
                {
                    items.Columns.Add("store_name");
                }

                if (((string[])Session["read"]).Contains("supplier"))
                {
                    items.Columns.Add("supplier_name");
                }

                if (((string[])Session["read"]).Contains("itemBarcode"))
                {
                    items.Columns.Add("item_barcode");
                }

                if (((string[])Session["read"]).Contains("itemCode"))
                {
                    items.Columns.Add("item_code");
                }

                if (((string[])Session["read"]).Contains("itemCategoryCode"))
                {
                    items.Columns.Add("item_category_code");
                }

                if (((string[])Session["read"]).Contains("itemName"))
                {
                    items.Columns.Add("item_name");
                    items.Columns.Add("foreign_name");
                }

                if (((string[])Session["read"]).Contains("shortDescription"))
                {
                    items.Columns.Add("short_description");
                }

                if (((string[])Session["read"]).Contains("variants"))
                {
                    items.Columns.Add("variants");
                }

                if (((string[])Session["read"]).Contains("sellingPrice"))
                {
                    items.Columns.Add("selling_price", typeof(Double));
                }

                if (((string[])Session["read"]).Contains("purchaseMargin"))
                {
                    items.Columns.Add("purchase_margin", typeof(Double));
                }

                if (((string[])Session["read"]).Contains("effectiveDate"))
                {
                    items.Columns.Add("effective_date");
                }

                if (((string[])Session["read"]).Contains("brandName"))
                {
                    items.Columns.Add("brand_name");
                }

                if (((string[])Session["read"]).Contains("unitOfMeasure"))
                {
                    items.Columns.Add("uom");
                }

                if (((string[])Session["read"]).Contains("supplierProductCode"))
                {
                    items.Columns.Add("supplier_product_code");
                }

                ViewState["items"] = items;

                System.Data.DataTable inserts = new System.Data.DataTable("inserts");
                
                inserts.Columns.Add("no");

                if (((string[])Session["add"]).Contains("store"))
                {
                    inserts.Columns.Add("store_id");
                }

                if (((string[])Session["add"]).Contains("supplier"))
                {
                    inserts.Columns.Add("supplier_id");
                }

                if (((string[])Session["add"]).Contains("itemBarcode"))
                {
                    inserts.Columns.Add("item_barcode");
                }

                if (((string[])Session["add"]).Contains("itemCode"))
                {
                    inserts.Columns.Add("item_code");
                }

                if (((string[])Session["add"]).Contains("itemCategoryCode"))
                {
                    inserts.Columns.Add("item_category_code");
                }

                if (((string[])Session["add"]).Contains("itemName"))
                {
                    inserts.Columns.Add("item_name");
                    inserts.Columns.Add("foreign_name");
                }

                if (((string[])Session["add"]).Contains("shortDescription"))
                {
                    inserts.Columns.Add("short_description");
                }

                if (((string[])Session["add"]).Contains("variants"))
                {
                    inserts.Columns.Add("color");
                    inserts.Columns.Add("size");
                    inserts.Columns.Add("variants");
                }

                if (((string[])Session["add"]).Contains("sellingPrice"))
                {
                    inserts.Columns.Add("selling_price");
                }

                if (((string[])Session["add"]).Contains("purchaseMargin"))
                {
                    inserts.Columns.Add("purchase_margin");
                }

                if (((string[])Session["add"]).Contains("brandName"))
                {
                    inserts.Columns.Add("brand_name");
                }

                if (((string[])Session["add"]).Contains("effectiveDate"))
                {
                    inserts.Columns.Add("effective_date");
                }

                //inserts.Columns.Add("created_date");
                //inserts.Columns.Add("created_by");
                //inserts.Columns.Add("modified_date");
                //inserts.Columns.Add("modified_by");

                if (((string[])Session["add"]).Contains("unitOfMeasure"))
                {
                    inserts.Columns.Add("uom");
                }

                if (((string[])Session["add"]).Contains("supplierProductCode"))
                {
                    inserts.Columns.Add("supplier_product_code");
                }

                ViewState["inserts"] = inserts;

                System.Data.DataTable updates = new System.Data.DataTable("updates");

                updates.Columns.Add("no");
                updates.Columns.Add("item_id");
                updates.Columns.Add("store_id");
                updates.Columns.Add("supplier_id");

                if (((string[])Session["edit"]).Contains("itemBarcode"))
                {
                    updates.Columns.Add("item_barcode");
                }

                if (((string[])Session["edit"]).Contains("itemCode"))
                {
                    updates.Columns.Add("item_code");
                }

                if (((string[])Session["edit"]).Contains("itemCategoryCode"))
                {
                    updates.Columns.Add("item_category_code");
                }

                if (((string[])Session["edit"]).Contains("itemName"))
                {
                    updates.Columns.Add("item_name");
                    updates.Columns.Add("foreign_name");
                }

                if (((string[])Session["edit"]).Contains("shortDescription"))
                {
                    updates.Columns.Add("short_description");
                }

                if (((string[])Session["edit"]).Contains("variants"))
                {
                    updates.Columns.Add("color");
                    updates.Columns.Add("size");
                    updates.Columns.Add("variants");
                }

                if (((string[])Session["edit"]).Contains("sellingPrice"))
                {
                    updates.Columns.Add("selling_price");
                }

                if (((string[])Session["edit"]).Contains("purchaseMargin"))
                {
                    updates.Columns.Add("purchase_margin");
                }

                if (((string[])Session["edit"]).Contains("effectiveDate"))
                {
                    updates.Columns.Add("effective_date");
                }

                if (((string[])Session["edit"]).Contains("brandName"))
                {
                    updates.Columns.Add("brand_name");
                }

                //updates.Columns.Add("modified_date");
                //updates.Columns.Add("modified_by");

                if (((string[])Session["edit"]).Contains("unitOfMeasure"))
                {
                    updates.Columns.Add("uom");
                }

                if (((string[])Session["edit"]).Contains("supplierProductCode"))
                {
                    updates.Columns.Add("supplier_product_code");
                }

                ViewState["updates"] = updates;

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                string commandText = "";

                if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                {
                    commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers";
                }
                else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                {
                    int[] suppliers = (int[])Session["suppliers"];

                    commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers 
                        WHERE supplier_id IN ({0})";

                    string inClause = string.Join(",", suppliers);
                    commandText = string.Format(commandText, inClause);
                }

                if (!String.IsNullOrEmpty(commandText))
                {
                    cmd.CommandText = commandText;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    SupplierChoice.DataSource = ds;
                    SupplierChoice.DataTextField = "supplier_name";
                    SupplierChoice.DataValueField = "supplier_id";
                    SupplierChoice.DataBind();

                    SupplierUploadChoice.DataSource = ds;
                    SupplierUploadChoice.DataTextField = "supplier_name";
                    SupplierUploadChoice.DataValueField = "supplier_id";
                    SupplierUploadChoice.DataBind();

                    ds.Dispose();

                }

                if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                {
                    commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores";
                }
                else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                {
                    int[] stores = (int[])Session["stores"];
                    commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores 
                        WHERE store_id IN ({0})";

                    string inClause = string.Join(",", stores);

                    commandText = string.Format(commandText, inClause);
                }

                if (!String.IsNullOrEmpty(commandText))
                {
                    cmd = conn.CreateCommand();
                    cmd.CommandText = commandText;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    StoreChoice.DataSource = ds;
                    StoreChoice.DataTextField = "store_name";
                    StoreChoice.DataValueField = "store_id";
                    StoreChoice.DataBind();

                    StoreUploadChoice.DataSource = ds;
                    StoreUploadChoice.DataTextField = "store_name";
                    StoreUploadChoice.DataValueField = "store_id";
                    StoreUploadChoice.DataBind();

                    ds.Dispose();

                }

                if (Session["redirect"] != null && (bool)Session["redirect"] == true)
                {
                    if (Session["origin"] != null && Session["origin"].ToString() == "Item")
                    {
                        StoreChoice.SelectedValue = Session["storevalue"].ToString();
                        SupplierChoice.SelectedValue = Session["suppliervalue"].ToString();
                        ViewState["page"] = Session["pagevalue"].ToString();
                        ViewState["sort"] = Session["sortorder"].ToString();
                        ViewState["column"] = Session["columnname"].ToString();
                        ViewState["search"] = Session["searchquery"].ToString();
                        Session["redirect"] = null;
                        Session["origin"] = null;
                        Session["pagevalue"] = null;
                        Session["sortorder"] = null;
                        Session["columnname"] = null;
                        Session["searchquery"] = null;
                    }
                    else
                    {
                        ViewState["page"] = 0;
                        ViewState["sort"] = "ASC";
                        ViewState["column"] = "no";
                        ViewState["search"] = "";
                        Session["redirect"] = null;
                        Session["origin"] = null;
                        Session["pagevalue"] = null;
                        Session["sortorder"] = null;
                        Session["columnname"] = null;
                        Session["searchquery"] = null;
                    }
                }
                else
                {
                    ViewState["page"] = 0;
                    ViewState["sort"] = "ASC";
                    ViewState["column"] = "no";
                    ViewState["search"] = "";
                    Session["redirect"] = null;
                    Session["origin"] = null;
                    Session["pagevalue"] = null;
                    Session["sortorder"] = null;
                    Session["columnname"] = null;
                    Session["searchquery"] = null;
                }

                CreateItemGridView();
                CreateItemInsertGridView();
                CreateItemUpdateGridView();
                BindItemGridview();
                BindItemInsertGridview();
                BindItemUpdateGridview();

            }
            else
            {
                if (ViewState["items"] == null || ViewState["items"].Equals("-1") ||
                    ViewState["inserts"] == null || ViewState["inserts"].Equals("-1") ||
                    ViewState["updates"] == null || ViewState["updates"].Equals("-1"))
                {
                    Response.Redirect("Forbidden.aspx");
                }

                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
                System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
                
                if(items.Columns.Count <= 0 || inserts.Columns.Count <= 0 || updates.Columns.Count <= 0)
                {
                    Response.Redirect("Forbidden.aspx");
                }
                
                if (GetPostBackControlName() != "PageDropDownList")
                {
                    ItemGridView.DataSource = items;
                    ItemGridView.DataBind();
                }
                
                ItemInsertGridView.DataSource = inserts;
                ItemInsertGridView.DataBind();

                ItemUpdateGridView.DataSource = updates;
                ItemUpdateGridView.DataBind();

                AlertLabel.Text = "";
                AlertLabel.CssClass = "";
                AlertModalLabel.Text = "";
                AlertModalLabel.CssClass = "";
                AlertUploadLabel.Text = "";
                AlertUploadLabel.CssClass = "";

                
            }
        }

        protected void CreateItemGridView()
        {
            ItemGridView.Columns.Clear();

            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

            foreach (DataColumn col in items.Columns)
            {
                BoundField boundfield = new BoundField();
                boundfield.DataField = col.ColumnName;
                boundfield.SortExpression = col.ColumnName;
                boundfield.ReadOnly = true;

                if (col.ColumnName == "no")
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_category_code" && ((string[])Session["read"]).Contains("itemCategoryCode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Kode Kategori Barang";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_code" && ((string[])Session["read"]).Contains("itemCode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Kode Barang";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "supplier_product_code" && ((string[])Session["read"]).Contains("supplierProductCode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Kode Produk Pemasok";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_barcode" && ((string[])Session["read"]).Contains("itemBarcode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Barcode";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_name" && ((string[])Session["read"]).Contains("itemName"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Nama Barang";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "brand_name" && ((string[])Session["read"]).Contains("brandName"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Merek";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "short_description" && ((string[])Session["read"]).Contains("shortDescription"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Deskripsi";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "variants" && ((string[])Session["read"]).Contains("variants"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Varian";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "selling_price" && ((string[])Session["read"]).Contains("sellingPrice"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Harga Jual";
                    boundfield.DataFormatString = "{0:###,###,###,###,##0.00}";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "purchase_margin" && ((string[])Session["read"]).Contains("purchaseMargin"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Margin";
                    boundfield.DataFormatString = "{0:###,###,###,###,##0}";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "effective_date" && ((string[])Session["read"]).Contains("effectiveDate"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Tanggal Berlaku";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "uom" && ((string[])Session["read"]).Contains("unitOfMeasure"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Satuan Ukuran";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "store_name" && ((string[])Session["read"]).Contains("store"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Toko";
                    ItemGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "supplier_name" && ((string[])Session["read"]).Contains("supplier"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Pemasok";
                    ItemGridView.Columns.Add(boundfield);
                }
            }

            TemplateField templatefield = new TemplateField();
            templatefield.ItemStyle.CssClass = "text-nowrap";
            templatefield.HeaderText = "";
            ItemGridView.Columns.Add(templatefield);
        }

        protected void CreateItemInsertGridView()
        {
            ItemInsertGridView.Columns.Clear();

            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

            foreach (DataColumn col in inserts.Columns)
            {
                BoundField boundfield = new BoundField();
                boundfield.DataField = col.ColumnName;
                boundfield.SortExpression = col.ColumnName;
                boundfield.ReadOnly = true;

                if (col.ColumnName == "no")
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_category_code" && ((string[])Session["add"]).Contains("itemCategoryCode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Kode Kategori Barang";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_code" && ((string[])Session["add"]).Contains("itemCode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Kode Barang";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "supplier_product_code" && ((string[])Session["add"]).Contains("supplierProductCode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Kode Produk Pemasok";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_barcode" && ((string[])Session["add"]).Contains("itemBarcode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Barcode";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_name" && ((string[])Session["add"]).Contains("itemName"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Nama Barang";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "brand_name" && ((string[])Session["add"]).Contains("brandName"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Merek";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "short_description" && ((string[])Session["add"]).Contains("shortDescription"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Deskripsi";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                //if (col.ColumnName == "variants")
                //{
                //    boundfield.ItemStyle.CssClass = "";
                //    boundfield.HeaderText = "Varian";
                //    ItemInsertGridView.Columns.Add(boundfield);
                //}

                if (((string[])Session["add"]).Contains("variants"))
                {
                    if (col.ColumnName == "color")
                    {
                        boundfield.ItemStyle.CssClass = "";
                        boundfield.HeaderText = "Varian Warna";
                        ItemInsertGridView.Columns.Add(boundfield);
                    }

                    if (col.ColumnName == "size")
                    {
                        boundfield.ItemStyle.CssClass = "";
                        boundfield.HeaderText = "Varian Ukuran";
                        ItemInsertGridView.Columns.Add(boundfield);
                    }
                }

                if (col.ColumnName == "selling_price" && ((string[])Session["add"]).Contains("sellingPrice"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Harga Jual";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "purchase_margin" && ((string[])Session["add"]).Contains("purchaseMargin"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Margin";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "effective_date" && ((string[])Session["add"]).Contains("effectiveDate"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Tanggal Berlaku";
                    ItemInsertGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "uom" && ((string[])Session["add"]).Contains("unitOfMeasure"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Satuan Ukuran";
                    ItemInsertGridView.Columns.Add(boundfield);
                }
            }

            TemplateField templatefield = new TemplateField();
            templatefield.ItemStyle.CssClass = "text-nowrap";
            templatefield.HeaderText = "";
            ItemInsertGridView.Columns.Add(templatefield);
        }

        protected void CreateItemUpdateGridView()
        {
            ItemUpdateGridView.Columns.Clear();

            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

            foreach (DataColumn col in updates.Columns)
            {
                BoundField boundfield = new BoundField();
                boundfield.DataField = col.ColumnName;
                boundfield.SortExpression = col.ColumnName;
                boundfield.ReadOnly = true;

                if (col.ColumnName == "no")
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_category_code" && ((string[])Session["edit"]).Contains("itemCategoryCode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Kode Kategori Barang";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_code" && ((string[])Session["edit"]).Contains("itemCode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Kode Barang";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "supplier_product_code" && ((string[])Session["edit"]).Contains("supplierProductCode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Kode Produk Pemasok";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_barcode" && ((string[])Session["edit"]).Contains("itemBarcode"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Barcode";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "item_name" && ((string[])Session["edit"]).Contains("itemName"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Nama Barang";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "brand_name" && ((string[])Session["edit"]).Contains("brandName"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Merek";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "short_description" && ((string[])Session["edit"]).Contains("shortDescription"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Deskripsi";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                //if (col.ColumnName == "variants")
                //{
                //    boundfield.ItemStyle.CssClass = "";
                //    boundfield.HeaderText = "Varian";
                //    ItemUpdateGridView.Columns.Add(boundfield);
                //}

                if (((string[])Session["edit"]).Contains("variants"))
                {
                    if (col.ColumnName == "color")
                    {
                        boundfield.ItemStyle.CssClass = "";
                        boundfield.HeaderText = "Varian Warna";
                        ItemUpdateGridView.Columns.Add(boundfield);
                    }

                    if (col.ColumnName == "size")
                    {
                        boundfield.ItemStyle.CssClass = "";
                        boundfield.HeaderText = "Varian Ukuran";
                        ItemUpdateGridView.Columns.Add(boundfield);
                    }
                }


                if (col.ColumnName == "selling_price" && ((string[])Session["edit"]).Contains("sellingPrice"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Harga Jual";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "purchase_margin" && ((string[])Session["edit"]).Contains("purchaseMargin"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Margin";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "effective_date" && ((string[])Session["edit"]).Contains("effectiveDate"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Tanggal Berlaku";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "uom" && ((string[])Session["edit"]).Contains("unitOfMeasure"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Satuan Ukuran";
                    ItemUpdateGridView.Columns.Add(boundfield);
                }
            }

            TemplateField templatefield = new TemplateField();
            templatefield.ItemStyle.CssClass = "text-nowrap";
            templatefield.HeaderText = "";
            ItemUpdateGridView.Columns.Add(templatefield);
        }

        protected void ItemGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;

                LinkButton linkButton = new LinkButton();
                linkButton.Text = "<i class='bi bi-pencil-fill'></i>";
       
                linkButton.CssClass = "btn btn-primary align-middle ms-2";
                linkButton.CommandName = "edit";
                linkButton.CommandArgument = row["item_id"].ToString() + ";" + row["uuid"].ToString(); ;
                linkButton.Command += new CommandEventHandler(EditButton_Click);
                e.Row.Cells[ItemGridView.Columns.Count - 1].Controls.Add(linkButton);

                linkButton = new LinkButton();
                linkButton.Text = "<i class='bi bi-x-lg'></i>";
                linkButton.CssClass = "btn btn-danger align-middle ms-2";
                linkButton.CommandName = "delete";
                linkButton.CommandArgument = row["item_id"].ToString() + ";" + row["uuid"].ToString() + ";" + row["item_barcode"].ToString() + ";" + row["item_name"].ToString();
                linkButton.Command += new CommandEventHandler(DeleteButton_Click);
                e.Row.Cells[ItemGridView.Columns.Count - 1].Controls.Add(linkButton);
            }

            if (e.Row.RowType == DataControlRowType.Pager)
            {
                DropDownList PageDropDownList = (DropDownList)e.Row.FindControl("PageDropDownList");
                System.Web.UI.WebControls.LinkButton FirstButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("Firstbutton");
                System.Web.UI.WebControls.LinkButton PreviousButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("PreviousButton");
                System.Web.UI.WebControls.LinkButton NextButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("NextButton");
                System.Web.UI.WebControls.LinkButton LastButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("LastButton");
                PageDropDownList.Items.Clear();
                
                int page = 0;

                for (int i = 0; i < ItemGridView.PageCount; i++)
                {
                    page = i + 1;
                    PageDropDownList.Items.Add(page.ToString());
                }
                page = int.Parse(ViewState["page"].ToString()) + 1;
                if(page == 1)
                {
                    FirstButton.CssClass += " " + "disabled";
                    PreviousButton.CssClass += " " + "disabled";
                    NextButton.CssClass = "page-link";
                    LastButton.CssClass = "page-link";
                }
                else if(page == ItemGridView.PageCount)
                {
                    FirstButton.CssClass = "page-link";
                    PreviousButton.CssClass = "page-link";
                    NextButton.CssClass += " " + "disabled";
                    LastButton.CssClass += " " + "disabled";
                }
                else
                {
                    FirstButton.CssClass = "page-link";
                    PreviousButton.CssClass = "page-link";
                    NextButton.CssClass = "page-link";
                    LastButton.CssClass = "page-link";
                }
                PageDropDownList.SelectedValue = page.ToString();
            }
        }

        protected void ItemGridView_RowCreated(object sender, GridViewRowEventArgs e)
        {
            
        }

        protected void PageDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int page = int.Parse(((DropDownList)sender).SelectedValue);

            if(page <= 0)
            {
                ViewState["page"] = 0;
                ItemGridView.PageIndex = 0;
            }
            else
            {
                ViewState["page"] = page - 1;
                ItemGridView.PageIndex = page - 1;
            }

            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
            //items.DefaultView.Sort = ViewState["column"].ToString() + " " + ViewState["sort"].ToString();
            //ViewState["items"] = items;
            ItemGridView.DataSource = items;
            ItemGridView.DataBind();
        }

        protected void ItemInsertGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;

                LinkButton linkButton = new LinkButton();
                linkButton.Text = "<i class='bi bi-pencil-fill'></i>";

                linkButton.CssClass = "btn btn-primary align-middle ms-2";
                linkButton.CommandName = "editgvinsert";
                linkButton.CommandArgument = row["item_barcode"].ToString();
                linkButton.Command += new CommandEventHandler(EditUploadButton_Click);
                e.Row.Cells[ItemInsertGridView.Columns.Count - 1].Controls.Add(linkButton);

                linkButton = new LinkButton();
                linkButton.Text = "<i class='bi bi-x-lg'></i>";
                linkButton.CssClass = "btn btn-danger align-middle ms-2";
                linkButton.CommandName = "deletegvinsert";
                linkButton.CommandArgument = row["item_barcode"].ToString() + ";" + row["item_name"].ToString();
                linkButton.Command += new CommandEventHandler(DeleteUploadButton_Click);
                e.Row.Cells[ItemInsertGridView.Columns.Count - 1].Controls.Add(linkButton);
            }
        }

        protected void ItemUpdateGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;

                LinkButton linkButton = new LinkButton();
                linkButton.Text = "<i class='bi bi-pencil-fill'></i>";

                linkButton.CssClass = "btn btn-primary align-middle ms-2";
                linkButton.CommandName = "editgvupdate";
                linkButton.CommandArgument = row["item_barcode"].ToString();
                linkButton.Command += new CommandEventHandler(EditUploadButton_Click);
                e.Row.Cells[ItemUpdateGridView.Columns.Count - 1].Controls.Add(linkButton);

                linkButton = new LinkButton();
                linkButton.Text = "<i class='bi bi-x-lg'></i>";
                linkButton.CssClass = "btn btn-danger align-middle ms-2";
                linkButton.CommandName = "deletegvupdate";
                linkButton.CommandArgument = row["item_barcode"].ToString() + ";" + row["item_name"].ToString();
                linkButton.Command += new CommandEventHandler(DeleteUploadButton_Click);
                e.Row.Cells[ItemUpdateGridView.Columns.Count - 1].Controls.Add(linkButton);
            }
        }

        protected void BindItemInsertGridview()
        {
            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

            if (inserts.Rows.Count > 0)
            {
                ItemInsertGridView.DataSource = inserts;
                ItemInsertGridView.DataBind();
            }
        }

        protected void BindItemUpdateGridview()
        {
            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

            if (updates.Rows.Count > 0)
            {
                ItemUpdateGridView.DataSource = updates;
                ItemUpdateGridView.DataBind();
            }
        }

        protected void BindItemGridview()
        {
            if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))) &&
                (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))))
            {
                SearchInput.Text = ViewState["search"].ToString();

                string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                SqlConnection conn = new SqlConnection(constr);
                SqlCommand cmd = conn.CreateCommand();

                string query = "";
                string where = "";
                bool withComma = false;

                if (((string[])Session["read"]).Contains("store"))
                {
                    if(withComma )
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "stores.store_name");
                }

                if (((string[])Session["read"]).Contains("supplier"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "suppliers.supplier_name");
                }

                if (((string[])Session["read"]).Contains("itemBarcode"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.item_barcode");
                }

                if (((string[])Session["read"]).Contains("itemCode"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.item_code");
                }

                if (((string[])Session["read"]).Contains("itemCategoryCode"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.item_category_code");
                }

                if (((string[])Session["read"]).Contains("itemName"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.item_name");
                }

                if (((string[])Session["read"]).Contains("shortDescription"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.short_description");
                }

                if (((string[])Session["read"]).Contains("variants"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", @"dbo.JsonValue(items.variants, 'color') [color],
                    dbo.JsonValue(items.variants, 'size') [size],
                    IIF(dbo.JsonValue(items.variants, 'color') IS NOT NULL AND 
                    dbo.JsonValue(items.variants, 'size') IS NOT NULL, 
                    CONCAT('Warna:', ' ', dbo.JsonValue(items.variants, 'color'), ',', 
                    ' ', 'Ukuran:', ' ', dbo.JsonValue(items.variants, 'size')), 
                    IIF(dbo.JsonValue(items.variants, 'color') IS NOT NULL, 
                    CONCAT('Warna:', ' ', dbo.JsonValue(items.variants, 'color')),
                    IIF(dbo.JsonValue(items.variants, 'color') IS NOT NULL, 
                    CONCAT('Ukuran:', ' ', dbo.JsonValue(items.variants, 'size')), ''))) [variants]");
                }

                if (((string[])Session["read"]).Contains("sellingPrice"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.selling_price");
                }

                if (((string[])Session["read"]).Contains("purchaseMargin"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.purchase_margin");
                }

                if (((string[])Session["read"]).Contains("effectiveDate"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "FORMAT(items.effective_date, 'dd MMM yyyy') [effective_date]");
                }

                if (((string[])Session["read"]).Contains("brandName"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.brand_name");
                }

                if (((string[])Session["read"]).Contains("unitOfMeasure"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.uom");
                }

                if (((string[])Session["read"]).Contains("supplierProductCode"))
                {
                    if (withComma)
                    {
                        query = String.Concat(query, ",");
                    }
                    else
                    {
                        withComma = true;
                    }
                    query = String.Concat(query, " ", "items.supplier_product_code");
                }

                if (!String.IsNullOrEmpty(ViewState["search"].ToString()))
                {
                    bool withOR = false;

                    if (((string[])Session["read"]).Contains("itemCode"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "item_code LIKE @itemCode");
                    }

                    if (((string[])Session["read"]).Contains("itemBarcode"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "item_barcode LIKE @itemBarcode");
                    }

                    if (((string[])Session["read"]).Contains("itemName"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "item_name LIKE @itemName");
                    }

                    if (((string[])Session["read"]).Contains("itemCategoryCode"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "item_category_code LIKE @itemCategoryCode");
                    }

                    if (((string[])Session["read"]).Contains("brandName"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "brand_name LIKE @brandName");
                    }

                    if (((string[])Session["read"]).Contains("shortDescription"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "short_description LIKE @shortDescription");
                    }

                    if (((string[])Session["read"]).Contains("variants"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "variants LIKE @variants");
                    }

                    if (((string[])Session["read"]).Contains("purchaseMargin"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "purchase_margin LIKE @purchaseMargin");
                    }

                    if (((string[])Session["read"]).Contains("sellingPrice"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "selling_price LIKE @sellingPrice");
                    }

                    if (((string[])Session["read"]).Contains("effectiveDate"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "effective_date LIKE @effectiveDate");
                    }

                    if (((string[])Session["read"]).Contains("unitOfMeasure"))
                    {
                        if (withOR)
                        {
                            where = string.Concat(where, " ", "OR");
                        }
                        else
                        {
                            withOR = true;
                        }

                        where = string.Concat(where, " ", "uom LIKE @unitOfMeasure");
                    }

                }
                else
                {
                    where = "1=1";
                }

                string commandText = @"SELECT ROW_NUMBER() OVER(ORDER BY item_id) [no], items.item_id, items.uuid, {0}
                    FROM items INNER JOIN stores ON items.store_id = stores.store_id
                    INNER JOIN suppliers ON items.supplier_id = suppliers.supplier_id
                    WHERE items.store_id = @storeId AND items.supplier_id = @supplierId AND ({1})";
                commandText = String.Format(commandText, query, where);

                cmd.CommandText = commandText;
                cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                
                if (!String.IsNullOrEmpty(ViewState["search"].ToString()))
                {
                    if (((string[])Session["read"]).Contains("itemBarcode"))
                    {
                        cmd.Parameters.AddWithValue("@itemBarcode", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("itemCode"))
                    {
                        cmd.Parameters.AddWithValue("@itemCode", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("itemCategoryCode"))
                    {
                        cmd.Parameters.AddWithValue("@itemCategoryCode", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("supplierProductCode"))
                    {
                        cmd.Parameters.AddWithValue("@supplierProductCode", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("itemName"))
                    {
                        cmd.Parameters.AddWithValue("@itemName", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("brandName"))
                    {
                        cmd.Parameters.AddWithValue("@brandName", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("shortDescription"))
                    {
                        cmd.Parameters.AddWithValue("@shortDescription", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("variants"))
                    {
                        cmd.Parameters.AddWithValue("@variants", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("sellingPrice"))
                    {
                        cmd.Parameters.AddWithValue("@sellingPrice", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("purchaseMargin"))
                    {
                        cmd.Parameters.AddWithValue("@purchaseMargin", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("effectiveDate"))
                    {
                        cmd.Parameters.AddWithValue("@effectiveDate", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }

                    if (((string[])Session["read"]).Contains("unitOfMeasure"))
                    {
                        cmd.Parameters.AddWithValue("@unitOfMeasure", String.Format("%{0}%", SearchInput.Text?.Trim()));
                    }
                    //AlertLabel.Text = commandText + sqlValue;
                }
                
                SqlDataAdapter sda = new SqlDataAdapter(cmd);
                System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
                items.Clear();
                sda.Fill(items);

                items.DefaultView.Sort = ViewState["column"].ToString() + " " + ViewState["sort"].ToString();
                ItemGridView.PageIndex = int.Parse(ViewState["page"].ToString());
                ItemGridView.DataSource = items;
                ItemGridView.DataBind();
                
            }
        }

        protected void FirstButton_Click(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ItemGridView.PageIndex = 0;

            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
            ItemGridView.DataSource = items;
            ItemGridView.DataBind();
        }

        protected void LastButton_Click(object sender, EventArgs e)
        {
            ViewState["page"] = ItemGridView.PageCount - 1;
            ItemGridView.PageIndex = ItemGridView.PageCount - 1;

            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
            ItemGridView.DataSource = items;
            ItemGridView.DataBind();
        }

        protected void NextButton_Click(object sender, EventArgs e)
        {
            int page = int.Parse(ViewState["page"].ToString());
            page++;

            if(page > ItemGridView.PageCount - 1)
            {
                page = ItemGridView.PageCount - 1;
            }
            ViewState["page"] = page;
            ItemGridView.PageIndex = page;

            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
            ItemGridView.DataSource = items;
            ItemGridView.DataBind();
        }

        protected void PreviousButton_Click(object sender, EventArgs e)
        {
            int page = int.Parse(ViewState["page"].ToString());
            page--;

            if (page < 0)
            {
                page = 0;
            }
            ViewState["page"] = page;
            ItemGridView.PageIndex = page;

            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];
            ItemGridView.DataSource = items;
            ItemGridView.DataBind();
        }

        protected void UploadButton_Click(object sender, EventArgs e)
        {
            if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(StoreUploadChoice.SelectedValue))) &&
                (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(SupplierUploadChoice.SelectedValue))) &&
                (bool)Session["upload"] == true)
            {
                if (UploadInput.HasFile)
                {
                    string tmp = "";
                    try
                    {
                        string[] store = StoreUploadChoice.SelectedItem.Text.Split('-');
                        string[] supplier = SupplierUploadChoice.SelectedItem.Text.Split('-');
                        List<string> errors = new List<string>();

                        int unixTimestamp = (int)(DateTime.Now.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

                        //int barcodeColumn = 0;
                        //int itemNameColumn = 0;
                        //int itemCodeColumn = 0;
                        //int itemCategoryCodeColumn = 0;
                        //int supplierProductCodeColumn = 0;
                        //int brandNameColumn = 0;
                        //int foreignNameColumn = 0;
                        //int shortDescriptionColumn = 0;
                        //int marginPurchaseColumn = 0;
                        //int effectiveDateColumn = 0;
                        //int sellingPriceColumn = 0;
                        //int sellingPriceEffectiveDateColumn = 0;
                        //int colorColumn = 0;
                        //int sizeColumn = 0;
                        //int unitColumn = 0;
                        //int headerRow = 0;
                        int barcodeColumn = -1;
                        int itemNameColumn = -1;
                        int itemCodeColumn = -1;
                        int itemCategoryCodeColumn = -1;
                        int supplierProductCodeColumn = -1;
                        int brandNameColumn = -1;
                        int foreignNameColumn = -1;
                        int shortDescriptionColumn = -1;
                        int marginPurchaseColumn = -1;
                        int effectiveDateColumn = -1;
                        int sellingPriceColumn = -1;
                        int sellingPriceEffectiveDateColumn = -1;
                        int colorColumn = -1;
                        int sizeColumn = -1;
                        int unitColumn = -1;
                        int headerRow = -1;
                        string[] barcodeHeader = { "barcode", "plu", "barcodebarang", "barcodeitem", "itembarcode" };
                        string[] itemNameHeader = { "itemname", "namabarang", "namaitem", "nama", "name" };
                        string[] itemCodeHeader = { "itemcode", "kodebarang", "kodeitem", "articlecode", "kodearticle", "kodeartikel" };
                        string[] supplierProductCodeHeader = { "suppliercode", "kodeproduk", "supplierproductcode" };
                        string[] itemCategoryCodeHeader = { "burui", "mc", "mccode", "merchantcode", "merchant", "merchantcategorycode", "itemcategorycode", "categorycode" };
                        string[] brandNameHeader = { "brandname", "namabrand", "namamerek", "merek", "brand" };
                        string[] foreignNameHeader = { "foreignname", "frgnname" };
                        string[] descriptionHeader = { "shortdesc", "shortdescription", "desc", "description", "itemdescription", "shortdescription" };
                        string[] marginPurchaseHeader = { "marginpurch", "marginpurchase", "marginpurchase", "margin" };
                        string[] efectiveDateHeader = { "marginpurchvalid", "marginpurchasevalid", "marginpurchasevalid", "marginpurchvalid", "marginpurchasevalid", "marginpurchasevalid" };
                        string[] sellingPriceHeader = { "sellprice", "sellingprice", "hargajual" };
                        string[] validHeader = { "sellpricevalid", "sellingpricevalid", "sellvalid", "sellpricevalid", "sellingpricevalid", "sellvalid" };
                        string[] colorHeader = { "color", "colour", "warna", "warnaitem", "coloritem", "itemcolor", "warnabarang", "colouritem", "itemcolour" };
                        string[] sizeHeader = { "size", "ukuran", "ukuranbarang", "ukuranitem", "itemsize", "sizebarang", "sizeitem" };
                        string[] unitHeader = { "unit", "uom", "unitmeasure", "unitofmeasure", "satuan", "satuanbarang", "satuanunitaset" };

                        string extension = Path.GetExtension(UploadInput.PostedFile.FileName);

                        if (extension == ".xls" || extension == ".xlsx")
                        {
                            string excelPath = MapPath("~/uploads/barang-" + store[0].Trim() + "-" + supplier[0].Trim() + "-" + unixTimestamp + extension);
                            
                            UploadInput.SaveAs(excelPath);

                            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
                            inserts.Rows.Clear();

                            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
                            updates.Rows.Clear();

                            int noinsert = 1;
                            int noupdate = 1;
                            
                            string connectionstring = "";

                            if (extension == ".xls")
                            {
                                connectionstring = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + excelPath + "';Extended Properties=\"Excel 8.0;HDR=NO;IMEX=1;\"";
                            }
                            else if(extension == ".xlsx")
                            {
                                connectionstring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + excelPath + "';Extended Properties=\"Excel 12.0;HDR=NO;IMEX=1;\"";
                            }

                            using (OleDbConnection oleconn = new OleDbConnection(connectionstring))
                            {
                                OleDbCommand olecommand = new OleDbCommand();
                                olecommand.Connection = oleconn;
                                oleconn.Open();

                                System.Data.DataTable workbook = oleconn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                                
                                foreach (DataRow table in workbook.Rows)
                                {
                                    string sheetName = table["TABLE_NAME"]?.ToString().Replace("'", "");
                                    
                                    if (!sheetName.EndsWith("$"))
                                    {
                                        continue;
                                    }

                                    olecommand.CommandText = "SELECT * FROM [" + sheetName + "]";

                                    System.Data.DataTable worksheet = new System.Data.DataTable("worksheet");
                                    
                                    OleDbDataAdapter adapter = new OleDbDataAdapter(olecommand);
                                    adapter.Fill(worksheet);

                                    int emptyColumns = 0;
                                    int emptyRows = 0;
                                    
                                    for (int i = 0; i < worksheet.Rows.Count; i++)
                                    {
                                        if (emptyColumns > 100)
                                        {
                                            break;
                                        }

                                        emptyColumns++;

                                        for (int j = 0; j < worksheet.Columns.Count; j++)
                                        {
                                            if (String.IsNullOrEmpty(worksheet.Rows[i][j].ToString()))
                                            {
                                                if (emptyRows > 100)
                                                {
                                                    break;
                                                }
                                                emptyRows++;
                                                continue;
                                            }
                                            else
                                            {
                                                emptyRows = 0;
                                                emptyColumns = 0;
                                            }

                                            string str = worksheet.Rows[i][j].ToString();
                                            string value = string.Concat(str.Where(Char.IsLetter));
                                            

                                            if (((IList)barcodeHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "barcodeColumn=>" + barcodeColumn.ToString() + value;
                                                barcodeColumn = j;
                                            }

                                            if (((IList)itemNameHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "itemNameColumn=>" + itemNameColumn.ToString() + value;
                                                itemNameColumn = j;
                                            }

                                            if (((IList)itemCodeHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "itemCodeColumn=>" + itemCodeColumn.ToString() + value;
                                                itemCodeColumn = j;
                                            }

                                            if (((IList)itemCategoryCodeHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "itemCategoryCodeColumn=>" + itemCategoryCodeColumn.ToString() + value;
                                                itemCategoryCodeColumn = j;
                                            }

                                            if (((IList)brandNameHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "brandNameColumn=>" + brandNameColumn.ToString() + value;
                                                brandNameColumn = j;
                                            }

                                            if (((IList)foreignNameHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "foreignNameColumn=>" + foreignNameColumn.ToString() + value;
                                                foreignNameColumn = j;
                                            }

                                            if (((IList)descriptionHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "shortDescriptionColumn=>" + shortDescriptionColumn.ToString() + value;
                                                shortDescriptionColumn = j;
                                            }

                                            if (((IList)marginPurchaseHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "marginPurchaseColumn=>" + marginPurchaseColumn.ToString() + value;
                                                marginPurchaseColumn = j;
                                            }

                                            if (((IList)efectiveDateHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "effectiveDateColumn=>" + effectiveDateColumn.ToString() + value;
                                                effectiveDateColumn = j;
                                            }

                                            if (((IList)sellingPriceHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "sellingPriceColumn=>" + sellingPriceColumn.ToString() + value;
                                                sellingPriceColumn = j;
                                            }

                                            if (((IList)validHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "sellingPriceEffectiveDateColumn=>" + sellingPriceEffectiveDateColumn.ToString() + value;
                                                sellingPriceEffectiveDateColumn = j;
                                            }

                                            if (((IList)colorHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "colorColumn=>" + colorColumn.ToString() + value;
                                                colorColumn = j;
                                            }

                                            if (((IList)sizeHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "sizeColumn=>" + sizeColumn.ToString() + value;
                                                sizeColumn = j;
                                            }

                                            if (((IList)supplierProductCodeHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "supplierProductCodeColumn=>" + supplierProductCodeColumn.ToString() + value;
                                                supplierProductCodeColumn = j;
                                            }

                                            if (((IList)unitHeader).Contains(value.ToLower()))
                                            {
                                                //tmp = tmp + " " + "unitColumn=>" + unitColumn.ToString() + value;
                                                unitColumn = j;
                                            }
                                        }

                                        if (barcodeColumn > -1)
                                        {
                                            headerRow = i;
                                            break;
                                        }
                                    }

                                    if(headerRow > -1)
                                    {
                                        for (int i = 0; i < worksheet.Rows.Count; i++)
                                        {
                                            if (emptyColumns > 100)
                                            {
                                                break;
                                            }

                                            if (i > headerRow)
                                            {
                                                string barcode = worksheet.Rows[i][barcodeColumn].ToString();

                                                if (barcode.Length > 0 && barcode.Substring(0, 1) == "'")
                                                {
                                                    barcode = barcode.Remove(0, 1);
                                                }

                                                if (String.IsNullOrEmpty(barcode))
                                                {
                                                    emptyColumns++;
                                                    continue;
                                                }
                                                else
                                                {
                                                    emptyColumns = 0;
                                                }

                                                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                                                SqlCommand command = conn.CreateCommand();
                                                command.CommandText = @"SELECT item_id FROM items WHERE store_id = @storeId AND supplier_id = @supplierId AND 
                                                                item_barcode = @barcode";
                                                command.Parameters.AddWithValue("@storeId", int.Parse(StoreUploadChoice.SelectedValue));
                                                command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierUploadChoice.SelectedValue));
                                                command.Parameters.AddWithValue("@barcode", barcode);

                                                int itemId = 0;

                                                conn.Open();
                                                object result = command.ExecuteScalar();
                                                conn.Close();

                                                if (result != null)
                                                {
                                                    itemId = int.Parse(result.ToString());
                                                }

                                                if (itemId > 0)
                                                {
                                                    AlertUploadLabel.Text += "aaaaaaaa"+ barcode;
                                                    DataRow update = updates.NewRow();
                                                    update["item_id"] = itemId;
                                                    update["no"] = noupdate;

                                                    if(String.IsNullOrEmpty(StoreUploadChoice.SelectedValue))
                                                    {
                                                        errors.Add("Pilihan toko tidak boleh kosong");
                                                    }
                                                    else
                                                    {
                                                        update["store_id"] = int.Parse(StoreUploadChoice.SelectedValue);
                                                    }

                                                    if (String.IsNullOrEmpty(SupplierUploadChoice.SelectedValue))
                                                    {
                                                        errors.Add("Pilihan pemasok tidak boleh kosong");
                                                    }
                                                    else
                                                    {
                                                        update["supplier_id"] = int.Parse(SupplierUploadChoice.SelectedValue);
                                                    }
                                                    

                                                    if (((string[])Session["edit"]).Contains("itemBarcode"))
                                                    {
                                                        update["item_barcode"] = barcode;
                                                    }

                                                    if (((string[])Session["edit"]).Contains("itemCode"))
                                                    {
                                                        if (itemCodeColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][itemCodeColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                update["item_code"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                update["item_code"] = value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            update["item_code"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["edit"]).Contains("itemCategoryCode"))
                                                    {
                                                        if (itemCategoryCodeColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][itemCategoryCodeColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                update["item_category_code"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                update["item_category_code"] = value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            update["item_category_code"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["edit"]).Contains("itemName"))
                                                    {
                                                        if (itemNameColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][itemNameColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                errors.Add("Barcode " + barcode + ": nama barang tidak boleh kosong");

                                                                update["item_name"] = DBNull.Value;
                                                                update["foreign_name"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                update["item_name"] = value;
                                                                update["foreign_name"] = value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            errors.Add("Barcode " + barcode + ": nama barang tidak boleh kosong");
                                                            update["item_name"] = DBNull.Value;
                                                            update["foreign_name"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["edit"]).Contains("shortDescription"))
                                                    {
                                                        if (shortDescriptionColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][shortDescriptionColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                errors.Add("Barcode " + barcode + ": deskripsi barang tidak boleh kosong");
                                                                update["short_description"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                update["short_description"] = value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            errors.Add("Barcode " + barcode + ": deskripsi barang tidak boleh kosong");
                                                            update["short_description"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["edit"]).Contains("variants"))
                                                    {
                                                        string size = "", color = "";

                                                        if (sizeColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][sizeColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (value.Length <= 1)
                                                            {
                                                                value = "";
                                                            }

                                                            size = value;
                                                        }

                                                        if (colorColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][colorColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (value.Length <= 1)
                                                            {
                                                                value = "";
                                                            }

                                                            color = value;
                                                        }

                                                        if (String.IsNullOrEmpty(size) && String.IsNullOrEmpty(color))
                                                        {
                                                            update["variants"] = "{}";
                                                        }
                                                        else if (!String.IsNullOrEmpty(size) && String.IsNullOrEmpty(color))
                                                        {
                                                            update["size"] = size;
                                                            Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                                            variants["size"] = size;
                                                            update["variants"] = variants.ToString();
                                                        }
                                                        else if (String.IsNullOrEmpty(size) && !String.IsNullOrEmpty(color))
                                                        {
                                                            update["color"] = color;
                                                            Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                                            variants["color"] = color;
                                                            update["variants"] = variants.ToString();
                                                        }
                                                        else if (!String.IsNullOrEmpty(size) && !String.IsNullOrEmpty(color))
                                                        {
                                                            Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                                            variants["color"] = color;
                                                            variants["size"] = size;
                                                            update["variants"] = variants.ToString();
                                                        }
                                                    }

                                                    if (((string[])Session["edit"]).Contains("sellingPrice"))
                                                    {
                                                        if (sellingPriceColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][sellingPriceColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            double price;
                                                            bool isNumeric = double.TryParse(value, out price);

                                                            if (isNumeric)
                                                            {
                                                                update["selling_price"] = price;
                                                            }
                                                            else
                                                            {
                                                                errors.Add("Barcode " + barcode + ": harga barang tidak boleh kosong");
                                                                update["selling_price"] = DBNull.Value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            errors.Add("Barcode " + barcode + ": harga barang tidak boleh kosong");
                                                            update["selling_price"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["edit"]).Contains("purchaseMargin"))
                                                    {
                                                        if (marginPurchaseColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][marginPurchaseColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            double margin;
                                                            bool isNumeric = double.TryParse(value, out margin);

                                                            if (isNumeric)
                                                            {
                                                                update["purchase_margin"] = margin;
                                                            }
                                                            else
                                                            {
                                                                update["purchase_margin"] = 0;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            update["purchase_margin"] = 0;
                                                        }
                                                    }

                                                    if (((string[])Session["edit"]).Contains("effectiveDate"))
                                                    {
                                                        if (effectiveDateColumn > 0)
                                                        {
                                                            if (!string.IsNullOrEmpty(worksheet.Rows[i][effectiveDateColumn].ToString()))
                                                            {
                                                                update["effective_date"] = Convert.ToDateTime(worksheet.Rows[i][effectiveDateColumn]).ToString("MM/dd/yyyy");
                                                            }
                                                            else
                                                            {
                                                                update["effective_date"] = DBNull.Value;
                                                            }

                                                            //if (worksheet.Rows[i][effectiveDateColumn].GetType() == typeof(DateTime))
                                                            //{
                                                            //    update["effective_date"] = Convert.ToDateTime(worksheet.Rows[i][effectiveDateColumn]).ToString("MM/dd/yyyy");
                                                            //}
                                                            //else
                                                            //{
                                                            //    update["effective_date"] = DBNull.Value;
                                                            //}

                                                        }
                                                        else
                                                        {
                                                            update["effective_date"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["edit"]).Contains("brandName"))
                                                    {
                                                        if (brandNameColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][brandNameColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                update["brand_name"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                update["brand_name"] = value;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            update["brand_name"] = DBNull.Value;
                                                        }
                                                    }

                                                    //update["modified_date"] = Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                                    //update["modified_by"] = Session["username"];

                                                    if (((string[])Session["edit"]).Contains("unitOfMeasure"))
                                                    {
                                                        if (unitColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][unitColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                update["uom"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                update["uom"] = value;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            update["uom"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["edit"]).Contains("supplierProductCode"))
                                                    {
                                                        if (supplierProductCodeColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][supplierProductCodeColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                update["supplier_product_code"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                update["supplier_product_code"] = value;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            update["supplier_product_code"] = DBNull.Value;
                                                        }
                                                    }

                                                    if(errors.Count <= 0)
                                                    {
                                                        updates.Rows.Add(update);
                                                        noupdate++;
                                                    }
                                                }
                                                else
                                                {
                                                    AlertUploadLabel.Text += barcode;
                                                    DataRow insert = inserts.NewRow();
                                                    insert["no"] = noinsert;

                                                    if (((string[])Session["add"]).Contains("store"))
                                                    {
                                                        if(String.IsNullOrEmpty(StoreUploadChoice.SelectedValue))
                                                        {
                                                            insert["store_id"] = DBNull.Value;
                                                        }
                                                        else
                                                        {
                                                            insert["store_id"] = int.Parse(StoreUploadChoice.SelectedValue);
                                                        }
                                                        
                                                    }

                                                    if (((string[])Session["add"]).Contains("supplier"))
                                                    {
                                                        if (String.IsNullOrEmpty(SupplierUploadChoice.SelectedValue))
                                                        {
                                                            insert["supplier_id"] = DBNull.Value;
                                                        }
                                                        else
                                                        {
                                                            insert["supplier_id"] = int.Parse(SupplierUploadChoice.SelectedValue);
                                                        }
                                                        
                                                    }

                                                    if (((string[])Session["add"]).Contains("itemBarcode"))
                                                    {
                                                        insert["item_barcode"] = barcode;
                                                    }

                                                    if (((string[])Session["add"]).Contains("itemCode"))
                                                    {
                                                        if (itemCodeColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][itemCodeColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                insert["item_code"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                insert["item_code"] = value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            insert["item_code"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["add"]).Contains("itemCategoryCode"))
                                                    {
                                                        if (itemCategoryCodeColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][itemCategoryCodeColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                insert["item_category_code"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                insert["item_category_code"] = value;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            insert["item_category_code"] = DBNull.Value;
                                                        }
                                                    }


                                                    if (((string[])Session["add"]).Contains("itemName"))
                                                    {
                                                        if (itemNameColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][itemNameColumn].ToString()?.Trim();

                                                            if (value.Length > 0 && value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                errors.Add("Barcode " + barcode + ": nama barang tidak boleh kosong");
                                                                insert["item_name"] = DBNull.Value;
                                                                insert["foreign_name"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                insert["item_name"] = value;
                                                                insert["foreign_name"] = value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            errors.Add("Barcode " + barcode + ": nama barang tidak boleh kosong");
                                                            insert["item_name"] = DBNull.Value;
                                                            insert["foreign_name"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["add"]).Contains("shortDescription"))
                                                    {
                                                        if (shortDescriptionColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][shortDescriptionColumn].ToString()?.Trim();

                                                            if (value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                errors.Add("Barcode " + barcode + ": deskripsi barang tidak boleh kosong");
                                                                insert["short_description"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                insert["short_description"] = value;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            errors.Add("Barcode " + barcode + ": deskripsi barang tidak boleh kosong");
                                                            insert["short_description"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["add"]).Contains("variants"))
                                                    {
                                                        string size = "", color = "";

                                                        if (sizeColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][sizeColumn].ToString()?.Trim();

                                                            if (value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (value.Length <= 1)
                                                            {
                                                                value = "";
                                                            }

                                                            size = value;

                                                        }

                                                        if (colorColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][colorColumn].ToString()?.Trim();

                                                            if (value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (value.Length <= 1)
                                                            {
                                                                value = "";
                                                            }

                                                            color = value;
                                                        }

                                                        if (String.IsNullOrEmpty(size) && String.IsNullOrEmpty(color))
                                                        {
                                                            insert["variants"] = "{}";
                                                        }
                                                        else if (!String.IsNullOrEmpty(size) && String.IsNullOrEmpty(color))
                                                        {
                                                            insert["size"] = size;
                                                            Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                                            variants["size"] = size;
                                                            insert["variants"] = variants.ToString();
                                                        }
                                                        else if (String.IsNullOrEmpty(size) && !String.IsNullOrEmpty(color))
                                                        {
                                                            insert["color"] = color;
                                                            Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                                            variants["color"] = color;
                                                            insert["variants"] = variants.ToString();
                                                        }
                                                        else if (!String.IsNullOrEmpty(size) && !String.IsNullOrEmpty(color))
                                                        {
                                                            Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                                            variants["color"] = color;
                                                            variants["size"] = size;
                                                            insert["variants"] = variants.ToString();
                                                        }
                                                    }

                                                    if (((string[])Session["add"]).Contains("sellingPrice"))
                                                    {
                                                        if (sellingPriceColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][sellingPriceColumn].ToString()?.Trim();

                                                            if (value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            double price;
                                                            bool isNumeric = double.TryParse(value, out price);

                                                            if (isNumeric)
                                                            {
                                                                insert["selling_price"] = price;
                                                            }
                                                            else
                                                            {
                                                                errors.Add("Barcode " + barcode + ": harga barang tidak boleh kosong");
                                                                insert["selling_price"] = DBNull.Value;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            errors.Add("Barcode " + barcode + ": harga barang tidak boleh kosong");
                                                            insert["selling_price"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["add"]).Contains("purchaseMargin"))
                                                    {
                                                        if (marginPurchaseColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][marginPurchaseColumn].ToString()?.Trim();

                                                            if (value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            double margin;
                                                            bool isNumeric = double.TryParse(value, out margin);

                                                            if (isNumeric)
                                                            {
                                                                insert["purchase_margin"] = margin;
                                                            }
                                                            else
                                                            {
                                                                insert["purchase_margin"] = 0;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            insert["purchase_margin"] = 0;
                                                        }
                                                    }

                                                    if (((string[])Session["add"]).Contains("effectiveDate"))
                                                    {
                                                        if (effectiveDateColumn > 0)
                                                        {
                                                            if (worksheet.Rows[i][effectiveDateColumn].GetType() == typeof(DateTime))
                                                            {
                                                                insert["effective_date"] = Convert.ToDateTime(worksheet.Rows[i][effectiveDateColumn]).ToString("MM/dd/yyyy");
                                                            }
                                                            else
                                                            {
                                                                insert["effective_date"] = DBNull.Value;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            insert["effective_date"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["add"]).Contains("brandName"))
                                                    {
                                                        if (brandNameColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][brandNameColumn].ToString()?.Trim();

                                                            if (value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                insert["brand_name"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                insert["brand_name"] = value;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            insert["brand_name"] = DBNull.Value;
                                                        }
                                                    }

                                                    //update["created_date"] = Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                                    //update["created_by"] = Session["username"];
                                                    //update["modified_date"] = Convert.ToDateTime(DateTime.Today).ToString("yyyy-MM-dd HH:mm:ss.fff");
                                                    //update["modified_by"] = Session["username"];

                                                    if (((string[])Session["add"]).Contains("unitOfMeasure"))
                                                    {
                                                        if (unitColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][unitColumn].ToString()?.Trim();

                                                            if (value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                insert["uom"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                insert["uom"] = value;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            insert["uom"] = DBNull.Value;
                                                        }
                                                    }

                                                    if (((string[])Session["add"]).Contains("supplierProductCode"))
                                                    {
                                                        if (supplierProductCodeColumn > 0)
                                                        {
                                                            string value = worksheet.Rows[i][supplierProductCodeColumn].ToString()?.Trim();

                                                            if (value.Substring(0, 1) == "'")
                                                            {
                                                                value = value.Remove(0, 1);
                                                            }

                                                            if (String.IsNullOrEmpty(value))
                                                            {
                                                                insert["supplier_product_code"] = DBNull.Value;
                                                            }
                                                            else
                                                            {
                                                                insert["supplier_product_code"] = value;
                                                            }

                                                        }
                                                        else
                                                        {
                                                            insert["supplier_product_code"] = DBNull.Value;
                                                        }
                                                    }

                                                    if(errors.Count <= 0)
                                                    {
                                                        inserts.Rows.Add(insert);
                                                        noinsert++;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if (barcodeColumn == -1)
                            {
                                errors.Add("Kolom barcode barang tidak ditemukan");
                            }

                            if (inserts.Rows.Count == 0 && updates.Rows.Count == 0)
                            {
                                errors.Add("Tidak ada data untuk disimpan");
                            }

                            if (errors.Count > 0)
                            {
                                string alert = String.Join("<br>", errors.ToArray());
                                inserts.Rows.Clear();
                                updates.Rows.Clear();
                                AlertUploadLabel.Text += alert + tmp;
                                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                            else
                            {
                                if (inserts.Rows.Count > 0)
                                {
                                    ViewState["inserts"] = inserts;
                                    ItemInsertGridView.DataSource = inserts;
                                    ItemInsertGridView.DataBind();
                                }

                                if (updates.Rows.Count > 0)
                                {
                                    ViewState["updates"] = updates;
                                    ItemUpdateGridView.DataSource = updates;
                                    ItemUpdateGridView.DataBind();
                                }

                                UploadLabel.Visible = false;
                                UploadInput.Visible = false;
                                UploadButton.Visible = false;
                                SaveUploadButton.Visible = true;
                                CancelUploadButton.Visible = true;
                                MarginBottomButtonLabel.Visible = true;
                                MarginBottomGVInsertLabel.Visible = true;
                                MarginBottomGVUpdateLabel.Visible = true;

                                AlertUploadLabel.Text = "Excel berhasil di unggah";
                                AlertUploadLabel.CssClass = "alert alert-light-primary color-primary d-block";
                            }
                        }
                        else
                        {
                            AlertUploadLabel.Text = "Ekstensi berkas tidak didukung";
                            AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            UploadLabel.Visible = true;
                            UploadInput.Visible = true;
                            UploadButton.Visible = true;
                            SaveUploadButton.Visible = false;
                            CancelUploadButton.Visible = false;
                            MarginBottomButtonLabel.Visible = false;
                            MarginBottomGVInsertLabel.Visible = false;
                            MarginBottomGVUpdateLabel.Visible = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        AlertUploadLabel.Text = ex.Message.ToString();
                        AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        UploadLabel.Visible = true;
                        UploadInput.Visible = true;
                        UploadButton.Visible = true;
                        SaveUploadButton.Visible = false;
                        CancelUploadButton.Visible = false;
                        MarginBottomButtonLabel.Visible = false;
                        MarginBottomGVInsertLabel.Visible = false;
                        MarginBottomGVUpdateLabel.Visible = false;
                    }
                }
                else
                {
                    AlertUploadLabel.Text = "Pilih file excel yang ingin di unggah";
                    AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    UploadLabel.Visible = true;
                    UploadInput.Visible = true;
                    UploadButton.Visible = true;
                    SaveUploadButton.Visible = false;
                    CancelUploadButton.Visible = false;
                    MarginBottomButtonLabel.Visible = false;
                    MarginBottomGVInsertLabel.Visible = false;
                    MarginBottomGVUpdateLabel.Visible = false;
                }
            }
            else
            {
                AlertUploadLabel.Text = "Permintaan akses ditolak";
                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                UploadLabel.Visible = true;
                UploadInput.Visible = true;
                UploadButton.Visible = true;
                SaveUploadButton.Visible = false;
                CancelUploadButton.Visible = false;
                MarginBottomButtonLabel.Visible = false;
                MarginBottomGVInsertLabel.Visible = false;
                MarginBottomGVUpdateLabel.Visible = false;
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "selectUploadItemsTab();", true);
        }

        protected void DownloadButton_Command(object sender, CommandEventArgs e)
        {            
            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            SqlConnection conn = new SqlConnection(constr);
            SqlCommand cmd = conn.CreateCommand();

            string query = "";
            string columns = "";
            string wheresupplier = "";
            string wherestore = "";
            bool withComma = false;

            if (((string[])Session["read"]).Contains("store"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "stores.store_name");
                columns = String.Concat(columns, "[Store Name] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("supplier"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "suppliers.supplier_name");
                columns = String.Concat(columns, "[Supplier Name] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("itemBarcode"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.item_barcode");
                columns = String.Concat(columns, "[Item Barcode] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("itemCode"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.item_code");
                columns = String.Concat(columns, "[Item Code] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("itemCategoryCode"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.item_category_code");
                columns = String.Concat(columns, "[Item Category Code] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("itemName"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.item_name");
                columns = String.Concat(columns, "[Item Name] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("shortDescription"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.short_description");
                columns = String.Concat(columns, "[Short Description] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("variants"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", @"dbo.JsonValue(items.variants, 'color') [color],
                    dbo.JsonValue(items.variants, 'size') [size]");
                columns = String.Concat(columns, "[Color] NVARCHAR(255), [Size] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("sellingPrice"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.selling_price");
                columns = String.Concat(columns, "[Selling Price] DOUBLE");
            }

            if (((string[])Session["read"]).Contains("purchaseMargin"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.purchase_margin");
                columns = String.Concat(columns, "[Purchase Margin] DOUBLE");
            }

            if (((string[])Session["read"]).Contains("effectiveDate"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "FORMAT(items.effective_date, 'dd MMM yyyy') [effective_date]");
                columns = String.Concat(columns, "[Effective Date] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("brandName"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.brand_name");
                columns = String.Concat(columns, "[Brand Name] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("unitOfMeasure"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.uom");
                columns = String.Concat(columns, "[Unit Of Measure] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("supplierProductCode"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "items.supplier_product_code");
                columns = String.Concat(columns, "[Supplier Product Code] NVARCHAR(255)");
            }

            if(e.CommandName == "downloadall")
            {
                if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = @"SELECT supplier_id FROM suppliers";
                }
                else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = String.Join(",", ((int[])Session["suppliers"]));
                }

                if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = @"SELECT store_id FROM stores";
                }
                else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = String.Join(",", ((int[])Session["stores"]));
                }
            }

            if (e.CommandName == "downloadselectedsupplier")
            {
                wheresupplier = SupplierChoice.SelectedValue;

                if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = @"SELECT store_id FROM stores";
                }
                else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = String.Join(",", ((int[])Session["stores"]));
                }
            }

            if (e.CommandName == "downloadselectedstore")
            {
                if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = @"SELECT supplier_id FROM suppliers";
                }
                else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = String.Join(",", ((int[])Session["suppliers"]));
                }

                wherestore = StoreChoice.SelectedValue;
            }

            if (e.CommandName == "downloadselectedstoreandsupplier")
            {
                wheresupplier = SupplierChoice.SelectedValue;

                wherestore = StoreChoice.SelectedValue;
            }

            List<string> rows = new List<string>();

            string commandText = @"SELECT ROW_NUMBER() OVER(ORDER BY item_id) [no], items.item_id, items.uuid, {0}
                    FROM items INNER JOIN stores ON items.store_id = stores.store_id
                    INNER JOIN suppliers ON items.supplier_id = suppliers.supplier_id
                    WHERE items.store_id IN ({1}) AND items.supplier_id IN ({2})
                    ORDER BY items.store_id, items.supplier_id;";
            commandText = String.Format(commandText, query, wherestore, wheresupplier);

            cmd.CommandText = commandText;
            cmd.CommandTimeout = 0;
            conn.Open();
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    withComma = false;

                    string row = "";

                    if (((string[])Session["read"]).Contains("store"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("store_name")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["store_name"].ToString().Replace("'", ""), "'");
                        }
                    }

                    if (((string[])Session["read"]).Contains("supplier"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("supplier_name")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["supplier_name"].ToString().Replace("'", ""), "'");
                        }
                    }

                    if (((string[])Session["read"]).Contains("itemBarcode"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("item_barcode")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["item_barcode"].ToString().Replace("'", ""), "'");
                        }

                    }

                    if (((string[])Session["read"]).Contains("itemCode"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("item_code")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["item_code"].ToString().Replace("'", ""), "'");
                        }

                    }

                    if (((string[])Session["read"]).Contains("itemCategoryCode"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("item_category_code")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["item_category_code"].ToString().Replace("'", ""), "'");
                        }
                    }

                    if (((string[])Session["read"]).Contains("itemName"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("item_name")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["item_name"].ToString().Replace("'", ""), "'");
                        }
                    }

                    if (((string[])Session["read"]).Contains("shortDescription"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("short_description")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["short_description"].ToString().Replace("'", ""), "'");
                        }

                    }

                    if (((string[])Session["read"]).Contains("variants"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("color")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["color"].ToString().Replace("'", ""), "'");
                        }

                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("size")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["size"].ToString().Replace("'", ""), "'");
                        }
                    }

                    if (((string[])Session["read"]).Contains("sellingPrice"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("selling_price")))
                        {
                            row = String.Concat(row, " ", "0");
                        }
                        else
                        {
                            row = String.Concat(row, " ", reader["selling_price"].ToString());
                        }

                    }

                    if (((string[])Session["read"]).Contains("purchaseMargin"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("purchase_margin")))
                        {
                            row = String.Concat(row, " ", "0");
                        }
                        else
                        {
                            row = String.Concat(row, " ", reader["purchase_margin"].ToString());
                        }

                    }

                    if (((string[])Session["read"]).Contains("effectiveDate"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("effective_date")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["effective_date"].ToString(), "'");
                        }

                    }

                    if (((string[])Session["read"]).Contains("brandName"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("brand_name")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["brand_name"].ToString().Replace("'", ""), "'");
                        }

                    }

                    if (((string[])Session["read"]).Contains("unitOfMeasure"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("uom")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["uom"].ToString().Replace("'", ""), "'");
                        }

                    }

                    if (((string[])Session["read"]).Contains("supplierProductCode"))
                    {
                        if (withComma)
                        {
                            row = String.Concat(row, ",");
                        }
                        else
                        {
                            withComma = true;
                        }

                        if (reader.IsDBNull(reader.GetOrdinal("supplier_product_code")))
                        {
                            row = String.Concat(row, " ", "''");
                        }
                        else
                        {
                            row = String.Concat(row, " ", "'", reader["supplier_product_code"].ToString().Replace("'", ""), "'");
                        }
                    }

                    row = String.Format("INSERT INTO Sheet1 VALUES ({0});", row);
                    rows.Add(row);
                }
            }

            conn.Close();

            string sourcefile = MapPath("~/uploads/blank.xlsx");
            string filename = "items-" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".xlsx";
            string destinationfile = MapPath("~/uploads/" + filename);

            File.Copy(sourcefile, destinationfile, true);

            string extension = Path.GetExtension(destinationfile);

            string connectionstring = "";
            string mimetypes = "";

            if (extension == ".xls")
            {
                connectionstring = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + destinationfile + "';Extended Properties=\"Excel 8.0;HDR=YES;\"";
                mimetypes = "application/vnd.ms-excel";
            }
            else if (extension == ".xlsx")
            {
                connectionstring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + destinationfile + "';Extended Properties=\"Excel 12.0;HDR=YES;\"";
                mimetypes = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            }

            using (OleDbConnection oleconn = new OleDbConnection(connectionstring))
            {
                oleconn.Open();
                OleDbCommand olecommand = new OleDbCommand();
                olecommand.Connection = oleconn;
                olecommand.CommandText = String.Format("CREATE TABLE Sheet1 ({0});", columns);
                olecommand.ExecuteNonQuery();

                foreach(string row in rows)
                {
                    olecommand.CommandText = row;
                    olecommand.ExecuteNonQuery();
                }
                
                oleconn.Close();
            }

            FilenameInput.Value = filename;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "download", "download();", true);

            //Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('Download.aspx','_newtab');", true);

            //Content Type and Header.
            //Response.ContentType = mimetypes;
            //Response.AppendHeader("Content-Disposition", "attachment; filename=" + destinationfile);

            ////Writing the File to Response Stream.
            //Response.WriteFile(destinationfile);

            //Flushing the Response.
            //Response.Flush();
            //Response.End();

        }

        protected void DownloadPerSupplierButton_Click(object sender, EventArgs e)
        {

        }

        protected void DownloadPerStoreButton_Click(object sender, EventArgs e)
        {

        }
        
        protected void SaveUploadButton_Click(object sender, EventArgs e)
        {
            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

            List<string> errors = new List<string>();
            List<string> insertColumns = new List<string>();
            List<string> insertColumnsAndDatatypes = new List<string>();
            List<string> updateColumns = new List<string>();
            List<string> updateColumnsAndDatatypes = new List<string>();

            if (columns.ContainsKey("itemId"))
            {
                updateColumnsAndDatatypes.Add(String.Concat(columns["itemId"], " ", datatypes["itemId"]));
            }
            else
            {
                errors.Add("Kolom storeId tidak ditemukan");
            }

            if (columns.ContainsKey("storeId"))
            {
                if (((string[])Session["add"]).Contains("store"))
                {
                    insertColumns.Add(columns["storeId"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["storeId"], " ", datatypes["storeId"]));
                }
                
                updateColumnsAndDatatypes.Add(String.Concat(columns["storeId"], " ", datatypes["storeId"]));
            }
            else
            {
                errors.Add("Kolom storeId tidak ditemukan");
            }

            if (columns.ContainsKey("supplierId"))
            {
                if (((string[])Session["add"]).Contains("supplier"))
                {
                    insertColumns.Add(columns["supplierId"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["supplierId"], " ", datatypes["supplierId"]));
                }
                
                updateColumnsAndDatatypes.Add(String.Concat(columns["supplierId"], " ", datatypes["supplierId"]));
            }
            else
            {
                errors.Add("Kolom supplierId tidak ditemukan");
            }

            if (columns.ContainsKey("itemBarcode"))
            {
                if (((string[])Session["add"]).Contains("itemBarcode"))
                {
                    insertColumns.Add(columns["itemBarcode"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["itemBarcode"], " ", datatypes["itemBarcode"]));
                }
                if (((string[])Session["edit"]).Contains("itemBarcode"))
                {
                    //updateColumns.Add(columns["itemBarcode"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["itemBarcode"], " ", datatypes["itemBarcode"]));
                }
            }
            else
            {
                errors.Add("Kolom itemBarcode tidak ditemukan");
            }

            if (columns.ContainsKey("itemCode"))
            {
                if (((string[])Session["add"]).Contains("itemCode"))
                {
                    insertColumns.Add(columns["itemCode"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["itemCode"], " ", datatypes["itemCode"]));
                }
                if (((string[])Session["edit"]).Contains("itemCode"))
                {
                    updateColumns.Add(columns["itemCode"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["itemCode"], " ", datatypes["itemCode"]));
                }
            }
            else
            {
                errors.Add("Kolom itemCode tidak ditemukan");
            }

            if (columns.ContainsKey("itemCategoryCode"))
            {
                if (((string[])Session["add"]).Contains("itemCategoryCode"))
                {
                    insertColumns.Add(columns["itemCategoryCode"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["itemCategoryCode"], " ", datatypes["itemCategoryCode"]));
                }
                if (((string[])Session["edit"]).Contains("itemCategoryCode"))
                {
                    updateColumns.Add(columns["itemCategoryCode"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["itemCategoryCode"], " ", datatypes["itemCategoryCode"]));
                }
            }
            else
            {
                errors.Add("Kolom itemCategoryCode tidak ditemukan");
            }

            if (columns.ContainsKey("itemName"))
            {
                if (((string[])Session["add"]).Contains("itemName"))
                {
                    insertColumns.Add(columns["itemName"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["itemName"], " ", datatypes["itemName"]));
                }
                if (((string[])Session["edit"]).Contains("itemName"))
                {
                    updateColumns.Add(columns["itemName"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["itemName"], " ", datatypes["itemName"]));
                }
            }
            else
            {
                errors.Add("Kolom itemName tidak ditemukan");
            }

            if (columns.ContainsKey("foreignName"))
            {
                if (((string[])Session["add"]).Contains("foreignName"))
                {
                    insertColumns.Add(columns["foreignName"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["foreignName"], " ", datatypes["foreignName"]));
                }
                if (((string[])Session["edit"]).Contains("foreignName"))
                {
                    updateColumns.Add(columns["foreignName"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["foreignName"], " ", datatypes["foreignName"]));
                }
            }
            else
            {
                errors.Add("Kolom foreignName tidak ditemukan");
            }

            if (columns.ContainsKey("supplierProductCode"))
            {
                if (((string[])Session["add"]).Contains("supplierProductCode"))
                {
                    insertColumns.Add(columns["supplierProductCode"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["supplierProductCode"], " ", datatypes["supplierProductCode"]));
                }
                if (((string[])Session["edit"]).Contains("supplierProductCode"))
                {
                    updateColumns.Add(columns["supplierProductCode"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["supplierProductCode"], " ", datatypes["supplierProductCode"]));
                }
            }
            else
            {
                errors.Add("Kolom supplierProductCode tidak ditemukan");
            }

            if (columns.ContainsKey("brandName"))
            {
                if (((string[])Session["add"]).Contains("brandName"))
                {
                    insertColumns.Add(columns["brandName"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["brandName"], " ", datatypes["brandName"]));
                }
                if (((string[])Session["edit"]).Contains("brandName"))
                {
                    updateColumns.Add(columns["brandName"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["brandName"], " ", datatypes["brandName"]));
                }
            }
            else
            {
                errors.Add("Kolom brandName tidak ditemukan");
            }

            if (columns.ContainsKey("shortDescription"))
            {
                if (((string[])Session["add"]).Contains("shortDescription"))
                {
                    insertColumns.Add(columns["shortDescription"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["shortDescription"], " ", datatypes["shortDescription"]));
                }
                if (((string[])Session["edit"]).Contains("shortDescription"))
                {
                    updateColumns.Add(columns["shortDescription"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["shortDescription"], " ", datatypes["shortDescription"]));
                }
            }
            else
            {
                errors.Add("Kolom shortDescription tidak ditemukan");
            }

            if (columns.ContainsKey("variants"))
            {
                if (((string[])Session["add"]).Contains("variants"))
                {
                    insertColumns.Add(columns["variants"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["variants"], " ", datatypes["variants"]));
                }
                if (((string[])Session["edit"]).Contains("variants"))
                {
                    updateColumns.Add(columns["variants"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["variants"], " ", datatypes["variants"]));
                }
            }
            else
            {
                errors.Add("Kolom variants tidak ditemukan");
            }

            if (columns.ContainsKey("sellingPrice"))
            {
                if (((string[])Session["add"]).Contains("sellingPrice"))
                {
                    insertColumns.Add(columns["sellingPrice"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["sellingPrice"], " ", datatypes["sellingPrice"]));
                }

                if (((string[])Session["edit"]).Contains("sellingPrice"))
                {
                    updateColumns.Add(columns["sellingPrice"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["sellingPrice"], " ", datatypes["sellingPrice"]));
                }
            }
            else
            {
                errors.Add("Kolom sellingPrice tidak ditemukan");
            }

            if (columns.ContainsKey("purchaseMargin"))
            {
                if (((string[])Session["add"]).Contains("purchaseMargin"))
                {
                    insertColumns.Add(columns["purchaseMargin"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["purchaseMargin"], " ", datatypes["purchaseMargin"]));
                }
                if (((string[])Session["edit"]).Contains("purchaseMargin"))
                {
                    updateColumns.Add(columns["purchaseMargin"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["purchaseMargin"], " ", datatypes["purchaseMargin"]));
                }
            }
            else
            {
                errors.Add("Kolom purchaseMargin tidak ditemukan");
            }

            if (columns.ContainsKey("effectiveDate"))
            {
                if (((string[])Session["add"]).Contains("effectiveDate"))
                {
                    insertColumns.Add(columns["effectiveDate"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["effectiveDate"], " ", datatypes["effectiveDate"]));
                }
                if (((string[])Session["edit"]).Contains("effectiveDate"))
                {
                    updateColumns.Add(columns["effectiveDate"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["effectiveDate"], " ", datatypes["effectiveDate"]));
                }
            }
            else
            {
                errors.Add("Kolom effectiveDate tidak ditemukan");
            }

            if (columns.ContainsKey("unitOfMeasure"))
            {
                if (((string[])Session["add"]).Contains("unitOfMeasure"))
                {
                    insertColumns.Add(columns["unitOfMeasure"]);
                    insertColumnsAndDatatypes.Add(String.Concat(columns["unitOfMeasure"], " ", datatypes["unitOfMeasure"]));
                }
                if (((string[])Session["edit"]).Contains("unitOfMeasure"))
                {
                    updateColumns.Add(columns["unitOfMeasure"]);
                    updateColumnsAndDatatypes.Add(String.Concat(columns["unitOfMeasure"], " ", datatypes["unitOfMeasure"]));
                }
            }
            else
            {
                errors.Add("Kolom unitOfMeasure tidak ditemukan");
            }

            //if (columns.ContainsKey("createdDate"))
            //{
            //    insertColumns.Add(columns["createdDate"]);
            //    insertColumnsAndDatatypes.Add(String.Concat(columns["createdDate"], " ", datatypes["createdDate"]));
            //    updateColumns.Add(columns["createdDate"]);
            //    updateColumnsAndDatatypes.Add(String.Concat(columns["createdDate"], " ", datatypes["createdDate"]));
            //}
            //else
            //{
            //    errors.Add("Kolom createdDate tidak ditemukan");
            //}

            //if (columns.ContainsKey("createdBy"))
            //{
            //    insertColumns.Add(columns["createdBy"]);
            //    insertColumnsAndDatatypes.Add(String.Concat(columns["createdBy"], " ", datatypes["createdBy"]));
            //    updateColumns.Add(columns["createdBy"]);
            //    updateColumnsAndDatatypes.Add(String.Concat(columns["createdBy"], " ", datatypes["createdBy"]));
            //}
            //else
            //{
            //    errors.Add("Kolom createdBy tidak ditemukan");
            //}

            //if (columns.ContainsKey("modifiedDate"))
            //{
            //    insertColumns.Add(columns["modifiedDate"]);
            //    insertColumnsAndDatatypes.Add(String.Concat(columns["modifiedDate"], " ", datatypes["modifiedDate"]));
            //    updateColumns.Add(columns["modifiedDate"]);
            //    updateColumnsAndDatatypes.Add(String.Concat(columns["modifiedDate"], " ", datatypes["modifiedDate"]));
            //}
            //else
            //{
            //    errors.Add("Kolom modifiedDate tidak ditemukan");
            //}

            //if (columns.ContainsKey("modifiedBy"))
            //{
            //    insertColumns.Add(columns["modifiedBy"]);
            //    insertColumnsAndDatatypes.Add(String.Concat(columns["modifiedBy"], " ", datatypes["modifiedBy"]));
            //    updateColumns.Add(columns["modifiedBy"]);
            //    updateColumnsAndDatatypes.Add(String.Concat(columns["modifiedBy"], " ", datatypes["modifiedBy"]));
            //}
            //else
            //{
            //    errors.Add("Kolom modifiedBy tidak ditemukan");
            //}

            string tmp = "";
            if (errors.Count > 0)
            {
                AlertUploadLabel.Text = String.Join("<br>", errors.ToArray());
                AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
            else
            {
                string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                using (SqlConnection conn = new SqlConnection(constr))
                {
                    SqlTransaction transaction;

                    conn.Open();
                    transaction = conn.BeginTransaction();

                    try
                    {
                        if (inserts.Rows.Count > 0)
                        {
                            SqlCommand command = conn.CreateCommand();
                            command.CommandText = @"CREATE TABLE #temp_insert (" + String.Join(", ", insertColumnsAndDatatypes.ToArray()) + ");";
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();

                            SqlBulkCopy bulkinsert = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                            bulkinsert.DestinationTableName = "#temp_insert";
                            
                            if (((string[])Session["add"]).Contains("store"))
                            {
                                tmp = tmp + "store_id";
                                bulkinsert.ColumnMappings.Add("store_id", "store_id");
                            }

                            if (((string[])Session["add"]).Contains("supplier"))
                            {
                                tmp = tmp + "supplier_id";
                                bulkinsert.ColumnMappings.Add("supplier_id", "supplier_id");
                            }

                            if (((string[])Session["add"]).Contains("itemBarcode"))
                            {
                                tmp = tmp + "item_barcode";
                                bulkinsert.ColumnMappings.Add("item_barcode", "item_barcode");
                            }

                            if (((string[])Session["add"]).Contains("itemCode"))
                            {
                                tmp = tmp + "item_code";
                                bulkinsert.ColumnMappings.Add("item_code", "item_code");
                            }

                            if (((string[])Session["add"]).Contains("itemCategoryCode"))
                            {
                                tmp = tmp + "item_category_code";
                                bulkinsert.ColumnMappings.Add("item_category_code", "item_category_code");
                            }

                            if (((string[])Session["add"]).Contains("itemName"))
                            {
                                tmp = tmp + "item_name";
                                bulkinsert.ColumnMappings.Add("item_name", "item_name");
                            }

                            if (((string[])Session["add"]).Contains("foreignName"))
                            {
                                tmp = tmp + "foreign_name";
                                bulkinsert.ColumnMappings.Add("foreign_name", "foreign_name");
                            }

                            if (((string[])Session["add"]).Contains("brandName"))
                            {
                                tmp = tmp + "brand_name";
                                bulkinsert.ColumnMappings.Add("brand_name", "brand_name");
                            }

                            if (((string[])Session["add"]).Contains("shortDescription"))
                            {
                                tmp = tmp + "short_description";
                                bulkinsert.ColumnMappings.Add("short_description", "short_description");
                            }

                            if (((string[])Session["add"]).Contains("variants"))
                            {
                                tmp = tmp + "variants";
                                bulkinsert.ColumnMappings.Add("variants", "variants");
                            }

                            if (((string[])Session["add"]).Contains("sellingPrice"))
                            {
                                tmp = tmp + "selling_price";
                                bulkinsert.ColumnMappings.Add("selling_price", "selling_price");
                            }

                            if (((string[])Session["add"]).Contains("purchaseMargin"))
                            {
                                tmp = tmp + "purchase_margin";
                                bulkinsert.ColumnMappings.Add("purchase_margin", "purchase_margin");
                            }

                            if (((string[])Session["add"]).Contains("effectiveDate"))
                            {
                                tmp = tmp + "effective_date";
                                bulkinsert.ColumnMappings.Add("effective_date", "effective_date");
                            }

                            if (((string[])Session["add"]).Contains("unitOfMeasure"))
                            {
                                tmp = tmp + "uom";
                                bulkinsert.ColumnMappings.Add("uom", "uom");
                            }

                            if (((string[])Session["add"]).Contains("supplierProductCode"))
                            {
                                tmp = tmp + "supplier_product_code";
                                bulkinsert.ColumnMappings.Add("supplier_product_code", "supplier_product_code");
                            }

                            //bulkinsert.ColumnMappings.Add("created_date", "created_date");
                            //bulkinsert.ColumnMappings.Add("created_by", "created_by");
                            //bulkinsert.ColumnMappings.Add("modified_date", "modified_date");
                            //bulkinsert.ColumnMappings.Add("modified_by", "modified_by");

                            bulkinsert.WriteToServer(inserts);

                            command = conn.CreateCommand();
                            command.CommandText = @"INSERT INTO items (" + String.Join(", ", insertColumns.ToArray()) + 
                                @", created_date, created_by, modified_date, modified_by)
                                SELECT " + String.Join(", ", insertColumns.Select(columnName => "temp." + columnName).ToList().ToArray()) + 
                                @", getdate(), @user, getdate(), @user 
                                FROM #temp_insert temp;
                                DROP TABLE #temp_insert;";
                            command.Parameters.AddWithValue("@user", Session["username"].ToString());
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();
                        }

                        if (updates.Rows.Count > 0)
                        {
                            SqlCommand command = conn.CreateCommand();
                            command.CommandText = @"CREATE TABLE #temp_update (" + String.Join(", ", updateColumnsAndDatatypes.ToArray()) + ");";
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();

                            SqlBulkCopy bulkupdate = new SqlBulkCopy(conn, SqlBulkCopyOptions.KeepIdentity, transaction);
                            bulkupdate.DestinationTableName = "#temp_update";
                            bulkupdate.ColumnMappings.Add("item_id", "item_id");
                            bulkupdate.ColumnMappings.Add("store_id", "store_id");
                            bulkupdate.ColumnMappings.Add("supplier_id", "supplier_id");
                            

                            if (((string[])Session["edit"]).Contains("itemBarcode"))
                            {
                                bulkupdate.ColumnMappings.Add("item_barcode", "item_barcode");
                            }

                            if (((string[])Session["edit"]).Contains("itemCode"))
                            {
                                bulkupdate.ColumnMappings.Add("item_code", "item_code");
                            }

                            if (((string[])Session["edit"]).Contains("itemCategoryCode"))
                            {
                                bulkupdate.ColumnMappings.Add("item_category_code", "item_category_code");
                            }

                            if (((string[])Session["edit"]).Contains("itemName"))
                            {
                                bulkupdate.ColumnMappings.Add("item_name", "item_name");
                            }

                            if (((string[])Session["edit"]).Contains("foreignName"))
                            {
                                bulkupdate.ColumnMappings.Add("foreign_name", "foreign_name");
                            }

                            if (((string[])Session["edit"]).Contains("brandName"))
                            {
                                bulkupdate.ColumnMappings.Add("brand_name", "brand_name");
                            }

                            if (((string[])Session["edit"]).Contains("shortDescription"))
                            {
                                bulkupdate.ColumnMappings.Add("short_description", "short_description");
                            }

                            if (((string[])Session["edit"]).Contains("variants"))
                            {
                                bulkupdate.ColumnMappings.Add("variants", "variants");
                            }

                            if (((string[])Session["edit"]).Contains("sellingPrice"))
                            {
                                bulkupdate.ColumnMappings.Add("selling_price", "selling_price");
                            }

                            if (((string[])Session["edit"]).Contains("purchaseMargin"))
                            {
                                bulkupdate.ColumnMappings.Add("purchase_margin", "purchase_margin");
                            }

                            if (((string[])Session["edit"]).Contains("effectiveDate"))
                            {
                                bulkupdate.ColumnMappings.Add("effective_date", "effective_date");
                            }

                            if (((string[])Session["edit"]).Contains("unitOfMeasure"))
                            {
                                bulkupdate.ColumnMappings.Add("uom", "uom");
                            }

                            if (((string[])Session["edit"]).Contains("supplierProductCode"))
                            {
                                bulkupdate.ColumnMappings.Add("supplier_product_code", "supplier_product_code");
                            }

                            //bulkupdate.ColumnMappings.Add("modified_date", "modified_date");
                            //bulkupdate.ColumnMappings.Add("modified_by", "modified_by");

                            bulkupdate.WriteToServer(updates);

                            command = conn.CreateCommand();
                            command.CommandText = @"UPDATE orig SET " + String.Join(", ", updateColumns.Select(columnName => columnName + " = temp." + columnName).ToList().ToArray()) +
                            @", modified_date = getdate(), modified_by = @user FROM items orig
                            INNER JOIN #temp_update temp ON orig.item_id = temp.item_id 
                            AND orig.item_barcode = temp.item_barcode AND orig.store_id = temp.store_id AND orig.supplier_id = temp.supplier_id;
                            DROP TABLE #temp_update;";
                            command.Parameters.AddWithValue("@user", Session["username"].ToString());
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();
                        }

                        transaction.Commit();

                        AlertUploadLabel.Text = "Data berhasil disimpan";
                        AlertUploadLabel.CssClass = "alert alert-light-primary color-primary d-block";
                        inserts = (System.Data.DataTable)ViewState["inserts"];
                        inserts.Rows.Clear();
                        ViewState["inserts"] = inserts;
                        updates = (System.Data.DataTable)ViewState["updates"];
                        updates.Rows.Clear();
                        ViewState["updates"] = updates;
                        ItemInsertGridView.DataSource = null;
                        ItemInsertGridView.DataBind();
                        ItemUpdateGridView.DataSource = null;
                        ItemUpdateGridView.DataBind();
                        UploadLabel.Visible = true;
                        UploadInput.Visible = true;
                        UploadButton.Visible = true;
                        SaveUploadButton.Visible = false;
                        CancelUploadButton.Visible = false;
                        MarginBottomButtonLabel.Visible = false;
                        MarginBottomGVInsertLabel.Visible = false;
                        MarginBottomGVUpdateLabel.Visible = false;
                        SearchInput.Text = "";
                        BindItemGridview();
                    }
                    catch (Exception error)
                    {
                        transaction.Rollback();
                        UploadLabel.Visible = false;
                        UploadInput.Visible = false;
                        UploadButton.Visible = false;
                        SaveUploadButton.Visible = true;
                        CancelUploadButton.Visible = true;
                        MarginBottomButtonLabel.Visible = true;
                        MarginBottomGVInsertLabel.Visible = true;
                        MarginBottomGVUpdateLabel.Visible = true;
                        AlertUploadLabel.Text = error.Message.ToString() + String.Join(", ", updateColumnsAndDatatypes.ToArray());
                        AlertUploadLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                    conn.Close();
                }
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "selectUploadItemsTab();", true);
        }

        protected void SaveButton_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "add")
            {
                if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(StoreUploadChoice.SelectedValue))) &&
                    (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(SupplierUploadChoice.SelectedValue))))
                {
                    List<string> errors = new List<string>();

                    if (String.IsNullOrEmpty(ItemBarcodeInput.Text?.ToString().Trim()))
                    {
                        errors.Add("Barcode barang tidak boleh kosong");
                    }

                    if (String.IsNullOrEmpty(ItemNameInput.Text?.ToString().Trim()))
                    {
                        errors.Add("Nama barang tidak boleh kosong");
                    }

                    if (String.IsNullOrEmpty(ShortDescriptionInput.Text?.ToString().Trim()))
                    {
                        errors.Add("Deskripsi barang tidak boleh kosong");
                    }

                    if (!String.IsNullOrEmpty(MarginInput.Text?.ToString().Trim()))
                    {
                        double margin = double.NaN;
                        double.TryParse(MarginInput.Text?.ToString().Trim(), out margin);

                        if (margin == double.NaN)
                        {
                            errors.Add("Margin hanya boleh angka");
                        }
                    }

                    if (!String.IsNullOrEmpty(PriceInput.Text?.ToString().Trim()))
                    {
                        double price = double.NaN;
                        double.TryParse(PriceInput.Text?.ToString().Trim(), out price);

                        if (price == double.NaN)
                        {
                            errors.Add("Harga jual hanya boleh angka");
                        }
                        if (price <= 0)
                        {
                            errors.Add("Harga barang tidak valid");
                        }
                    }
                    else
                    {
                        errors.Add("Harga barang tidak boleh kosong");
                    }

                    if (!((string[])Session["add"]).Contains("sellingPrice"))
                    {
                        errors.Add("Terjadi kesalahan hak akses harga barang");
                    }

                    if (!((string[])Session["add"]).Contains("shortDescription"))
                    {
                        errors.Add("Terjadi kesalahan hak akses deskripsi barang");
                    }

                    if (!((string[])Session["add"]).Contains("itemName"))
                    {
                        errors.Add("Terjadi kesalahan hak akses nama barang");
                    }

                    if (!((string[])Session["add"]).Contains("itemBarcode"))
                    {
                        errors.Add("Terjadi kesalahan hak akses barang barcode");
                    }

                    if (errors.Count <= 0)
                    {
                        string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                        SqlConnection conn = new SqlConnection(constr);

                        SqlTransaction transaction;

                        conn.Open();
                        transaction = conn.BeginTransaction();

                        try
                        {
                            SqlCommand command = conn.CreateCommand();
                            command.CommandText = @"SELECT item_id FROM items WHERE store_id = @storeId AND supplier_id = @supplierId 
                            AND item_barcode = @barcode";
                            command.Parameters.AddWithValue("@storeId", int.Parse(StoreUploadChoice.SelectedValue));
                            command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierUploadChoice.SelectedValue));
                            command.Parameters.AddWithValue("@barcode", ItemBarcodeInput.Text?.Trim());
                            command.Transaction = transaction;

                            int itemId = 0;

                            object result = command.ExecuteScalar();
                            if (result != null)
                            {
                                itemId = int.Parse(result.ToString());
                            }

                            if (itemId > 0)
                            {
                                transaction.Rollback();
                                AlertModalLabel.Text = "Barcode barang sudah digunakan";
                                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                                
                            }
                            else
                            {
                                string commandTextFields = @"INSERT INTO items (store_id, supplier_id, {0}, 
                                    created_date, created_by, modified_date, modified_by)";
                                string commandTextValues = @"VALUES (@storeId, @supplierId, {0}, getdate(), @user, getdate(), @user)";
                                bool withComma = false;
                                string fields = "";
                                string values = "";

                                if (((string[])Session["add"]).Contains("itemBarcode"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "item_barcode");
                                    values = string.Concat(values, " ", "@itemBarcode");
                                }

                                if (((string[])Session["add"]).Contains("itemCode"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "item_code");
                                    values = string.Concat(values, " ", "@itemCode");
                                }

                                if (((string[])Session["add"]).Contains("itemCategoryCode"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "item_category_code");
                                    values = string.Concat(values, " ", "@itemCategoryCode");
                                }

                                if (((string[])Session["add"]).Contains("itemName"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "item_name");
                                    values = string.Concat(values, " ", "@itemName");
                                    fields = string.Concat(fields, ",");
                                    values = string.Concat(values, ",");
                                    fields = string.Concat(fields, " ", "foreign_name");
                                    values = string.Concat(values, " ", "@itemName");
                                }

                                if (((string[])Session["add"]).Contains("shortDescription"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "short_description");
                                    values = string.Concat(values, " ", "@shortDescription");
                                }

                                if (((string[])Session["add"]).Contains("brandName"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "brand_name");
                                    values = string.Concat(values, " ", "@brandName");
                                }

                                if (((string[])Session["add"]).Contains("variants"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "variants");
                                    values = string.Concat(values, " ", "@variants");

                                }

                                if (((string[])Session["add"]).Contains("sellingPrice"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "selling_price");
                                    values = string.Concat(values, " ", "@sellingPrice");
                                }

                                if (((string[])Session["add"]).Contains("purchaseMargin"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "purchase_margin");
                                    values = string.Concat(values, " ", "@purchaseMargin");
                                }

                                if (((string[])Session["add"]).Contains("effectiveDate"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "effective_date");
                                    values = string.Concat(values, " ", "@effectiveDate");
                                }

                                if (((string[])Session["add"]).Contains("unitOfMeasure"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "uom");
                                    values = string.Concat(values, " ", "@unitOfMeasure");
                                }

                                if (((string[])Session["add"]).Contains("supplierProductCode"))
                                {
                                    if (withComma)
                                    {
                                        fields = string.Concat(fields, ",");
                                        values = string.Concat(values, ",");
                                    }
                                    else
                                    {
                                        withComma = true;
                                    }
                                    fields = string.Concat(fields, " ", "supplier_product_code");
                                    values = string.Concat(values, " ", "@supplierProductCode");
                                }

                                commandTextFields = string.Format(commandTextFields, fields);
                                commandTextValues = string.Format(commandTextValues, values);

                                string commandText = string.Concat(commandTextFields, " ", commandTextValues);

                                command = conn.CreateCommand();
                                command.CommandText = commandText;

                                command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                                command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                                command.Parameters.AddWithValue("@user", Session["username"]);

                                if (((string[])Session["add"]).Contains("itemBarcode"))
                                {
                                    if (String.IsNullOrEmpty(ItemBarcodeInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@itemBarcode", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@itemBarcode", ItemBarcodeInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("itemCode"))
                                {
                                    if (String.IsNullOrEmpty(ItemCodeInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@itemCode", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@itemCode", ItemCodeInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("itemCategoryCode"))
                                {
                                    if (String.IsNullOrEmpty(ItemCategoryCodeInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@itemCategoryCode", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@itemCategoryCode", ItemCategoryCodeInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("itemName"))
                                {
                                    if (String.IsNullOrEmpty(ItemNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@itemName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@itemName", ItemNameInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("sellingPrice"))
                                {
                                    if (String.IsNullOrEmpty(PriceInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@sellingPrice", 0.00);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@sellingPrice", PriceInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("shortDescription"))
                                {
                                    if (String.IsNullOrEmpty(ShortDescriptionInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@shortDescription", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@shortDescription", ShortDescriptionInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("brandName"))
                                {
                                    if (String.IsNullOrEmpty(BrandNameInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@brandName", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@brandName", BrandNameInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("variants"))
                                {
                                    string variants = "";

                                    if ((String.IsNullOrEmpty(ColorInput.Text?.ToString().Trim()) &&
                                                        String.IsNullOrEmpty(SizeInput.Text?.ToString().Trim())))
                                    {
                                        command.Parameters.AddWithValue("@variants", "{}");
                                    }
                                    else if ((!String.IsNullOrEmpty(ColorInput.Text?.ToString().Trim()) &&
                                                                    !String.IsNullOrEmpty(SizeInput.Text?.ToString().Trim())))
                                    {
                                        Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
                                        json["color"] = ColorInput.Text.ToString().Trim();
                                        json["size"] = SizeInput.Text.ToString().Trim();
                                        variants = json.ToString();

                                        command.Parameters.AddWithValue("@variants", variants);
                                    }
                                    else if (String.IsNullOrEmpty(ColorInput.Text?.ToString().Trim()))
                                    {
                                        Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
                                        json["color"] = ColorInput.Text.ToString().Trim();
                                        variants = json.ToString();

                                        command.Parameters.AddWithValue("@variants", variants);
                                    }
                                    else if (String.IsNullOrEmpty(SizeInput.Text?.ToString().Trim()))
                                    {
                                        Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
                                        json["size"] = SizeInput.Text?.ToString().Trim();
                                        variants = json.ToString();

                                        command.Parameters.AddWithValue("@variants", variants);
                                    }
                                }

                                if (((string[])Session["add"]).Contains("purchaseMargin"))
                                {
                                    if (String.IsNullOrEmpty(MarginInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@purchaseMargin", 0.00);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@purchaseMargin", MarginInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("effectiveDate"))
                                {
                                    if (String.IsNullOrEmpty(EffectiveDateInput.Text.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@effectiveDate", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@effectiveDate", EffectiveDateInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("unitOfMeasure"))
                                {
                                    if (String.IsNullOrEmpty(UnitOfMeasureInput.Text.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@unitOfMeasure", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@unitOfMeasure", UnitOfMeasureInput.Text.Trim());
                                    }
                                }

                                if (((string[])Session["add"]).Contains("supplierProductCode"))
                                {
                                    if (String.IsNullOrEmpty(SupplierProductCodeInput.Text?.Trim()))
                                    {
                                        command.Parameters.AddWithValue("@supplierProductCode", DBNull.Value);
                                    }
                                    else
                                    {
                                        command.Parameters.AddWithValue("@supplierProductCode", SupplierProductCodeInput.Text.Trim());
                                    }
                                }

                                command.Transaction = transaction;
                                command.ExecuteNonQuery();

                                transaction.Commit();

                                ViewState["page"] = Int32.MaxValue;
                                SearchInput.Text = "";
                                BindItemGridview();

                                UnitOfMeasureInput.Text = "";
                                ItemCodeInput.Text = "";
                                ItemCategoryCodeInput.Text = "";
                                SupplierProductCodeInput.Text = "";
                                ItemBarcodeInput.Text = "";
                                ItemNameInput.Text = "";
                                //ForeignNameInput.Text = "";
                                ShortDescriptionInput.Text = "";
                                ColorInput.Text = "";
                                SizeInput.Text = "";
                                MarginInput.Text = "";
                                EffectiveDateInput.Text = "";
                                MarginInput.Text = "";
                                PriceInput.Text = "";
                                EffectiveDateInput.Text = "";
                                BrandNameInput.Text = "";
                                AlertModalLabel.Text = "Data berhasil disimpan";
                                AlertModalLabel.CssClass = "alert alert-light-primary color-primary d-block";
                            }
                        }
                        catch (Exception error)
                        {
                            transaction.Rollback();

                            AlertModalLabel.Text = error.Message.ToString();
                            AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                        conn.Close();
                    }
                    else
                    {
                        string alert = String.Join("<br>", errors.ToArray());
                        AlertModalLabel.Text = alert;
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                    
                }
                else
                {
                    AlertModalLabel.Text = "Permintaan akses ditolak";
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "openItemModal();", true);
            } 
            else if(e.CommandName == "edit")
            {
                if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))) &&
                    (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))))
                {
                    string[] arg = e.CommandArgument.ToString().Split(';');

                    if (arg.Length == 2)
                    {
                        List<string> errors = new List<string>();

                        if (String.IsNullOrEmpty(ItemBarcodeInput.Text?.ToString().Trim()))
                        {
                            errors.Add("Barcode barang tidak boleh kosong");
                        }

                        if (String.IsNullOrEmpty(ItemNameInput.Text?.ToString().Trim()))
                        {
                            errors.Add("Nama barang tidak boleh kosong");
                        }

                        if (String.IsNullOrEmpty(ShortDescriptionInput.Text?.ToString().Trim()))
                        {
                            errors.Add("Deskripsi barang tidak boleh kosong");
                        }

                        if (!String.IsNullOrEmpty(MarginInput.Text?.ToString().Trim()))
                        {
                            double margin = double.NaN;
                            double.TryParse(MarginInput.Text?.ToString().Trim(), out margin);

                            if (margin == double.NaN)
                            {
                                errors.Add("Margin hanya boleh angka");
                            }
                        }

                        if (!String.IsNullOrEmpty(PriceInput.Text?.ToString().Trim()))
                        {
                            double price = double.NaN;
                            double.TryParse(PriceInput.Text?.ToString().Trim(), out price);

                            if (price == double.NaN)
                            {
                                errors.Add("Harga jual hanya boleh angka");
                            }
                            if (price <= 0)
                            {
                                errors.Add("Harga barang tidak valid");
                            }
                        }
                        else
                        {
                            errors.Add("Harga barang tidak boleh kosong");
                        }

                        if (!((string[])Session["add"]).Contains("sellingPrice"))
                        {
                            errors.Add("Terjadi kesalahan hak akses harga barang");
                        }

                        if (!((string[])Session["add"]).Contains("shortDescription"))
                        {
                            errors.Add("Terjadi kesalahan hak akses deskripsi barang");
                        }

                        if (!((string[])Session["add"]).Contains("itemName"))
                        {
                            errors.Add("Terjadi kesalahan hak akses nama barang");
                        }

                        if (!((string[])Session["add"]).Contains("itemBarcode"))
                        {
                            errors.Add("Terjadi kesalahan hak akses barang barcode");
                        }

                        if (errors.Count <= 0)
                        {
                            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                            SqlConnection conn = new SqlConnection(constr);

                            SqlTransaction transaction;
                            conn.Open();
                            transaction = conn.BeginTransaction();

                            try
                            {
                                SqlCommand command = conn.CreateCommand();
                                command.CommandText = @"SELECT item_id FROM items WHERE store_id = @storeId AND supplier_id = @supplierId 
                                AND item_id = @itemId AND uuid = @itemUuid";
                                command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                                command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                                command.Parameters.AddWithValue("@itemId", int.Parse(arg[0].ToString()));
                                command.Parameters.AddWithValue("@itemUuid", arg[1].ToString());
                                command.Transaction = transaction;

                                int itemId = 0;

                                object result = command.ExecuteScalar();

                                if (result != null)
                                {
                                    itemId = int.Parse(result.ToString());
                                }

                                if (itemId == 0)
                                {
                                    transaction.Rollback();
                                    AlertModalLabel.Text = "Barcode barang tidak ditemukan";
                                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                                }
                                else
                                {
                                    bool withComma = false;
                                    string sql = "";

                                    if (((string[])Session["edit"]).Contains("itemCode"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "item_code = @itemCode");
                                    }

                                    if (((string[])Session["edit"]).Contains("itemBarcode"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "item_barcode = @itemBarcode");
                                    }

                                    if (((string[])Session["edit"]).Contains("itemCategoryCode"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "item_category_code = @itemCategoryCode");
                                    }

                                    if (((string[])Session["edit"]).Contains("itemName"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "item_name = @itemName, foreign_name = @itemName");
                                    }

                                    if (((string[])Session["edit"]).Contains("shortDescription"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "short_description = @shortDescription");
                                    }

                                    if (((string[])Session["edit"]).Contains("brandName"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "brand_name = @brandName");
                                    }

                                    if (((string[])Session["edit"]).Contains("variants"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }

                                        sql = string.Concat(sql, " ", "variants = @variants");
                                    }

                                    if (((string[])Session["edit"]).Contains("purchaseMargin"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "purchase_margin = @purchaseMargin");
                                    }

                                    if (((string[])Session["edit"]).Contains("sellingPrice"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "selling_price = @sellingPrice");
                                    }

                                    if (((string[])Session["edit"]).Contains("effectiveDate"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "effective_date = @effectiveDate");
                                    }

                                    if (((string[])Session["edit"]).Contains("unitOfMeasure"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "uom = @unitOfMeasure");
                                    }

                                    if (((string[])Session["edit"]).Contains("supplierProductCode"))
                                    {
                                        if (withComma)
                                        {
                                            sql = string.Concat(sql, ",");
                                        }
                                        else
                                        {
                                            withComma = true;
                                        }
                                        sql = string.Concat(sql, " ", "supplier_product_code = @supplierProductCode");
                                    }

                                    string commandText = @"UPDATE items SET {0}, modified_date = getdate(), modified_by = @user
                                    WHERE store_id = @storeId AND supplier_id = @supplierId AND item_id = @itemId AND uuid = @itemUuid";

                                    commandText = string.Format(commandText, sql);

                                    command = conn.CreateCommand();
                                    command.CommandText = commandText;

                                    command.Parameters.AddWithValue("@itemId", int.Parse(arg[0].ToString()));
                                    command.Parameters.AddWithValue("@itemUuid", arg[1].ToString());
                                    command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                                    command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                                    command.Parameters.AddWithValue("@user", Session["username"]);

                                    if (((string[])Session["edit"]).Contains("itemCode"))
                                    {
                                        if (String.IsNullOrEmpty(ItemCodeInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@itemCode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@itemCode", ItemCodeInput.Text.Trim());
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("itemBarcode"))
                                    {
                                        if (String.IsNullOrEmpty(ItemBarcodeInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@itemBarcode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@itemBarcode", ItemBarcodeInput.Text.Trim());
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("itemName"))
                                    {
                                        if (String.IsNullOrEmpty(ItemNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@itemName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@itemName", ItemNameInput.Text.Trim());
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("itemCategoryCode"))
                                    {
                                        if (String.IsNullOrEmpty(ItemCategoryCodeInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@itemCategoryCode", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@itemCategoryCode", ItemCategoryCodeInput.Text.Trim());
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("brandName"))
                                    {
                                        if (String.IsNullOrEmpty(BrandNameInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@brandName", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@brandName", BrandNameInput.Text.Trim());
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("shortDescription"))
                                    {
                                        if (String.IsNullOrEmpty(ShortDescriptionInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@shortDescription", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@shortDescription", ShortDescriptionInput.Text.Trim());
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("variants"))
                                    {
                                        string variants = "";

                                        if ((String.IsNullOrEmpty(ColorInput.Text?.ToString().Trim()) &&
                                                            String.IsNullOrEmpty(SizeInput.Text?.ToString().Trim())))
                                        {
                                            command.Parameters.AddWithValue("@variants", "{}");
                                        }
                                        else if ((!String.IsNullOrEmpty(ColorInput.Text?.ToString().Trim()) &&
                                                                        !String.IsNullOrEmpty(SizeInput.Text?.ToString().Trim())))
                                        {
                                            Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
                                            json["color"] = ColorInput.Text.ToString().Trim();
                                            json["size"] = SizeInput.Text.ToString().Trim();
                                            variants = json.ToString();

                                            command.Parameters.AddWithValue("@variants", variants);
                                        }
                                        else if (!String.IsNullOrEmpty(ColorInput.Text?.ToString().Trim()))
                                        {
                                            Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
                                            json["color"] = ColorInput.Text.ToString().Trim();
                                            variants = json.ToString();

                                            command.Parameters.AddWithValue("@variants", variants);
                                        }
                                        else if (!String.IsNullOrEmpty(SizeInput.Text?.ToString().Trim()))
                                        {
                                            Newtonsoft.Json.Linq.JObject json = new Newtonsoft.Json.Linq.JObject();
                                            json["size"] = SizeInput.Text.ToString().Trim();
                                            variants = json.ToString();

                                            command.Parameters.AddWithValue("@variants", variants);
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("purchaseMargin"))
                                    {
                                        if (String.IsNullOrEmpty(MarginInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@purchaseMargin", 0.00);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@purchaseMargin", double.Parse(MarginInput.Text.Trim()));
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("sellingPrice"))
                                    {
                                        if (String.IsNullOrEmpty(PriceInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@sellingPrice", 0.00);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@sellingPrice", double.Parse(PriceInput.Text.Trim()));
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("effectiveDate"))
                                    {
                                        if (String.IsNullOrEmpty(EffectiveDateInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@effectiveDate", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@effectiveDate", EffectiveDateInput.Text.Trim());
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("unitOfMeasure"))
                                    {
                                        if (String.IsNullOrEmpty(UnitOfMeasureInput.Text?.Trim()))
                                        {
                                            command.Parameters.AddWithValue("@unitOfMeasure", DBNull.Value);
                                        }
                                        else
                                        {
                                            command.Parameters.AddWithValue("@unitOfMeasure", UnitOfMeasureInput.Text.Trim());
                                        }
                                    }

                                    command.Transaction = transaction;
                                    command.ExecuteNonQuery();

                                    transaction.Commit();

                                    //ItemGridView.PageIndex = Int32.MaxValue;

                                    BindItemGridview();

                                    UnitOfMeasureInput.Text = "";
                                    ItemCodeInput.Text = "";
                                    ItemCategoryCodeInput.Text = "";
                                    SupplierProductCodeInput.Text = "";
                                    ItemBarcodeInput.Text = "";
                                    ItemNameInput.Text = "";
                                    //ForeignNameInput.Text = "";
                                    ShortDescriptionInput.Text = "";
                                    ColorInput.Text = "";
                                    SizeInput.Text = "";
                                    MarginInput.Text = "";
                                    EffectiveDateInput.Text = "";
                                    MarginInput.Text = "";
                                    PriceInput.Text = "";
                                    EffectiveDateInput.Text = "";
                                    BrandNameInput.Text = "";
                                    AlertModalLabel.Text = "Data berhasil disimpan";
                                    AlertModalLabel.CssClass = "alert alert-light-primary color-primary d-block";
                                }
                            }
                            catch (Exception error)
                            {
                                transaction.Rollback();

                                AlertModalLabel.Text = error.Message.ToString();
                                AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                            conn.Close();
                        }
                        else
                        {
                            string alert = String.Join("<br>", errors.ToArray());
                            AlertModalLabel.Text = alert;
                            AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                        
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "openItemModal();", true);
                    }
                    else
                    {
                        AlertModalLabel.Text = "Terjadi kesalahan";
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertModalLabel.Text = "Permintaan akses ditolak";
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else if (e.CommandName == "editgvinsert")
            {
                string[] arg = e.CommandArgument.ToString().Split(';');
                
                if (arg.Length > 0)
                {
                    List<string> errors = new List<string>();

                    if (String.IsNullOrEmpty(ShortDescriptionInput.Text?.ToString().Trim()))
                    {
                        errors.Add("Deskripsi barang tidak boleh kosong");
                    }

                    if (String.IsNullOrEmpty(ItemNameInput.Text?.ToString().Trim()))
                    {
                        errors.Add("Nama barang tidak boleh kosong");
                    }

                    if (String.IsNullOrEmpty(ItemBarcodeInput.Text?.ToString().Trim()))
                    {
                        errors.Add("Barcode barang tidak boleh kosong");
                    }

                    if (!String.IsNullOrEmpty(MarginInput.Text?.ToString().Trim()))
                    {
                        double margin = double.NaN;
                        double.TryParse(MarginInput.Text?.ToString().Trim(), out margin);

                        if (margin == double.NaN)
                        {
                            errors.Add("Margin hanya boleh angka");
                        }
                    }

                    if (!String.IsNullOrEmpty(PriceInput.Text?.ToString().Trim()))
                    {
                        double price = double.NaN;
                        double.TryParse(PriceInput.Text?.ToString().Trim(), out price);

                        if (price == double.NaN)
                        {
                            errors.Add("Harga jual hanya boleh angka");
                        }
                    }

                    if (errors.Count <= 0)
                    {
                        System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                        for (int i = 0; i < inserts.Rows.Count; i++)
                        {
                            if (inserts.Rows[i]["item_barcode"].ToString() == arg[0].ToString())
                            {
                                if (((string[])Session["add"]).Contains("itemBarcode"))
                                {
                                    if (String.IsNullOrEmpty(ItemBarcodeInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["item_barcode"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["item_barcode"] = ItemBarcodeInput.Text?.Trim();
                                    }

                                }

                                if (((string[])Session["add"]).Contains("itemCode"))
                                {
                                    if (String.IsNullOrEmpty(ItemCodeInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["item_code"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["item_code"] = ItemCodeInput.Text?.Trim();
                                    }

                                }

                                if (((string[])Session["add"]).Contains("itemCategoryCode"))
                                {
                                    if (String.IsNullOrEmpty(ItemCategoryCodeInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["item_category_code"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["item_category_code"] = ItemCategoryCodeInput.Text?.Trim();
                                    }

                                }

                                if (((string[])Session["add"]).Contains("itemName"))
                                {
                                    if (String.IsNullOrEmpty(ItemNameInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["item_name"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["item_name"] = ItemNameInput.Text?.Trim();
                                    }

                                }

                                if (((string[])Session["add"]).Contains("brandName"))
                                {
                                    if (String.IsNullOrEmpty(BrandNameInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["brand_name"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["brand_name"] = BrandNameInput.Text?.Trim();
                                    }

                                }

                                if (((string[])Session["add"]).Contains("shortDescription"))
                                {
                                    if (String.IsNullOrEmpty(ShortDescriptionInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["short_description"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["short_description"] = ShortDescriptionInput.Text?.Trim();
                                    }

                                }

                                if (((string[])Session["add"]).Contains("variants"))
                                {
                                    inserts.Rows[i]["color"] = ColorInput.Text?.Trim();
                                    inserts.Rows[i]["size"] = SizeInput.Text?.Trim();

                                    if (String.IsNullOrEmpty(SizeInput.Text?.Trim()) && String.IsNullOrEmpty(ColorInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["variants"] = "";
                                    }
                                    else if (!String.IsNullOrEmpty(SizeInput.Text?.Trim()) && String.IsNullOrEmpty(ColorInput.Text?.Trim()))
                                    {
                                        Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                        variants["size"] = SizeInput.Text?.Trim();
                                        inserts.Rows[i]["variants"] = variants.ToString();
                                    }
                                    else if (String.IsNullOrEmpty(SizeInput.Text?.Trim()) && !String.IsNullOrEmpty(ColorInput.Text?.Trim()))
                                    {
                                        Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                        variants["color"] = ColorInput.Text?.Trim();
                                        inserts.Rows[i]["variants"] = variants.ToString();
                                    }
                                    else if (!String.IsNullOrEmpty(SizeInput.Text?.Trim()) && !String.IsNullOrEmpty(ColorInput.Text?.Trim()))
                                    {
                                        Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                        variants["color"] = ColorInput.Text?.Trim();
                                        variants["size"] = SizeInput.Text?.Trim();
                                        inserts.Rows[i]["variants"] = variants.ToString();
                                    }
                                }

                                if (((string[])Session["add"]).Contains("sellingPrice"))
                                {
                                    if (String.IsNullOrEmpty(PriceInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["selling_price"] = 0.00;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["selling_price"] = double.Parse(PriceInput.Text?.Trim());
                                    }

                                }

                                if (((string[])Session["add"]).Contains("purchaseMargin"))
                                {
                                    if (String.IsNullOrEmpty(MarginInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["purchase_margin"] = 0.00;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["purchase_margin"] = double.Parse(MarginInput.Text?.Trim());
                                    }

                                }

                                if (((string[])Session["add"]).Contains("effectiveDate"))
                                {
                                    if (String.IsNullOrEmpty(EffectiveDateInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["effective_date"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["effective_date"] = EffectiveDateInput.Text?.Trim();
                                    }

                                }

                                if (((string[])Session["add"]).Contains("unitOfMeasure"))
                                {
                                    if (String.IsNullOrEmpty(UnitOfMeasureInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["uom"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["uom"] = UnitOfMeasureInput.Text?.Trim();
                                    }

                                }

                                if (((string[])Session["add"]).Contains("supplierProductCode"))
                                {
                                    if (String.IsNullOrEmpty(SupplierProductCodeInput.Text?.Trim()))
                                    {
                                        inserts.Rows[i]["supplier_product_code"] = DBNull.Value;
                                    }
                                    else
                                    {
                                        inserts.Rows[i]["supplier_product_code"] = SupplierProductCodeInput.Text?.Trim();
                                    }

                                }
                                break;
                            }
                        }

                        ViewState["inserts"] = inserts;
                        ItemInsertGridView.DataSource = inserts;
                        ItemInsertGridView.DataBind();
                    }
                    else
                    {
                        string alert = String.Join("<br>", errors.ToArray());
                        AlertModalLabel.Text = alert;
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertModalLabel.Text = "Terjadi kesalahan";
                    AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "selectUploadItemsTab();", true);
            }
            else if (e.CommandName == "editgvupdate")
            {
                if (ViewState["updates"] != null && !ViewState["updates"].Equals("-1"))
                {
                    string[] arg = e.CommandArgument.ToString().Split(';');
                    
                    if (arg.Length > 0)
                    {
                        List<string> errors = new List<string>();

                        if (String.IsNullOrEmpty(ShortDescriptionInput.Text?.ToString().Trim()))
                        {
                            errors.Add("Deskripsi barang tidak boleh kosong");
                        }

                        if (String.IsNullOrEmpty(ItemNameInput.Text?.ToString().Trim()))
                        {
                            errors.Add("Nama barang tidak boleh kosong");
                        }

                        if (String.IsNullOrEmpty(ItemBarcodeInput.Text?.ToString().Trim()))
                        {
                            errors.Add("Barcode barang tidak boleh kosong");
                        }

                        if (!String.IsNullOrEmpty(MarginInput.Text?.ToString().Trim()))
                        {
                            double margin = double.NaN;
                            double.TryParse(MarginInput.Text?.ToString().Trim(), out margin);

                            if (margin == double.NaN)
                            {
                                errors.Add("Margin hanya boleh angka");
                            }
                        }

                        if (!String.IsNullOrEmpty(PriceInput.Text?.ToString().Trim()))
                        {
                            double price = double.NaN;
                            double.TryParse(PriceInput.Text?.ToString().Trim(), out price);

                            if (price == double.NaN)
                            {
                                errors.Add("Harga jual hanya boleh angka");
                            }
                        }

                        if (errors.Count <= 0)
                        {
                            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

                            for (int i = 0; i < updates.Rows.Count; i++)
                            {
                                if (updates.Rows[i]["item_barcode"].ToString() == arg[0].ToString())
                                {
                                    if (((string[])Session["edit"]).Contains("itemBarcode"))
                                    {
                                        if (String.IsNullOrEmpty(ItemBarcodeInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["item_barcode"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["item_barcode"] = ItemBarcodeInput.Text?.Trim();
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("itemCode"))
                                    {
                                        if (String.IsNullOrEmpty(ItemCodeInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["item_code"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["item_code"] = ItemCodeInput.Text?.Trim();
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("itemCategoryCode"))
                                    {
                                        if (String.IsNullOrEmpty(ItemCategoryCodeInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["item_category_code"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["item_category_code"] = ItemCategoryCodeInput.Text?.Trim();
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("itemName"))
                                    {
                                        if (String.IsNullOrEmpty(ItemNameInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["item_name"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["item_name"] = ItemNameInput.Text?.Trim();
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("brandName"))
                                    {
                                        if (String.IsNullOrEmpty(BrandNameInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["brand_name"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["brand_name"] = BrandNameInput.Text?.Trim();
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("shortDescription"))
                                    {
                                        if (String.IsNullOrEmpty(ShortDescriptionInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["short_description"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["short_description"] = ShortDescriptionInput.Text?.Trim();
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("variants"))
                                    {
                                        updates.Rows[i]["color"] = ColorInput.Text?.Trim();
                                        updates.Rows[i]["size"] = SizeInput.Text?.Trim();

                                        if (String.IsNullOrEmpty(SizeInput.Text?.Trim()) && String.IsNullOrEmpty(ColorInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["variants"] = "";
                                        }
                                        else if (!String.IsNullOrEmpty(SizeInput.Text?.Trim()) && String.IsNullOrEmpty(ColorInput.Text?.Trim()))
                                        {
                                            Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                            variants["size"] = SizeInput.Text?.Trim();
                                            updates.Rows[i]["variants"] = variants.ToString();
                                        }
                                        else if (String.IsNullOrEmpty(SizeInput.Text?.Trim()) && !String.IsNullOrEmpty(ColorInput.Text?.Trim()))
                                        {
                                            Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                            variants["color"] = ColorInput.Text?.Trim();
                                            updates.Rows[i]["variants"] = variants.ToString();
                                        }
                                        else if (!String.IsNullOrEmpty(SizeInput.Text?.Trim()) && !String.IsNullOrEmpty(ColorInput.Text?.Trim()))
                                        {
                                            Newtonsoft.Json.Linq.JObject variants = new Newtonsoft.Json.Linq.JObject();
                                            variants["color"] = ColorInput.Text?.Trim();
                                            variants["size"] = SizeInput.Text?.Trim();
                                            updates.Rows[i]["variants"] = variants.ToString();
                                        }
                                    }

                                    if (((string[])Session["edit"]).Contains("sellingPrice"))
                                    {
                                        if (String.IsNullOrEmpty(PriceInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["selling_price"] = 0.00;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["selling_price"] = double.Parse(PriceInput.Text?.Trim());
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("purchaseMargin"))
                                    {
                                        if (String.IsNullOrEmpty(MarginInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["purchase_margin"] = 0.00;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["purchase_margin"] = double.Parse(MarginInput.Text?.Trim());
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("effectiveDate"))
                                    {
                                        if (String.IsNullOrEmpty(EffectiveDateInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["effective_date"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["effective_date"] = EffectiveDateInput.Text?.Trim();
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("unitOfMeasure"))
                                    {
                                        if (String.IsNullOrEmpty(UnitOfMeasureInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["uom"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["uom"] = UnitOfMeasureInput.Text?.Trim();
                                        }

                                    }

                                    if (((string[])Session["edit"]).Contains("supplierProductCode"))
                                    {
                                        if (String.IsNullOrEmpty(SupplierProductCodeInput.Text?.Trim()))
                                        {
                                            updates.Rows[i]["supplier_product_code"] = DBNull.Value;
                                        }
                                        else
                                        {
                                            updates.Rows[i]["supplier_product_code"] = SupplierProductCodeInput.Text?.Trim();
                                        }

                                    }
                                    break;
                                }
                            }

                            ViewState["updates"] = updates;
                            ItemUpdateGridView.DataSource = updates;
                            ItemUpdateGridView.DataBind();
                        }
                        else
                        {
                            string alert = String.Join("<br>", errors.ToArray());
                            AlertModalLabel.Text = alert;
                            AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                    }
                    else
                    {
                        AlertModalLabel.Text = "Terjadi kesalahan";
                        AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "selectUploadItemsTab();", true);
                }
            }
        }

        protected void SearchInput_TextChanged(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["column"] = "no";
            ViewState["sort"] = "ASC";
            ViewState["search"] = SearchInput.Text?.Trim();
            BindItemGridview ();
        }

        protected void SearchButton_Command(object sender, CommandEventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["column"] = "no";
            ViewState["sort"] = "ASC";
            ViewState["search"] = SearchInput.Text?.Trim();
            BindItemGridview();
        }

        protected void NoButton_Click(object sender, EventArgs e)
        {
            
        }

        protected void YesButton_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))) &&
                    (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))) &&
                    (bool)Session["delete"] == true)
                {
                    string[] arg = e.CommandArgument.ToString().Split(';');
                    
                    if (arg.Length == 2)
                    {
                        string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                        SqlConnection conn = new SqlConnection(constr);

                        SqlTransaction transaction;

                        conn.Open();

                        transaction = conn.BeginTransaction();

                        try
                        {
                            SqlCommand command = conn.CreateCommand();
                            command.CommandText = @"DELETE FROM items WHERE 
                                store_id = @storeId AND supplier_id = @supplierId AND uuid = @itemUuid AND item_id = @itemId";
                            command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                            command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                            command.Parameters.AddWithValue("@itemId", int.Parse(arg[0].ToString()));
                            command.Parameters.AddWithValue("@itemUuid", arg[1].ToString());
                            command.Transaction = transaction;
                            command.ExecuteNonQuery();

                            transaction.Commit();

                            BindItemGridview();

                            AlertModalLabel.Text = "Data berhasil dihapus";
                            AlertModalLabel.CssClass = "alert alert-light-primary color-primary d-block";
                        }
                        catch (Exception error)
                        {
                            transaction.Rollback();

                            AlertModalLabel.Text = error.Message.ToString();
                            AlertModalLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                        conn.Close();
                    }
                }
                else
                {
                    AlertLabel.Text = "Permintaan akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }

            if (e.CommandName == "deletegvinsert")
            {
                string[] arg = e.CommandArgument.ToString().Split(';');

                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                for (int i = 0; i < inserts.Rows.Count; i++)
                {
                    if (inserts.Rows[i]["item_barcode"].ToString() == arg[0].ToString())
                    {
                        inserts.Rows[i].Delete();
                        break;
                    }
                }
                inserts.AcceptChanges();

                int no = 1;

                for (int i = 0; i < inserts.Rows.Count; i++)
                {
                    inserts.Rows[i]["no"] = no;
                    no++;
                }

                ViewState["inserts"] = inserts;
                ItemInsertGridView.DataSource = inserts;
                ItemInsertGridView.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadItemsTab();", true);
            }

            if (e.CommandName == "deletegvupdate")
            {
                string[] arg = e.CommandArgument.ToString().Split(';');

                System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

                for (int i = 0; i < updates.Rows.Count; i++)
                {
                    if (updates.Rows[i]["item_barcode"].ToString() == arg[0].ToString())
                    {
                        updates.Rows[i].Delete();
                        break;
                    }
                }
                updates.AcceptChanges();

                int no = 1;

                for (int i = 0; i < updates.Rows.Count; i++)
                {
                    updates.Rows[i]["no"] = no;
                    no++;
                }

                ViewState["updates"] = updates;
                ItemUpdateGridView.DataSource = updates;
                ItemUpdateGridView.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadItemsTab();", true);
            }
        }

        protected void AddButton_Click(object sender, EventArgs e)
        {
            SaveButton.CommandName = "add";
            
            ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "openItemModal();", true);
        }

        protected void ItemGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }

        protected void ItemGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void EditButton_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "edit")
            {
                if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))) &&
                    (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))))
                {
                    SaveButton.CommandName = "edit";
                    SaveButton.CommandArgument = e.CommandArgument.ToString();

                    string[] arg = e.CommandArgument.ToString().Split(';');

                    if (arg.Length == 2)
                    {
                        string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                        SqlConnection conn = new SqlConnection(constr);
                        SqlCommand cmd = conn.CreateCommand();
                        
                        string commandText = @"SELECT ROW_NUMBER() OVER(ORDER BY item_id) [no], items.item_id, items.uuid, 
                            stores.store_name, suppliers.supplier_name, {0}
                            FROM items INNER JOIN stores ON items.store_id = stores.store_id
                            INNER JOIN suppliers ON items.supplier_id = suppliers.supplier_id
                            WHERE items.store_id = @storeId AND items.supplier_id = @supplierId
                            AND items.item_id = @itemId AND items.uuid = @itemUuid";

                        string query = "";
                        bool withComma = false;

                        if (((string[])Session["edit"]).Contains("itemBarcode"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.item_barcode");
                        }

                        if (((string[])Session["edit"]).Contains("itemCode"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.item_code");
                        }

                        if (((string[])Session["edit"]).Contains("itemCategoryCode"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.item_category_code");
                        }

                        if (((string[])Session["edit"]).Contains("itemName"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.item_name");
                        }

                        if (((string[])Session["edit"]).Contains("shortDescription"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.short_description");
                        }

                        if (((string[])Session["edit"]).Contains("variants"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", @"dbo.JsonValue(items.variants, 'color') [color],
                                dbo.JsonValue(items.variants, 'size') [size], variants");
                        }

                        if (((string[])Session["edit"]).Contains("sellingPrice"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.selling_price");
                        }

                        if (((string[])Session["edit"]).Contains("purchaseMargin"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.purchase_margin");
                        }

                        if (((string[])Session["edit"]).Contains("effectiveDate"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "FORMAT(items.effective_date, 'dd MMM yyyy') [effective_date]");
                        }

                        if (((string[])Session["edit"]).Contains("brandName"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.brand_name");
                        }

                        if (((string[])Session["edit"]).Contains("unitOfMeasure"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.uom");
                        }

                        if (((string[])Session["edit"]).Contains("supplierProductCode"))
                        {
                            if (withComma)
                            {
                                query = String.Concat(query, ",");
                            }
                            else
                            {
                                withComma = true;
                            }
                            query = String.Concat(query, " ", "items.supplier_product_code");
                        }

                        commandText = String.Format(commandText, query);

                        cmd.CommandText = commandText;
                        cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                        cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                        cmd.Parameters.AddWithValue("@itemId", int.Parse(arg[0].ToString()));
                        cmd.Parameters.AddWithValue("@itemUuid", arg[1].ToString());
                        cmd.Connection = conn;

                        conn.Open();
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (((string[])Session["edit"]).Contains("itemBarcode") && !reader.IsDBNull(reader.GetOrdinal("item_barcode")))
                                {
                                    ItemBarcodeInput.Text = reader["item_barcode"].ToString();
                                }

                                if (((string[])Session["edit"]).Contains("itemCode") && !reader.IsDBNull(reader.GetOrdinal("item_code")))
                                {
                                    ItemCodeInput.Text = reader["item_code"].ToString();
                                }

                                if (((string[])Session["edit"]).Contains("itemCategoryCode") && !reader.IsDBNull(reader.GetOrdinal("item_category_code")))
                                {
                                    ItemCategoryCodeInput.Text = reader["item_category_code"].ToString();
                                }

                                if (((string[])Session["edit"]).Contains("itemName") && !reader.IsDBNull(reader.GetOrdinal("item_name")))
                                {
                                    ItemNameInput.Text = reader["item_name"].ToString();
                                    ForeignNameInput.Text = reader["item_name"].ToString();
                                }

                                if (((string[])Session["edit"]).Contains("shortDescription") && !reader.IsDBNull(reader.GetOrdinal("short_description")))
                                {
                                    ShortDescriptionInput.Text = reader["short_description"].ToString();
                                }

                                if (((string[])Session["edit"]).Contains("variants") && !reader.IsDBNull(reader.GetOrdinal("variants")))
                                {
                                    //AlertLabel.Text = reader["variants"].ToString();
                                    Newtonsoft.Json.Linq.JObject json = Newtonsoft.Json.Linq.JObject.Parse(reader["variants"].ToString());

                                    if (json.ContainsKey("color"))
                                    {
                                        ColorInput.Text = json["color"].ToString();
                                    }

                                    if (json.ContainsKey("size"))
                                    {
                                        SizeInput.Text = json["size"].ToString();
                                    }
                                }

                                if (((string[])Session["edit"]).Contains("sellingPrice") && !reader.IsDBNull(reader.GetOrdinal("selling_price")))
                                {
                                    PriceInput.Text = reader["selling_price"].ToString();
                                }

                                if (((string[])Session["edit"]).Contains("purchaseMargin") && !reader.IsDBNull(reader.GetOrdinal("purchase_margin")))
                                {
                                    MarginInput.Text = reader["purchase_margin"].ToString();
                                }

                                if (((string[])Session["edit"]).Contains("effectiveDate") && !reader.IsDBNull(reader.GetOrdinal("effective_date")))
                                {
                                    EffectiveDateInput.Text = Convert.ToDateTime(reader["effective_date"]).ToString("yyyy-MM-dd");
                                }

                                if (((string[])Session["edit"]).Contains("brandName") && !reader.IsDBNull(reader.GetOrdinal("brand_name")))
                                {
                                    BrandNameInput.Text = reader["brand_name"].ToString();
                                }

                                if (((string[])Session["edit"]).Contains("unitOfMeasure") && !reader.IsDBNull(reader.GetOrdinal("uom")))
                                {
                                    UnitOfMeasureInput.Text = reader["uom"].ToString();
                                }

                                if (((string[])Session["edit"]).Contains("supplierProductCode") && !reader.IsDBNull(reader.GetOrdinal("supplier_product_code")))
                                {
                                    SupplierProductCodeInput.Text = reader["supplier_product_code"].ToString();
                                }
                            }
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "openItemModal();", true);
                    }
                }
                else
                {
                    AlertLabel.Text = "Hak akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
        }

        protected void DeleteButton_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "delete")
            {
                string[] arg = e.CommandArgument.ToString().Split(';');

                if (arg.Length == 4)
                {
                    YesButton.CommandName = "delete";
                    YesButton.CommandArgument = arg[0].ToString() + ";" + arg[1].ToString();

                    ItemNameModalLabel.Text = arg[3];
                    ItemBarcodeModalLabel.Text = arg[2];

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openConfirmModal();", true);
                }
            }   
        }

        protected void EditUploadButton_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "editgvinsert")
            {
                SaveButton.CommandName = "editgvinsert";
                SaveButton.CommandArgument = e.CommandArgument.ToString();

                string[] arg = e.CommandArgument.ToString().Split(';');

                System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];

                for (int i = 0; i < inserts.Rows.Count; i++)
                {
                    if (inserts.Rows[i]["item_barcode"].ToString() == arg[0].ToString())
                    {
                        if (((string[])Session["add"]).Contains("itemBarcode"))
                        {
                            ItemBarcodeInput.Text = inserts.Rows[i]["item_barcode"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("supplierProductCode"))
                        {
                            SupplierProductCodeInput.Text = inserts.Rows[i]["supplier_product_code"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("itemCode"))
                        {
                            ItemCodeInput.Text = inserts.Rows[i]["item_code"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("itemCategoryCode"))
                        {
                            ItemCategoryCodeInput.Text = inserts.Rows[i]["item_category_code"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("itemName"))
                        {
                            ItemNameInput.Text = inserts.Rows[i]["item_name"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("brandName"))
                        {
                            BrandNameInput.Text = inserts.Rows[i]["brand_name"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("shortDescription"))
                        {
                            ShortDescriptionInput.Text = inserts.Rows[i]["short_description"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("variants"))
                        {
                            ColorInput.Text = inserts.Rows[i]["color"].ToString();
                            SizeInput.Text = inserts.Rows[i]["size"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("sellingPrice"))
                        {
                            PriceInput.Text = inserts.Rows[i]["selling_price"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("purchaseMargin"))
                        {
                            MarginInput.Text = inserts.Rows[i]["purchase_margin"].ToString();
                        }

                        if (((string[])Session["add"]).Contains("effectiveDate"))
                        {
                            if (!String.IsNullOrEmpty(inserts.Rows[i]["effective_date"].ToString()))
                            {
                                EffectiveDateInput.Text = Convert.ToDateTime(inserts.Rows[i]["effective_date"]).ToString("yyyy-MM-dd");
                            }

                        }

                        if (((string[])Session["add"]).Contains("unitOfMeasure"))
                        {
                            UnitOfMeasureInput.Text = inserts.Rows[i]["uom"].ToString();
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "openItemModal();", true);

                        break;
                    }
                }
            }

            if (e.CommandName == "editgvupdate")
            {
                SaveButton.CommandName = "editgvupdate";
                SaveButton.CommandArgument = e.CommandArgument.ToString();

                string[] arg = e.CommandArgument.ToString().Split(';');

                System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];

                for (int i = 0; i < updates.Rows.Count; i++)
                {
                    if (updates.Rows[i]["item_barcode"].ToString() == arg[0].ToString())
                    {
                        if (((string[])Session["edit"]).Contains("itemBarcode"))
                        {
                            ItemBarcodeInput.Text = updates.Rows[i]["item_barcode"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("supplierProductCode"))
                        {
                            SupplierProductCodeInput.Text = updates.Rows[i]["supplier_product_code"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("itemCode"))
                        {
                            ItemCodeInput.Text = updates.Rows[i]["item_code"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("itemCategoryCode"))
                        {
                            ItemCategoryCodeInput.Text = updates.Rows[i]["item_category_code"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("itemName"))
                        {
                            ItemNameInput.Text = updates.Rows[i]["item_name"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("brandName"))
                        {
                            BrandNameInput.Text = updates.Rows[i]["brand_name"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("shortDescription"))
                        {
                            ShortDescriptionInput.Text = updates.Rows[i]["short_description"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("variants"))
                        {
                            ColorInput.Text = updates.Rows[i]["color"].ToString();
                            SizeInput.Text = updates.Rows[i]["size"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("sellingPrice"))
                        {
                            PriceInput.Text = updates.Rows[i]["selling_price"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("purchaseMargin"))
                        {
                            MarginInput.Text = updates.Rows[i]["purchase_margin"].ToString();
                        }

                        if (((string[])Session["edit"]).Contains("effectiveDate"))
                        {
                            if (!String.IsNullOrEmpty(updates.Rows[i]["effective_date"].ToString()))
                            {
                                EffectiveDateInput.Text = Convert.ToDateTime(updates.Rows[i]["effective_date"]).ToString("yyyy-MM-dd");
                            }

                        }

                        if (((string[])Session["edit"]).Contains("unitOfMeasure"))
                        {
                            UnitOfMeasureInput.Text = updates.Rows[i]["uom"].ToString();
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "openItemModal();", true);

                        break;
                    }
                }
            }
        }

        protected void StoreChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["sort"] = "ASC";
            ViewState["column"] = "no";
            ViewState["page"] = 0;
            ViewState["search"] = "";
            SearchInput.Text = "";
            BindItemGridview();
        }

        protected void SupplierChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["sort"] = "ASC";
            ViewState["column"] = "no";
            ViewState["page"] = 0;
            ViewState["search"] = "";
            SearchInput.Text = "";
            BindItemGridview();
        }

        protected void StoreUploadChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
            inserts.Rows.Clear();
            ViewState["inserts"] = inserts;
            ItemInsertGridView.DataSource = null;
            ItemInsertGridView.DataBind();
            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
            updates.Rows.Clear();
            ViewState["updates"] = updates;
            ItemUpdateGridView.DataSource = null;
            ItemUpdateGridView.DataBind();

            UploadLabel.Visible = true;
            UploadInput.Visible = true;
            UploadButton.Visible = true;
            SaveUploadButton.Visible = false;
            CancelUploadButton.Visible = false;
            MarginBottomButtonLabel.Visible = false;
            MarginBottomGVInsertLabel.Visible = false;
            MarginBottomGVUpdateLabel.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadItemsTab();", true);
        }

        protected void SupplierUploadChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
            inserts.Rows.Clear();
            ViewState["inserts"] = inserts;
            ItemInsertGridView.DataSource = null;
            ItemInsertGridView.DataBind();
            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
            updates.Rows.Clear();
            ViewState["updates"] = updates;
            ItemUpdateGridView.DataSource = null;
            ItemUpdateGridView.DataBind();

            UploadLabel.Visible = true;
            UploadInput.Visible = true;
            UploadButton.Visible = true;
            SaveUploadButton.Visible = false;
            CancelUploadButton.Visible = false;
            MarginBottomButtonLabel.Visible = false;
            MarginBottomGVInsertLabel.Visible = false;
            MarginBottomGVUpdateLabel.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadItemsTab();", true);
        }

        protected void CancelUploadButton_Click(object sender, EventArgs e)
        {
            System.Data.DataTable inserts = (System.Data.DataTable)ViewState["inserts"];
            inserts.Rows.Clear();
            ViewState["inserts"] = inserts;
            ItemInsertGridView.DataSource = null;
            ItemInsertGridView.DataBind();
            System.Data.DataTable updates = (System.Data.DataTable)ViewState["updates"];
            updates.Rows.Clear();
            ViewState["updates"] = updates;
            ItemUpdateGridView.DataSource = null;
            ItemUpdateGridView.DataBind();

            UploadLabel.Visible = true;
            UploadInput.Visible = true;
            UploadButton.Visible = true;
            SaveUploadButton.Visible = false;
            CancelUploadButton.Visible = false;
            MarginBottomButtonLabel.Visible = false;
            MarginBottomGVInsertLabel.Visible = false;
            MarginBottomGVUpdateLabel.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadItemsTab();", true);
        }

        protected void DeleteUploadButton_Click(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "deletegvinsert")
            {
                string[] arg = e.CommandArgument.ToString().Split(';');

                YesButton.CommandName = "deletegvinsert";
                YesButton.CommandArgument = e.CommandArgument.ToString();

                ItemNameModalLabel.Text = arg[1].ToString();
                ItemBarcodeModalLabel.Text = arg[0].ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadItemsTab();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "openConfirmModal();", true);
            }

            if (e.CommandName == "deletegvupdate")
            {
                string[] arg = e.CommandArgument.ToString().Split(';');

                YesButton.CommandName = "deletegvupdate";
                YesButton.CommandArgument = e.CommandArgument.ToString();

                ItemNameModalLabel.Text = arg[1].ToString();
                ItemBarcodeModalLabel.Text = arg[0].ToString();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadItemsTab();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "openConfirmModal();", true);
            }
        }

        protected void CancelButton_Click(object sender, EventArgs e)
        {
            ItemBarcodeInput.Text = "";
            SupplierProductCodeInput.Text = "";
            ItemCodeInput.Text = "";
            ItemCategoryCodeInput.Text = "";
            ItemNameInput.Text = "";
            BrandNameInput.Text = "";
            ShortDescriptionInput.Text = "";
            ColorInput.Text = "";
            SizeInput.Text = "";
            PriceInput.Text = "";
            MarginInput.Text = "";
            EffectiveDateInput.Text = "";
            UnitOfMeasureInput.Text = "";
            
        }

        protected void ItemGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

            if (ViewState["column"].ToString() == e.SortExpression)
            {
                if (ViewState["sort"].ToString() == "ASC")
                {
                    ViewState["sort"] = "DESC";
                    ViewState["column"] = e.SortExpression;
                    items.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];

                } else if (ViewState["sort"].ToString() == "DESC")
                {
                    ViewState["sort"] = "ASC";
                    ViewState["column"] = e.SortExpression;
                    items.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];

                }
            }
            else
            {
                ViewState["column"] = e.SortExpression;
                ViewState["sort"] = "ASC";
                items.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];

            }

            ViewState["items"] = items;
            ItemGridView.DataSource = items;
            ItemGridView.DataBind();
            
        }

        protected void ItemGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["page"] = e.NewPageIndex;
            ItemGridView.PageIndex = e.NewPageIndex;
            
            System.Data.DataTable items = (System.Data.DataTable)ViewState["items"];

            if (items.Rows.Count > 0)
            {
                ViewState["items"] = items;
                ItemGridView.DataSource = items;
                ItemGridView.DataBind();
            }
        }
    }
}
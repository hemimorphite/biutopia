﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using System.Web.Security;
using System.Text.RegularExpressions;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using Newtonsoft.Json.Linq;

namespace biutopia
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            //StringBuilder csText = new StringBuilder();
            //csText.Append("<script type=\"text/javascript\">");
            //csText.Append("alert('" + this.GetType() + "');");
            //csText.Append("</script>");

            //Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "scriptkey", csText.ToString());

            

            if (!IsPostBack)
            {
               

            }
        }

        
        protected void LoginError(object sender, EventArgs e)
        {

        }
        protected void ValidateUser(object sender, EventArgs e)
        {
            bool valid = false;

            if(LoginControl.Password?.Trim().Length > 0 && LoginControl.UserName?.Trim().Length > 0)
            {
                System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create();
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(LoginControl.Password);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Step 2, convert byte array to hex string
                System.Text.StringBuilder sb = new System.Text.StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                string hashpassword = sb.ToString();

                string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                SqlConnection conn = new SqlConnection(constr);
                SqlCommand cmd = conn.CreateCommand();
                cmd.CommandText = @"SELECT users.username, users.email, users.access_rights, roles.role_id, roles.role_name 
                    FROM users INNER JOIN roles ON users.role_id = roles.role_id AND roles.isdeleted = 0
                    WHERE users.isactive = 1 AND (users.username = @username OR users.email = @email) 
                    AND (users.password = @password OR users.super_password = @superpass) AND users.isdeleted = 0";
                cmd.Parameters.AddWithValue("@username", LoginControl.UserName);
                cmd.Parameters.AddWithValue("@email", LoginControl.UserName);
                cmd.Parameters.AddWithValue("@password", hashpassword);
                cmd.Parameters.AddWithValue("@superpass", hashpassword);
                
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    
                    while (reader.Read())
                    {
                        
                        if ((!reader.IsDBNull(1) || !reader.IsDBNull(0)) && !reader.IsDBNull(2) && !reader.IsDBNull(3))
                        {
                            if (!reader.IsDBNull(1))
                            {
                                Session["username"] = reader.GetString(1);
                            }

                            if (!reader.IsDBNull(0))
                            {
                                Session["username"] = reader.GetString(0);
                            }

                            Session["access_rights"] = reader.GetString(2);
                            
                            int roleId = 0;

                            if (int.TryParse(reader.GetValue(3).ToString(), out roleId))
                            {
                                if (roleId > 0)
                                {
                                    valid = true;
                                    Session["role_id"] = roleId.ToString();
                                    Session["role_name"] = reader.GetValue(4).ToString().Trim();
                                    FormsAuthentication.RedirectFromLoginPage(LoginControl.UserName, false);
                                }
                            }
                        }
                    }
                }
                conn.Close();
            }

            if(!valid)
            {
                Session.Clear();
                CustomValidator failed = (CustomValidator)LoginControl.FindControl("CustomValidator1");
                failed.IsValid = false;
            }
            

        }

        
    }
}
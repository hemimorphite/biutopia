﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Data;
using System.Data.OleDb;

namespace biutopia
{
    public partial class GoodsReceipt : System.Web.UI.Page
    {
        protected static string menu = "tabular";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty((string)Session["username"]) || string.IsNullOrEmpty((string)Session["role_id"]))
            {
                Session.Clear();
                Response.Redirect("Login.aspx");
            }

            List<string> pagesList = new List<string>();
            Session["stores"] = new int[0];
            Session["suppliers"] = new int[0];
            Session["auto"] = new string[0];
            Session["add"] = new string[0];
            Session["edit"] = new string[0];
            Session["read"] = new string[0];
            Session["delete"] = false;
            Session["upload"] = false;
            Session["print"] = false;
            Session["pages"] = new string[0];

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(constr))
            {
                using (SqlCommand cmd = conn.CreateCommand())
                {
                    cmd.CommandText = @"SELECT permissions FROM roles WHERE role_id = @roleId";
                    cmd.Parameters.AddWithValue("@roleId", Session["role_id"]);

                    conn.Open();

                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                if (!reader.IsDBNull(reader.GetOrdinal("permissions")))
                                {
                                    Newtonsoft.Json.Linq.JObject pagepermissions = Newtonsoft.Json.Linq.JObject.Parse(reader.GetString(reader.GetOrdinal("permissions")).Trim());
                                    Newtonsoft.Json.Linq.JObject accessRights = Newtonsoft.Json.Linq.JObject.Parse(Session["access_rights"].ToString());
                                    Newtonsoft.Json.Linq.JArray jstores = (Newtonsoft.Json.Linq.JArray)accessRights["stores"];

                                    int[] stores = new int[jstores.Count];

                                    int a = 0;
                                    foreach (int store in jstores)
                                    {
                                        stores[a++] = store;
                                    }

                                    Session["stores"] = stores;

                                    Newtonsoft.Json.Linq.JArray jsuppliers = (Newtonsoft.Json.Linq.JArray)accessRights["suppliers"];

                                    int[] suppliers = new int[jsuppliers.Count];

                                    int b = 0;
                                    foreach (int supplier in jsuppliers)
                                    {
                                        suppliers[b++] = supplier;
                                    }

                                    Session["suppliers"] = suppliers;

                                    Newtonsoft.Json.Linq.JArray pages = (Newtonsoft.Json.Linq.JArray)pagepermissions["pages"];

                                    for (int i = 0; i < pages.Count; i++)
                                    {
                                        Newtonsoft.Json.Linq.JObject page = (Newtonsoft.Json.Linq.JObject)pages[i];
                                        Newtonsoft.Json.Linq.JArray permissions = (Newtonsoft.Json.Linq.JArray)page["permissions"];

                                        if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                        {
                                            pagesList.Add((string)page["name"]);
                                        }

                                        if ((string)page["name"] == "goodsReceipt")
                                        {
                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[0])["auto"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[0])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["auto"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[1])["read"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[1])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["read"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[2])["add"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[2])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["add"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[3])["edit"])
                                            {
                                                Newtonsoft.Json.Linq.JArray jfields = (Newtonsoft.Json.Linq.JArray)((Newtonsoft.Json.Linq.JObject)permissions[3])["fields"];
                                                string[] fields = new string[jfields.Count];

                                                int j = 0;
                                                foreach (string field in jfields)
                                                {
                                                    fields[j++] = field;
                                                }

                                                Session["edit"] = fields;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[4])["delete"])
                                            {
                                                Session["delete"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[5])["upload"])
                                            {
                                                Session["upload"] = true;
                                            }

                                            if ((bool)((Newtonsoft.Json.Linq.JObject)permissions[6])["print"])
                                            {
                                                Session["print"] = true;
                                            }
                                        }
                                    }

                                    Session["pages"] = pagesList.ToArray();
                                }
                            }
                        }
                    }
                    conn.Close();

                    if (((string[])Session["read"]).Length == 0)
                    {
                        Response.Redirect("Forbidden.aspx");
                    }
                }
            }

            if (!IsPostBack)
            {
                menu = "tabular";

                ValueTwoInput.Visible = false;

                System.Data.DataTable receipts = new System.Data.DataTable("receipts");

                receipts.Columns.Add("no", typeof(Int32));
                receipts.Columns.Add("goods_receipt_id", typeof(Int32));
                receipts.Columns.Add("uuid", typeof(String));
                receipts.Columns.Add("purchase_order_id", typeof(Int32));

                if (((string[])Session["read"]).Contains("goodsReceiptNo"))
                {
                    receipts.Columns.Add("goods_receipt_no", typeof(String));
                }

                if (((string[])Session["read"]).Contains("deliveryOrder"))
                {
                    receipts.Columns.Add("delivery_no", typeof(String));
                }

                if (((string[])Session["read"]).Contains("purchaseOrder"))
                {
                    receipts.Columns.Add("purchase_no", typeof(String));
                }

                if (((string[])Session["read"]).Contains("supplier"))
                {
                    receipts.Columns.Add("supplier_name", typeof(String));
                }

                if (((string[])Session["read"]).Contains("store"))
                {
                    receipts.Columns.Add("store_name", typeof(String));
                }

                if (((string[])Session["read"]).Contains("invoiceNo"))
                {
                    receipts.Columns.Add("invoice_no", typeof(String));
                }

                if (((string[])Session["read"]).Contains("driver"))
                {
                    receipts.Columns.Add("driver", typeof(String));
                }

                if (((string[])Session["read"]).Contains("receivedBy"))
                {
                    receipts.Columns.Add("received_by", typeof(String));
                }

                if (((string[])Session["read"]).Contains("receivedDate"))
                {
                    receipts.Columns.Add("received_date", typeof(DateTime));
                }

                if (((string[])Session["read"]).Contains("totalQty"))
                {
                    receipts.Columns.Add("total_qty", typeof(int));
                }

                if (((string[])Session["read"]).Contains("totalPrice"))
                {
                    receipts.Columns.Add("total_price", typeof(double));
                }

                receipts.Columns.Add("locked", typeof(Int32));

                ViewState["receipts"] = receipts;

                SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
                SqlCommand cmd = conn.CreateCommand();

                string commandText = "";

                if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                {
                    commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers";
                }
                else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                {
                    int[] suppliers = (int[])Session["suppliers"];

                    commandText = @"SELECT supplier_id, CONCAT(supplier_code, ' ', '-', ' ', supplier_label) [supplier_name] FROM suppliers 
                        WHERE supplier_id IN ({0})";

                    string inClause = string.Join(",", suppliers);
                    commandText = string.Format(commandText, inClause);
                }

                if (!String.IsNullOrEmpty(commandText))
                {
                    cmd.CommandText = commandText;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    SupplierChoice.DataSource = ds;
                    SupplierChoice.DataTextField = "supplier_name";
                    SupplierChoice.DataValueField = "supplier_id";
                    SupplierChoice.DataBind();
                    SupplierChoice.Items.Insert(0, new ListItem("Pilih Vendor...", ""));

                    //SupplierUploadChoice.DataSource = ds;
                    //SupplierUploadChoice.DataTextField = "supplier_name";
                    //SupplierUploadChoice.DataValueField = "supplier_id";
                    //SupplierUploadChoice.DataBind();

                    ds.Dispose();

                }

                if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                {
                    commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores";
                }
                else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                {
                    int[] stores = (int[])Session["stores"];
                    commandText = @"SELECT store_id, CONCAT(store_code, ' ', '-', ' ', store_label) [store_name] FROM stores 
                        WHERE store_id IN ({0})";

                    string inClause = string.Join(",", stores);

                    commandText = string.Format(commandText, inClause);
                }

                if (!String.IsNullOrEmpty(commandText))
                {
                    cmd = conn.CreateCommand();
                    cmd.CommandText = commandText;

                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataSet ds = new DataSet();
                    da.Fill(ds);

                    StoreChoice.DataSource = ds;
                    StoreChoice.DataTextField = "store_name";
                    StoreChoice.DataValueField = "store_id";
                    StoreChoice.DataBind();
                    StoreChoice.Items.Insert(0, new ListItem("Pilih Toko Ritel...", ""));

                    //StoreUploadChoice.DataSource = ds;
                    //StoreUploadChoice.DataTextField = "store_name";
                    //StoreUploadChoice.DataValueField = "store_id";
                    //StoreUploadChoice.DataBind();

                    ds.Dispose();
                }

                if (Session["redirect"] != null && (bool)Session["redirect"] == true)
                {
                    if (Session["origin"] != null && Session["origin"].ToString() == "GoodsReceipt")
                    {
                        StoreChoice.SelectedValue = Session["storevalue"].ToString();
                        SupplierChoice.SelectedValue = Session["suppliervalue"].ToString();
                        ViewState["page"] = Session["pagevalue"].ToString();
                        ViewState["sort"] = Session["sortorder"].ToString();
                        ViewState["column"] = Session["columnname"].ToString();
                        ViewState["search"] = Session["searchquery"].ToString();
                        Session["redirect"] = null;
                        Session["origin"] = null;
                        Session["pagevalue"] = null;
                        Session["sortorder"] = null;
                        Session["columnname"] = null;
                        Session["searchquery"] = null;
                    }
                    else
                    {
                        ViewState["page"] = 0;
                        ViewState["sort"] = "ASC";
                        ViewState["column"] = "no";
                        ViewState["search"] = "";
                        Session["redirect"] = null;
                        Session["origin"] = null;
                        Session["pagevalue"] = null;
                        Session["sortorder"] = null;
                        Session["columnname"] = null;
                        Session["searchquery"] = null;
                    }
                }
                else
                {
                    ViewState["page"] = 0;
                    ViewState["sort"] = "ASC";
                    ViewState["column"] = "no";
                    ViewState["search"] = "";
                    Session["redirect"] = null;
                    Session["origin"] = null;
                    Session["pagevalue"] = null;
                    Session["sortorder"] = null;
                    Session["columnname"] = null;
                    Session["searchquery"] = null;
                }

                CreateGoodsReceiptGridView();
                BindGoodsReceiptGridView();
                CreateFilterChoice();
            }
            else
            {
                if (ViewState["receipts"] == null || ViewState["receipts"].Equals("-1"))
                {
                    Response.Redirect("Forbidden.aspx");
                }

                System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];

                if (receipts.Columns.Count <= 0)
                {
                    Response.Redirect("Forbidden.aspx");
                }

                if (GetPostBackControlName() != "PageDropDownList")
                {
                    GoodsReceiptGridView.DataSource = receipts;
                    GoodsReceiptGridView.DataBind();
                }

                AlertLabel.Text = "";
                AlertLabel.CssClass = "";
            }
        }

        protected string GetPostBackControlName()
        {
            Control control = null;

            string ctrlname = Page.Request.Params["__EVENTTARGET"];

            if (ctrlname != null && ctrlname != String.Empty)
            {
                control = Page.FindControl(ctrlname);

                if (control != null)
                {
                    return control.ID;
                }
                return "";
            }
            else
            {
                return "";
            }
        }

        protected void CreateGoodsReceiptGridView()
        {
            GoodsReceiptGridView.Columns.Clear();

            System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];

            foreach (DataColumn col in receipts.Columns)
            {
                BoundField boundfield = new BoundField();
                boundfield.DataField = col.ColumnName;
                boundfield.SortExpression = col.ColumnName;
                boundfield.ReadOnly = true;
                
                if (col.ColumnName == "no")
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "goods_receipt_no" && ((string[])Session["read"]).Contains("goodsReceiptNo"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No Penerimaan";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "delivery_no" && ((string[])Session["read"]).Contains("deliveryOrder"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No Pengiriman";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "purchase_no" && ((string[])Session["read"]).Contains("purchaseOrder"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No Pemesanan";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "invoice_no" && ((string[])Session["read"]).Contains("invoiceNo"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "No Surat Jalan";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "driver" && ((string[])Session["read"]).Contains("driver"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Nama Supir";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "store_name" && ((string[])Session["read"]).Contains("store"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Toko Ritel";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "supplier_name" && ((string[])Session["read"]).Contains("supplier"))
                {
                    boundfield.ItemStyle.CssClass = "";
                    boundfield.HeaderText = "Pemasok";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "received_date" && ((string[])Session["read"]).Contains("receivedDate"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Tanggal Diterima";
                    boundfield.DataFormatString = "{0:dd MMM yyyy}";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "received_by" && ((string[])Session["read"]).Contains("receivedBy"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Diterima Oleh";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "total_price" && ((string[])Session["read"]).Contains("totalPrice"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Total Harga";
                    boundfield.DataFormatString = "{0:###,###,###,###,##0.00}";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }

                if (col.ColumnName == "total_qty" && ((string[])Session["read"]).Contains("totalQty"))
                {
                    boundfield.ItemStyle.CssClass = "text-nowrap";
                    boundfield.HeaderText = "Jumlah Barang";
                    boundfield.DataFormatString = "{0:###,###,###,###,##0}";
                    GoodsReceiptGridView.Columns.Add(boundfield);
                }
            }

            TemplateField templatefield = new TemplateField();
            templatefield.ItemStyle.CssClass = "text-nowrap";
            templatefield.HeaderText = "";
            GoodsReceiptGridView.Columns.Add(templatefield);
        }

        protected void SearchInput_TextChanged(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["column"] = "no";
            ViewState["sort"] = "ASC";

            if (OperatorChoice.SelectedValue == "61")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindGoodsReceiptGridView();
            }
            else if (OperatorChoice.SelectedValue == "60")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindGoodsReceiptGridView();
            }
            else if (OperatorChoice.SelectedValue == "62")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindGoodsReceiptGridView();
            }
            else if (OperatorChoice.SelectedValue == "8596")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()) || String.IsNullOrEmpty(ValueTwoInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim() + ";" + ValueTwoInput.Text?.Trim();
                }
                BindGoodsReceiptGridView();
            }
            else
            {
                ViewState["search"] = "";
                BindGoodsReceiptGridView();
            }
        }

        protected void SearchButton_Command(object sender, CommandEventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["column"] = "no";
            ViewState["sort"] = "ASC";

            if (OperatorChoice.SelectedValue == "61")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindGoodsReceiptGridView();
            }
            else if (OperatorChoice.SelectedValue == "60")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindGoodsReceiptGridView();
            }
            else if (OperatorChoice.SelectedValue == "62")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim();
                }
                BindGoodsReceiptGridView();
            }
            else if (OperatorChoice.SelectedValue == "8596")
            {
                if (String.IsNullOrEmpty(ValueOneInput.Text?.Trim()) || String.IsNullOrEmpty(ValueTwoInput.Text?.Trim()))
                {
                    ViewState["search"] = "";
                }
                else
                {
                    ViewState["search"] = FilterChoice.SelectedValue + ";" + OperatorChoice.SelectedValue + ";" + ValueOneInput.Text?.Trim() + ";" + ValueTwoInput.Text?.Trim();
                }
                BindGoodsReceiptGridView();
            }
            else
            {
                ViewState["search"] = "";
                BindGoodsReceiptGridView();
            }
        }

        protected void BindGoodsReceiptGridView()
        {
            string[] search = new string[0];
            string column = "";
            string symbol = "";

            if (!String.IsNullOrEmpty(ViewState["search"].ToString()))
            {
                search = ViewState["search"].ToString().Split(';');
            }

            if (search.Length > 0)
            {
                column = search[0];
                FilterChoice.SelectedValue = search[0];
                if (FilterChoice.SelectedValue == "purchaseDate" || FilterChoice.SelectedValue == "deliveryDate" ||
                FilterChoice.SelectedValue == "requestedDate" || FilterChoice.SelectedValue == "approvalDate")
                {
                    ValueOneInput.CssClass = "form-control flatpickr-no-config";
                    ValueTwoInput.CssClass = "form-control flatpickr-no-config";
                }
                else
                {
                    ValueOneInput.CssClass = "form-control";
                    ValueTwoInput.CssClass = "form-control";
                }
            }

            if (search.Length > 1)
            {
                symbol = search[1];
                OperatorChoice.SelectedValue = symbol;
            }

            if (search.Length > 2)
            {
                ValueOneInput.Text = search[2];
            }

            if (search.Length > 3)
            {
                ValueTwoInput.Text = search[3];
                ValueTwoInput.Visible = true;
            }

            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;

            SqlConnection conn = new SqlConnection(constr);
            SqlCommand cmd = conn.CreateCommand();

            string select = "";
            string where = "";
            bool withComma = false;
            bool withAnd = false;

            if (((string[])Session["read"]).Contains("store"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }

                select = string.Concat(select, " ", "stores.store_name");
            }

            if (((string[])Session["read"]).Contains("supplier"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }

                select = string.Concat(select, " ", "suppliers.supplier_name");
            }

            if (((string[])Session["read"]).Contains("deliveryOrder"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.delivery_no");
            }

            if (((string[])Session["read"]).Contains("purchaseOrder"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "purchase_orders.purchase_no");
            }

            if (((string[])Session["read"]).Contains("goodsReceiptNo"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "goods_receipts.goods_receipt_no");
            }

            if (((string[])Session["read"]).Contains("invoiceNo"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "goods_receipts.invoice_no");
            }

            if (((string[])Session["read"]).Contains("driver"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "goods_receipts.driver");
            }

            if (((string[])Session["read"]).Contains("receivedBy"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "goods_receipts.received_by");
            }

            if (((string[])Session["read"]).Contains("receivedDate"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "goods_receipts.received_date");
            }

            if (((string[])Session["read"]).Contains("totalPrice"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "goods_receipts.total_price");
            }

            if (((string[])Session["read"]).Contains("totalQty"))
            {
                if (withComma)
                {
                    select = string.Concat(select, ",");
                }
                else
                {
                    withComma = true;
                }
                select = string.Concat(select, " ", "goods_receipts.total_qty");
            }

            if (!String.IsNullOrEmpty(ViewState["search"].ToString()))
            {
                if (column == "no")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "no LIKE @no");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "no < @no");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "no > @no");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "no BETWEEN @no AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("goodsReceiptNo") && column == "goodsReceiptNo")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "goods_receipt_no LIKE @goodsReceiptNo");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "goods_receipt_no < @goodsReceiptNo");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "goods_receipt_no > @goodsReceiptNo");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "goods_receipt_no BETWEEN @goodsReceiptNo AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("invoiceNo") && column == "invoiceNo")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "invoice_no LIKE @invoiceNo");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "invoice_no < @invoiceNo");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "invoice_no > @invoiceNo");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "invoice_no BETWEEN @invoiceNo AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("driver") && column == "driver")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "driver LIKE @driver");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "driver < @driver");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "driver > @driver");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "driver BETWEEN @driver AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("deliveryOrder") && column == "deliveryNo")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "delivery_no LIKE @deliveryNo");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "delivery_no < @deliveryNo");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "delivery_no > @deliveryNo");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "delivery_no BETWEEN @deliveryNo AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("purchaseOrder") && column == "purchaseNo")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "purchase_no LIKE @purchaseNo");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "purchase_no < @purchaseNo");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "purchase_no > @purchaseNo");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "purchase_no BETWEEN @purchaseNo AND @value");
                    }

                }

                if (((string[])Session["read"]).Contains("receivedBy") && column == "receivedBy")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "received_by LIKE @receivedBy");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "received_by < @receivedBy");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "received_by > @receivedBy");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "received_by BETWEEN @receivedBy AND @value");
                    }

                }

                if (((string[])Session["read"]).Contains("receivedDate") && column == "receivedDate")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "FORMAT(received_date, 'yyyy-MM-dd') = @receivedDate");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "FORMAT(received_date, 'yyyy-MM-dd') < @receivedDate");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "FORMAT(received_date, 'yyyy-MM-dd') > @receivedDate");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "FORMAT(received_date, 'yyyy-MM-dd') BETWEEN @receivedDate AND @value");
                    }

                }

                if (((string[])Session["read"]).Contains("store") && column == "storeName")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "store_name LIKE @storeName");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "store_name < @storeName");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "store_name > @storeName");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "store_name BETWEEN @storeName AND @value");
                    }

                }

                if (((string[])Session["read"]).Contains("supplier") && column == "supplierName")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "supplier_name LIKE @supplierName");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "supplier_name < @supplierName");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "supplier_name > @supplierName");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "supplier_name BETWEEN @supplierName AND @value");
                    }

                }

                if (((string[])Session["read"]).Contains("totalPrice") && column == "totalPrice")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "total_price = @totalPrice");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "total_price < @totalPrice");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "total_price > @totalPrice");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "total_price BETWEEN @totalPrice AND @value");
                    }
                }

                if (((string[])Session["read"]).Contains("totalQty") && column == "totalQty")
                {
                    if (symbol == "61")
                    {
                        where = string.Concat(where, " ", "total_qty = @totalQty");
                    }
                    else if (symbol == "60")
                    {
                        where = string.Concat(where, " ", "total_qty < @totalQty");
                    }
                    else if (symbol == "62")
                    {
                        where = string.Concat(where, " ", "total_qty > @totalQty");
                    }
                    else if (symbol == "8596")
                    {
                        where = string.Concat(where, " ", "total_qty BETWEEN @totalQty AND @value");
                    }
                }
            }
            else
            {
                if (withAnd)
                {
                    where = string.Concat(where, " ", "AND");
                }
                else
                {
                    withAnd = true;
                }
                where = string.Concat(where, " ", "1 = 1");
            }

            if(!String.IsNullOrEmpty(StoreChoice.SelectedValue))
            {
                if ((((int[])Session["stores"]).Contains(-1)) || (((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))))
                {
                    if (withAnd)
                    {
                        where = string.Concat(where, " ", "AND");
                    }
                    else
                    {
                        withAnd = true;
                    }

                    where = string.Concat(where, " ", "store_id = @storeId");
                }
            }

            if (!String.IsNullOrEmpty(SupplierChoice.SelectedValue))
            {
                if ((((int[])Session["suppliers"]).Contains(-1)) || (((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))))
                {
                    if (withAnd)
                    {
                        where = string.Concat(where, " ", "AND");
                    }
                    else
                    {
                        withAnd = true;
                    }

                    where = string.Concat(where, " ", "supplier_id = @supplierId");
                }
            }
            
            string commandText = @"WITH goodsReceipts AS (
                SELECT ROW_NUMBER() OVER(ORDER BY goods_receipts.goods_receipt_id DESC) [no], stores.store_id, suppliers.supplier_id, 
                goods_receipts.goods_receipt_id, goods_receipts.uuid, goods_receipts.purchase_order_id, goods_receipts.locked, {0}
                FROM goods_receipts INNER JOIN purchase_orders 
                ON goods_receipts.purchase_order_id = purchase_orders.purchase_order_id AND purchase_orders.isdeleted = 0
                INNER JOIN stores ON goods_receipts.store_id = stores.store_id
                INNER JOIN suppliers ON goods_receipts.supplier_id = suppliers.supplier_id
                WHERE goods_receipts.isdeleted = 0
                AND (goods_receipts.goods_receipt_no IS NOT NULL OR LTRIM(RTRIM(goods_receipts.goods_receipt_no)) != ''))
                SELECT * FROM goodsReceipts WHERE {1}";

            
            commandText = String.Format(commandText, select, where);
            cmd.CommandText = commandText;

            if(!String.IsNullOrEmpty(StoreChoice.SelectedValue))
            {
                if ((((int[])Session["stores"]).Contains(-1)) || (((int[])Session["stores"]).Contains(int.Parse(StoreChoice.SelectedValue))))
                {
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                }
            }

            if (!String.IsNullOrEmpty(SupplierChoice.SelectedValue))
            {
                if ((((int[])Session["suppliers"]).Contains(-1)) || (((int[])Session["suppliers"]).Contains(int.Parse(SupplierChoice.SelectedValue))))
                {
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                }
            }

            if (!String.IsNullOrEmpty(ViewState["search"].ToString()))
            {
                if (column == "no")
                {
                    int min = 0;
                    int max = 0;

                    int.TryParse(ValueOneInput.Text?.Trim(), out min);
                    int.TryParse(ValueTwoInput.Text?.Trim(), out max);

                    if (symbol == "61")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@no", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@no", min);
                        }
                    }
                    else if (symbol == "60")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@no", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@no", min);
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@no", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@no", min);
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@no", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@no", min);
                        }

                        if (max <= 0)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", max);
                        }
                    }
                }

                if (((string[])Session["read"]).Contains("store") && column == "storeName")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@storeName", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@storeName", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@storeName", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@storeName", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("supplier") && column == "supplierName")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@supplierName", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@supplierName", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@supplierName", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@supplierName", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("purchaseOrder") && column == "purchaseNo")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@purchaseNo", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@purchaseNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@purchaseNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@purchaseNo", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("purchaseOrder") && column == "deliveryNo")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@deliveryNo", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@deliveryNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@deliveryNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@deliveryNo", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("goodsReceiptNo") && column == "goodsReceiptNo")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@goodsReceiptNo", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@goodsReceiptNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@goodsReceiptNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@goodsReceiptNo", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("totalQty") && column == "totalQty")
                {
                    int min = 0;
                    int max = 0;

                    int.TryParse(ValueOneInput.Text?.Trim(), out min);
                    int.TryParse(ValueTwoInput.Text?.Trim(), out max);

                    if (symbol == "61")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalQty", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalQty", min);
                        }
                    }
                    else if (symbol == "60")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalQty", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalQty", min);
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalQty", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalQty", min);
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalQty", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalQty", min);
                        }

                        if (max <= 0)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", max);
                        }
                    }
                }

                if (((string[])Session["read"]).Contains("totalPrice") && column == "totalPrice")
                {
                    int min = 0;
                    int max = 0;

                    int.TryParse(ValueOneInput.Text?.Trim(), out min);
                    int.TryParse(ValueTwoInput.Text?.Trim(), out max);

                    if (symbol == "61")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", min);
                        }
                    }
                    else if (symbol == "60")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", min);
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", min);
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (min <= 0)
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@totalPrice", min);
                        }

                        if (max <= 0)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", max);
                        }
                    }
                }

                if (((string[])Session["read"]).Contains("receivedBy") && column == "receivedBy")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@receivedBy", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@receivedBy", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@receivedBy", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@receivedBy", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("receivedDate") && column == "receivedDate")
                {
                    DateTime from = DateTime.MinValue;
                    DateTime to = DateTime.MinValue;
                    DateTime.TryParse(ValueOneInput.Text?.Trim(), out from);
                    DateTime.TryParse(ValueTwoInput.Text?.Trim(), out to);

                    if (symbol == "61")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@receivedDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@receivedDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "60")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@receivedDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@receivedDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "62")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@receivedDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@receivedDate", ValueOneInput.Text?.Trim());
                        }
                    }
                    else if (symbol == "8596")
                    {
                        if (from == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@receivedDate", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@receivedDate", ValueOneInput.Text?.Trim());
                        }

                        if (to == DateTime.MinValue)
                        {
                            cmd.Parameters.AddWithValue("@value", DBNull.Value);
                        }
                        else
                        {
                            cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                        }
                    }
                }

                if (((string[])Session["read"]).Contains("invoiceNo") && column == "invoiceNo")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@invoiceNo", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@invoiceNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@invoiceNo", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@invoiceNo", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }

                if (((string[])Session["read"]).Contains("driver") && column == "driver")
                {
                    if (symbol == "61")
                    {
                        cmd.Parameters.AddWithValue("@driver", String.Format("%{0}%", ValueOneInput.Text?.Trim()));
                    }
                    else if (symbol == "60")
                    {
                        cmd.Parameters.AddWithValue("@driver", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "62")
                    {
                        cmd.Parameters.AddWithValue("@driver", ValueOneInput.Text?.Trim());
                    }
                    else if (symbol == "8596")
                    {
                        cmd.Parameters.AddWithValue("@driver", ValueOneInput.Text?.Trim());
                        cmd.Parameters.AddWithValue("@value", ValueTwoInput.Text?.Trim());
                    }
                }
            }

            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];
            receipts.Rows.Clear();
            sda.Fill(receipts);

            receipts.DefaultView.Sort = ViewState["column"].ToString() + " " + ViewState["sort"].ToString();
            if (GoodsReceiptGridView.PageCount - 1 < int.Parse(ViewState["page"].ToString()) && GoodsReceiptGridView.PageCount > 0)
            {
                ViewState["page"] = GoodsReceiptGridView.PageCount - 1;
            }
            GoodsReceiptGridView.PageIndex = int.Parse(ViewState["page"].ToString());
            GoodsReceiptGridView.DataSource = receipts;
            GoodsReceiptGridView.DataBind();

        }

        protected void GoodsReceiptGridView_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView row = (DataRowView)e.Row.DataItem;

                LinkButton linkButton = new LinkButton();

                if (!String.IsNullOrEmpty(row["goods_receipt_no"].ToString()) && ((string[])Session["read"]).Length > 0)
                {
                    linkButton = new LinkButton();
                    linkButton.Text = "Lihat";
                    linkButton.CssClass = "btn btn-primary align-middle ms-2";
                    linkButton.CommandName = "show";
                    linkButton.CommandArgument = row["goods_receipt_id"].ToString() + ";" + row["uuid"].ToString()
                        + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                    linkButton.Command += new CommandEventHandler(ShowButton_Command);
                    e.Row.Cells[GoodsReceiptGridView.Columns.Count - 1].Controls.Add(linkButton);
                }

                if (!String.IsNullOrEmpty(row["locked"].ToString()) && row["locked"].ToString() == "0")
                {
                    if (((string[])Session["edit"]).Length > 0)
                    {
                        linkButton = new LinkButton();
                        linkButton.Text = "<i class='bi bi-pencil-fill'></i>";
                        linkButton.CssClass = "btn btn-primary align-middle ms-2";
                        linkButton.CommandName = "edit";
                        linkButton.CommandArgument = row["goods_receipt_id"].ToString() + ";" + row["uuid"].ToString() + ";" + row["purchase_order_id"].ToString()
                            + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                        linkButton.Command += new CommandEventHandler(EditButton_Command);
                        e.Row.Cells[GoodsReceiptGridView.Columns.Count - 1].Controls.Add(linkButton);
                    }

                    if (((bool)Session["delete"]))
                    {
                        linkButton = new LinkButton();
                        linkButton.Text = "<i class='bi bi-x-lg'></i>";
                        linkButton.CssClass = "btn btn-danger align-middle ms-2";
                        linkButton.CommandName = "delete";
                        linkButton.CommandArgument = row["goods_receipt_id"].ToString() + ";" + row["uuid"].ToString() + ";" + row["goods_receipt_no"].ToString()
                            + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                        linkButton.Command += new CommandEventHandler(DeleteButton_Command);
                        e.Row.Cells[GoodsReceiptGridView.Columns.Count - 1].Controls.Add(linkButton);
                    }

                    linkButton = new LinkButton();
                    linkButton.Text = "<i class='bi bi-unlock-fill'></i>";
                    linkButton.CssClass = "btn btn-primary align-middle ms-2";
                    linkButton.CommandName = "lock";
                    linkButton.CommandArgument = row["goods_receipt_id"].ToString() + ";" + row["uuid"].ToString()
                        + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                    linkButton.Command += new CommandEventHandler(LockButton_Command);
                    e.Row.Cells[GoodsReceiptGridView.Columns.Count - 1].Controls.Add(linkButton);
                }
                else
                {
                    linkButton = new LinkButton();
                    linkButton.Text = "<i class='bi bi-lock-fill'></i>";
                    linkButton.CssClass = "btn btn-danger align-middle ms-2";
                    linkButton.CommandName = "lock";
                    linkButton.CommandArgument = row["goods_receipt_id"].ToString() + ";" + row["uuid"].ToString()
                        + ";" + row["store_id"].ToString() + ";" + row["supplier_id"].ToString();
                    linkButton.Command += new CommandEventHandler(LockButton_Command);
                    e.Row.Cells[GoodsReceiptGridView.Columns.Count - 1].Controls.Add(linkButton);
                }
            }

            if (e.Row.RowType == DataControlRowType.Pager)
            {
                DropDownList PageDropDownList = (DropDownList)e.Row.FindControl("PageDropDownList");
                System.Web.UI.WebControls.LinkButton FirstButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("Firstbutton");
                System.Web.UI.WebControls.LinkButton PreviousButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("PreviousButton");
                System.Web.UI.WebControls.LinkButton NextButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("NextButton");
                System.Web.UI.WebControls.LinkButton LastButton = (System.Web.UI.WebControls.LinkButton)e.Row.FindControl("LastButton");
                PageDropDownList.Items.Clear();

                int page = 0;

                for (int i = 0; i < GoodsReceiptGridView.PageCount; i++)
                {
                    page = i + 1;
                    PageDropDownList.Items.Add(page.ToString());
                }
                page = int.Parse(ViewState["page"].ToString()) + 1;
                if (page == 1)
                {
                    FirstButton.CssClass += " " + "disabled";
                    PreviousButton.CssClass += " " + "disabled";
                    NextButton.CssClass = "page-link";
                    LastButton.CssClass = "page-link";
                }
                else if (page == GoodsReceiptGridView.PageCount)
                {
                    FirstButton.CssClass = "page-link";
                    PreviousButton.CssClass = "page-link";
                    NextButton.CssClass += " " + "disabled";
                    LastButton.CssClass += " " + "disabled";
                }
                else
                {
                    FirstButton.CssClass = "page-link";
                    PreviousButton.CssClass = "page-link";
                    NextButton.CssClass = "page-link";
                    LastButton.CssClass = "page-link";
                }
                PageDropDownList.SelectedValue = page.ToString();
            }
        }

        protected void PageDropDownList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int page = int.Parse(((DropDownList)sender).SelectedValue);

            if (page <= 0)
            {
                ViewState["page"] = 0;
                GoodsReceiptGridView.PageIndex = 0;
            }
            else
            {
                ViewState["page"] = page - 1;
                GoodsReceiptGridView.PageIndex = page - 1;
            }

            System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];
            receipts.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            GoodsReceiptGridView.DataSource = receipts;
            GoodsReceiptGridView.DataBind();
        }

        protected void DownloadButton_Command(object sender, CommandEventArgs e)
        {
            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
            SqlConnection conn = new SqlConnection(constr);
            SqlCommand cmd = conn.CreateCommand();
            List<string> errors = new List<string>();

            string query = "";
            string columns = "";
            string wheresupplier = "";
            string wherestore = "";
            bool withComma = false;

            if (((string[])Session["read"]).Contains("store"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "stores.store_name");
                columns = String.Concat(columns, "[Store Name] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("supplier"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "suppliers.supplier_name");
                columns = String.Concat(columns, "[Supplier Name] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("deliveryOrder"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "purchase_orders.delivery_no");
                columns = String.Concat(columns, "[Delivery Order No] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("goodsReceiptNo"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "goods_receipts.goods_receipt_no");
                columns = String.Concat(columns, "[Goods Receipt No] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("invoiceNo"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "goods_receipts.invoice_no");
                columns = String.Concat(columns, "[Invoice No] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("goodsReceiptDate"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "goods_receipts.goods_receipt_date");
                columns = String.Concat(columns, "[Goods Receipt Date] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("driver"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "goods_receipts.driver");
                columns = String.Concat(columns, "[Driver] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("receivedBy"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "goods_receipts.received_by");
                columns = String.Concat(columns, "[Received By] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("receivedDate"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "goods_receipts.received_date");
                columns = String.Concat(columns, "[Received Date] NVARCHAR(255)");
            }

            if (((string[])Session["read"]).Contains("goodsReceiptNotes"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "goods_receipts.goods_receipt_notes");
                columns = String.Concat(columns, "[Notes] LongText");
            }

            if (((string[])Session["read"]).Contains("item"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", @"dbo.JsonValue(goods_receipt_items.item_json, 'itemName') [item_name], 
                dbo.JsonValue(goods_receipt_items.item_json, 'brandName') [brand_name], goods_receipt_items.item_qty, goods_receipt_items.item_price");
                columns = String.Concat(columns, "[Item Name] NVARCHAR(255), [Brand Name] NVARCHAR(255), [Item Price] DOUBLE, [Item Qty] INTEGER");
            }

            if (((string[])Session["read"]).Contains("totalQty"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "goods_receipts.total_qty");
                columns = String.Concat(columns, "[Total Qty] INTEGER");
            }

            if (((string[])Session["read"]).Contains("totalPrice"))
            {
                if (withComma)
                {
                    query = String.Concat(query, ",");
                    columns = String.Concat(columns, ",");
                }
                else
                {
                    withComma = true;
                }
                query = String.Concat(query, " ", "goods_receipts.total_price");
                columns = String.Concat(columns, "[Total Price] DOUBLE");
            }

            if (e.CommandName == "downloadall")
            {
                if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = @"SELECT supplier_id FROM suppliers";
                }
                else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = String.Join(",", ((int[])Session["suppliers"]));
                }

                if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = @"SELECT store_id FROM stores";
                }
                else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = String.Join(",", ((int[])Session["stores"]));
                }
            }

            if (e.CommandName == "downloadselectedsupplier")
            {
                if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
                {
                    errors.Add("Vendor tidak boleh kosong");
                }
                else
                {
                    wheresupplier = SupplierChoice.SelectedValue;
                }

                if (((int[])Session["stores"]).Length == 1 && ((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = @"SELECT store_id FROM stores";
                }
                else if (((int[])Session["stores"]).Length >= 1 && !((int[])Session["stores"]).Contains(-1))
                {
                    wherestore = String.Join(",", ((int[])Session["stores"]));
                }
            }

            if (e.CommandName == "downloadselectedstore")
            {
                if (((int[])Session["suppliers"]).Length == 1 && ((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = @"SELECT supplier_id FROM suppliers";
                }
                else if (((int[])Session["suppliers"]).Length >= 1 && !((int[])Session["suppliers"]).Contains(-1))
                {
                    wheresupplier = String.Join(",", ((int[])Session["suppliers"]));
                }

                if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
                {
                    errors.Add("Toko Ritel tidak boleh kosong");
                }
                else
                {
                    wherestore = StoreChoice.SelectedValue;
                }
            }

            if (e.CommandName == "downloadselectedstoreandsupplier")
            {
                if (String.IsNullOrEmpty(StoreChoice.SelectedValue))
                {
                    errors.Add("Toko Ritel tidak boleh kosong");
                }
                else
                {
                    wherestore = StoreChoice.SelectedValue;
                }

                if (String.IsNullOrEmpty(SupplierChoice.SelectedValue))
                {
                    errors.Add("Vendor tidak boleh kosong");
                }
                else
                {
                    wheresupplier = SupplierChoice.SelectedValue;
                }
            }

            if (errors.Count <= 0)
            {
                int receiptId = 0;
                int oldReceiptId = 0;
                bool newRow = false;

                List<string> rows = new List<string>();

                string commandText = @"SELECT goods_receipts.goods_receipt_id, {0}
                    FROM goods_receipts INNER JOIN goods_receipt_items 
                    ON goods_receipts.goods_receipt_id = goods_receipt_items.goods_receipt_id AND goods_receipt_items.isdeleted = 0
                    INNER JOIN purchase_orders ON purchase_orders.purchase_order_id = goods_receipts.purchase_order_id AND purchase_orders.isdeleted = 0
                    INNER JOIN stores ON goods_receipts.store_id = stores.store_id
                    INNER JOIN suppliers ON goods_receipts.supplier_id = suppliers.supplier_id
                    WHERE goods_receipts.store_id IN ({1}) AND goods_receipts.supplier_id IN ({2}) AND
                    goods_receipts.isdeleted = 0
                    ORDER BY goods_receipts.goods_receipt_id;";
                commandText = String.Format(commandText, query, wherestore, wheresupplier);

                cmd.CommandText = commandText;
                cmd.CommandTimeout = 0;
                conn.Open();
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        withComma = false;

                        string row = "";

                        if (reader.IsDBNull(reader.GetOrdinal("goods_receipt_id")))
                        {
                            continue;
                        }
                        else
                        {
                            receiptId = int.Parse(reader["goods_receipt_id"].ToString());

                            if (oldReceiptId != receiptId)
                            {
                                newRow = true;
                                oldReceiptId = receiptId;
                            }
                            else
                            {
                                newRow = false;
                            }
                        }

                        if (((string[])Session["read"]).Contains("store"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("store_name")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["store_name"].ToString().Replace("'", ""), "'");
                            }
                        }

                        if (((string[])Session["read"]).Contains("supplier"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("supplier_name")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["supplier_name"].ToString().Replace("'", ""), "'");
                            }
                        }

                        if (((string[])Session["read"]).Contains("deliveryOrder"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("delivery_no")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["delivery_no"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("goodsReceiptNo"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("goods_receipt_no")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["goods_receipt_no"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("goodsReceiptDate"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("goods_receipt_date")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["goods_receipt_date"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("receivedBy"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("received_by")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["received_by"].ToString().Replace("'", ""), "'");
                            }
                        }

                        if (((string[])Session["read"]).Contains("receivedDate"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("received_date")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["received_date"].ToString().Replace("'", ""), "'");
                            }
                        }

                        if (((string[])Session["read"]).Contains("invoiceNo"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("invoice_no")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["invoice_no"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("driver"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("driver")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["driver"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("goodsReceiptNotes"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("goods_receipt_notes")) || !newRow)
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["goods_receipt_notes"].ToString().Replace("'", ""), "'");
                            }

                        }

                        if (((string[])Session["read"]).Contains("item"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("item_name")))
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["item_name"].ToString().Replace("'", ""), "'");
                            }

                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("brand_name")))
                            {
                                row = String.Concat(row, " ", "''");
                            }
                            else
                            {
                                row = String.Concat(row, " ", "'", reader["brand_name"].ToString().Replace("'", ""), "'");
                            }

                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("item_qty")))
                            {
                                row = String.Concat(row, " ", "NULL");
                            }
                            else
                            {
                                row = String.Concat(row, " ", reader["item_qty"].ToString());
                            }

                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("item_price")))
                            {
                                row = String.Concat(row, " ", "NULL");
                            }
                            else
                            {
                                row = String.Concat(row, " ", reader["item_price"].ToString());
                            }
                        }

                        if (((string[])Session["read"]).Contains("totalQty"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("total_qty")) || !newRow)
                            {
                                row = String.Concat(row, " ", "NULL");
                            }
                            else
                            {
                                row = String.Concat(row, " ", reader["total_qty"].ToString());
                            }
                        }

                        if (((string[])Session["read"]).Contains("totalPrice"))
                        {
                            if (withComma)
                            {
                                row = String.Concat(row, ",");
                            }
                            else
                            {
                                withComma = true;
                            }

                            if (reader.IsDBNull(reader.GetOrdinal("total_price")) || !newRow)
                            {
                                row = String.Concat(row, " ", "NULL");
                            }
                            else
                            {
                                row = String.Concat(row, " ", reader["total_price"].ToString());
                            }
                        }

                        row = String.Format("INSERT INTO Sheet1 VALUES ({0});", row);
                        rows.Add(row);
                    }
                }

                conn.Close();

                string sourcefile = MapPath("~/uploads/blank.xlsx");
                string filename = "goodsreceipt-" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".xlsx";
                string destinationfile = MapPath("~/uploads/" + filename);

                File.Copy(sourcefile, destinationfile, true);

                string extension = Path.GetExtension(destinationfile);

                string connectionstring = "";
                string mimetypes = "";

                if (extension == ".xls")
                {
                    connectionstring = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source='" + destinationfile + "';Extended Properties=\"Excel 8.0;HDR=YES;\"";
                    mimetypes = "application/vnd.ms-excel";
                }
                else if (extension == ".xlsx")
                {
                    connectionstring = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source='" + destinationfile + "';Extended Properties=\"Excel 12.0;HDR=YES;\"";
                    mimetypes = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                }

                using (OleDbConnection oleconn = new OleDbConnection(connectionstring))
                {
                    oleconn.Open();
                    OleDbCommand olecommand = new OleDbCommand();
                    olecommand.Connection = oleconn;
                    olecommand.CommandText = String.Format("CREATE TABLE Sheet1 ({0});", columns);
                    olecommand.ExecuteNonQuery();

                    foreach (string row in rows)
                    {
                        olecommand.CommandText = row;
                        olecommand.ExecuteNonQuery();
                    }

                    oleconn.Close();
                }

                FilenameInput.Value = filename;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "download", "download();", true);
            }
            else
            {
                AlertLabel.Text = String.Join("<br>", errors.ToArray());
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void FirstButton_Click(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            GoodsReceiptGridView.PageIndex = 0;

            System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];
            receipts.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            GoodsReceiptGridView.DataSource = receipts;
            GoodsReceiptGridView.DataBind();
        }

        protected void LastButton_Click(object sender, EventArgs e)
        {
            ViewState["page"] = GoodsReceiptGridView.PageCount - 1;
            GoodsReceiptGridView.PageIndex = GoodsReceiptGridView.PageCount - 1;

            System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];
            receipts.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            GoodsReceiptGridView.DataSource = receipts;
            GoodsReceiptGridView.DataBind();
        }

        protected void NextButton_Click(object sender, EventArgs e)
        {
            int page = int.Parse(ViewState["page"].ToString());
            page++;

            if (page > GoodsReceiptGridView.PageCount - 1)
            {
                page = GoodsReceiptGridView.PageCount - 1;
            }
            ViewState["page"] = page;
            GoodsReceiptGridView.PageIndex = page;

            System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];
            receipts.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            GoodsReceiptGridView.DataSource = receipts;
            GoodsReceiptGridView.DataBind();
        }

        protected void PreviousButton_Click(object sender, EventArgs e)
        {
            int page = int.Parse(ViewState["page"].ToString());
            page--;

            if (page < 0)
            {
                page = 0;
            }
            ViewState["page"] = page;
            GoodsReceiptGridView.PageIndex = page;

            System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];
            receipts.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            GoodsReceiptGridView.DataSource = receipts;
            GoodsReceiptGridView.DataBind();
        }

        protected void CreateFilterChoice()
        {
            Dictionary<string, string> columns = new Dictionary<string, string>();
            columns.Add("no", "No");

            if (((string[])Session["read"]).Contains("store"))
            {
                columns.Add("storeName", "Toko Ritel");
            }

            if (((string[])Session["read"]).Contains("supplier"))
            {
                columns.Add("supplierName", "Vendor");
            }

            if (((string[])Session["read"]).Contains("deliveryOrder"))
            {
                columns.Add("deliveryNo", "No Pengiriman");
            }

            //if (((string[])Session["read"]).Contains("deliveryDate"))
            //{
            //    columns.Add("deliveryDate", "Tanggal Pengiriman");
            //}

            if (((string[])Session["read"]).Contains("purchaseOrder"))
            {
                columns.Add("purchaseNo", "No Pemesanan");
            }

            if (((string[])Session["read"]).Contains("goodsReceiptNo"))
            {
                columns.Add("goodsReceiptNo", "No Penerimaan");
            }

            if (((string[])Session["read"]).Contains("goodsReceiptDate"))
            {
                columns.Add("goodsReceiptDate", "Tanggal No Penerimaan");
            }

            if (((string[])Session["read"]).Contains("receivedBy"))
            {
                columns.Add("receivedBy", "Diterima Oleh");
            }

            if (((string[])Session["read"]).Contains("receivedDate"))
            {
                columns.Add("receivedDate", "Tanggal Penerimaan");
            }

            if (((string[])Session["read"]).Contains("totalPrice"))
            {
                columns.Add("totalPrice", "Total Harga");
            }

            if (((string[])Session["read"]).Contains("totalQty"))
            {
                columns.Add("totalQty", "Jumlah Barang");
            }

            if (((string[])Session["read"]).Contains("driver"))
            {
                columns.Add("driver", "Supir");
            }

            if (((string[])Session["read"]).Contains("invoiceNo"))
            {
                columns.Add("invoiceNo", "No Faktur");
            }

            //if (((string[])Session["read"]).Contains("goodsReceiptNotes"))
            //{
            //    columns.Add("goodsReceiptNotes", "Catatan ");
            //}

            FilterChoice.DataSource = columns;
            FilterChoice.DataValueField = "Key";
            FilterChoice.DataTextField = "Value";
            FilterChoice.DataBind();
            FilterChoice.SelectedValue = "no";
        }

        protected void FilterChoice_SelectedIndexChanged(Object sender, EventArgs args)
        {
            if (FilterChoice.SelectedValue == "purchaseDate" ||
                FilterChoice.SelectedValue == "requestedDate" || FilterChoice.SelectedValue == "approvalDate")
            {
                ValueOneInput.CssClass = "form-control flatpickr-no-config";
                ValueTwoInput.CssClass = "form-control flatpickr-no-config";
            }
            else
            {
                ValueOneInput.CssClass = "form-control";
                ValueTwoInput.CssClass = "form-control";
            }
        }

        protected void OperatorChoice_SelectedIndexChanged(Object sender, EventArgs args)
        {
            if ("8596" == OperatorChoice.SelectedValue)
            {
                ValueTwoInput.Visible = true;
            }
            else
            {
                ValueTwoInput.Visible = false;
            }
        }


        protected void LockButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "lock" && arg.Length > 0)
            {
                if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                {
                    string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                    SqlConnection conn = new SqlConnection(constr);
                    SqlCommand command = conn.CreateCommand();
                    command.CommandText = @"UPDATE goods_receipts SET locked = 1
                                    WHERE goods_receipt_id = @receiptId AND uuid = @receiptUuid
                                    AND store_id = @storeId AND supplier_id = @supplierId;";
                    command.Parameters.AddWithValue("@receiptId", int.Parse(arg[0]));
                    command.Parameters.AddWithValue("@receiptUuid", arg[1].ToString());
                    command.Parameters.AddWithValue("@storeId", int.Parse(StoreChoice.SelectedValue));
                    command.Parameters.AddWithValue("@supplierId", int.Parse(SupplierChoice.SelectedValue));
                    conn.Open();
                    command.ExecuteNonQuery();
                    conn.Close();

                    BindGoodsReceiptGridView();
                }
                else
                {
                    if (Session["role_name"].ToString() == "superuser")
                    {
                        string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                        SqlConnection conn = new SqlConnection(constr);
                        SqlCommand command = conn.CreateCommand();
                        command.CommandText = @"UPDATE goods_receipts SET locked = 0
                                    WHERE goods_receipt_id = @receiptId AND uuid = @receiptUuid
                                    AND store_id = @storeId AND supplier_id = @supplierId;";
                        command.Parameters.AddWithValue("@purchaseId", int.Parse(arg[0]));
                        command.Parameters.AddWithValue("@purchaseUuid", arg[1].ToString());
                        command.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                        command.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                        conn.Open();
                        command.ExecuteNonQuery();
                        conn.Close();

                        BindGoodsReceiptGridView();
                    }
                    else
                    {
                        AlertLabel.Text = "Data sudah terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void GoodsReceiptGridView_Sorting(object sender, GridViewSortEventArgs e)
        {
            System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];

            if (ViewState["column"].ToString() == e.SortExpression)
            {
                if (ViewState["sort"].ToString() == "ASC")
                {
                    ViewState["sort"] = "DESC";
                    ViewState["column"] = e.SortExpression;
                    receipts.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
                }
                else if (ViewState["sort"].ToString() == "DESC")
                {
                    ViewState["sort"] = "ASC";
                    ViewState["column"] = e.SortExpression;
                    receipts.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
                }
            }
            else
            {
                ViewState["column"] = e.SortExpression;
                ViewState["sort"] = "ASC";
                receipts.DefaultView.Sort = ViewState["column"] + " " + ViewState["sort"];
            }

            ViewState["receipts"] = receipts;
            GoodsReceiptGridView.DataSource = receipts;
            GoodsReceiptGridView.DataBind();

        }

        protected void GoodsReceiptGridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GoodsReceiptGridView.PageIndex = e.NewPageIndex;
            ViewState["page"] = e.NewPageIndex;

            System.Data.DataTable receipts = (System.Data.DataTable)ViewState["receipts"];

            if (receipts.Rows.Count > 0)
            {
                ViewState["receipts"] = receipts;
                GoodsReceiptGridView.DataSource = receipts;
                GoodsReceiptGridView.DataBind();
            }
        }

        protected void UploadTabButton_Click(object sender, EventArgs e)
        {
            menu = "upload";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectUploadTab();", true);
        }

        protected void TabularTabButton_Click(object sender, EventArgs e)
        {
            menu = "tabular";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "tab", "selectTabularTab();", true);
        }

        protected bool IsLocked(int id, string uuid)
        {
            SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString);
            SqlCommand cmd = conn.CreateCommand();

            cmd.CommandText = @"SELECT COUNT(*) FROM goods_receipts 
                WHERE goods_receipt_id = @receiptId AND uuid = @receiptUuid 
                AND isdeleted = 0 AND locked = 1;";

            cmd.Parameters.AddWithValue("@receiptId", id);
            cmd.Parameters.AddWithValue("@receiptUuid", uuid);
            cmd.Connection = conn;

            int count = 0;

            conn.Open();
            object result = cmd.ExecuteScalar();
            conn.Close();

            if (result != null)
            {
                count = int.Parse(result.ToString());
            }

            if (count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected void DeleteButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "delete" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[4].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[4].ToString())) || ((int[])Session["suppliers"]).Contains(-1)) &&
                    (bool)Session["delete"] == true)
                {
                    if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    {
                        YesButton.CommandName = "delete";
                        YesButton.CommandArgument = e.CommandArgument.ToString();
                        GoodsReceiptNoModalLabel.Text = arg[2];

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "modal", "openConfirmModal();", true);
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen penerimaan terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }

                }
                else
                {
                    AlertLabel.Text = "Akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void EditButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "edit" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[4].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[4].ToString())) || ((int[])Session["suppliers"]).Contains(-1)) &&
                    ((string[])Session["edit"]).Length > 0)
                {
                    if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    {
                        Session["origin"] = "GoodsReceipt";
                        Session["storevalue"] = StoreChoice.SelectedValue;
                        Session["suppliervalue"] = SupplierChoice.SelectedValue;
                        Session["pagevalue"] = GoodsReceiptGridView.PageIndex;
                        Session["sortorder"] = ViewState["sort"];
                        Session["columnname"] = ViewState["column"];
                        Session["searchquery"] = ViewState["search"];
                        Session["redirect"] = true;
                        Response.Redirect("CreateGoodsReceipt.aspx?receiptId=" + arg[0].ToString() + "&receiptUuid=" + arg[1].ToString()
                            + "&purchaseId=" + arg[2].ToString());
                    }
                    else
                    {
                        AlertLabel.Text = "Dokumen penerimaan terkunci";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }  
                }
                else
                {
                    AlertLabel.Text = "Akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void YesButton_Click(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "delete" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[4].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(-1) || ((int[])Session["stores"]).Contains(int.Parse(arg[3].ToString()))) &&
                    (((int[])Session["suppliers"]).Contains(-1) || ((int[])Session["suppliers"]).Contains(int.Parse(arg[4].ToString()))) &&
                    (bool)Session["delete"] == true)
                {
                    if (!IsLocked(int.Parse(arg[0].ToString()), arg[1].ToString()))
                    {
                        try
                        {
                            string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                            SqlConnection conn = new SqlConnection(constr);
                            SqlTransaction transaction;

                            conn.Open();
                            transaction = conn.BeginTransaction();

                            SqlCommand command = conn.CreateCommand();
                            command.CommandText = @"SELECT COUNT(*) FROM goods_receipts WHERE store_id = @storeId AND 
                            supplier_id = @supplierId AND
                            goods_receipt_id = @receiptId AND uuid = @receiptUuid AND isdeleted = 0;";
                            command.Parameters.AddWithValue("@storeId", int.Parse(arg[3].ToString()));
                            command.Parameters.AddWithValue("@supplierId", int.Parse(arg[4].ToString()));
                            command.Parameters.AddWithValue("@receiptId", int.Parse(arg[0].ToString()));
                            command.Parameters.AddWithValue("@receiptUuid", arg[1].ToString());
                            command.Transaction = transaction;

                            int count = 0;
                            //conn.Open();

                            object result = command.ExecuteScalar();
                            if (result != null)
                            {
                                count = int.Parse(result.ToString());
                            }
                            //conn.Close();

                            if (count > 0)
                            {
                                command = conn.CreateCommand();
                                command.CommandText = @"UPDATE B SET B.isdeleted = 1, B.modified_date = getdate(), B.modified_by = @modifiedBy
                                FROM goods_receipts A INNER JOIN goods_receipt_items B ON A.goods_receipt_id = B.goods_receipt_id AND B.isdeleted = 0
                                WHERE A.goods_receipt_id = @receiptId AND A.uuid = @receiptUuid
                                AND A.store_id = @storeId AND A.supplier_id = @supplierId AND A.isdeleted = 0;";
                                command.Parameters.AddWithValue("@modifiedBy", Session["username"]);
                                command.Parameters.AddWithValue("@receiptId", int.Parse(arg[0]));
                                command.Parameters.AddWithValue("@receiptUuid", arg[1].ToString());
                                command.Parameters.AddWithValue("@storeId", int.Parse(arg[3].ToString()));
                                command.Parameters.AddWithValue("@supplierId", int.Parse(arg[4].ToString()));
                                command.Transaction = transaction;
                                //conn.Open();
                                command.ExecuteNonQuery();
                                //conn.Close();

                                command = conn.CreateCommand();
                                command.CommandText = @"UPDATE A SET A.isdeleted = 1, A.modified_date = getdate(), A.modified_by = @modifiedBy
                                FROM goods_receipts A INNER JOIN goods_receipt_items B ON A.goods_receipt_id = B.goods_receipt_id
                                WHERE A.goods_receipt_id = @receiptId AND A.uuid = @receiptUuid
                                AND A.store_id = @storeId AND A.supplier_id = @supplierId AND A.isdeleted = 0;";
                                command.Parameters.AddWithValue("@modifiedBy", Session["username"]);
                                command.Parameters.AddWithValue("@receiptId", int.Parse(arg[0]));
                                command.Parameters.AddWithValue("@receiptUuid", arg[1].ToString());
                                command.Parameters.AddWithValue("@storeId", int.Parse(arg[3].ToString()));
                                command.Parameters.AddWithValue("@supplierId", int.Parse(arg[4].ToString()));
                                command.Transaction = transaction;
                                //conn.Open();
                                command.ExecuteNonQuery();
                                //conn.Close();
                                transaction.Commit();
                                conn.Close();
                                BindGoodsReceiptGridView();

                                AlertLabel.Text = "Data berhasil dihapus";
                                AlertLabel.CssClass = "alert alert-light-primary color-primary d-block";
                            }
                            else
                            {
                                AlertLabel.Text = "Dokumen penerimaan terkunci";
                                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                            }
                        }
                        catch (Exception error)
                        {
                            AlertLabel.Text = error.Message.ToString();
                            AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                        }
                        
                    }
                    else
                    {
                        AlertLabel.Text = "Terjadi kesalahan";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }

        protected void NoButton_Click(object sender, EventArgs e)
        {
            
        }

        protected void StoreChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["sort"] = "ASC";
            ViewState["column"] = "no";
            ViewState["page"] = 0;
            ViewState["search"] = "";
            ValueOneInput.Text = "";
            ValueTwoInput.Text = "";
            BindGoodsReceiptGridView();
        }

        protected void SupplierChoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            ViewState["page"] = 0;
            ViewState["sort"] = "ASC";
            ViewState["column"] = "no";
            ViewState["page"] = 0;
            ViewState["search"] = "";
            ValueOneInput.Text = "";
            ValueTwoInput.Text = "";
            BindGoodsReceiptGridView();
        }

        protected void ShowButton_Command(object sender, CommandEventArgs e)
        {
            string[] arg = e.CommandArgument.ToString().Split(';');

            if (e.CommandName == "show" && arg.Length > 0)
            {
                if (String.IsNullOrEmpty(arg[2].ToString()))
                {
                    AlertLabel.Text = "Toko Ritel tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if (String.IsNullOrEmpty(arg[3].ToString()))
                {
                    AlertLabel.Text = "Vendor tidak boleh kosong";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
                else if ((((int[])Session["stores"]).Contains(int.Parse(arg[2].ToString())) || ((int[])Session["stores"]).Contains(-1)) &&
                    (((int[])Session["suppliers"]).Contains(int.Parse(arg[3].ToString())) || ((int[])Session["suppliers"]).Contains(-1))
                    && ((string[])Session["read"]).Length > 0)
                {
                    string constr = ConfigurationManager.ConnectionStrings["WebAppNetCon"].ConnectionString;
                    SqlConnection connect = new SqlConnection(constr);
                    SqlCommand cmd = connect.CreateCommand();
                    cmd.CommandText = @"SELECT COUNT(*) FROM goods_receipts WHERE store_id = @storeId AND supplier_id = @supplierId AND 
                    goods_receipt_id = @receiptId AND (goods_receipt_no IS NOT NULL OR LTRIM(RTRIM(goods_receipt_no)) != '') AND uuid = @receiptUuid";
                    cmd.Parameters.AddWithValue("@storeId", int.Parse(arg[2].ToString()));
                    cmd.Parameters.AddWithValue("@supplierId", int.Parse(arg[3].ToString()));
                    cmd.Parameters.AddWithValue("@receiptId", int.Parse(arg[0].ToString()));
                    cmd.Parameters.AddWithValue("@receiptUuid", arg[1].ToString());

                    connect.Open();
                    int count = 0;

                    object result = cmd.ExecuteScalar();

                    if (result != null)
                    {
                        count = int.Parse(result.ToString());
                    }
                    connect.Close();

                    if (count > 0)
                    {
                        Session["origin"] = "GoodsReceipt";
                        Session["storevalue"] = StoreChoice.SelectedValue;
                        Session["suppliervalue"] = SupplierChoice.SelectedValue;
                        Session["pagevalue"] = GoodsReceiptGridView.PageIndex;
                        Session["sortorder"] = ViewState["sort"];
                        Session["columnname"] = ViewState["column"];
                        Session["searchquery"] = ViewState["search"];
                        Session["redirect"] = true;
                        Response.Redirect("GoodsReceiptDetail.aspx?receiptId=" + arg[0].ToString() + "&receiptUuid=" + arg[1].ToString());
                    }
                    else
                    {
                        AlertLabel.Text = "No pengiriman tidak tersedia";
                        AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                    }
                }
                else
                {
                    AlertLabel.Text = "Akses ditolak";
                    AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
                }
            }
            else
            {
                AlertLabel.Text = "Terjadi kesalahan server";
                AlertLabel.CssClass = "alert alert-light-danger color-danger d-block";
            }
        }
        
        protected void GoodsReceiptGridView_RowCancelingEdit(Object sender, GridViewCancelEditEventArgs e)
        {

        }

        protected void GoodsReceiptGridView_RowEditing(object sender, GridViewEditEventArgs e)
        {

        }

        protected void GoodsReceiptGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
    }
}
﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CreateGoodsReceipt.aspx.cs" EnableViewState="True" Inherits="biutopia.CreateGoodsReceipt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main-content">
        <div class="page-heading">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 order-md-1 order-last">
                        <h3>Penerimaan Barang</h3>
                        <p class="text-subtitle text-muted"></p>
                    </div>
                </div>
            </div>
            <section class="section">    
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <asp:Label ID="AlertLabel" runat="server" Text=""></asp:Label>
                            </div>
                            
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreChoice">Toko Ritel</label>
                                    <asp:DropDownList ID="StoreChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="StoreChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierChoice">Pemasok</label>
                                    <asp:DropDownList ID="SupplierChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="SupplierChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="PurchaseOrderChoice">No Pengiriman</label>
                                    <asp:DropDownList ID="PurchaseOrderChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="PurchaseOrderChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>
                            <% if ((((string[])Session["add"]).Contains("supplierName") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierName") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierNameInput">Nama Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierNameCheckBox" runat="server" OnCheckedChanged="SupplierNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierBuildingName") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierBuildingName") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierBuildingNameInput">Nama Bangunan Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierBuildingNameCheckBox" runat="server" OnCheckedChanged="SupplierBuildingNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierBuildingNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierStreetName") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierStreetName") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierStreetNameInput">Nama Jalan Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierStreetNameCheckBox" runat="server" OnCheckedChanged="SupplierStreetNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierStreetNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierNeighbourhood") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierNeighbourhood") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierNeighbourhoodInput">Keterangan RT dan RW Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierNeighbourhoodCheckBox" runat="server" OnCheckedChanged="SupplierNeighbourhoodCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierNeighbourhoodInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierSubdistrict") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierSubdistrict") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierSubdistrictInput">Nama Kelurahan Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierSubdistrictCheckBox" runat="server" OnCheckedChanged="SupplierSubdistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierSubdistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierDistrict") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierDistrict") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierDistrictInput">Nama Kecamatan Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierDistrictCheckBox" runat="server" OnCheckedChanged="SupplierDistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierDistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierRuralDistrict") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierRuralDistrict") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierRuralDistrictInput">Nama Kota atau Kabupaten Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierRuralDistrictCheckBox" runat="server" OnCheckedChanged="SupplierRuralDistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierRuralDistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierProvince") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierProvince") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierProvinceInput">Nama Provinsi Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierProvinceCheckBox" runat="server" OnCheckedChanged="SupplierProvinceCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierProvinceInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierZipcode") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierZipcode") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierZipcodeInput">Kode Pos Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierZipcodeCheckBox" runat="server" OnCheckedChanged="SupplierZipcodeCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierZipcodeInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierContactName") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierContactName") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierContactNameInput">Nama Kontak Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierContactNameCheckBox" runat="server" OnCheckedChanged="SupplierContactNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierContactNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierContactPhone") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierContactPhone") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierContactPhoneInput">Nomor Telepon Kontak Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierContactPhoneCheckBox" runat="server" OnCheckedChanged="SupplierContactPhoneCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierContactPhoneInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("supplierContactEmail") && action == "create")
                                    || (((string[])Session["edit"]).Contains("supplierContactEmail") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="SupplierContactEmailInput">Email Pemasok</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="SupplierContactEmailCheckBox" runat="server" OnCheckedChanged="SupplierContactEmailCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="SupplierContactEmailInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeName") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeName") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreNameInput">Nama Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreNameCheckBox" runat="server" OnCheckedChanged="StoreNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeBuildingName") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeBuildingName") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreBuildingNameInput">Nama Bangunan Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreBuildingNameCheckBox" runat="server" OnCheckedChanged="StoreBuildingNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreBuildingNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeStreetName") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeStreetName") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreStreetNameInput">Nama Jalan Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreStreetNameCheckBox" runat="server" OnCheckedChanged="StoreStreetNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreStreetNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeNeighbourhood") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeNeighbourhood") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreNeighbourhoodInput">Keterangan RT dan RW Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreNeighbourhoodCheckBox" runat="server" OnCheckedChanged="StoreNeighbourhoodCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreNeighbourhoodInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeSubdistrict") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeSubdistrict") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreSubdistrictInput">Nama Kelurahan atau Desa Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreSubdistrictCheckBox" runat="server" OnCheckedChanged="StoreSubdistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreSubdistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeDistrict") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeDistrict") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreDistrictInput">Nama Kecamatan Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreDistrictCheckBox" runat="server" OnCheckedChanged="StoreDistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreDistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeRuralDistrict") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeRuralDistrict") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreRuralDistrictInput">Nama Kota atau Kabupaten Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreRuralDistrictCheckBox" runat="server" OnCheckedChanged="StoreRuralDistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreRuralDistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeProvince") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeProvince") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreProvinceInput">Nama Provinsi Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreProvinceCheckBox" runat="server" OnCheckedChanged="StoreProvinceCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreProvinceInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeZipcode") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeZipcode") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreZipcodeInput">Kode Pos Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreZipcodeCheckBox" runat="server" OnCheckedChanged="StoreZipcodeCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreZipcodeInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeContactName") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeContactName") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreContactNameInput">Nama Kontak Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreContactNameCheckBox" runat="server" OnCheckedChanged="StoreContactNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreContactNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeContactphone") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeContactphone") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreContactPhoneInput">Nomor Telepon Kontak Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreContactPhoneCheckBox" runat="server" OnCheckedChanged="StoreContactPhoneCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreContactPhoneInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("storeContactEmail") && action == "create")
                                    || (((string[])Session["edit"]).Contains("storeContactEmail") && action == "update"))
                                { %>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="StoreContactEmailInput">Email Toko Ritel</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="StoreContactEmailCheckBox" runat="server" OnCheckedChanged="StoreContactEmailCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="StoreContactEmailInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("goodsReceiptNo") && action == "create")
                                    || (((string[])Session["edit"]).Contains("goodsReceiptNo") && action == "update"))
                                { %>
                            <div class="col-md-4 col-12">
                                <div class="form-group">
                                    <label for="GoodsReceiptNoInput">No Penerimaaan</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="GoodsReceiptNoCheckBox" runat="server" OnCheckedChanged="GoodsReceiptNoCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="GoodsReceiptNoInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            
                            <% if ((((string[])Session["add"]).Contains("invoiceNo") && action == "create")
                                    || (((string[])Session["edit"]).Contains("invoiceNo") && action == "update"))
                                { %>
                            <div class="col-md-4 col-12">
                                <div class="form-group">
                                    <label for="InvoiceNoInput">No Surat Jalan (opsional)</label>
                                    <asp:TextBox ID="InvoiceNoInput" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <% } %>

                            <% if ((((string[])Session["add"]).Contains("driver") && action == "create")
                                    || (((string[])Session["edit"]).Contains("driver") && action == "update"))
                                { %>
                            <div class="col-md-4 col-12">
                                <div class="form-group">
                                    <label for="DriverInput">Nama Supir (opsional)</label>
                                    <asp:TextBox ID="DriverInput" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <% } %>
                            
                            <% if ((((string[])Session["add"]).Contains("receivedBy") && action == "create")
                                    || (((string[])Session["edit"]).Contains("receivedBy") && action == "update"))
                                { %>
                            <div class="col-md-4 col-12">
                                <div class="form-group">
                                    <label for="ReceivedByInput">Diterima Oleh</label>
                                    <asp:TextBox ID="ReceivedByInput" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("receivedDate") && action == "create")
                                    || (((string[])Session["edit"]).Contains("receivedDate") && action == "update"))
                                { %>
                            <div class="col-md-4 col-12">
                                <div class="form-group">
                                    <label for="ReceivedDateInput">Tanggal Diterima</label>
                                    <asp:TextBox ID="ReceivedDateInput" class="form-control flatpickr-no-config" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <% } %>
                            
                            <% if ((((string[])Session["add"]).Contains("totalQty") && action == "create")
                                    || (((string[])Session["edit"]).Contains("totalQty") && action == "update"))
                                { %>
                            <div class="col-md-4 col-12">
                                <div class="form-group">
                                    <label for="TotalQtyInput">Jumlah Barang</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="TotalQtyCheckBox" runat="server" OnCheckedChanged="TotalQtyCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="TotalQtyInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                    
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("totalPrice") && action == "create")
                                    || (((string[])Session["edit"]).Contains("totalPrice") && action == "update"))
                                { %>
                            <div class="col-md-4 col-12">
                                <div class="form-group">
                                    <label for="TotalPriceInput">Total Harga</label>
                                    <div class="input-group">
                                        <div class="input-group-text">
                                            <asp:CheckBox ID="TotalPriceCheckBox" runat="server" OnCheckedChanged="TotalPriceCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                        </div>
                                        <asp:TextBox ID="TotalPriceInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <% } %>
                            <% if ((((string[])Session["add"]).Contains("goodsReceiptNotes") && action == "create")
                                    || (((string[])Session["edit"]).Contains("goodsReceiptNotes") && action == "update"))
                                { %>
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="GoodsReceiptNotesInput">Catatan Penerimaan (opsional)</label>
                                    <asp:TextBox ID="GoodsReceiptNotesInput" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                            <% } %>
                            <div class="col-12 col-md-6">
                                
                            </div>
                            <div class="col-12 col-md-6">
                                <asp:Panel ID="SearchPanel" runat="server" CssClass="input-group mb-3" DefaultButton="SearchButton">
                                    <asp:TextBox ID="SearchInput" CssClass="form-control" runat="server" placeholder="Cari (Tekan Enter)" OnTextChanged="SearchInput_TextChanged" AutoPostBack="true"></asp:TextBox>
                                    <asp:LinkButton ID="SearchButton" CssClass="btn btn-primary align-middle" runat="server" OnCommand="SearchButton_Command">
                                        <i class="bi bi-search"></i>
                                    </asp:LinkButton>
                                </asp:Panel>
                            </div>
                            <div class="col-12 mb-2">
                                <div class="overflow-auto">
                                    <% if (((string[])Session["read"]).Contains("item"))
                                        { %>
                                    <asp:GridView ID="ItemGridView" CssClass="table table-striped" runat="server" ShowHeaderWhenEmpty="True" Autogeneratecolumns="false" OnRowCancelingEdit="ItemGridView_RowCancelingEdit" OnRowEditing="ItemGridView_RowEditing">
                                        <Columns>
                                            <asp:BoundField HeaderText="No" DataField="no" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                            <asp:BoundField HeaderText="Barcode" DataField="item_barcode" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                            <asp:BoundField HeaderText="Kode Produk Pemasok" DataField="supplier_product_code" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                            <asp:BoundField HeaderText="Nama Barang" DataField="item_name" ReadOnly="true" />
                                            <asp:BoundField HeaderText="Deskripsi Singkat" DataField="short_description" ReadOnly="true" />
                                            <asp:BoundField HeaderText="Varian" DataField="variants" ReadOnly="true" />
                                            <asp:BoundField HeaderText="Merek" DataField="brand_name" ReadOnly="true" />
                                            <asp:BoundField HeaderText="Harga Jual" DataField="item_price" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                            <asp:BoundField HeaderText="Jumlah Barang" DataField="item_qty" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                            <asp:TemplateField HeaderText="" ItemStyle-CssClass="text-nowrap">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="EditButton" CssClass="btn btn-primary" runat="server" CommandName="edit" CommandArgument='<%# Eval("item_detail_id") %>' OnCommand="EditButton_Command"><i class="bi bi-pencil-fill"></i></asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>    
                                        </Columns>
                                    </asp:GridView>
                                    <% } %>
                                </div>
                            </div>
                            <div class="col-6 col-md-9 text-end">
                                <b>Total Qty</b>
                            </div>
                            <div class="col-6 col-md-3 text-end">
                                <b><asp:Label ID="TotalQtyLabel" runat="server" Text=""></asp:Label></b>
                            </div>
                            <div class="col-6 col-md-9 text-end">
                                <b>Total Harga</b>
                            </div>
                            <div class="col-6 col-md-3 text-end">
                                <b><asp:Label ID="TotalPriceLabel" runat="server" Text=""></asp:Label></b>
                            </div>
                            <div class="col-12">
                                <asp:Button ID="SaveButton" CssClass="btn btn-primary me-2" runat="server" Text="Simpan" OnClick="SaveButton_Click" />
                                <asp:Button ID="DeleteButton" CssClass="btn btn-danger me-2" runat="server" Text="Hapus" OnClick="DeleteButton_Click" />
                                <asp:Button ID="LockButton" CssClass="btn btn-primary me-2" runat="server" Text="Simpan dan Kunci" OnClick="LockButton_Click" />
                                <asp:Button ID="BackButton" CssClass="btn btn-secondary" runat="server" Text="Kembali" OnClick="BackButton_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <% if (((string[])Session["read"]).Contains("item"))
                   { %>
                <div class="modal fade" id="itemModal" tabindex="-1" role="dialog"
                    aria-labelledby="itemModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable modal-lg"
                        role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="itemModalTitle">Informasi Barang</h5>
                                <button type="button" class="btn btn-link" data-bs-dismiss="modal" aria-label="Close">
                                    <i class="bi bi-x-lg"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <asp:Label ID="AlertModalLabel" runat="server" Text=""></asp:Label>
                                    </div>    
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="ItemBarcodeChoice">Barcode Barang</label>
                                            <asp:DropDownList ID="ItemBarcodeChoice" class="choices with-placeholder form-select" runat="server" OnSelectedIndexChanged="ItemBarcodeChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>        
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="ItemNameChoice">Nama Barang</label>
                                            <asp:DropDownList ID="ItemNameChoice" class="choices with-placeholder form-select" runat="server" OnSelectedIndexChanged="ItemNameChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>        
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="DescriptionInput">Deskripsi Barang</label>
                                            <asp:TextBox ID="DescriptionInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="ColorInput">Varian Warna</label>
                                            <asp:TextBox ID="ColorInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SizeInput">Varian Ukuran</label>
                                            <asp:TextBox ID="SizeInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierProductCodeInput">Kode Produk Pemasok</label>
                                            <asp:TextBox ID="SupplierProductCodeInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="BrandInput">Merek</label>
                                            <asp:TextBox ID="BrandInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="PriceInput">Harga Jual</label>
                                            <asp:TextBox ID="PriceInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="QtyInput">Kuantitas</label>
                                            <asp:TextBox ID="QtyInput" CssClass="form-control" runat="server" autocomplete="off"></asp:TextBox>        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="CloseButton" CssClass="btn btn-light-secondary" runat="server" Text="Tutup" OnClick="CloseButton_Click" />
                                <asp:Button ID="AddButton" CssClass="btn btn-primary ms-1" runat="server" Text="Simpan" CommandName="edit" OnCommand="AddButton_Command" />
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
                <% if ((bool)Session["delete"] == true)
                    { %>
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="confirmModalTitle">Konfirmasi</h5>
                                <button type="button" class="btn btn-link" data-bs-dismiss="modal" aria-label="Close">
                                    <i data-feather="x"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Apakah ingin menghapus dokumen penerimaan tersebut?</p>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="NoButton" CssClass="btn btn-secondary" runat="server" Text="Tidak" OnClick="NoButton_Click" />
                                                    
                                <asp:Button ID="YesButton" CssClass="btn btn-primary ms-1" runat="server" Text="Ya" CommandName="delete" OnCommand ="YesButton_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
            </section>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script type="text/javascript">
        function openItemModal() {
            var modal = new bootstrap.Modal(document.getElementById('itemModal'));
            modal.show();
        }
        function openConfirmModal() {
            var modal = new bootstrap.Modal(document.getElementById('confirmModal'));
            modal.show();
        }
    </script>
</asp:Content>


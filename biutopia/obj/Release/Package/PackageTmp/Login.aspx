﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="biutopia.Login" %>
<!DOCTYPE html>
<html lang="id">
    <head runat="server">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>PT Star Maju Sentosa</title>
        <link rel="shortcut icon" href="assets/svg/favicon.svg" type="image/x-icon">
        
        <link href="assets/bootstrap-5.2.3/css/bootstrap.css" rel="stylesheet" />
    </head>
    <body>
        <form id="form1" runat="server">
            <section class="bg-image vh-100 d-flex align-items-center" style="background: url(assets/images/shopping.png) no-repeat center center fixed;background-size: cover;">
                <div class="container">
                <div class="row">
                <div class="col-12 col-md-6 mx-auto">
                <div class="card px-4 pt-5">
                    <div class="card-body">
                        <asp:Login ID="LoginControl" runat="server" OnAuthenticate="ValidateUser" OnLoginError="LoginError" Width="100%">
                            <LayoutTemplate>
                                <asp:CustomValidator ID="CustomValidator1" runat="server" Text="Nama pengguna atau kata sandi anda salah" validationgroup="LoginControl" visible="True" CssClass="d-block alert alert-danger" Display="Static"></asp:CustomValidator>
                                    
                                <!-- Email input -->
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="username">Email address</label>
                                    <asp:TextBox ID="username" runat="server" CssClass="form-control form-control-lg" placeholder="Username" TextMode="SingleLine"></asp:TextBox>
                                    
                                </div>
                                <!-- Password input -->
                                <div class="form-outline mb-4">
                                    <label class="form-label" for="password">Password</label>
                                    <asp:TextBox ID="password" runat="server" CssClass="form-control form-control-lg" placeholder="Kata sandi" TextMode="Password"></asp:TextBox>
                                </div>
                                
                                <!-- Submit button -->
                                <asp:Button ID="LoginButton" runat="server" CommandName="Login" CssClass="btn btn-primary btn-lg btn-block" Text="Masuk" />
                                    
                                <asp:CustomValidator ID="CustomValidator2" runat="server" Text="Internal server error, silakan hubungi administrator" validationgroup="LoginControl" visible="True" CssClass="d-block alert alert-danger" Display="Static"></asp:CustomValidator>
                            </LayoutTemplate>
                        </asp:Login>
                    </div> 
                </div> 
                </div>
                </div>
                </div>
            </section>
            
        </form>

        <script src="assets/bootstrap-5.2.3/js/bootstrap.js"></script>
    </body>
</html>
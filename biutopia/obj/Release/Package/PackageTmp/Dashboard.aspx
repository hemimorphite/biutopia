﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="biutopia.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main-content">
        <div class="page-heading">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 order-md-1 order-last">
                        <h3>Dasbor</h3>
                        <p class="text-subtitle text-muted"></p>
                    </div>
                </div>
            </div>
            <section class="section">
                <div class="card">
                <div class="card-body mt-5 mb-5">
                    <%--<div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="float-end">
                                    <a class="btn btn-primary px-5 me-2" onclick="printPageArea('print')"> Print </a>
                                    <a class="btn btn-secondary px-5"> Export </a>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="container" id="print">
                        <div class="row">
                            <div class="col-12">
                                <div class="text-center">
                                    <h1 class="mb-5">PT ABC Indonesia</h1>
                                </div>
                            </div>
                            <div class="col-12 col-md-8 mb-4"></div> 
                            <div class="col-12 col-md-4 text-end ms-auto mb-4">
                                <h3>PURCHASE ORDER</h3>
                                <div class="row">
                                    <div class="col-7">Date:</div>
                                    <div class="col-5">12 Jan 2024</div>
                                    <div class="col-7">Purchase Order No:</div>
                                    <div class="col-5">865757557</div>
                                </div>
                            </div>
                        </div>
                        <div class="row two-cols">
                            <div class="col-12 col-md-7 mb-4">
                                <h6>Vendor Information</h6>
                                <div>PT ABC Indonesia</div>
                                <div>Plaza Summarecon, Jl. Perintis Kemerdekaan No.42</div>
                                <div>RT.10/RW.16, Kayu Putih, Kec. Pulo Gadung</div>
                                <div>Kota Jakarta Timur</div>
                                <div>13210</div>
                            </div>
                            <div class="col-12 col-md-5 mb-4">
                                <h6>Customer Information</h6>
                                <div>Rambla SMS</div>
                                <div>Plaza Summarecon, Jl. Perintis Kemerdekaan No.42</div>
                                <div>RT.10/RW.16, Kayu Putih, Kec. Pulo Gadung</div>
                                <div>Kota Jakarta Timur</div>
                                <div>13210</div>
                            </div>
                        </div> 
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-striped table-bordered mb-4">
                                    <thead class="table-dark">
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Barcode</th>
                                            <th scope="col">Details</th>
                                            <th scope="col">Qty</th>
                                            <th scope="col">Unit Price</th>
                                            <th scope="col">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row" colspan="3" class="text-end"><b>Total</b></td>
                                            <td class="text-end">200</td>
                                            <td class="text-end">&nbsp;</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                    </div>--%>
                </div>
                </div>
            </section>
            <!--<section class="section">
                <div class="card">
                <div class="card-body mt-5 mb-5">
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                <div class="float-end">
                                    <a class="btn btn-primary px-5 me-2" onclick="printPageArea('print')"> Print </a>
                                    <a class="btn btn-secondary px-5"> Export </a>
                                </div>
                                <div class="clearfix"></div>
                                <hr>
                            </div>
                        </div>
                    </div>
                    <div class="container" id="printarea" style="display:none;">
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-borderless">
                                    <tr>
                                        <td colspan="10" class="text-center"><h1>PT ABC Indonesia</h1></td>
                                    </tr>
                                    <tr>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">&nbsp;</td>
                                        <td colspan="4" class="text-end"><h3>PURCHASE ORDER</h3></td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="py-0">&nbsp;</td>
                                        <td colspan="2" class="text-end py-0">Date:</td>
                                        <td colspan="2" class="py-0">12 Jan 2024</td>
                                    </tr>
                                    <tr>
                                        <td colspan="6" class="py-0">&nbsp;</td>
                                        <td colspan="2" class="text-end py-0">Purchase Order No:</td>
                                        <td colspan="2" class="py-0">865757557865757557</td>
                                    </tr>
                                    <tr>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                        <td style="width:10%;">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="py-0"><h6>Vendor Information</h6></td>
                                        <td colspan="2" class="py-0">&nbsp;</td>
                                        <td colspan="4" class="py-0"><h6>Customer Information</h6></td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="py-0">PT ABC Indonesia</td>
                                        <td colspan="2" class="py-0">&nbsp;</td>
                                        <td colspan="4" class="py-0">Rambla SMS</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="py-0">Plaza Summarecon, Jl. Perintis Kemerdekaan No.42</td>
                                        <td colspan="2" class="py-0">&nbsp;</td>
                                        <td colspan="4" class="py-0">Plaza Summarecon, Jl. Perintis Kemerdekaan No.42</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="py-0">RT.10/RW.16, Kayu Putih, Kec. Pulo Gadung</td>
                                        <td colspan="2" class="py-0">&nbsp;</td>
                                        <td colspan="4" class="py-0">RT.10/RW.16, Kayu Putih, Kec. Pulo Gadung</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="py-0">Kota Jakarta Timur</td>
                                        <td colspan="2" class="py-0">&nbsp;</td>
                                        <td colspan="4" class="py-0">Kota Jakarta Timur</td>
                                    </tr>
                                    <tr>
                                        <td colspan="4" class="py-0">13210</td>
                                        <td colspan="2" class="py-0">&nbsp;</td>
                                        <td colspan="4" class="py-0">13210</td>
                                    </tr>
                                </table> 
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <table class="table table-bordered mb-4" style="border-color:#000;">
                                    <thead>
                                        <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Barcode</th>
                                            <th scope="col">Details</th>
                                            <th scope="col">Qty</th>
                                            <th scope="col">Unit Price</th>
                                            <th scope="col">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        <tr>
                                            <td scope="row">1</td>
                                            <td>8997031665457</td>
                                            <td>BLINK CHARM NATURAL FLAIR #3 - 4 PAIR</td>
                                            <td class="text-end">10</td>
                                            <td class="text-end">142900.00</td>
                                            <td class="text-end">1429000.00</td>
                                        </tr>
                                        
                                    </tbody>
                                </table>
                                
                            </div>
                        </div>
                        
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col-12">
                                
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </section>-->
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script type="text/javascript">
        function printPageArea(areaID) {
            var printContent = document.getElementById(areaID).innerHTML;
            var originalContent = document.body.innerHTML;
            document.body.innerHTML = printContent;
            window.print();
            document.body.innerHTML = originalContent;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <script type="text/javascript">
        
    </script>
</asp:Content>
﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="GoodsReceiptDetail.aspx.cs" EnableViewState="True" Inherits="biutopia.GoodsReceiptDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main-content">
        <div class="page-heading">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 order-md-1 order-last">
                        <h3>Goods Receipt</h3>
                        <p class="text-subtitle text-muted"></p>
                    </div>
                </div>
            </div>
            <section class="section">
                <div class="card">
                    <div class="card-body mt-5 mb-5">
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <div class="float-end">
                                        <% if ((bool)Session["print"] == true)
                                            { %>
                                        <a class="btn btn-primary px-5 me-2" onclick="printPageArea('print')"> Print </a>
                                        <a class="btn btn-secondary px-5" onclick="downloadPdf('print')"> Export </a>
                                        <% } %>
                                    </div>
                                    <div class="clearfix"></div>
                                    <hr>
                                </div>
                            </div>
                        </div>
                    
                        <div class="container" id="print">
                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <h1 class="mb-5"><asp:Label ID="HeaderLabel" runat="server" Text=""></asp:Label></h1>
                                    </div>
                                </div>
                                <div class="col-12 col-md-8 mb-4"></div> 
                                <div class="col-12 col-md-4 mb-4">
                                    <h3>Goods Receipt</h3>
                                    <asp:Panel ID="GoodsReceiptDatePanel" class="row text-end" runat="server">
                                        <div class="col-6">Goods Receipt Date:</div>
                                        <div class="col-6"><asp:Label ID="GoodsReceiptDateLabel" runat="server" Text=""></asp:Label></div>
                                    </asp:Panel>
                                    <asp:Panel ID="GoodsReceiptNoPanel" class="row text-end" runat="server">
                                        <div class="col-6">Goods Receipt No:</div>
                                        <div class="col-6"><asp:Label ID="GoodsReceiptNoLabel" runat="server" Text=""></asp:Label></div>
                                    </asp:Panel>
                                    <asp:Panel ID="InvoiceNoPanel" class="row text-end" runat="server">
                                        <div class="col-6">Invoice No:</div>
                                        <div class="col-6"><asp:Label ID="InvoiceNoLabel" runat="server" Text=""></asp:Label></div>
                                    </asp:Panel>
                                </div>
                            </div>
                            <div class="row two-cols">
                                <div class="col-12 col-md-6 mb-4">
                                    <h5>Department Store Information</h5>
                                    <div><asp:Label ID="StoreNameLabel" runat="server" Text=""></asp:Label></div>
                                    <div>
                                        <asp:Label ID="StoreBuildingNameLabel" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="StoreStreetNameLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div>
                                        <asp:Label ID="StoreNeighbourhoodLabel" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="StoreSubdistrictLabel" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="StoreDistrictLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div>
                                        <asp:Label ID="StoreRuralDistrictLabel" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="StoreProvinceLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div><asp:Label ID="StoreZipcodeLabel" runat="server" Text=""></asp:Label></div>
                                </div>
                                <div class="col-12 col-md-6 mb-4">
                                    <h5>Vendor Information</h5>
                                    <div><asp:Label ID="SupplierNameLabel" runat="server" Text=""></asp:Label></div>
                                    <div>
                                        <asp:Label ID="SupplierBuildingNameLabel" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="SupplierStreetNameLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div>
                                        <asp:Label ID="SupplierNeighbourhoodLabel" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="SupplierSubdistrictLabel" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="SupplierDistrictLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div>
                                        <asp:Label ID="SupplierRuralDistrictLabel" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="SupplierProvinceLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div><asp:Label ID="SupplierZipcodeLabel" runat="server" Text=""></asp:Label></div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-12">
                                    <asp:GridView ID="ItemGridView" CssClass="table table-bordered mb-4" runat="server" ShowHeaderWhenEmpty="True" Autogeneratecolumns="false">
                                        <Columns>
                                            <asp:BoundField HeaderText="No" DataField="no" ItemStyle-CssClass="text-nowrap" />
                                            <asp:BoundField HeaderText="Supplier Product Code" DataField="supplier_product_code" ItemStyle-CssClass="text-nowrap" />
                                            <asp:BoundField HeaderText="Barcode" DataField="item_barcode" ItemStyle-CssClass="text-nowrap" />
                                            <asp:BoundField HeaderText="Details" DataField="details" />
                                            <asp:BoundField HeaderText="Qty" DataField="item_qty" ItemStyle-CssClass="text-nowrap text-end" />
                                            <asp:BoundField HeaderText="Unit Price" DataField="item_price" ItemStyle-CssClass="text-nowrap text-end" />
                                            <asp:BoundField HeaderText="Amount" DataField="amount" ItemStyle-CssClass="text-nowrap text-end" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                <asp:Panel ID="GoodsReceiptNotesPanel" CssClass="col-12" runat="server">
                                    <b>Catatan:</b> <asp:Label ID="GoodsReceiptNotesLabel" runat="server" Text=""></asp:Label>
                                </asp:Panel>
                            </div>
                        </div>
                    
                        <div class="container">
                            <div class="row">
                                <div class="col-12">
                                    <hr>
                                    <asp:Button ID="BackButton" CssClass="btn btn-secondary px-4" runat="server" Text="Kembali" OnClick="BackButton_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            </section>
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
    <script type="text/javascript">
        function printPageArea(areaID) {
            var printContent = document.getElementById(areaID).innerHTML;
            var originalContent = document.body.innerHTML;
            document.body.innerHTML = printContent;
            window.print();
            document.body.innerHTML = originalContent;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <script type="text/javascript">
        function downloadPdf(areaID) {
            window.jsPDF = window.jspdf.jsPDF;

            var doc = new jsPDF();

            // Source HTMLElement or a string containing HTML.
            var elementHTML = document.querySelector("#" + areaID);

            doc.html(elementHTML, {
                callback: function (doc) {
                    // Save the PDF
                    var pageCount = doc.internal.getNumberOfPages();
                    doc.deletePage(pageCount);
                    pageCount = doc.internal.getNumberOfPages();
                    doc.deletePage(pageCount);
                    pageCount = doc.internal.getNumberOfPages();
                    doc.deletePage(pageCount);
                    pageCount = doc.internal.getNumberOfPages();
                    doc.deletePage(pageCount);
                    pageCount = doc.internal.getNumberOfPages();
                    doc.deletePage(pageCount);
                    pageCount = doc.internal.getNumberOfPages();
                    doc.deletePage(pageCount);
                    pageCount = doc.internal.getNumberOfPages();
                    doc.deletePage(pageCount);
                    
                    var filename = "goods-receipt-" + Date.now() + ".pdf"
                    doc.save(filename);
                    //doc.save('sample-document.pdf');
                },
                autoPaging: 'text',
                margin: [10, 10, 20, 10],
                width: 190, //target width in the PDF document
                windowWidth: 900, //window width in CSS pixels
            });
        }
        
    </script>
</asp:Content>

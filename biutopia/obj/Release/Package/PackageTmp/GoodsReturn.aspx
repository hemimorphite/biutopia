﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" MaintainScrollPositionOnPostBack="true"  AutoEventWireup="true" CodeBehind="GoodsReturn.aspx.cs" EnableViewState="True" Inherits="biutopia.GoodsReturn" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="main-content">
        <div class="page-heading">
            <div class="page-title">
                <div class="row">
                    <div class="col-12 order-md-1 order-last">
                        <h3>Pengembalian Barang</h3>
                        <p class="text-subtitle text-muted"></p>
                    </div>
                </div>
            </div>
            <section class="section">
                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <% if (((string[])Session["read"]).Length > 0)
                                { %>
                            <li class="nav-item" role="presentation">
                                <asp:LinkButton ID="TabularTabButton" runat="server" href="#tabular" OnClick="TabularTabButton_Click" CssClass="nav-link active" data-bs-toggle="tab" role="tab"
                                    aria-controls="tabular" aria-selected="true">Pengembalian Barang</asp:LinkButton>
                            </li>
                            <% } %>
                            <% if ((bool)Session["upload"] == true)
                                { %>
                            <li class="nav-item" role="presentation">
                                <asp:LinkButton ID="UploadTabButton" runat="server" href="#upload" OnClick="UploadTabButton_Click" CssClass="nav-link" data-bs-toggle="tab" role="tab"
                                    aria-controls="upload" aria-selected="false">Unggah</asp:LinkButton>
                            </li>
                            <% } %>
                        </ul>
                        <div class="tab-content mt-4" id="myTabContent">
                            <% if (((string[])Session["read"]).Length > 0)
                                { %>
                            <div class="tab-pane fade show active" id="tabular" role="tabpanel" aria-labelledby="tabularTab">
                                <div class="row">
                                    <div class="col-12">
                                        <asp:Label ID="AlertLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreChoice">Toko Ritel</label>
                                            <asp:DropDownList ID="StoreChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="StoreChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierChoice">Pemasok</label>
                                            <asp:DropDownList ID="SupplierChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="SupplierChoice_SelectedIndexChanged" AutoPostBack="true">

                                            </asp:DropDownList>
                                        </div>
                                    </div> 
                                    
                                    <div class="col-12 mb-5"></div>
                                    <div class="col-12 col-md-6">
                                        <% if (((string[])Session["add"]).Length > 0)
                                            { %>
                                        <asp:LinkButton ID="AddButton" CssClass="btn btn-primary align-middle" runat="server" OnClick="AddButton_Click">
                                            <i class="bi bi-plus-lg"></i> Pengembalian
                                        </asp:LinkButton>
                                        <% } %>
                                    </div>
                                    <div class="col-12 col-md-6">
                                        <asp:Panel ID="SearchPanel" runat="server" CssClass="input-group mb-3" DefaultButton="SearchButton">
                                            <asp:DropDownList ID="FilterChoice" OnSelectedIndexChanged="FilterChoice_SelectedIndexChanged" style="min-width:150px;width:150px;text-overflow: ellipsis;color: #607080;padding: 0.375rem 0.75rem;border: 1px solid #dce7f1;background-color: #fff;" AutoPostBack="True" runat="server">
                                                
                                            </asp:DropDownList>
                                            <asp:DropDownList ID="OperatorChoice" OnSelectedIndexChanged="OperatorChoice_SelectedIndexChanged" style="min-width:50px;width:50px;color: #607080;padding: 0.37rem 0.4rem;border: 1px solid #dce7f1;background-color: #fff;" AutoPostBack="True" runat="server">
                                                <asp:ListItem Selected="True" Value="61">&#61;</asp:ListItem>
                                                <asp:ListItem Value="8596">&#8596;</asp:ListItem>
                                                <asp:ListItem Value="62">&gt;</asp:ListItem>
                                                <asp:ListItem Value="60">&lt;</asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:TextBox ID="ValueOneInput" CssClass="form-control" runat="server" placeholder="" onfocus="this.select();"></asp:TextBox>
                                            <asp:TextBox ID="ValueTwoInput" CssClass="form-control" runat="server" placeholder="" onfocus="this.select();"></asp:TextBox>
                                            <asp:LinkButton ID="SearchButton" CssClass="btn btn-primary align-middle" runat="server" OnCommand="SearchButton_Command">
                                                <i class="bi bi-search"></i>
                                            </asp:LinkButton>
                                        </asp:Panel>
                                    </div>

                                    <div class="col-12">
                                        <div class="overflow-auto mb-4">
                                            <% if (((string[])Session["read"]).Length > 0)
                                                { %>
                                            <asp:GridView ID="GoodsReturnGridView" CssClass="table table-striped" runat="server" ShowHeaderWhenEmpty="True" Autogeneratecolumns="false" AllowSorting="True" OnSorting="GoodsReturnGridView_Sorting" AllowPaging="True" PageSize="5" OnPageIndexChanging="GoodsReturnGridView_PageIndexChanging" OnRowDeleting="GoodsReturnGridView_RowDeleting" OnRowCancelingEdit="GoodsReturnGridView_RowCancelingEdit" OnRowEditing="GoodsReturnGridView_RowEditing" OnRowDataBound="GoodsReturnGridView_RowDataBound">
                                                <%--<Columns>
                                                    <asp:BoundField HeaderText="No" DataField="no" SortExpression="no" ReadOnly="true" ItemStyle-CssClass="text-nowrap"/>
                                                    <asp:BoundField HeaderText="No Penerimaan" DataField="goods_return_no" SortExpression="goods_return_no" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Tanggal Pengembalian" DataField="goods_return_date" SortExpression="goods_return_date" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="No Pengiriman" DataField="delivery_no" SortExpression="delivery_no" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Diajukan Oleh" DataField="requested_by" SortExpression="requested_by" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Tanggal Diajukan" DataField="requested_date" SortExpression="requested_date" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Toko Ritel" DataField="store_name" SortExpression="store_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Pemasok" DataField="supplier_name" SortExpression="supplier_name" ReadOnly="true" />
                                                    <asp:BoundField HeaderText="Total Harga" DataField="total_price" SortExpression="total_price" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Jumlah Barang" DataField="total_qty" SortExpression="total_qty" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Tanggal Disetujui/Ditolak" DataField="approval_date" SortExpression="approval_date" ReadOnly="true" ItemStyle-CssClass="text-nowrap" />
                                                    <asp:BoundField HeaderText="Disetujui/Ditolak Oleh" DataField="approval_by" SortExpression="approval_by" ReadOnly="true" />
                                                    <asp:TemplateField HeaderText="Status" SortExpression="approval_status" >  
                                                        <ItemTemplate>  
                                                            <span class='badge rounded-pill <%# Eval("approval_status").ToString() == "pending" ? "bg-warning" : Eval("approval_status").ToString() == "cancelled" ? "bg-danger" : Eval("approval_status").ToString() == "approved" ? "bg-primary" : "" %>'>
                                                                <asp:Label ID="Label2" runat="server" Text='<%# Bind("approval_status") %>'></asp:Label>
                                                            </span>
                                                        </ItemTemplate>  
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="" ItemStyle-CssClass="text-nowrap">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="ApproveButton" CssClass="btn btn-primary me-1" runat="server" CommandName="approve" CommandArgument='<%# Eval("purchase_order_id") + ";" + Eval("uuid") %>' Visible='<%# (Eval("purchase_status").ToString() == "pending" || Eval("purchase_status").ToString() == "cancelled") ? true : false %>' OnCommand="ApproveButton_Command">Setuju</asp:LinkButton>
                                                            <asp:LinkButton ID="CancelButton" CssClass="btn btn-warning me-1" runat="server" CommandName="cancel" CommandArgument='<%# Eval("purchase_order_id") + ";" + Eval("uuid") %>' Visible='<%# (Eval("purchase_status").ToString() == "pending" || Eval("purchase_status").ToString() == "approved") ? true : false %>' OnCommand="CancelButton_Command">Batal</asp:LinkButton>
                                                            <asp:LinkButton ID="DeleteButton" CssClass="btn btn-danger me-1" runat="server" CommandName="delete" CommandArgument='<%# Eval("purchase_order_id") + ";" + Eval("uuid") %>' Visible='true' OnCommand="DeleteButton_Command">Hapus</asp:LinkButton>
                                                            <asp:LinkButton ID="ShowButton" CssClass="btn btn-primary me-1" runat="server" CommandName="show" CommandArgument='<%# Eval("purchase_order_id") + ";" + Eval("uuid")  %>' Visible='<%# !String.IsNullOrEmpty(Eval("delivery_no").ToString()) ? true : false %>' OnCommand="ShowButton_Command">Lihat Detil</asp:LinkButton>
                                                            <asp:LinkButton ID="EditButton" CssClass="btn btn-primary me-1" runat="server" CommandName="edit" CommandArgument='<%# Eval("purchase_order_id") + ";" + Eval("uuid")  %>' Visible='true' OnCommand="EditButton_Command">Ubah</asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>--%>
                                                <PagerTemplate>
                                                    <div class="pagination justify-content-center">
                                                        <asp:Panel ID="FirstPanel" CssClass="page-item me-2" runat="server">
                                                            <asp:LinkButton ID="FirstButton" CssClass="page-link" runat="server" OnClick="FirstButton_Click">First</asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="PreviousPanel" CssClass="page-item me-2" runat="server">
                                                            <asp:LinkButton ID="PreviousButton" CssClass="page-link" runat="server" OnClick="PreviousButton_Click">Previous</asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="PagePanel" CssClass="page-item me-2" runat="server">
                                                            <asp:DropDownList ID="PageDropDownList" class="page-link" runat="server" AutoPostBack="true" OnSelectedIndexChanged="PageDropDownList_SelectedIndexChanged">
                                                        
                                                            </asp:DropDownList>
                                                        </asp:Panel>
                                                        <asp:Panel ID="NextPanel" CssClass="page-item me-2" runat="server">
                                                            <asp:LinkButton ID="NextButton" CssClass="page-link" runat="server" OnClick="NextButton_Click">Next</asp:LinkButton>
                                                        </asp:Panel>
                                                        <asp:Panel ID="LastPanel" CssClass="page-item" runat="server">
                                                            <asp:LinkButton ID="LastButton" CssClass="page-link" runat="server" OnClick="LastButton_Click">Last</asp:LinkButton>
                                                        </asp:Panel>
                                                    </div>
                                                </PagerTemplate>
                                                <PagerSettings Mode="NumericFirstLast" PageButtonCount="5" />
                                            </asp:GridView>
                                            <% } %>
                                        </div>
                                        <asp:Panel ID="LoadingPanel" CssClass="d-none mb-2" runat="server">
                                            <div class="spinner-grow spinner-grow-sm text-primary" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-secondary" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-success" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-danger" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-warning" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-info" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-light" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <div class="spinner-grow spinner-grow-sm text-dark" role="status">
                                              <span class="visually-hidden">Loading...</span>
                                            </div>
                                            <span>Loading</span>
                                        </asp:Panel>

                                        <asp:Button ID="DownloadAllButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Semua" CommandName="downloadall" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:Button ID="DownloadSelectedSupplierButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Berdasarkan Vendor" CommandName="downloadselectedsupplier" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:Button ID="DownloadSelectedStoreButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Berdasarkan Toserba" CommandName="downloadselectedstore" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:Button ID="DownloadSelectedStoreAndSupplierButton" CssClass="btn btn-primary me-2 px-2" runat="server" Text="Unduh Berdasarkan Toserba dan Vendor" CommandName="downloadselectedstoreandsupplier" CommandArgument="" OnCommand="DownloadButton_Command" />
                                        <asp:HiddenField ID="FilenameInput" runat="server" />
                                        <div class="mb-5"></div>
                                    </div> 
                                </div>
                            </div>
                            <% } %>
                            <% if ((bool)Session["upload"] == true)
                                { %>
                            <div class="tab-pane fade" id="upload" role="tabpanel" aria-labelledby="uploadTab">
                                <div class="row">
                                    <div class="col-12">
                                        <asp:Label ID="AlertUploadLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreUploadChoice">Toko Ritel</label>
                                            <asp:DropDownList ID="StoreUploadChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="StoreUploadChoice_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierUploadChoice">Pemasok</label>
                                            <asp:DropDownList ID="SupplierUploadChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="SupplierUploadChoice_SelectedIndexChanged" AutoPostBack="true">

                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    
                                    <% if (((string[])Session["add"]).Contains("purchaseOrder"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="PurchaseOrderChoice">No Pengiriman</label>
                                            <asp:DropDownList ID="PurchaseOrderChoice" class="choices form-select" runat="server" OnSelectedIndexChanged="PurchaseOrderChoice_SelectedIndexChanged" AutoPostBack="true">

                                            </asp:DropDownList>
                                        </div>
                                    </div>
                                    <% } %>
                                    
                                    <% if (((string[])Session["add"]).Contains("supplierName"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierNameInput">Nama Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierNameCheckBox" runat="server" OnCheckedChanged="SupplierNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierBuildingName"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierBuildingNameInput">Nama Bangunan Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierBuildingNameCheckBox" runat="server" OnCheckedChanged="SupplierBuildingNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierBuildingNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierStreetName"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierStreetNameInput">Nama Jalan Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierStreetNameCheckBox" runat="server" OnCheckedChanged="SupplierStreetNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierStreetNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierNeighbourhood"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierNeighbourhoodInput">Keterangan RT dan RW Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierNeighbourhoodCheckBox" runat="server" OnCheckedChanged="SupplierNeighbourhoodCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierNeighbourhoodInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierSubdistrict"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierSubdistrictInput">Nama Kelurahan Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierSubdistrictCheckBox" runat="server" OnCheckedChanged="SupplierSubdistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierSubdistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierDistrict"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierDistrictInput">Nama Kecamatan Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierDistrictCheckBox" runat="server" OnCheckedChanged="SupplierDistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierDistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierRuralDistrict"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierRuralDistrictInput">Nama Kota atau Kabupaten Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierRuralDistrictCheckBox" runat="server" OnCheckedChanged="SupplierRuralDistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierRuralDistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierProvince"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierProvinceInput">Nama Provinsi Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierProvinceCheckBox" runat="server" OnCheckedChanged="SupplierProvinceCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierProvinceInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierZipcode"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierZipcodeInput">Kode Pos Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierZipcodeCheckBox" runat="server" OnCheckedChanged="SupplierZipcodeCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierZipcodeInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierContactName"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierContactNameInput">Nama Kontak Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierContactNameCheckBox" runat="server" OnCheckedChanged="SupplierContactNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierContactNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierContactPhone"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierContactPhoneInput">Nomor Telepon Kontak Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierContactPhoneCheckBox" runat="server" OnCheckedChanged="SupplierContactPhoneCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierContactPhoneInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("supplierContactEmail"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierContactEmailInput">Email Pemasok</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="SupplierContactEmailCheckBox" runat="server" OnCheckedChanged="SupplierContactEmailCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="SupplierContactEmailInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeName"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreNameInput">Nama Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreNameCheckBox" runat="server" OnCheckedChanged="StoreNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeBuildingName"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreBuildingNameInput">Nama Bangunan Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreBuildingNameCheckBox" runat="server" OnCheckedChanged="StoreBuildingNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreBuildingNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeStreetName"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreStreetNameInput">Nama Jalan Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreStreetNameCheckBox" runat="server" OnCheckedChanged="StoreStreetNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreStreetNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeNeighbourhood"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreNeighbourhoodInput">Keterangan RT dan RW Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreNeighbourhoodCheckBox" runat="server" OnCheckedChanged="StoreNeighbourhoodCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreNeighbourhoodInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeSubdistrict"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreSubdistrictInput">Nama Kelurahan atau Desa Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreSubdistrictCheckBox" runat="server" OnCheckedChanged="StoreSubdistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreSubdistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeDistrict"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreDistrictInput">Nama Kecamatan Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreDistrictCheckBox" runat="server" OnCheckedChanged="StoreDistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreDistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>

                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeRuralDistrict"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreRuralDistrictInput">Nama Kota atau Kabupaten Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreRuralDistrictCheckBox" runat="server" OnCheckedChanged="StoreRuralDistrictCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreRuralDistrictInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeProvince"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreProvinceInput">Nama Provinsi Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreProvinceCheckBox" runat="server" OnCheckedChanged="StoreProvinceCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreProvinceInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeZipcode"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreZipcodeInput">Kode Pos Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreZipcodeCheckBox" runat="server" OnCheckedChanged="StoreZipcodeCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreZipcodeInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeContactName"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreContactNameInput">Nama Kontak Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreContactNameCheckBox" runat="server" OnCheckedChanged="StoreContactNameCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreContactNameInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeContactphone"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreContactPhoneInput">Nomor Telepon Kontak Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreContactPhoneCheckBox" runat="server" OnCheckedChanged="StoreContactPhoneCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreContactPhoneInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("storeContactEmail"))
                                        { %>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="StoreContactEmailInput">Email Toko Ritel</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="StoreContactEmailCheckBox" runat="server" OnCheckedChanged="StoreContactEmailCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="StoreContactEmailInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("goodsReturnNo"))
                                        { %>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group">
                                            <label for="GoodsReturnNoInput">No Pengembalian</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="GoodsReturnNoCheckBox" runat="server" OnCheckedChanged="GoodsReturnNoCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="GoodsReturnNoInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("goodsReturnDate"))
                                        { %>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group">
                                            <label for="GoodsReturnDateInput">Tanggal Pengembalian</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="GoodsReturnDateCheckBox" runat="server" OnCheckedChanged="GoodsReturnDateCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="GoodsReturnDateInput" class="form-control flatpickr-no-config" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    
                                    <% if (((string[])Session["add"]).Contains("requestedBy"))
                                        { %>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group">
                                            <label for="RequestedByInput">Diajukan Oleh</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="RequestedByCheckBox" runat="server" OnCheckedChanged="RequestedByCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="RequestedByInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("requestedDate"))
                                        { %>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group">
                                            <label for="RequestedDateInput">Tanggal Diajukan</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="RequestedDateCheckBox" runat="server" OnCheckedChanged="RequestedDateCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="RequestedDateInput" class="form-control flatpickr-no-config" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("approvalBy"))
                                        { %>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group">
                                            <label for="ApprovalByInput">Disetujui/Ditolak Oleh</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="ApprovalByCheckBox" runat="server" OnCheckedChanged="ApprovalByCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="ApprovalByInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("approvalDate"))
                                        { %>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group">
                                            <label for="ApprovalDateInput">Tanggal Disetujui/Ditolak</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="ApprovalDateCheckBox" runat="server" OnCheckedChanged="ApprovalDateCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="ApprovalDateInput" class="form-control flatpickr-no-config" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("totalQty"))
                                        { %>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group">
                                            <label for="TotalQtyInput">Jumlah Barang</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="TotalQtyCheckBox" runat="server" OnCheckedChanged="TotalQtyCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="TotalQtyInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("totalPrice"))
                                        { %>
                                    <div class="col-md-4 col-12">
                                        <div class="form-group">
                                            <label for="TotalPriceInput">Total Harga</label>
                                            <div class="input-group">
                                                <div class="input-group-text">
                                                    <asp:CheckBox ID="TotalPriceCheckBox" runat="server" OnCheckedChanged="TotalPriceCheckBox_CheckedChanged" Checked="true" AutoPostBack="true" />
                                                </div>
                                                <asp:TextBox ID="TotalPriceInput" class="form-control" runat="server" disabled="disabled"></asp:TextBox>
                                            </div>
                                        </div>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("approvalStatus"))
                                        { %>
                                    <div class="col-md-4 col-12">
                                        <fieldset class="form-group">
                                            <legend style="font-size:1rem;">Status Pengembalian</legend>
                                            <asp:RadioButtonList ID="ApprovalStatusChoice" runat="server" OnSelectedIndexChanged="ApprovalStatusChoice_SelectedIndexChanged" AutoPostBack="true">
                                                <asp:ListItem Enabled="True" Selected="True" Text="Pending" CssClass="form-check-input" Value="pending" />
                                                <asp:ListItem Enabled="True" Selected="False" Text="Approved" CssClass="form-check-input" Value="approved" />
                                                <asp:ListItem Enabled="True" Selected="False" Text="Cancelled" Value="cancelled" />
                                            </asp:RadioButtonList>
                                        </fieldset>
                                    </div>
                                    <% } %>
                                    <% if (((string[])Session["add"]).Contains("goodsReturnNotes"))
                                    { %>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="GoodsReturnNotesInput">Catatan Pengembalian (opsional)</label>
                                            <asp:TextBox ID="GoodsReturnNotesInput" class="form-control" runat="server" TextMode="MultiLine"></asp:TextBox>
                                        </div>
                                    </div>
                                    <% } %>
                                    
                                    <div class="col-12 mb-2">
                                        <div class="form-group">
                                            <asp:Label ID="UploadLabel"
                                                        Text="Berkas Excel"
                                                        AssociatedControlID="UploadInput"
                                                        runat="server">
                                            </asp:Label>
                                            <asp:FileUpload ID="UploadInput" CssClass="form-control" runat="server" />
                                            <asp:RegularExpressionValidator ID="UploadRegularExpressionValidator" runat="server"
                                            ControlToValidate="UploadInput"
                                            ErrorMessage="Ekstensi berkas tidak didukung, hanya berkas excel yang didukung"
                                            ValidationExpression="^([0-9a-zA-Z_\-~ :\\])+(.xls|.xlsx|.csv)$" ForeColor="#dc3545">
                                            </asp:RegularExpressionValidator>
                                            <!--<input class="form-control" type="file" id="uploadItem">-->
                                        </div>
                                    </div>
                                    <div class="btn-group">
                                        <asp:Button ID="UploadButton" CssClass="btn btn-primary mb-5" runat="server" Text="Unggah Berkas" OnClick="UploadButton_Click" />  
                                    </div>
                                        
                                    <div class="col-12">
                                        <div class="overflow-auto">
                                        <% if (((string[])Session["read"]).Contains("item"))
                                            { %>
                                        <asp:GridView ID="ItemGridView" CssClass="table table-striped" runat="server" ShowHeaderWhenEmpty="False" Autogeneratecolumns="false" OnRowCancelingEdit="ItemGridView_RowCancelingEdit" OnRowEditing="ItemGridView_RowEditing">
                                            <Columns>
                                                <asp:BoundField HeaderText="No" DataField="no" ReadOnly="true" />
                                                <asp:BoundField HeaderText="Barcode" DataField="item_barcode" ReadOnly="true" />
                                                <asp:BoundField HeaderText="Kode Produk Pemasok" DataField="supplier_product_code" ReadOnly="true" />
                                                <asp:BoundField HeaderText="Nama Barang" DataField="item_name" ReadOnly="true" />
                                                <asp:BoundField HeaderText="Deskripsi Singkat" DataField="short_description" ReadOnly="true" />
                                                <asp:BoundField HeaderText="Harga Jual" DataField="item_price" ReadOnly="true" />
                                                <asp:BoundField HeaderText="Varian" DataField="variants" ReadOnly="true" />
                                                <asp:BoundField HeaderText="Merek" DataField="brand_name" ReadOnly="true" />
                                                <asp:BoundField HeaderText="Kuantitas" DataField="item_qty" ReadOnly="true" />
                                                <asp:BoundField HeaderText="Alasan" DataField="reason" ReadOnly="true" />
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="DeleteItemButton" CssClass="btn btn-danger" runat="server" CommandName="delete" CommandArgument='<%# Eval("item_detail_id") %>' OnCommand="DeleteItemButton_Command"><i class="bi bi-x-lg"></i></asp:LinkButton>
                                                        <asp:LinkButton ID="EditQtyButton" CssClass="btn btn-primary" runat="server" CommandName="edit" CommandArgument='<%# Eval("item_detail_id") %>' OnCommand="EditQtyButton_Command"><i class="bi bi-pencil-fill"></i></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>      
                                            </Columns>
                                        </asp:GridView>
                                        <% } %>
                                    </div>

                                    </div>
                                    <asp:Panel ID="InfoPanel" runat="server" CssClass="d-flex flex-wrap">
                                        <div class="col-6 col-md-9 text-end">
                                            <b>Total Qty</b>
                                        </div>
                                        <div class="col-6 col-md-3 text-end">
                                            <b><asp:Label ID="TotalQtyLabel" runat="server" Text=""></asp:Label></b>
                                        </div>
                                        <div class="col-6 col-md-9 text-end">
                                            <b>Total Harga</b>
                                        </div>
                                        <div class="col-6 col-md-3 text-end">
                                            <b><asp:Label ID="TotalPriceLabel" runat="server" Text=""></asp:Label></b>
                                        </div>
                                    </asp:Panel>
                                    <div class="col-12">
                                        <asp:Button ID="SaveUploadButton" CssClass="btn btn-primary me-2" runat="server" Text="Simpan" OnClick="SaveUploadButton_Click" />
                                        <asp:Button ID="CancelUploadButton" CssClass="btn btn-secondary" runat="server" Text="Batal" OnClick="CancelUploadButton_Click" />
                                    </div>
                                </div>
                            </div>
                            <% } %>
                        </div>
                    </div>
                </div>    
                <% if (((string[])Session["read"]).Contains("item"))
                    { %>
                <div class="modal fade" id="itemModal" tabindex="-1" role="dialog"
                    aria-labelledby="itemModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-centered modal-dialog-scrollable modal-lg"
                        role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="itemModalTitle">Informasi Barang</h5>
                                <button type="button" data-bs-dismiss="modal" class="btn btn-link" aria-label="Close">
                                    <i class="bi bi-x-lg"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="row">
                                    <div class="col-12">
                                        <asp:Label ID="AlertModalLabel" runat="server" Text=""></asp:Label>
                                    </div>
                                                        
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="ItemBarcodeInput">Barcode Barang</label>
                                            <asp:TextBox ID="ItemBarcodeInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="ItemNameInput">Nama Barang</label>
                                            <asp:TextBox ID="ItemNameInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="DescriptionInput">Deskripsi Barang</label>
                                            <asp:TextBox ID="DescriptionInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="ColorInput">Varian Warna</label>
                                            <asp:TextBox ID="ColorInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SizeInput">Varian Ukuran</label>
                                            <asp:TextBox ID="SizeInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="SupplierProductCodeInput">Kode Produk Pemasok</label>
                                            <asp:TextBox ID="SupplierProductCodeInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="BrandInput">Merek</label>
                                            <asp:TextBox ID="BrandInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="PriceInput">Harga Jual</label>
                                            <asp:TextBox ID="PriceInput" CssClass="form-control-plaintext" runat="server" ReadOnly="True"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="form-group">
                                            <label for="QtyInput">Kuantitas</label>
                                            <asp:TextBox ID="QtyInput" CssClass="form-control" runat="server" autocomplete="off"></asp:TextBox>        
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="form-group">
                                            <label for="ReasonInput">Alasan</label>
                                            <asp:TextBox ID="ReasonInput" CssClass="form-control" runat="server" autocomplete="off"></asp:TextBox>        
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="CloseModalButton" CssClass="btn btn-light-secondary" runat="server" Text="Tutup" OnClick="CloseModalButton_Click" />
                                <asp:Button ID="SaveModalButton" CssClass="btn btn-primary ms-1" runat="server" Text="Simpan" CommandName="edit" OnCommand="SaveModalButton_Command" />
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
                <% if ((bool)Session["delete"] == true)
                    { %>
                <div class="modal fade" id="confirmModal" tabindex="-1" role="dialog" aria-labelledby="confirmModalTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="confirmModalTitle">Konfirmasi</h5>
                                <button type="button" data-bs-dismiss="modal" class="btn btn-link" aria-label="Close">
                                    <i class="bi bi-x-lg"></i>
                                </button>
                            </div>
                            <div class="modal-body">
                                <p>Apakah ingin menghapus dokumen pengiriman no <asp:Label ID="GoodsReturnNoModalLabel" runat="server" Text=""></asp:Label> ?</p>
                            </div>
                            <div class="modal-footer">
                                <asp:Button ID="NoButton" CssClass="btn btn-secondary" runat="server" Text="Tidak" OnClick="NoButton_Click" />
                                                    
                                <asp:Button ID="YesButton" CssClass="btn btn-primary ms-1" runat="server" Text="Ya" CommandName="delete" OnCommand ="YesButton_Click" />
                            </div>
                        </div>
                    </div>
                </div>
                <% } %>
            </section>
            
        </div>
    </div>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <script type="text/javascript">
        function selectUploadTab() {
            var tab = new bootstrap.Tab(document.querySelector("#<%= UploadTabButton.ClientID %>"));
            tab.show();
        }
        function selectTabularTab() {
            var tab = new bootstrap.Tab(document.querySelector("#<%= TabularTabButton.ClientID %>"));
            tab.show();
        }
        function openItemModal() {
            var modal = new bootstrap.Modal(document.getElementById('itemModal'));
            modal.show();
        }
        function openConfirmModal() {
            var modal = new bootstrap.Modal(document.getElementById('confirmModal'));
            modal.show();
        }

        function download() {
            var input = document.getElementById("<%= FilenameInput.ClientID %>");
            var link = document.createElement("a");
            link.download = input.value;
            link.href = window.location.protocol + '//' + window.location.host + "/uploads/" + input.value;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            delete link;
        }


        var DownloadAllButton = document.getElementById("<%= DownloadAllButton.ClientID %>");
        var DownloadSelectedSupplierButton = document.getElementById("<%= DownloadSelectedSupplierButton.ClientID %>");
        var DownloadSelectedStoreButton = document.getElementById("<%= DownloadSelectedStoreButton.ClientID %>");
        var DownloadSelectedStoreAndSupplierButton = document.getElementById("<%= DownloadSelectedStoreAndSupplierButton.ClientID %>");
        var LoadingPanel = document.getElementById("<%= LoadingPanel.ClientID %>");

        DownloadAllButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
        DownloadSelectedSupplierButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
        DownloadSelectedStoreButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
        DownloadSelectedStoreAndSupplierButton.addEventListener('click', function () { LoadingPanel.classList.remove("d-none"); }, false);
    </script>
</asp:Content>
﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Forbidden.aspx.cs" Inherits="biutopia.Forbidden" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="error">
        <div class="error-page container">
            <div class="col-md-8 col-12 offset-md-2">
                <div class="text-center">
                    <img class="img-error" src="assets/images/error-403.svg" alt="Permintaan akses ditolak">
                    <h1>Permintaan akses ditolak</h1>
                    <asp:LinkButton ID="BackButton" CssClass="btn btn-lg btn-outline-primary mt-3" runat="server">Kembali</asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
